<?php

/**
 * Plugin Name: Team Booking
 * Plugin URI: http://stroheimdesign.com
 * Description: Booking system for reservations and appointments
 * Version: 1.4.2.1
 * Author: VonStroheim
 * Author URI: http://codecanyon.net/user/vonstroheim
 * Text Domain: team-booking
 * Domain Path: /languages
 */
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

///////////////////////
// Plugin constants  //
///////////////////////
define('TEAMBOOKING_PATH', plugin_dir_path(__FILE__));
define('TEAMBOOKING_URL', plugin_dir_url(__FILE__));
define('TEAMBOOKING_FILE_PATH', __FILE__);
define('TEAMBOOKING_VERSION', '1.4.2.1');

// Set the right include path (temporary)...
$prev_include_path = set_include_path(dirname(__FILE__));

////////////////////
// Require files  //
////////////////////
require_once dirname(__FILE__) . '/includes/tb_functions.php';
require_once dirname(__FILE__) . '/includes/tb_mappers.php';
require_once dirname(__FILE__) . '/libs/google/src/Google/autoload.php';
require_once dirname(__FILE__) . '/includes/tb_autoloader.php';

////////////////////////////
// Global plugin settings //
////////////////////////////
$team_booking_settings = get_option('team_booking');

/////////////////////////////////////
// Admin-Frontend classes selector //
/////////////////////////////////////
if (is_admin() && (!defined('DOING_AJAX') || !DOING_AJAX)) {
    /*
     * ZipArchive class loading for XLSX support
     */
    try {
        include_once dirname(TEAMBOOKING_FILE_PATH) . '/libs/xlsxwriter/xlsxwriter.class.php';
    } catch (Exception $e) {
        // ZipArchive library not supported
    }
} else {
    // Nothing yet
}

//////////////////
// Main loader  //
//////////////////
$plugin_init = new TeamBooking_Loader();
$plugin_init->load();

// ...restore previous include_path
set_include_path($prev_include_path);