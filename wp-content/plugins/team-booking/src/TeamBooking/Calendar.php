<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

/**
 * Main Calendar class
 */
class TeamBooking_Calendar
{

    //------------------------------------------------------------

    /** @var  Google_Client */
    private static $client;

    /** @var  Google_Service_Calendar */
    private static $service;

    /** @var TeamBookingSettings */
    private static $settings;

    public function __construct()
    {
        static::$settings = tbGetSettings();
        // Create a new Google Client object
        static::$client = new Google_Client();
        static::$client->addScope('https://www.googleapis.com/auth/calendar');
        static::$client->setApplicationName(static::$settings->getApplicationProjectName());
        static::$client->setClientId(static::$settings->getApplicationClientId());
        static::$client->setClientSecret(static::$settings->getApplicationClientSecret());
        static::$client->setRedirectUri(admin_url() . 'admin-ajax.php?action=teambooking_oauth_callback');
        static::$client->setAccessType('offline');
        // Has to be set before the auth call
        static::$service = new Google_Service_Calendar(static::$client);
    }

    //------------------------------------------------------------

    /**
     * Authenticate method
     *
     * @param string $code The code
     * @return string Access Token
     */
    public function authenticate($code)
    {
        try {
            static::$client->authenticate($code);
        } catch (Exception $ex) {
            // TODO: better error message handling?
            die($ex->getMessage());
        }

        return static::$client->getAccessToken();
    }

    //------------------------------------------------------------

    /**
     * Get the email address of the Coworker's Google Account
     *
     * To make clear what Google Account the Coworker has configured
     * with TeamBooking, it gets the email address for convenience.
     * In order to do that, an id_token (OpenID) is used.
     * If the stored one is expired, then the Coworker tokens
     * will be refreshed and the new values stored.
     *
     * @param string $access_token
     * @param string $coworker_id
     * @return string|\Exception
     */
    public function getTokenEmailAccount($access_token, $coworker_id)
    {
        // prepare the client service with the access token
        $this->setAccessToken($access_token);
        if (isset(json_decode($access_token)->id_token)) {
            try {
                // get token info
                $token_info = static::$client->verifyIdToken();
            } catch (Exception $e) {
                // the id_token is expired, perhaps? Let's try to refresh it
                try {
                    static::$client->refreshToken(static::$client->getRefreshToken());
                } catch (Exception $ex) {
                    // can't be refreshed, let's check if the tokes has been revoked
                    $original_error = tb_looking_for_JSON_in_string($ex->getMessage());
                    if (!is_null($original_error)) {
                        if (!is_null(json_decode($original_error[0]))) {
                            $array_message = json_decode($original_error[0]);
                            if ($array_message->error_description == 'Token has been revoked.') {
                                // The token we have, has been revoked, let's advice
                                return '<span class="tbk-revoked-upstream-advice">' . __('Uh-oh, your authorization was revoked upstream, please press the revoke button below, and authorize again!', 'teambooking') . '</span>';
                            }
                        }
                    }

                    // unknown error...
                    return $ex;
                }
                $coworker_data = static::$settings->getCoworkerData($coworker_id);
                $coworker_data->setAccessToken(static::$client->getAccessToken());
                // save the new value
                static::$settings->updateCoworkerData($coworker_data, $coworker_id);
                static::$settings->save();
                // get token info (try again)
                try {
                    $token_info = static::$client->verifyIdToken();
                } catch (Exception $ex) {
                    // no hope...
                    return $ex;
                }
            }
            // get account email address
            $attributes = $token_info->getAttributes();

            return $attributes['payload']['email'];
        } else {
            return '';
        }
    }

    //------------------------------------------------------------

    /**
     * Returns a list of Google Calendars for a specific
     * access token (owned calendars only)
     *
     * @param string $access_token
     * @return array
     */
    public function getGoogleCalendarList($access_token)
    {
        // prepare the client service with the access token
        $this->setAccessToken($access_token);

        try {
            $calendar_list = static::$service->calendarList->listCalendarList()->getItems();
        } catch (Exception $exc) {
            // something went wrong, let's return an empty array
            return array();
        }
        // preparing the list
        $return_array = array();
        foreach ($calendar_list as $calendar_entry) {
            /* @var $calendar_entry Google_Service_Calendar_CalendarListEntry */
            if ($calendar_entry->getAccessRole() == 'owner') {
                // we're filtering the owned calendars only
                $return_array[] = $calendar_entry;
            }
        }

        return $return_array;
    }

    //------------------------------------------------------------

    /**
     * Set the access token
     *
     * @param string $access_token
     * @return string Returns the Refresh Token, if available, or NULL
     */
    public function setAccessToken($access_token)
    {
        static::$client->setAccessToken($access_token);

        return static::$client->getRefreshToken();
    }

    //------------------------------------------------------------

    /**
     * Revokes a token
     *
     * @param string $token
     * @return boolean
     */
    public function revokeToken($token)
    {
        $client = new Google_Client();
        try {
            $client->revokeToken($token);
        } catch (Exception $e) {
            $error_log = new TeamBooking_ErrorLog();
            $error_log->setCoworkerId(get_current_user_id());
            $error_log->setErrorCode($e->getCode());
            $error_log->setMessage($e->getMessage());
            tbErrorLog($error_log);
            $message = $e->getMessage();

            return $message;
        }

        return TRUE;
    }

    //------------------------------------------------------------

    /**
     * Get the authorization link
     *
     * @return string Authorization Url
     */
    public function createAuthUrl()
    {
        // unique state id to avoid exploits
        $state = md5(rand());
        if (!session_id()) {
            session_start();
        }
        $_SESSION['tbk-auth-state'] = $state;
        static::$client->addScope('openid');
        static::$client->addScope('email');
        static::$client->setState($state);

        return static::$client->createAuthUrl();
    }

    //------------------------------------------------------------

    /**
     * It tests the ownership of a Google Calendar ID
     *
     * @param int $coworker_id
     * @param string $calendar_id
     * @return boolean
     */
    public function testCalendarID($coworker_id, $calendar_id)
    {
        $coworker_data = static::$settings->getCoworkerData($coworker_id);
        $access_token = $coworker_data->getAccessToken();
        if (!empty($access_token)) {
            $this->setAccessToken($access_token);
            try {
                $request = static::$service->events->listEvents($calendar_id);
            } catch (Exception $e) {
                if ($e->getCode() == 404) {
                    return FALSE;
                } else {
                    return $e->getMessage();
                }
            }

            return TRUE;
        } else {
            return NULL;
        }
    }

    //------------------------------------------------------------

    /**
     * Return an array with service titles to search with GData Full-text query string
     *
     * The result array is in the form:
     *
     * [coworker_id_i][$service_id_j] => (title)
     * [...]
     * [coworker_id_j][$service_id_i] => (title)
     * ...
     *
     * @param array $services
     * @param array $coworker_ids
     * @return array
     * @throws Exception
     */
    private function getFullTextQueries($services, array $coworker_ids = array())
    {
        // Declaring the queries return array
        $queries = array();
        // Getting the authorized coworkers id list
        $coworkers = tbGetAuthCoworkersIdList();
        // If the coworkers are specified, let's filter them
        if (!empty($coworker_ids)) {
            $coworkers = array_intersect($coworkers, $coworker_ids);
        }
        // Starting the coworkers loop
        foreach ($coworkers as $coworker_id) {
            // Retrieving the coworker data
            $coworker_data = static::$settings->getCoworkerData($coworker_id);
            // Starting the services loop
            foreach ($services as $service_id) {
                // Let's skip the service, if the coworker doesn't participate
                if (!$coworker_data->getCustomEventSettings($service_id)->isParticipate()) {
                    continue;
                }
                // Let's skip the service, if the service is inactive
                if (!static::$settings->getService($service_id)->isActive()) {
                    continue;
                }
                // If the service class is "Service", we must skip it
                if (static::$settings->getService($service_id)->isClass('service')) {
                    continue;
                }
                // Let's put into the array the search term for the FREE slots
                $queries[$coworker_id][$service_id]['free'] = $coworker_data->getCustomEventSettings($service_id)->getLinkedEventTitle();
                /**
                 * Now, the search term for the BOOKED slots (Appointment class only)
                 *
                 * Booked slots are necessary if the setting to show the soldout slots
                 * is active. They are necessary too, for the container mode to work.
                 * We can avoid that query if we're sure that the container mode will
                 * not be used and at the same time the soldout slots are hidden.
                 */
                if (static::$settings->getService($service_id)->isClass('appointment')) {
                    if (static::$settings->getService($service_id)->getShowSoldout() || static::$settings->getService($service_id)->getDurationRule() != 'inherited' || (static::$settings->getService($service_id)->getDurationRule() == 'coworker' && $coworker_data->getCustomEventSettings($service_id)->getDurationRule() != 'inherited')) {
                        $queries[$coworker_id][$service_id]['booked'] = $coworker_data->getCustomEventSettings($service_id)->getAfterBookedTitle();
                    } else {
                        $queries[$coworker_id][$service_id]['booked'] = NULL;
                    }
                } else {
                    $queries[$coworker_id][$service_id]['booked'] = NULL;
                }
            }
        }

        return $queries;
    }

    //------------------------------------------------------------

    /**
     * Retrieves a minimum getting time for the Google Calendar
     * events, tweaked with the specific coworker's min reservation
     * time limit for the specified service.
     *
     * @param int $coworker_id
     * @param string $service_id
     * @param string $min_get_time
     * @return stdClass
     */
    protected function getCoworkerMinTime($coworker_id, $service_id, $min_get_time)
    {
        $now = new DateTime();
        /**
         * Let's get the min time string for the given
         * coworker id and service id combination.
         *
         * The string is in the DateInterval format, with the
         * exception of *mid appended strings. These strings
         * means that the interval must be cut at midnight.
         *
         */
        $min_time_string = static::$settings->getCoworkerData($coworker_id)->getCustomEventSettings($service_id)->getMinTime();

        /**
         * Let's get the reference for the min time interval
         * specified by the given coworker for the given
         * service.
         *
         * It could be the START of the slot, or the END.
         */
        $reference = static::$settings->getCoworkerData($coworker_id)->getCustomEventSettings($service_id)->getMinTimeReference();

        /**
         * If the min time interval is a *mid appended one,
         * we should add the prepend interval to the
         * $now object AND set his time to midnight.
         */
        if (substr($min_time_string, -3) == 'mid') {
            // Get rid of 'mid' appendix
            $min_time_string = substr($min_time_string, 0, -3);
            // Add the interval
            $now->add(new DateInterval($min_time_string));
            // Set the time to midnight
            $now->setTime(0, 0);
        } else {
            // Just add the interval
            $now->add(new DateInterval($min_time_string));
        }

        /**
         * Let's return a formatted min time
         */
        if (strtotime($min_get_time) < strtotime($now->format('c'))) {
            /**
             * The min requesting time is less than the tweaked
             * min retrieving time, so we must return the tweaked
             * retrieving time (formatted).
             */
            $min_time = $now->format('c');
        } else {
            /**
             * The min requesting time is greater than or
             * equal to the tweaked min retrieving time,
             * so we can just return the first.
             */
            $min_time = $min_get_time;
        }

        /**
         * Preparing the object to return.
         *
         * We're using a stdClass for convenience,
         * so we can pass the reference along with it.
         */
        $return = new stdClass();
        $return->min_time = $min_time;
        $return->reference = $reference;

        return $return;
    }

    //------------------------------------------------------------

    /**
     * Fetch the events across all the (given) coworkers
     *
     * Returns a well-organized slots array
     *
     * @param array $services The requested services
     * @param array $coworker_ids The requested coworkers
     * @param string $min_get_time Minimum time for returned slots
     * @param string $max_get_time Maximum time for returned slots
     * @return array Slots
     */
    public function getSlots(array $services, array $coworker_ids, $min_get_time = NULL, $max_get_time = NULL)
    {

        /**
         * Get the reservations, for a later comparison.
         *
         * If a reservation is in pending state, the relative slot
         * in Google Calendar remains untouched, but the frontend calendar
         * must hide it.
         *
         */
        $reservation_in_database = tbGetReservationsByTime($min_get_time, $max_get_time);
        $reservation_in_database_pending = tbGetReservationsByTime($min_get_time, $max_get_time, TRUE);

        ############## EXPERIMENTAL ##################

        $experimental_call = new TeamBooking_Fetcher($reservation_in_database, $reservation_in_database_pending);
        $experimental_call->setRequestedCoworkers($coworker_ids);
        $experimental_call->setRequestedServices($services);
        $experimental_call->setRequestedMinTime($min_get_time);
        $experimental_call->setRequestedMaxTime($max_get_time);
        $return = $experimental_call->getSlots();

        /**
         * Let's clean the soldout/booked slots, if requested, on a service basis
         */
        foreach ($return as $key => $slot) {
            // Take pending reservations into account
            $slot = $this->computePendingReservations($slot, $reservation_in_database_pending, $reservation_in_database);
            $slot = $this->computeWaitingForApprovalReservations($slot, $reservation_in_database);
            if (static::$settings->getService($slot->getServiceId())->getShowSoldout() == FALSE && $slot->isSoldout()) {
                unset($return[$key]);
            }
        }

        /**
         * Let's fill the object.
         */
        $return_obj = new TeamBooking_SlotsResults();
        $return_obj->addSlotsFromArray((array)$return);

        /**
         * Return the results array.
         */
        $return_obj->trimSlots($min_get_time, $max_get_time);

        return $return_obj;
    }

    //------------------------------------------------------------

    /**
     * Sort slots by time.
     *
     * @param array $slots
     * @return array
     */
    private function sortSlotsByTime(array $slots)
    {
        uasort($slots, function ($a, $b) {
            return strtotime($a->getStartTime()) > strtotime($b->getStartTime());
        });

        return $slots;
    }

    //------------------------------------------------------------

    /**
     * Checks if a slot is a container one, for the given service title
     *
     * @param type $event_summary
     * @param type $linked_event_title
     * @return mixed
     */
    private function containerTitleCheck($event_summary, $linked_event_title)
    {
        if (strtolower(trim($event_summary)) == strtolower(trim($linked_event_title) . " container")) {
            return TRUE; // a single service container
        } else {
            if (substr(strtolower(trim($event_summary)), -9) == 'container') {
                $services_array = explode('+', substr(trim($event_summary), 0, -10));
                if (count($services_array) <= 1) {
                    return FALSE;
                } else {
                    foreach ($services_array as $service_name) {
                        if (strtolower(trim($service_name)) == strtolower(trim($linked_event_title))) {
                            return $services_array;
                        }
                    }
                }
            } else {
                return FALSE;
            }
        }
    }

    //------------------------------------------------------------

    /**
     * Renders the frontend calendar.
     *
     * @param TeamBooking_RenderParameters $parameters
     * @return array|TeamBooking_SlotsResults
     * @throws Exception
     */
    public function getCalendar(TeamBooking_RenderParameters $parameters)
    {
        // Retrieving the month
        $the_month = $parameters->getMonth();
        // If the month is not set, take the current
        if (empty($the_month)) {
            $parameters->setMonth(date_i18n('m'));
        }
        // Retrieving the year
        $the_year = $parameters->getYear();
        // If the year is not set, take the current
        if (empty($the_year)) {
            $parameters->setYear(date_i18n('Y'));
        }
        /**
         * Let's do a requested services count.
         *
         * If it's only one, and that one is a "Service" class,
         * then we need to render the form directly, without
         * the frontend calendar.
         */
        if (count($parameters->getRequestedServiceIds()) == 1) {
            // Let's put the value in a variable
            foreach ($parameters->getRequestedServiceIds() as $id) {
                $service_id = $id;
                break;
            }
        }
        if (isset($service_id) && static::$settings->getService($service_id)->getClass() == 'service') {
            // Calling the template for the form
            $template = new TeamBooking_Components_Frontend_UnscheduledForm(static::$settings, $parameters);
            if (static::$settings->getService($service_id)->getLoggedOnly() && !is_user_logged_in()) {
                echo $template->getContentRegisterAdvice();
            } else {
                echo $template->getContent();
            }
        } else {
            // Set the timezone from WP
            $timezone = tbGetTimezone();

            /**
             * Set the max_time
             *
             * Check for automatic month selection option.
             * If not active or if the call is an Ajax call,
             * then set the max_time parameter normally
             */
            if (!static::$settings->isFirstMonthWithFreeSlotShown() || $parameters->getIsAjaxCall()) {
                /**
                 * We're using the "Y-m-t" method to get the last day
                 * of the month, and set it nearly to midnight.
                 */
                $the_time = date_i18n('Y-m-t H:i:s', mktime(23, 59, 59, $parameters->getMonth(), 1, $parameters->getYear()));

                /**
                 * Dealing with the timezone.
                 *
                 * Let's create a DateTime object, with the
                 * reference to the end of the month.
                 * Set the timezone to that object, so we can
                 * extract the offset in that point in time,
                 * and add the offset interval to the object itself.
                 *
                 * That's because Google will not read the timezone
                 * for the max_time / min_time params.
                 */
                $time_object = new DateTime($the_time);
                $time_object->setTimezone($timezone);
                if ($timezone->getOffset($time_object) < 0) {
                    $time_object->add(new DateInterval('PT' . abs($timezone->getOffset($time_object)) . 'S'));
                } else {
                    $time_object->sub(new DateInterval('PT' . $timezone->getOffset($time_object) . 'S'));
                }

                // Let's set the max_time
                $max_time = $time_object->format('c');
            } else {
                // Let's keep the max_time null
                $max_time = NULL;
            }

            /**
             *  Set the min_time
             */
            $the_time = date_i18n('Y-m-d H:i:s', mktime(0, 0, 0, $parameters->getMonth(), 1, $parameters->getYear()));
            // Dealing with the timezone
            $time_object = new DateTime($the_time);
            $time_object->setTimezone($timezone);
            if ($timezone->getOffset($time_object) < 0) {
                $time_object->add(new DateInterval('PT' . abs($timezone->getOffset($time_object)) . 'S'));
            } else {
                $time_object->sub(new DateInterval('PT' . $timezone->getOffset($time_object) . 'S'));
            }
            // Let's set the min_time
            $min_time = $time_object->format('c');

            /**
             * Get slots
             */
            if ($parameters->getSlotsObj() instanceof TeamBooking_SlotsResults) {
                $slots = new TeamBooking_SlotsResults();
                $slots->addSlotsFromArray($parameters->getSlotsObj()->getSlotsByService($parameters->getRequestedServiceIds()));
            } else {
                $slots = $this->getSlots($parameters->getServiceIds(), $parameters->getCoworkerIds(), $min_time, $max_time);
                $parameters->setSlotsObj($slots);
                if (is_array($parameters->getRequestedServiceIds()) && ($parameters->getRequestedServiceIds() !== $parameters->getServiceIds())) {
                    $requested_slots = $slots->getSlotsByService($parameters->getRequestedServiceIds());
                    $slots = new TeamBooking_SlotsResults();
                    $slots->addSlotsFromArray($requested_slots);
                    unset($requested_slots);
                }
            }

            /**
             * Let's check those three conditions:
             *
             * 1. Load the calendar at first month with slots option active
             * 2. Is not an Ajax call
             * 3. The slots result object is not empty
             *
             * If all of them are met, then we have a slots object that
             * must be trimmed to the first month with slots only.
             *
             * [TODO: may not be necessary anymore!!]
             *
             */
            if (static::$settings->isFirstMonthWithFreeSlotShown() && !$parameters->getIsAjaxCall() && $slots->thereAreSlots()) {
                // Conditions met, let's pick the closest slot in time
                $closest_slot = $slots->getClosestSlot();
                // Set the month picking it from the first slot
                $parameters->setMonth(date('m', strtotime($closest_slot->getStartTime())));
                // Set the year picking it from the first slot
                $parameters->setYear(date('Y', strtotime($closest_slot->getStartTime())));
                // Set the max_time
                $the_time = date_i18n('Y-m-t H:i:s', mktime(23, 59, 59, $parameters->getMonth(), 1, $parameters->getYear()));
                $time_object = new DateTime($the_time);
                $time_object->setTimezone($timezone);
                if ($timezone->getOffset($time_object) < 0) {
                    $time_object->add(new DateInterval('PT' . abs($timezone->getOffset($time_object)) . 'S'));
                } else {
                    $time_object->sub(new DateInterval('PT' . $timezone->getOffset($time_object) . 'S'));
                }
                $max_time = $time_object->format('c');
                // Set the min_time
                $the_time = date_i18n('Y-m-d H:i:s', mktime(0, 0, 0, $parameters->getMonth(), 1, $parameters->getYear()));
                $time_object = new DateTime($the_time);
                $time_object->setTimezone($timezone);
                if ($timezone->getOffset($time_object) < 0) {
                    $time_object->add(new DateInterval('PT' . abs($timezone->getOffset($time_object)) . 'S'));
                } else {
                    $time_object->sub(new DateInterval('PT' . $timezone->getOffset($time_object) . 'S'));
                }
                $min_time = $time_object->format('c');
                // Finally, trim the slots
                $slots->trimSlots($min_time, $max_time);
            }

            /**
             * Preparing the slots for the template class.
             */
            $calendar = new TeamBooking_Components_Frontend_Calendar(static::$settings, $parameters);
            $calendar->slots_obj = $slots;
            echo $calendar->getContent();

            return $slots;
        }
    }

    //------------------------------------------------------------

    /**
     * @param TeamBooking_RenderParameters $parameters
     * @throws Exception
     */
    public function getSchedule(TeamBooking_RenderParameters $parameters)
    {
        // Prior to PHP 5.5 empty() only supports variables
        $the_day = $parameters->getDay();
        if (empty($the_day)) {
            $parameters->setDay(date_i18n('d'));
        }
        // Prior to PHP 5.5 empty() only supports variables
        $the_month = $parameters->getMonth();
        if (empty($the_month)) {
            $parameters->setMonth(date_i18n('m'));
        }
        // Prior to PHP 5.5 empty() only supports variables
        $the_year = $parameters->getYear();
        if (empty($the_year)) {
            $parameters->setYear(date_i18n('Y'));
        }

        if (count($parameters->getRequestedServiceIds()) == 1) {
            foreach ($parameters->getRequestedServiceIds() as $service_id) {
                $service = $service_id;
                break;
            }
        }
        if (isset($service) && static::$settings->getService($service)->getClass() == 'service') {
            echo "<div class='tb-day-schedule' data-day='" . $parameters->getDay() . "'></div>";
        } else {
            // Set the timezone from WP
            $timezone = tbGetTimezone();
            // Set time interval
            $date_time_object = new DateTime($parameters->getYear() . "-" . $parameters->getMonth() . "-" . $parameters->getDay(), $timezone);
            $min_time = $date_time_object->format('c');
            $date_time_object->add(new DateInterval('PT23H59M59S'));
            $max_time = $date_time_object->format('c');
            // Get slots
            if ($parameters->getIsAjaxCall()) {
                $slots = $parameters->getSlots();
            } else {
                $slots = $parameters->getSlotsObj()->getSlotsByDate(str_pad($parameters->getDay(), 2, '0', STR_PAD_LEFT), str_pad($parameters->getMonth(), 2, '0', STR_PAD_LEFT), $parameters->getYear());
                if (is_array($parameters->getRequestedServiceIds())) {
                    $refined_slots = new TeamBooking_SlotsResults();
                    $refined_slots->addSlotsFromArray($slots);
                    unset($slots);
                    $slots = $refined_slots->getSlotsByService($parameters->getRequestedServiceIds());
                    unset($refined_slots);
                }
            }
            // Calling the template
            $schedule = new TeamBooking_Components_Frontend_Schedule(static::$settings, $parameters);
            // Setting variables
            $schedule->setSlots($slots);
            // Render
            echo $schedule->getContent();
        }
    }

    //------------------------------------------------------------

    /**
     * @param TeamBooking_Slot $slot
     * @param array $pending_reservations
     * @param array $reservations
     * @return TeamBooking_Slot
     * @throws Exception
     */
    private function computePendingReservations(TeamBooking_Slot $slot, array $pending_reservations, array $reservations)
    {
        /*
         * There is a pending reservation record in the database regarding
         * this event?
         */
        $pending_attendees = NULL;

        // Looping through pending reservations
        foreach ($pending_reservations as $pending_reservation) {
            /* @var $pending_reservation TeamBooking_ReservationData */
            if ($pending_reservation->getGoogleCalendarEvent() == $slot->getEventId() &&
                !tbIsReservationTimedOut($pending_reservation, static::$settings) &&
                strtotime($pending_reservation->getSlotStart()) == strtotime($slot->getStartTime()) &&
                strtotime($pending_reservation->getSlotEnd()) == strtotime($slot->getEndTime())
            ) {
                /**
                 * There is a pending reservation with this event ID
                 * and the record is not timed out, so:
                 *
                 * If the service class is "Event", we can't simply
                 * skip the slot. Rather, we should decrease the
                 * available tickets (if any) by the tickets amount
                 * of the pending reservation record.
                 *
                 * In order to do that, we'll count all the tickets
                 * from all the pending reservations that are relative
                 * to this Google Calendar event.
                 */
                if (static::$settings->getService($slot->getServiceId())->isClass('event')) {
                    // Adding the pending attendees (tickets) to the global counter
                    $pending_attendees += $pending_reservation->getTickets();
                }
            }
        }

        /**
         * Is the slot's service class = "Event", and are the attendees for this slot
         * (counting the pending ones too, and the hide for approval too) equal to the
         * maximum allowed number?
         */
        $attendees_number = $pending_attendees; /* == NULL if there are no pending events */
        if (static::$settings->getService($slot->getServiceId())->isClass('event')) {
            /**
             * in order to count the tickets already reserved we should extract
             * the tickets number from the reservation record
             */

            // Looping through the non-pending reservations
            foreach ($reservations as $reservation) {
                /* @var $reservation TeamBooking_ReservationData */
                if ($reservation->getGoogleCalendarEvent() == $slot->getEventId() &&
                    $reservation->getServiceId() == $slot->getServiceId() &&
                    $reservation->isConfirmed()
                ) {
                    $attendees_number += (int)$reservation->getTickets();
                }
            }

            /**
             * If the total attendees number is greater that or equal to the maximum
             * tickets allowed, let's mark the slot as "Sold Out", that is the same
             * as "Booked", more or less.
             */
            if ($attendees_number >= static::$settings->getService($slot->getServiceId())->getAttendees()) {
                // Marking it as soldout
                $slot->setSoldoutTrue();
                $slot->setAttendeesNumber(static::$settings->getService($slot->getServiceId())->getAttendees());
            } else {
                $slot->setAttendeesNumber($attendees_number);
            }
        }

        return $slot;
    }

    //------------------------------------------------------------

    /**
     * @param TeamBooking_Slot $slot
     * @param array $reservations
     * @return TeamBooking_Slot
     * @throws Exception
     */
    private function computeWaitingForApprovalReservations(TeamBooking_Slot $slot, array $reservations)
    {
        /**
         * Let's skip a possible "waiting for approval" reservation, if
         * the option to skip it is selected
         */
        $temp_tickets = 0;
        if (!static::$settings->getService($slot->getServiceId())->getFreeUntilApproval()) {
            // Looping through the reservations
            foreach ($reservations as $reservation) {
                /* @var $reservation TeamBooking_ReservationData */
                if ($reservation->getGoogleCalendarEvent() == $slot->getEventId() &&
                    $reservation->isWaitingApproval() &&
                    $reservation->getServiceId() == $slot->getServiceId() &&
                    strtotime($reservation->getSlotStart()) == strtotime($slot->getStartTime()) &&
                    strtotime($reservation->getSlotEnd()) == strtotime($slot->getEndTime()) &&
                    static::$settings->getService($slot->getServiceId())->isClass('event')
                ) {
                    // Event, let's sum the total tickets to hide
                    $temp_tickets += $reservation->getTickets();
                }
            }
            if ($temp_tickets) {
                $temp_tickets += $slot->getAttendeesNumber();
                if ($temp_tickets >= static::$settings->getService($slot->getServiceId())->getAttendees()) {
                    // Marking it as soldout
                    $slot->setSoldoutTrue();
                    $slot->setAttendeesNumber(static::$settings->getService($slot->getServiceId())->getAttendees());
                } else {
                    $slot->setAttendeesNumber($temp_tickets);
                }
            }
        }

        return $slot;
    }

    //------------------------------------------------------------

    /**
     * @param $events
     * @param $result_record_id
     * @param TeamBookingCoworker $coworker_data
     * @param $coworker_id
     * @param $min_get_time
     * @return array
     * @throws Exception
     */
    private function sortEventResultsToSlotArrays($events, $result_record_id, TeamBookingCoworker $coworker_data, $coworker_id, $min_get_time)
    {
        $return = array(
            'free'       => array(),
            'booked'     => array(),
            'containers' => array(),
            'services'   => array(),
        );
        /**
         * Retrieving important informations about this results record,
         * and getting all the slots from it.
         *
         * The results record is identified by an ID that is equal to
         * the marker we've set for the relative request, plus the
         * "response-" string in front. The marker, was the serialized helper
         * array, so in this stage we can easily know which service
         * and which Google Calendar ID are related to this results
         * record.
         */
        $serialized_helper_array = substr($result_record_id, 9); // Get rid of "response-"
        /**
         * Once the "response-" part is cutted down, we must check if
         * we are in front of a booked slot results record.
         * Relative requests were identified by the "_booked" marker,
         * so let's look for it.
         */
        if (substr($serialized_helper_array, -7) == '_booked') {
            /**
             * This is a booked appointments slots result record
             * Let's strip the "_booked" marker.
             */
            $serialized_helper_array = substr($serialized_helper_array, 0, -7);
            $where_to_put = 'booked';
        } else {
            $where_to_put = 'free';
        }
        /**
         * In order to know the service and the Google Calendar ID
         * of this results record, we should unserialize the helper array.
         */
        $unserialized_helper_array = unserialize(base64_decode($serialized_helper_array));

        // Getting the service id
        $service_id = $unserialized_helper_array['service_id'];

        /**
         * Main Sorting Loop
         */
        foreach ($events->getItems() as $event) {
            $where_to_put_backup = $where_to_put;
            /* @var $event Google_Service_Calendar_Event */
            // Check for All Day event and set start/end time
            if ($event->getStart()->getDateTime()) {
                $allday = FALSE;
                $start = $event->getStart()->getDateTime();
                $end = $event->getEnd()->getDateTime();
            } else {
                $allday = TRUE;
                $start = $event->getStart()->getDate();
                $end = $event->getEnd()->getDate();
            }

            // Preparing Slot Object
            $slot = new TeamBooking_Slot();

            if ($where_to_put !== 'booked') {
                /**
                 * Is the slot a container one? If yes, we should
                 * mark it (so we can split it into single slots later).
                 *
                 * PLEASE NOTE: we should allow the container only if
                 * the duration rule is NOT "inherited", otherwise the
                 * container slot must be skipped.
                 */
                if ($this->containerTitleCheck($event->getSummary(), $coworker_data->getCustomEventSettings($service_id)->getLinkedEventTitle())) {
                    /**
                     * Let's check if the duration rule is set to "inherited",
                     * both in admin level or in coworker level.
                     */
                    if (static::$settings->getService($service_id)->getDurationRule() == 'inherited' || (static::$settings->getService($service_id)->getDurationRule() == 'coworker' && $coworker_data->getCustomEventSettings($service_id)->getDurationRule() == 'inherited')) {
                        // It is "inherited", skip the slot
                        continue;
                    } else {
                        // It is not "inherited", mark the slot as valid container
                        $slot->setContainerTrue();
                        // Is a multiple service container? If yes, let's put
                        // the service names array inside the slot
                        $check_result = $this->containerTitleCheck($event->getSummary(), $coworker_data->getCustomEventSettings($service_id)->getLinkedEventTitle());
                        if (is_array($check_result)) {
                            $all_services_list = static::$settings->getServiceIdList();
                            $new_check_result = array();
                            foreach ($all_services_list as $the_id) {
                                if (static::$settings->getService($the_id)->isActive()) {
                                    foreach ($check_result as $names_value) {
                                        if (strtolower($coworker_data->getCustomEventSettings($the_id)->getLinkedEventTitle()) == strtolower(trim($names_value))) {
                                            $new_check_result[] = $the_id;
                                        }
                                    }
                                }
                            }
                            $slot->setMultipleServices($new_check_result);
                        }
                    }
                } else {
                    // It's not a container
                    $slot->setContainerFalse();
                }

                /**
                 * If the slot is not a container, we should check that
                 * the reference time (start or end) is not less than the
                 * user min_time, or we should skip the slot*.
                 *
                 * If the slot is a container and the start time is less than
                 * the user min_time, we should simply skip it, and do the previous
                 * check on the derived slots, later.
                 *
                 * *This is necessary, due timeMin behaviour in Google Calendar API
                 * (it's always relative to the event's end time).
                 */
                $min_time = $this->getCoworkerMinTime($coworker_id, $service_id, $min_get_time);
                if (!$slot->isContainer()) {
                    // It is NOT a container
                    if ($min_time->reference == 'end') {
                        // The reference for the min_time is the slot's end time
                        if (strtotime($end) < strtotime($min_time->min_time)) {
                            // Skip this slot
                            continue;
                        }
                    } else {
                        // The reference for the min_time is the slot's start time
                        if (strtotime($start) < strtotime($min_time->min_time)) {
                            // Skip this slot
                            continue;
                        }
                    }
                }

                /**
                 * FullText queries are used to reduce API callback size, but
                 * are too loose (i.e. "Service" and "New Service" will be taken both),
                 * so let's check for exact title now.
                 *
                 * PLEASE NOTE: this step must be done AFTER the container check,
                 * or the container event will be always skipped, no matter what.
                 */
                if (strtolower($event->getSummary()) != strtolower($coworker_data->getCustomEventSettings($service_id)->getLinkedEventTitle())
                    && !$slot->isSoldout()
                    && !$slot->isContainer()
                ) {
                    // Event title do not match perfectly, and it's not a container, let's skip it
                    continue;
                }

                /**
                 * We're setting the slot's location
                 */
                $location_setting = static::$settings->getService($service_id)->getLocationSetting();
                if ($location_setting == 'inherited') {
                    $slot->setLocation($event->getLocation());
                } elseif ($location_setting == 'fixed') {
                    $slot->setLocation(static::$settings->getService($service_id)->getLocationAddress());
                } else {
                    $slot->setLocation(''); //empty
                }
                if ($allday) {
                    $slot->setAllDayTrue();
                } else {
                    $slot->setAllDayFalse();
                }
            } else {
                if (strtolower($event->getSummary()) != strtolower($coworker_data->getCustomEventSettings($service_id)->getAfterBookedTitle())
                    && !$slot->isSoldout()
                    && !$slot->isContainer()
                ) {
                    // Event title do not match perfectly, and it's not a container, let's skip it
                    continue;
                }
                /**
                 * We're setting the slot's location
                 */
                $slot->setLocation($event->getLocation());
                $slot->setSoldoutTrue();
            }

            /**
             * All the checks are done, we're ready to prepare the slot object now.
             */
            $slot->setCoworkerId($coworker_id);
            $slot->setEndTime($end);
            $slot->setStartTime($start);
            $slot->setEventId($event->getID());
            $slot->setServiceId($service_id);
            $slot->setCalendarId($unserialized_helper_array['calendar_id']);
            $slot->setServiceName(static::$settings->getService($service_id)->getName());
            $slot->setServiceInfo(static::$settings->getService($service_id)->getServiceInfo());

            $return['services'][] = $service_id;
            /**
             * The actual sorting
             */
            if ($where_to_put !== 'booked') {
                /**
                 * If the slot is a container, we put it in another array,
                 * because we should split it AFTER all the non-container events
                 * are browsed (that's because we need them to check if they are
                 * inside containers).
                 */
                if ($slot->isContainer()) {
                    $where_to_put = 'containers';
                }
            }
            if ($where_to_put === 'free') {
                $return[$where_to_put][$slot->getEventId()] = $slot;
            } else {
                $return[$where_to_put][] = $slot;
            }
            $where_to_put = $where_to_put_backup;
        }

        return $return;
    }

}
