<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_PaymentGateways_Stripe_Gateway implements TeamBooking_PaymentGateways_Gateway
{

    //------------------------------------------------------------

    private $settings;
    private $api_key;
    private $token;
    private $currency;
    private $quantity;
    private $price;
    private $item_name;
    private $receipt_email;

    public function __construct()
    {
        $this->settings = tbGetSettings();
    }

    //------------------------------------------------------------

    public function setApiKey($key)
    {
        $this->api_key = $key;
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

    public function setCurrency($code)
    {
        $this->currency = $code;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function setItemName($name)
    {
        $this->item_name = $name;
    }

    public function setReceiptEmail($email)
    {
        $this->receipt_email = $email;
    }

    //------------------------------------------------------------

    public function processPayment()
    {
        include dirname(__FILE__) . '/lib/init.php';
        \Stripe\Stripe::setApiKey($this->api_key);
        $parameters = array(
            "amount"      => $this->generateAmount(),
            // amount in cents, again
            "currency"    => $this->currency,
            "source"      => $this->token,
            "description" => $this->item_name,
        );
        if (!is_null($this->receipt_email)) {
            $parameters['receipt_email'] = $this->receipt_email;
        }
        try {
            $charge = \Stripe\Charge::create($parameters);
            $charge_array = $charge->__toArray();
            $return_array['charge id'] = $charge_array['id'];
            $return_array['receipt number'] = $charge_array['receipt_number'];
            $return_array['invoice id'] = $charge_array['invoice'];

            return $return_array;
        } catch (\Stripe\Error\Card $e) {
            // The card has been declined
            $error = new TeamBooking_Error($e->getCode());
            $error->setMessage($e->getMessage());

            return $error;
        }
    }

    //------------------------------------------------------------

    public function generateAmount()
    {
        $currency_code = tbGetSettings()->getCurrencyCode();
        $zero_decimal_currencies = array(
            'BIF' => 'Burundian Franc',
            'CLP' => 'Chilean Peso',
            'DJF' => 'Djiboutian Franc',
            'GNF' => 'Guinean Franc',
            'JPY' => 'Japanese Yen',
            'KMF' => 'Comorian Franc',
            'KRW' => 'South Korean Won',
            'MGA' => 'Malagasy Ariary',
            'PYG' => 'Paraguayan Guaraní',
            'RWF' => 'Rwandan Franc',
            'VND' => 'Vietnamese Đồng',
            'VUV' => 'Vanuatu Vatu',
            'XAF' => 'Central African Cfa Franc',
            'XOF' => 'West African Cfa Franc',
            'XPF' => 'Cfp Franc',
        );
        if (in_array($currency_code, array_keys($zero_decimal_currencies))) {
            return $this->price * $this->quantity;
        } else {
            return $this->price * $this->quantity * 100;
        }
    }

}
