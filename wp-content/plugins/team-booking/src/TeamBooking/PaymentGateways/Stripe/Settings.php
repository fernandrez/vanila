<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

/**
 * Option object for Stripe Gateway
 */
class TeamBooking_PaymentGateways_Stripe_Settings implements TeamBooking_PaymentGateways_Settings
{

    ///////////////
    //  GENERAL  //
    ///////////////
    private $use_gateway;
    private $gateway_id;
    /////////////////////////
    //  STRIPE SPECIFIC    //
    /////////////////////////
    private $secret_key;
    private $pub_key;
    private $send_receipt;
    private $load_library;

    public function __construct()
    {
        /////////////////////////
        //  GENERAL (defaults) //
        /////////////////////////
        $this->gateway_id = 'stripe';
        $this->use_gateway = FALSE;
        ////////////////////////////////
        // STRIPE SPECIFIC (defaults) //
        ////////////////////////////////
        $this->send_receipt = FALSE;
        $this->load_library = TRUE;
    }

    //------------------------------------------------------------

    public function isOffsite()
    {
        return FALSE;
    }

    //------------------------------------------------------------

    public function setUseGatewayOn()
    {
        $this->use_gateway = TRUE;
    }

    public function setUseGatewayOff()
    {
        $this->use_gateway = FALSE;
    }

    public function isActive()
    {
        return $this->use_gateway;
    }

    //------------------------------------------------------------

    public function getGatewayId()
    {
        return $this->gateway_id;
    }

    //------------------------------------------------------------

    public function setSecretKey($key)
    {
        $this->secret_key = $key;
    }

    public function getSecretKey()
    {
        return $this->secret_key;
    }

    //------------------------------------------------------------

    public function setPublishableKey($key)
    {
        $this->pub_key = $key;
    }

    public function getPublishableKey()
    {
        return $this->pub_key;
    }

    //------------------------------------------------------------

    public function setSendReceiptOn()
    {
        $this->send_receipt = TRUE;
    }

    public function setSendReceiptOff()
    {
        $this->send_receipt = FALSE;
    }

    public function getSendReceipt()
    {
        return $this->send_receipt;
    }

    //------------------------------------------------------------

    public function setLoadLibraryOn()
    {
        $this->load_library = TRUE;
    }

    public function setLoadLibraryOff()
    {
        $this->load_library = FALSE;
    }

    public function isLoadLibrary()
    {
        return $this->load_library;
    }

    //------------------------------------------------------------

    public function getLabel()
    {
        ?>
        <div class="ui mini blue label">Stripe</div>
        <?php
    }

    //------------------------------------------------------------

    public function getPayButton()
    {
        ob_start();
        ?>
        <div class="ui tb-icon basic tbk-button tbk-header tbk-pay-button" data-offsite="<?= $this->isOffsite() ?>"
             data-gateway="<?= $this->gateway_id ?>">
            <i class="stripe tb-icon"></i>

            <div class="tbk-content">
                <?= esc_html__('Pay with Stripe', 'teambooking') ?>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    public function prepareGateway(TeamBooking_ReservationData $data, $additional_parameter = NULL)
    {
        include TEAMBOOKING_PATH . 'src/TeamBooking/PaymentGateways/Stripe/Gateway.php';
        $stripe = new TeamBooking_PaymentGateways_Stripe_Gateway();
        $stripe->setApiKey($this->secret_key);
        $stripe->setToken($additional_parameter);
        $stripe->setPrice($data->getPrice());
        $stripe->setQuantity($data->getTickets());
        $stripe->setItemName($data->getServiceName());
        $stripe->setCurrency($data->getCurrencyCode());
        if ($this->send_receipt) {
            $stripe->setReceiptEmail($data->getCustomerEmail());
        }
        $response = $stripe->processPayment();

        return $response;
    }

    //------------------------------------------------------------

    public function saveBackendSettings(array $settings, $new_currency_code)
    {
        isset($settings['use_gateway']) && !empty($settings['secret_key']) && $this->verifyCurrency($new_currency_code) && !empty($settings['pub_key']) ? $this->setUseGatewayOn() : $this->setUseGatewayOff();
        isset($settings['secret_key']) ? $this->setSecretKey(trim($settings['secret_key'])) : $this->setSecretKey('');
        isset($settings['pub_key']) ? $this->setPublishableKey(trim($settings['pub_key'])) : $this->setPublishableKey('');
        isset($settings['send_receipt']) ? $this->setSendReceiptOn() : $this->setSendReceiptOff();
        isset($settings['load_library']) ? $this->setLoadLibraryOn() : $this->setLoadLibraryOff();
    }

    //------------------------------------------------------------

    public function getBackendSettingsTab()
    {
        ob_start();
        ?>
        <div class="tbk-panel-title">
            <h2>
                <a href="https://stripe.com" target="_blank">
                    <img src="https://stripe.com/img/logo.png?2" style="float:right;"/>
                </a>
                <?= esc_html__('Stripe', 'teambooking') ?>
            </h2>
        </div>
        <div class="tbk-content">
            <?php if (!$this->verifyCurrency(tbGetSettings()->getCurrencyCode())) { ?>
                <div class="tbk-setting-warning">
                    <span><?= esc_html__('Note', 'teambooking') ?></span>
                    <?= esc_html__("The selected currency is not supported by Stripe. Stripe gateway can't be activated.", 'teambooking') ?>
                </div>
            <?php } ?>
            <ul class="tbk-list">
                <li>
                    <h4>
                        <?= esc_html__('Use Stripe gateway', 'teambooking') ?>
                    </h4>

                    <p>
                        <label for="gateway_settings[<?= $this->gateway_id ?>][use_gateway]">
                            <input name="gateway_settings[<?= $this->gateway_id ?>][use_gateway]" type="checkbox"
                                   value="yes" <?php checked(1, $this->use_gateway) ?> >
                        </label>
                    </p>
                    <?php if (empty($this->pub_key) || empty($this->secret_key)) { ?>
                        <div class="tbk-setting-alert">
                            <span><?= esc_html__('Note', 'teambooking') ?></span>: <?= esc_html__("you can't activate the Stripe gateway if the Secret Key and Publishable Key fields are empty.", 'teambooking') ?>
                        </div>
                    <?php } ?>
                </li>
                <li>
                    <h4>
                        <?= esc_html__('Secret Key', 'teambooking') ?>
                        (<a href="https://dashboard.stripe.com/account/apikeys"
                            target="_blank"><?= esc_html__('where?', 'teambooking') ?></a>)
                    </h4>

                    <p>
                        <input type="text" name="gateway_settings[<?= $this->gateway_id ?>][secret_key]"
                               value="<?= $this->secret_key ?>" class="regular-text"/>
                    </p>

                    <div class="tbk-setting-alert">
                        <span><?= esc_html__('Note', 'teambooking') ?></span>: <?= esc_html__("use the test one for testing, the live one for real payments", 'teambooking') ?>
                    </div>
                </li>
                <li>
                    <h4>
                        <?= esc_html__('Publishable Key', 'teambooking') ?>
                        (<a href="https://dashboard.stripe.com/account/apikeys"
                            target="_blank"><?= esc_html__('where?', 'teambooking') ?></a>)
                    </h4>

                    <p>
                        <input type="text" name="gateway_settings[<?= $this->gateway_id ?>][pub_key]"
                               value="<?= $this->pub_key ?>" class="regular-text"/>
                    </p>

                    <div class="tbk-setting-alert">
                        <span><?= esc_html__('Note', 'teambooking') ?></span>: <?= esc_html__("use the test one for testing, the live one for real payments", 'teambooking') ?>
                    </div>
                </li>
                <li>
                    <h4>
                        <?= esc_html__('Send receipt to the customer', 'teambooking') ?>
                    </h4>

                    <p><?= esc_html__('The customer email provided in the reservation form will be used', 'teambooking') ?></p>

                    <p>
                        <label for="gateway_settings[<?= $this->gateway_id ?>][send_receipt]">
                            <input name="gateway_settings[<?= $this->gateway_id ?>][send_receipt]" type="checkbox"
                                   value="yes" <?php checked(1, $this->send_receipt) ?> >
                        </label>
                    </p>
                </li>
                <li>
                    <h4>
                        <?= esc_html__('Load Stripe.js library', 'teambooking') ?>
                    </h4>

                    <p><?= esc_html__('Remove this option only if another plugin is loading the same library', 'teambooking') ?></p>

                    <p>
                        <label for="gateway_settings[<?= $this->gateway_id ?>][load_library]">
                            <input name="gateway_settings[<?= $this->gateway_id ?>][load_library]" type="checkbox"
                                   value="yes" <?php checked(1, $this->load_library) ?> >
                        </label>
                    </p>
                </li>
                <li>
                    <?php submit_button(esc_html__('Save changes', 'teambooking'), 'primary', 'team_booking_payment_options_submit') ?>
                </li>
            </ul>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    public function getDataForm(TeamBooking_ReservationData $data, $reservation_database_id)
    {
        ob_start();
        $form_id = 'stripe-payment-form-' . mt_rand(100000, 999999);
        ?>
        <form class="ui form" action="" method="POST" id="<?= $form_id ?>">
            <!-- Error section -->
            <span class='ui error message'>
                <div class="tbk-header">
                    <?= esc_html__("You've got some errors...", 'teambooking') ?>
                </div>
                <p class="payment-errors"></p>
            </span>

            <div class="four fields">
                <div class="field">
                    <label><?= esc_html__('Credit Card number', 'teambooking') ?></label>
                    <input data-stripe="number"/>
                </div>
                <div class="field">
                    <label><?= esc_html__('CVC', 'teambooking') ?></label>
                    <input data-stripe="cvc"/>
                </div>
                <div class="field">
                    <label><?= esc_html__('Expiring month', 'teambooking') ?></label>
                    <input data-stripe="exp-month"/>
                </div>
                <div class="field">
                    <label><?= esc_html__('Expiring year', 'teambooking') ?></label>
                    <input data-stripe="exp-year"/>
                </div>
            </div>
            <div class="ui fluid primary submit tbk-button"><?= esc_html__('Submit payment', 'teambooking') ?></div>
        </form>
        <script>
            Stripe.setPublishableKey('<?= $this->pub_key ?>');
            var stripeResponseHandler = function (status, response) {
                var $form = jQuery('#<?= $form_id ?>');

                if (response.error) {
                    // Show the errors on the form
                    $form.find('.payment-errors').text(response.error.message);
                    $form.addClass('error');
                    $form.find('.submit').removeClass('disabled');
                    $form.find('.submit').removeClass('loading');
                } else {
                    // token contains id, last4, and card type
                    var token = response.id;
                    // and re-submit
                    var ajax_url = '<?= admin_url('admin-ajax.php') ?>';
                    var ajax_nonce = '<?= wp_create_nonce('teambooking_process_payment_onsite') ?>';
                    jQuery.post(
                        ajax_url,
                        {
                            // wp ajax action
                            action: 'tb_process_onsite_payment',
                            // Collect form data
                            reservation_data: '<?= base64_encode(serialize($data)) ?>',
                            gateway_id: '<?= $this->gateway_id ?>',
                            additional_parameter: token,
                            reservation_database_id: '<?= $reservation_database_id ?>',
                            // send the nonce along with the request
                            nonce: ajax_nonce
                        },
                        function (response) {
                            if (response.slice(0, 4) == 'http') {
                                // redirecting
                                window.location.href = response;
                                return;
                            }
                            // Load the response box inside the modal content
                            console.clear();
                            console.log(response);
                            $form.replaceWith(response);
                        }
                    );
                }
            };
            jQuery('#<?= $form_id ?>').on('click', '.submit', function (e) {
                var form = jQuery('#<?= $form_id ?>');
                // Reset the errors
                form.removeClass('error');
                form.find('.payment-errors').text('');
                // Disable the submit button to prevent repeated clicks
                form.find('.submit').addClass('disabled');
                form.find('.submit').addClass('loading');
                Stripe.card.createToken(form, stripeResponseHandler);
                // Prevent the form from submitting with the default action
                return false;
            });
        </script>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    public function verifyCurrency($code)
    {
        $supported_currencies = array();
        $supported_currencies['AED'] = 'United Arab Emirates Dirham';
        $supported_currencies['AFN'] = 'Afghan Afghani';
        $supported_currencies['ALL'] = 'Albanian Lek';
        $supported_currencies['AMD'] = 'Armenian Dram';
        $supported_currencies['ANG'] = 'Netherlands Antillean Gulden';
        $supported_currencies['AOA'] = 'Angolan Kwanza';
        $supported_currencies['ARS'] = 'Argentine Peso';
        $supported_currencies['AUD'] = 'Australian Dollar';
        $supported_currencies['AWG'] = 'Aruban Florin';
        $supported_currencies['AZN'] = 'Azerbaijani Manat';
        $supported_currencies['BAM'] = 'Bosnia & Herzegovina Convertible Mark';
        $supported_currencies['BBD'] = 'Barbadian Dollar';
        $supported_currencies['BDT'] = 'Bangladeshi Taka';
        $supported_currencies['BGN'] = 'Bulgarian Lev';
        $supported_currencies['BIF'] = 'Burundian Franc';
        $supported_currencies['BMD'] = 'Bermudian Dollar';
        $supported_currencies['BND'] = 'Brunei Dollar';
        $supported_currencies['BOB'] = 'Bolivian Boliviano';
        $supported_currencies['BRL'] = 'Brazilian Real';
        $supported_currencies['BSD'] = 'Bahamian Dollar';
        $supported_currencies['BWP'] = 'Botswana Pula';
        $supported_currencies['BZD'] = 'Belize Dollar';
        $supported_currencies['CAD'] = 'Canadian Dollar';
        $supported_currencies['CDF'] = 'Congolese Franc';
        $supported_currencies['CHF'] = 'Swiss Franc';
        $supported_currencies['CLP'] = 'Chilean Peso';
        $supported_currencies['CNY'] = 'Chinese Renminbi Yuan';
        $supported_currencies['COP'] = 'Colombian Peso';
        $supported_currencies['CRC'] = 'Costa Rican Colón';
        $supported_currencies['CVE'] = 'Cape Verdean Escudo';
        $supported_currencies['CZK'] = 'Czech Koruna';
        $supported_currencies['DJF'] = 'Djiboutian Franc';
        $supported_currencies['DKK'] = 'Danish Krone';
        $supported_currencies['DOP'] = 'Dominican Peso';
        $supported_currencies['DZD'] = 'Algerian Dinar';
        $supported_currencies['EGP'] = 'Egyptian Pound';
        $supported_currencies['ETB'] = 'Ethiopian Birr';
        $supported_currencies['EUR'] = 'Euro';
        $supported_currencies['FJD'] = 'Fijian Dollar';
        $supported_currencies['FKP'] = 'Falkland Islands Pound';
        $supported_currencies['GBP'] = 'British Pound';
        $supported_currencies['GEL'] = 'Georgian Lari';
        $supported_currencies['GIP'] = 'Gibraltar Pound';
        $supported_currencies['GMD'] = 'Gambian Dalasi';
        $supported_currencies['GNF'] = 'Guinean Franc';
        $supported_currencies['GTQ'] = 'Guatemalan Quetzal';
        $supported_currencies['GYD'] = 'Guyanese Dollar';
        $supported_currencies['HKD'] = 'Hong Kong Dollar';
        $supported_currencies['HNL'] = 'Honduran Lempira';
        $supported_currencies['HRK'] = 'Croatian Kuna';
        $supported_currencies['HTG'] = 'Haitian Gourde';
        $supported_currencies['HUF'] = 'Hungarian Forint';
        $supported_currencies['IDR'] = 'Indonesian Rupiah';
        $supported_currencies['ILS'] = 'Israeli New Sheqel';
        $supported_currencies['INR'] = 'Indian Rupee';
        $supported_currencies['ISK'] = 'Icelandic Króna';
        $supported_currencies['JMD'] = 'Jamaican Dollar';
        $supported_currencies['JPY'] = 'Japanese Yen';
        $supported_currencies['KES'] = 'Kenyan Shilling';
        $supported_currencies['KGS'] = 'Kyrgyzstani Som';
        $supported_currencies['KHR'] = 'Cambodian Riel';
        $supported_currencies['KMF'] = 'Comorian Franc';
        $supported_currencies['KRW'] = 'South Korean Won';
        $supported_currencies['KYD'] = 'Cayman Islands Dollar';
        $supported_currencies['KZT'] = 'Kazakhstani Tenge';
        $supported_currencies['LAK'] = 'Lao Kipa';
        $supported_currencies['LBP'] = 'Lebanese Pound';
        $supported_currencies['LKR'] = 'Sri Lankan Rupee';
        $supported_currencies['LRD'] = 'Liberian Dollar';
        $supported_currencies['LSL'] = 'Lesotho Loti';
        $supported_currencies['MAD'] = 'Moroccan Dirham';
        $supported_currencies['MDL'] = 'Moldovan Leu';
        $supported_currencies['MGA'] = 'Malagasy Ariary';
        $supported_currencies['MKD'] = 'Macedonian Denar';
        $supported_currencies['MNT'] = 'Mongolian Tögrög';
        $supported_currencies['MOP'] = 'Macanese Pataca';
        $supported_currencies['MRO'] = 'Mauritanian Ouguiya';
        $supported_currencies['MUR'] = 'Mauritian Rupee';
        $supported_currencies['MVR'] = 'Maldivian Rufiyaa';
        $supported_currencies['MWK'] = 'Malawian Kwacha';
        $supported_currencies['MXN'] = 'Mexican Peso';
        $supported_currencies['MYR'] = 'Malaysian Ringgit';
        $supported_currencies['MZN'] = 'Mozambican Metical';
        $supported_currencies['NAD'] = 'Namibian Dollar';
        $supported_currencies['NGN'] = 'Nigerian Naira';
        $supported_currencies['NIO'] = 'Nicaraguan Córdoba';
        $supported_currencies['NOK'] = 'Norwegian Krone';
        $supported_currencies['NPR'] = 'Nepalese Rupee';
        $supported_currencies['NZD'] = 'New Zealand Dollar';
        $supported_currencies['PAB'] = 'Panamanian Balboa';
        $supported_currencies['PEN'] = 'Peruvian Nuevo Sol';
        $supported_currencies['PGK'] = 'Papua New Guinean Kina';
        $supported_currencies['PHP'] = 'Philippine Peso';
        $supported_currencies['PKR'] = 'Pakistani Rupee';
        $supported_currencies['PLN'] = 'Polish Złoty';
        $supported_currencies['PYG'] = 'Paraguayan Guaraní';
        $supported_currencies['QAR'] = 'Qatari Riyal';
        $supported_currencies['RON'] = 'Romanian Leu';
        $supported_currencies['RSD'] = 'Serbian Dinar';
        $supported_currencies['RUB'] = 'Russian Ruble';
        $supported_currencies['RWF'] = 'Rwandan Franc';
        $supported_currencies['SAR'] = 'Saudi Riyal';
        $supported_currencies['SBD'] = 'Solomon Islands Dollar';
        $supported_currencies['SCR'] = 'Seychellois Rupee';
        $supported_currencies['SEK'] = 'Swedish Krona';
        $supported_currencies['SGD'] = 'Singapore Dollar';
        $supported_currencies['SHP'] = 'Saint Helenian Pound';
        $supported_currencies['SLL'] = 'Sierra Leonean Leone';
        $supported_currencies['SOS'] = 'Somali Shilling';
        $supported_currencies['SRD'] = 'Surinamese Dollar';
        $supported_currencies['STD'] = 'São Tomé and Príncipe Dobra';
        $supported_currencies['SZL'] = 'Swazi Lilangeni';
        $supported_currencies['THB'] = 'Thai Baht';
        $supported_currencies['TJS'] = 'Tajikistani Somoni';
        $supported_currencies['TOP'] = 'Tongan Paʻanga';
        $supported_currencies['TRY'] = 'Turkish Lira';
        $supported_currencies['TTD'] = 'Trinidad and Tobago Dollar';
        $supported_currencies['TWD'] = 'New Taiwan Dollar';
        $supported_currencies['TZS'] = 'Tanzanian Shilling';
        $supported_currencies['UAH'] = 'Ukrainian Hryvnia';
        $supported_currencies['UGX'] = 'Ugandan Shilling';
        $supported_currencies['USD'] = 'United States Dollar';
        $supported_currencies['UYU'] = 'Uruguayan Peso';
        $supported_currencies['UZS'] = 'Uzbekistani Som';
        $supported_currencies['VEF'] = 'Venezuelan Bolívar';
        $supported_currencies['VND'] = 'Vietnamese Đồng';
        $supported_currencies['VUV'] = 'Vanuatu Vatu';
        $supported_currencies['WST'] = 'Samoan Tala';
        $supported_currencies['XAF'] = 'Central African Cfa Franc';
        $supported_currencies['XCD'] = 'East Caribbean Dollar';
        $supported_currencies['XOF'] = 'West African Cfa Franc';
        $supported_currencies['XPF'] = 'Cfp Franc';
        $supported_currencies['YER'] = 'Yemeni Rial';
        $supported_currencies['ZAR'] = 'South African Rand';
        $supported_currencies['ZMW'] = 'Zambian Kwacha';
        if (in_array($code, array_keys($supported_currencies))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //------------------------------------------------------------

    public function listenerIPN($post_data)
    {
        return;
    }

}
