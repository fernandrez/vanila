<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

//------------------------------------------------------------

/**
 * This is the payment gateway interface
 */
interface TeamBooking_PaymentGateways_Gateway
{
    /**
     * The main payment routine
     */
    public function processPayment();
}