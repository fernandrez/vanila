<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

/**
 * Option object for PayPal Gateway
 */
class TeamBooking_PaymentGateways_PayPal_Settings implements TeamBooking_PaymentGateways_Settings
{

    ///////////////
    //  GENERAL  //
    ///////////////
    private $use_gateway;
    private $gateway_id;
    /////////////////////////
    //  PAYPAL SPECIFIC    //
    /////////////////////////
    private $account_email;
    private $redirect_url;
    private $use_sandbox;
    private $save_ipn_logs;
    private $use_curl;

    public function __construct()
    {
        /////////////////////////
        //  GENERAL (defaults) //
        /////////////////////////
        $this->gateway_id = 'paypal';
        $this->use_gateway = FALSE;
        ////////////////////////////////
        // PAYPAL SPECIFIC (defaults) //
        ////////////////////////////////
        $this->use_sandbox = FALSE;
        $this->save_ipn_logs = FALSE;
        $this->redirect_url = site_url();
        if (extension_loaded('curl')) {
            $this->use_curl = TRUE;
        } else {
            $this->use_curl = FALSE;
        }
    }

    //------------------------------------------------------------

    public function isOffsite()
    {
        return TRUE;
    }

    //------------------------------------------------------------

    public function setUseGatewayOn()
    {
        $this->use_gateway = TRUE;
    }

    public function setUseGatewayOff()
    {
        $this->use_gateway = FALSE;
    }

    public function isActive()
    {
        return $this->use_gateway;
    }

    //------------------------------------------------------------

    public function getGatewayId()
    {
        return $this->gateway_id;
    }

    //------------------------------------------------------------

    public function setAccountEmail($email)
    {
        $this->account_email = $email;
    }

    public function getAccountEmail()
    {
        return $this->account_email;
    }

    //------------------------------------------------------------

    public function setRedirectUrl($url)
    {
        $this->redirect_url = $url;
    }

    public function getRedirectUrl()
    {
        return $this->redirect_url;
    }

    //------------------------------------------------------------

    public function setUseCurlYes()
    {
        $this->use_curl = TRUE;
    }

    public function setUseCurlNo()
    {
        $this->use_curl = FALSE;
    }

    public function getUseCurl()
    {
        if (extension_loaded('curl')) {
            if (is_null($this->use_curl)) {
                return TRUE;
            } else {
                return $this->use_curl;
            }
        } else {
            return FALSE;
        }
    }

    //------------------------------------------------------------

    public function setUseSandboxYes()
    {
        $this->use_sandbox = TRUE;
    }

    public function setUseSandboxNo()
    {
        $this->use_sandbox = FALSE;
    }

    public function isUseSandbox()
    {
        return $this->use_sandbox;
    }

    //------------------------------------------------------------

    public function setSaveIpnLogsYes()
    {
        $this->save_ipn_logs = TRUE;
    }

    public function setSaveIpnLogsNo()
    {
        $this->save_ipn_logs = FALSE;
    }

    public function isSaveIpnLogs()
    {
        return $this->save_ipn_logs;
    }

    //------------------------------------------------------------

    public function getLabel()
    {
        ?>
        <div class="ui mini blue label">PayPal</div>
        <?php
    }

    //------------------------------------------------------------

    public function getPayButton()
    {
        ob_start();
        ?>
        <div class="ui tb-icon basic tbk-button tbk-header tbk-pay-button" data-offsite="<?= $this->isOffsite() ?>"
             data-gateway="<?= $this->gateway_id ?>" type="submit">
            <i class="paypal tb-icon"></i>

            <div class="tbk-content">
                <?= esc_html__('Pay with PayPal', 'teambooking') ?>
            </div>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    public function getDataForm(TeamBooking_ReservationData $data, $reservation_database_id)
    {
        return FALSE;
    }

    //------------------------------------------------------------

    public function prepareGateway(TeamBooking_ReservationData $data, $additional_parameter = NULL)
    {
        include TEAMBOOKING_PATH . 'src/TeamBooking/PaymentGateways/PayPal/Gateway.php';
        $paypal = new TeamBooking_PaymentGateways_PayPal_Gateway();
        $paypal->setCurrency($data->getCurrencyCode());
        $paypal->setItemName($data->getServiceName());
        $paypal->setPrice($data->getPrice());
        $paypal->setQuantity($data->getTickets());
        $paypal->setIpnId($data->getIpnUniqueId());
        // should we override the redirect URL for this service?
        if (tbGetSettings()->getService($data->getServiceId())->getRedirect()) {
            $paypal->setRedirectUrl(tbGetSettings()->getService($data->getServiceId())->getRedirectUrl());
        } else {
            $paypal->setRedirectUrl($this->getRedirectUrl());
        }

        return $paypal->processPayment();
    }

    //------------------------------------------------------------

    public function saveBackendSettings(array $settings, $new_currency_code)
    {
        isset($settings['use_gateway']) && !empty($settings['account_email']) && $this->verifyCurrency($new_currency_code) ? $this->setUseGatewayOn() : $this->setUseGatewayOff();
        isset($settings['account_email']) ? $this->setAccountEmail(strtolower($settings['account_email'])) : $this->setAccountEmail('');
        isset($settings['redirect_url']) ? $this->setRedirectUrl($settings['redirect_url']) : $this->setRedirectUrl('');
        isset($settings['use_sandbox']) ? $this->setUseSandboxYes() : $this->setUseSandboxNo();
        isset($settings['save_ipn_logs']) ? $this->setSaveIpnLogsYes() : $this->setSaveIpnLogsNo();
        if ($settings['use_curl'] === 'yes') {
            $this->setUseCurlYes();
        } else {
            $this->setUseCurlNo();
        }
    }

    //------------------------------------------------------------

    public function getBackendSettingsTab()
    {
        ob_start();
        ?>
        <div class="tbk-panel-title">
            <h2>
                <a href="https://www.paypal.com" target="_blank">
                    <img src="https://www.paypalobjects.com/webstatic/i/logo/rebrand/ppcom.png" style="float:right;"/>
                </a>
                <?= esc_html__('PayPal', 'teambooking') ?>
            </h2>
        </div>
        <div class="tbk-content">
            <?php if ($this->verifyCurrency(tbGetSettings()->getCurrencyCode())) { ?>
                <div class="tbk-setting-alert">
                    <span><?= esc_html__('Note', 'teambooking') ?></span>
                    <?= esc_html__('be sure that your PayPal account actually holds the selected currency, otherwise your payments may require manual confirmation, depending on Payment Receiving Preferences in your PayPal profile.', 'teambooking') ?>
                </div>
            <?php } else { ?>
                <div class="tbk-setting-warning">
                    <span><?= esc_html__('Note', 'teambooking') ?></span>
                    <?= esc_html__("The selected currency is not supported by PayPal. PayPal gateway can't be activated.", 'teambooking') ?>
                </div>
            <?php } ?>
            <ul class="tbk-list">
                <li>
                    <h4>
                        <?= esc_html__('Use PayPal gateway', 'teambooking') ?>
                    </h4>

                    <p>
                        <label for="paypal_use_gateway">
                            <input name="gateway_settings[<?= $this->gateway_id ?>][use_gateway]" type="checkbox"
                                   value="yes" <?php checked(1, $this->use_gateway) ?> >
                        </label>
                    </p>
                    <?php if (empty($this->account_email)) { ?>
                        <div class="tbk-setting-alert">
                            <span><?= esc_html__('Note', 'teambooking') ?></span>: <?= esc_html__("you can't activate the PayPal gateway if the email field is empty.", 'teambooking') ?>
                        </div>
                    <?php } ?>
                </li>
                <li>
                    <h4>
                        <?= esc_html__('PayPal account email', 'teambooking') ?>
                    </h4>

                    <p><?= esc_html__('Payments will be addressed to this account.', 'teambooking') ?></p>

                    <p>
                        <input type="text" name="gateway_settings[<?= $this->gateway_id ?>][account_email]"
                               value="<?= $this->account_email ?>" class="regular-text"/>
                    </p>
                </li>
                <li>
                    <h4>
                        <?= esc_html__('Redirect URL', 'teambooking') ?>
                    </h4>

                    <p><?= esc_html__('After PayPal payment, the customer will be redirected to this URL.', 'teambooking') ?></p>

                    <p>
                        <input type="text" name="gateway_settings[<?= $this->gateway_id ?>][redirect_url]"
                               value="<?= $this->redirect_url ?>" class="regular-text"/>
                    </p>
                </li>
                <li>
                    <h4>
                        <?= esc_html__('IPN listener method', 'teambooking') ?>
                    </h4>

                    <p><?= esc_html__('cURL is default, if supported by your server. If you have issues like reservations kept in pending status, try to change method.', 'teambooking') ?></p>
                    <fieldset>
                        <label><input type="radio" name="gateway_settings[<?= $this->gateway_id ?>][use_curl]"
                                      value="yes" <?php checked($this->getUseCurl(), TRUE) ?> <?php disabled(extension_loaded('curl'), FALSE); ?>/>
                            <span>cURL <?= !extension_loaded('curl') ? '(' . __('not supported', 'teambooking') . ')' : '' ?></span></label><br>
                        <label><input type="radio" name="gateway_settings[<?= $this->gateway_id ?>][use_curl]"
                                      value="no" <?php checked($this->getUseCurl(), FALSE) ?> /> <span>fsockopen</span></label>
                    </fieldset>
                </li>
                <li>
                    <h4>
                        <?= esc_html__('Use PayPal Sandbox for testing', 'teambooking') ?>
                    </h4>

                    <p><?= esc_html__('Fake payments. Activate this option only if you really know what the sandbox is.', 'teambooking') ?></p>

                    <p>
                        <label for="gateway_settings[<?= $this->gateway_id ?>][use_sandbox]">
                            <input name="gateway_settings[<?= $this->gateway_id ?>][use_sandbox]" type="checkbox"
                                   value="yes" <?php checked(1, $this->use_sandbox) ?> >
                        </label>
                    </p>

                    <div class="tbk-setting-alert">
                        <span><?= esc_html__('Note', 'teambooking') ?></span>: <?= esc_html__("if you're testing the payments in a localhost environment, the PayPal IPN won't reach your computer, and the plugin will act as if payment was never made.", 'teambooking') ?>
                    </div>
                </li>
                <li>
                    <h4>
                        <?= esc_html__('Save logs for IPN errors', 'teambooking') ?>
                    </h4>

                    <p><?= esc_html__('Activate this option to save a file with returned IPN response for error debug', 'teambooking') ?></p>

                    <p>
                        <label for="gateway_settings[<?= $this->gateway_id ?>][save_ipn_logs]">
                            <input name="gateway_settings[<?= $this->gateway_id ?>][save_ipn_logs]" type="checkbox"
                                   value="yes" <?php checked(1, $this->save_ipn_logs) ?> >
                        </label>
                    </p>
                </li>
                <li>
                    <?php submit_button(esc_html__('Save changes', 'teambooking'), 'primary', 'team_booking_payment_options_submit') ?>
                </li>
            </ul>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    public function verifyCurrency($code)
    {
        $supported_currencies = array(
            'AUD' => array(
                'label'  => 'Australian Dollar',
                'format' => '$ %s',
            ),
            'BRL' => array(
                'label'  => 'Brazilian Real',
                'format' => 'R$ %s',
            ),
            'CAD' => array(
                'label'  => 'Canadian Dollar',
                'format' => '$ %s',
            ),
            'CZK' => array(
                'label'  => 'Czech Koruna',
                'format' => '%s Kč',
            ),
            'DKK' => array(
                'label'  => 'Danish Krone',
                'format' => '%s kr',
            ),
            'EUR' => array(
                'label'  => 'Euro',
                'format' => '€ %s',
            ),
            'HKD' => array(
                'label'  => 'Hong Kong Dollar',
                'format' => '$ %s',
            ),
            'HUF' => array(
                'label'  => 'Hungarian Forint',
                'format' => '%s Ft',
            ),
            'ILS' => array(
                'label'  => 'Israeli New Sheqel',
                'format' => '₪ %s',
            ),
            'JPY' => array(
                'label'  => 'Japanese Yen',
                'format' => '¥ %s',
            ),
            'MYR' => array(
                'label'  => 'Malaysian Ringgit',
                'format' => 'RM %s',
            ),
            'MXN' => array(
                'label'  => 'Mexican Peso',
                'format' => '$ %s',
            ),
            'NOK' => array(
                'label'  => 'Norwegian Krone',
                'format' => '%s kr',
            ),
            'NZD' => array(
                'label'  => 'N.Z. Dollar',
                'format' => '$ %s',
            ),
            'PHP' => array(
                'label'  => 'Philippine Peso',
                'format' => '₱ %s',
            ),
            'PLN' => array(
                'label'  => 'Polish Zloty',
                'format' => '%s zł',
            ),
            'GBP' => array(
                'label'  => 'Pound Sterling',
                'format' => '£ %s',
            ),
            'SGD' => array(
                'label'  => 'Singapore Dollar',
                'format' => '$ %s',
            ),
            'SEK' => array(
                'label'  => 'Swedish Krona',
                'format' => '%s kr',
            ),
            'CHF' => array(
                'label'  => 'Swiss Franc',
                'format' => '%s Fr',
            ),
            'TWD' => array(
                'label'  => 'New Taiwan Dollar',
                'format' => 'NT$ %s',
            ),
            'THB' => array(
                'label'  => 'Thai Baht',
                'format' => '฿ %s',
            ),
            'TRY' => array(
                'label'  => 'Turkish Lira',
                'format' => 'TRY %s',
            ),
            'USD' => array(
                'label'  => 'U.S. Dollar',
                'format' => '$ %s',
            ),
        );
        if (in_array($code, array_keys($supported_currencies))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //------------------------------------------------------------

    public function listenerIPN($ipn_data)
    {
        // Log IPN errors
        if ($this->isSaveIpnLogs()) {
            ini_set('log_errors', TRUE);
            ini_set('error_log', dirname(__FILE__) . '/ipn_errors.log');
        }
        // instantiate the IPN listener
        include_once dirname(__FILE__) . '/lib/tb_ipnlistener.php';
        $listener = new IpnListener();

        // tell the IPN listener whether to use the PayPal test sandbox or not
        if ($this->isUseSandbox()) {
            $listener->use_sandbox = TRUE;
        } else {
            $listener->use_sandbox = FALSE;
        }

        // tell the IPN listener whether to use cURL or fsockopen
        if ($this->getUseCurl()) {
            $listener->use_curl = TRUE;
        } else {
            $listener->use_curl = FALSE;
        }

        // try to process the IPN POST
        try {
            $listener->requirePostMethod();
            $verified = $listener->processIpn();
        } catch (Exception $e) {
            // Log IPN errors
            if ($this->isSaveIpnLogs()) {
                error_log($e->getMessage());
            }
            exit(0);
        }

        if ($verified) {
            if (isset($ipn_data['charset'])) {
                $charset = $ipn_data['charset'];
                // If not UTF-8, convert all the values
                if (!$charset == 'utf-8') {
                    foreach ($ipn_data as $key => &$value) {
                        $value = mb_convert_encoding($value, 'utf-8', $charset);
                    }
                }
                // Store the charset values for future implementation
                $ipn_data['charset'] = 'utf-8';
                $ipn_data['charset_original'] = $charset;
            }

            // Is immediate payment required or not?
            $pending_reservations = tbGetReservations(TRUE);
            $pending_found = FALSE;
            foreach ($pending_reservations as $reservation_id => $res) {
                /* @var $res TeamBooking_ReservationData */
                if ($res->getIpnUniqueId() == $ipn_data['custom']) {
                    $reservation = $res;
                    $reservation_id = $reservation_id;
                    $pending_found = TRUE;
                    break;
                }
            }
            // Timeout check
            // TODO: better logic, i.e. check if it is still available?
            if ($pending_found) {
                $settings = tbGetSettings();
                if (tbIsReservationTimedOut($reservation, $settings)) {
                    // Delete the pending record
                    tbDeleteReservationById($reservation_id, TRUE);
                    // Send email notice to admin
                    $body = "A refund may be needed, here are the IPN details: \n";
                    $body .= $listener->getTextReport();
                    wp_mail(get_bloginfo('admin_email'), 'Payment received after expiring period', $body);
                    exit(0);
                }
            }
            $reservations = tbGetReservations();
            if (!$pending_found) { // Reservation is not pending, see if it's placed
                foreach ($reservations as $reservation_id => $res) {
                    /* @var $res TeamBooking_ReservationData */
                    if ($res->getIpnUniqueId() == $ipn_data['custom']) {
                        $reservation = $res;
                        $reservation_id = $reservation_id;
                    }
                }
            }

            $errmsg = '';   // stores errors from fraud checks

            if (!isset($reservation)) { // Reservation is still not found!
                $errmsg .= "'reservation_unique_id' not found: ";
                $errmsg .= $ipn_data['custom'] . "\n";
                $price = 0;
            } else {
                if ($reservation->getServiceClass() == 'event') {
                    $price = number_format($reservation->getPrice() * $reservation->getTickets(), 2, '.', '');
                } else {
                    $price = number_format($reservation->getPrice(), 2, '.', '');
                }
            }

            // 1. Make sure the payment status is "Completed" 
            if ($ipn_data['payment_status'] != 'Completed') {
                if ($ipn_data['payment_status'] == 'Pending' && $ipn_data['pending_reason'] == 'multi_currency') {
                    // Warning: the PayPal account
                    // settings requires manual confirmation of payments
                    // with currency that merchant don't holds
                    $body = "WARNING: You are asking for payments in a currency that your PayPal account doesn't hold! Please either change the currency, or change your PayPal -> Profile -> Payment receiving preferences for 'Allow payments sent to me in a currency I do not hold' to 'Yes, accept and convert them'.\n\n";
                    $body .= $listener->getTextReport();
                    wp_mail(get_bloginfo('admin_email'), 'Currency not held by your PayPal account', $body);
                    $multi_currency = TRUE;
                    unset($body);
                } else {
                    exit(0);
                }
            }
            // 2. Make sure seller email matches your primary account email.
            if ($ipn_data['receiver_email'] != $this->getAccountEmail()) {
                $errmsg .= "'receiver_email' does not match: ";
                $errmsg .= $ipn_data['receiver_email'] . "\n";
                $errmsg .= $this->getAccountEmail() . "\n";
            }
            // 3. Make sure the amount(s) paid match
            $paid_amount = $ipn_data['mc_gross'];
            if (isset($ipn_data['tax']) && is_numeric($ipn_data['tax'])) {
                $taxes = $ipn_data['tax'];
            } else {
                $taxes = 0.00;
            }
            if (($paid_amount - $taxes) != $price) {
                $errmsg .= "'mc_gross' does not match: ";
                $errmsg .= $paid_amount . "\n";
            }
            // 4. Make sure the currency code matches
            if ($ipn_data['mc_currency'] != tbGetSettings()->getCurrencyCode()) {
                $errmsg .= "'mc_currency' does not match: ";
                $errmsg .= $ipn_data['mc_currency'] . "\n";
            }
            // 5. Ensure the transaction is not a duplicate.
            $txn_id = $ipn_data['txn_id'];
            $exists = FALSE;
            foreach ($reservations as $res) {
                /* @var $res TeamBooking_ReservationData */
                if ($res->getPaymentGateway() == 'paypal') {
                    $payment_details = $res->getPaymentDetails();
                    if ($payment_details['transaction id'] == $txn_id) {
                        $exists = TRUE;
                    }
                }
            }
            if ($exists) {
                $errmsg .= "'txn_id' has already been processed: " . $txn_id . "\n";
            }

            if (!empty($errmsg)) {
                // manually investigate errors from the fraud checking
                $body = "IPN failed fraud checks: \n$errmsg\n\n";
                $body .= $listener->getTextReport();
                wp_mail(get_bloginfo('admin_email'), 'IPN Fraud Warning', $body);
                exit;
            } else {
                if ($multi_currency) {
                    $reservation->setPaymentMustBeManuallyConfirmedOnPayPalOn();
                } else {
                    $reservation->setPaid();
                }
                $reservation->setPaymentGateway($this->gateway_id);
                $payment_details_array = array();
                $payment_details_array['transaction id'] = $txn_id;
                $payment_details_array['paid amount'] = $paid_amount;
                $payment_details_array['payer email'] = $ipn_data['payer_email'];
                $reservation->setPaymentDetails($payment_details_array);
                if (!$pending_found) {
                    // Reservation was not pending, updating the placed one
                    $var = tbUpdateReservationById($reservation_id, $reservation);
                } else {
                    // Reservation was pending, delete it from pending table...
                    tbDeleteReservationById($reservation_id, TRUE);
                    // ...and do the reservation...
                    $var = new TeamBooking_Reservation($reservation);
                    $attempted = $var->doReservation();
                    // Check for errors
                    if ($attempted instanceof TeamBooking_Error) {
                        // At this point, payment is made but
                        // the reservation attempt throws errors.
                        $reservation->setStatusPending();
                        tbInsertReservation($reservation);
                        $errmsg = $attempted->getMessage();
                        $reservation->setPendingReason($errmsg);
                        $body = "Error message: \n$errmsg\n\n";
                        $body .= $listener->getTextReport();
                        wp_mail(get_bloginfo('admin_email'), 'Reservation error after payment is done.', $body);
                    } else {
                        // No errors
                        tbInsertReservation($reservation);
                    }
                }
                exit;
            }
            // FOR TESTING ONLY!!
            #ob_start();
            #var_dump($errmsg);
            #$output = ob_get_clean();
            #$outputFile = dirname(__FILE__) . '/ipn_dump.log';
            #$filehandle = fopen($outputFile, 'a') or die();
            #fwrite($filehandle, $output);
            #fclose($filehandle);
        } else {
            // manually investigate the invalid IPN
            wp_mail(get_bloginfo('admin_email'), 'Invalid IPN', $listener->getTextReport());
            exit;
        }
    }

}
