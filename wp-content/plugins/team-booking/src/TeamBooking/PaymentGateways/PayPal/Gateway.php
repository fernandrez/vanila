<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_PaymentGateways_PayPal_Gateway implements TeamBooking_PaymentGateways_Gateway
{

    //------------------------------------------------------------

    private $settings;
    private $currency;
    private $quantity;
    private $price;
    private $id;
    private $item_name;
    private $redirect_url;

    public function __construct()
    {
        $this->settings = tbGetSettings();
    }

    //------------------------------------------------------------

    public function setCurrency($code)
    {
        $this->currency = $code;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function setItemName($name)
    {
        $this->item_name = $name;
    }

    public function setIpnId($id)
    {
        $this->id = $id;
    }

    public function setRedirectUrl($url)
    {
        $this->redirect_url = $url;
    }

    //------------------------------------------------------------

    public function processPayment()
    {

        // Prepare GET data
        $query = array();
        $query['notify_url'] = admin_url() . 'admin-ajax.php?action=teambooking_ipn_listener&paypal=1';
        $query['cmd'] = '_xclick';
        $query['cbt'] = 'Return to ' . get_bloginfo('name');
        $query['currency_code'] = $this->currency;
        $query['business'] = $this->settings->getPaymentGatewaySettingObject('paypal')->getAccountEmail();
        $query['item_name'] = $this->item_name;
        $query['quantity'] = $this->quantity;
        $query['amount'] = $this->price;
        $query['custom'] = $this->id;
        $query['return'] = $this->redirect_url;

        // Prepare query string
        $query_string = http_build_query($query);

        // Return
        return $this->getPayPalUrl() . $query_string;
    }

    //------------------------------------------------------------

    private function getPayPalUrl()
    {
        if ($this->settings->getPaymentGatewaySettingObject('paypal')->isUseSandbox()) {
            return 'https://www.sandbox.paypal.com/cgi-bin/webscr?';
        } else {
            return 'https://www.paypal.com/cgi-bin/webscr?';
        }
    }

}
