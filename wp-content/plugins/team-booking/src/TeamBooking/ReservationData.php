<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_ReservationData
{

    //------------------------------------------------------------

    //////////////////////////
    //   COWORKER DATA      //
    //////////////////////////
    private $coworker_id;
    //////////////////////////
    //   CUSTOMER DATA      //
    //////////////////////////
    private $customer_user_id; // WordPress user id, if registered
    //////////////////////////
    //    GOOGLE DATA       //
    //////////////////////////
    private $google_calendar_event_parent;
    private $google_calendar_event;
    private $google_calendar_id;
    //////////////////////////
    //    SERVICE DATA      //
    //////////////////////////
    private $service_id;
    private $service_name;
    private $service_class;
    private $service_location;
    //////////////////////////
    // COLLECTED FORM DATA  //
    //////////////////////////
    private $form_fields;
    //////////////////////////
    //  RESERVATION DATA    //
    //////////////////////////
    private $start; // unix time
    private $end;   // unix time
    private $slot_start; // ATOM time
    private $slot_end;   // ATOM time
    private $description = ""; //TODO
    private $uri = ""; //TODO
    private $tickets;
    private $price;
    private $log_key;
    private $status;
    private $cancellation_reason;
    private $pending_reason;
    //////////////////////////
    //    PAYMENT DATA      //
    //////////////////////////
    private $payment_gateway;
    private $paid;
    private $payment_must_be_manually_confimed_on_paypal;
    private $payment_details; // array, gateway specific
    private $currency_code;
    private $ipn_unique_id; // Internally generated for IPN recatch
    //////////////////////////
    //  FILES REFERENCES    //
    //////////////////////////
    private $files;
    //////////////////////////
    //  ADDITIONAL DATA     //
    //////////////////////////
    private $post_id;
    private $post_title;

    public function __construct()
    {
        ;
    }

    //------------------------------------------------------------

    //////////////////////////
    //                      //
    //   COWORKER DATA      //
    //                      //
    //////////////////////////

    public function setCoworker($id)
    {
        $this->coworker_id = $id;
    }

    public function getCoworker()
    {
        return $this->coworker_id;
    }

    //------------------------------------------------------------

    //////////////////////////
    //                      //
    //   CUSTOMER DATA      //
    //                      //
    //////////////////////////

    public function setCustomerUserId($id)
    {
        $this->customer_user_id = $id;
    }

    public function getCustomerUserId()
    {
        if (isset($this->customer_user_id)) {
            return $this->customer_user_id;
        } else {
            return 0;
        }
    }

    public function getCustomerEmail()
    {
        $return = "";
        foreach ($this->form_fields as $field) {
            /* @var $field TeamBooking_ReservationFormField */
            if ($field->getName() == 'email') {
                $return = $field->getValue();
            }
        }

        return $return;
    }

    public function getCustomerDisplayName()
    {
        $return = "";
        foreach ($this->form_fields as $field) {
            /* @var $field TeamBooking_ReservationFormField */
            if ($field->getName() == 'first_name') {
                $return .= $field->getValue();
            }
        }
        foreach ($this->form_fields as $field) {
            /* @var $field TeamBooking_ReservationFormField */
            if ($field->getName() == 'second_name') {
                $return .= " " . $field->getValue();
            }
        }

        return $return;
    }

    public function getCustomerAddress()
    {
        $return = FALSE;
        foreach ($this->form_fields as $field) {
            /* @var $field TeamBooking_ReservationFormField */
            if ($field->getName() == 'address') {
                $return = $field->getValue();
            }
        }

        return $return;
    }

    //------------------------------------------------------------

    //////////////////////////
    //                      //
    //     GOOGLE DATA      //
    //                      //
    //////////////////////////

    public function setGoogleCalendarId($id)
    {
        $this->google_calendar_id = $id;
    }

    public function getGoogleCalendarId()
    {
        if ($this->google_calendar_id) {
            return $this->google_calendar_id;
        } else {
            return FALSE;
        }
    }

    public function setGoogleCalendarEvent($id)
    {
        $this->google_calendar_event = $id;
    }

    public function getGoogleCalendarEvent()
    {
        return $this->google_calendar_event;
    }

    public function setGoogleCalendarEventParent($id)
    {
        $this->google_calendar_event_parent = $id;
    }

    public function getGoogleCalendarEventParent()
    {
        if (!$this->google_calendar_event_parent) {
            return FALSE;
        } else {
            return $this->google_calendar_event_parent;
        }
    }

    //------------------------------------------------------------

    //////////////////////////
    //                      //
    //    SERVICE DATA      //
    //                      //
    //////////////////////////

    public function setServiceId($id)
    {
        $this->service_id = $id;
    }

    public function getServiceId()
    {
        return $this->service_id;
    }

    public function setServiceClass($class)
    {
        $this->service_class = $class;
    }

    public function getServiceClass()
    {
        if (!$this->service_class) {
            try {
                $return = tbGetSettings()->getService($this->service_id)->getClass();
            } catch (Exception $ex) {
                $return = FALSE;
            }

            return $return;
        } else {
            return $this->service_class;
        }
    }

    public function setServiceName($name)
    {
        $this->service_name = $name;
    }

    public function getServiceName()
    {
        return $this->service_name;
    }

    public function setServiceLocation($location)
    {
        $this->service_location = $location;
    }

    public function getServiceLocation()
    {
        return $this->service_location;
    }

    //------------------------------------------------------------

    //////////////////////////
    //                      //
    // COLLECTED FORM DATA  //
    //                      //
    //////////////////////////

    public function setFormFields(array $fields)
    {
        $this->form_fields = $fields;
    }

    public function getFormFields()
    {
        return $this->form_fields;
    }

    public function getFieldsArray()
    {
        $return = array();
        foreach ($this->form_fields as $field) {
            /* @var $field TeamBooking_ReservationFormField */
            $return[$field->getName()] = $field->getValue();
        }

        return $return;
    }

    //------------------------------------------------------------

    //////////////////////////
    //                      //
    //   RESERVATION DATA   //
    //                      //
    //////////////////////////

    public function setSlotStart($google_event_time)
    {
        $this->slot_start = $google_event_time;
    }

    public function getSlotStart()
    {
        return $this->slot_start;
    }

    public function setSlotEnd($google_event_time)
    {
        $this->slot_end = $google_event_time;
    }

    public function getSlotEnd()
    {
        return $this->slot_end;
    }

    public function setStart($time)
    {
        $this->start = $time;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function setEnd($time)
    {
        $this->end = $time;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function setUri($url)
    {
        $this->uri = $url;
    }

    public function getUri()
    {
        return $this->uri;
    }

    public function setServiceDescription($description)
    {
        $this->description = $description;
    }

    public function getServiceDescription()
    {
        return $this->description;
    }

    public function setTickets($number)
    {
        $this->tickets = $number;
    }

    public function getTickets()
    {
        return $this->tickets;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setCreationInstant($now)
    {
        $this->log_key = $now;
    }

    public function getCreationInstant()
    {
        return $this->log_key;
    }

    public function setStatusPending()
    {
        $this->status = 'pending';
    }

    public function setStatusWaitingApproval()
    {
        $this->status = 'waiting_approval';
    }

    public function setStatusConfirmed()
    {
        $this->status = 'confirmed';
    }

    /**
     * This is for Unscheduled Services
     */
    public function setStatusDone()
    {
        $this->status = 'done';
    }

    public function setStatusCancelled()
    {
        $this->status = 'cancelled';
    }

    public function isCancelled()
    {
        if ($this->status == 'cancelled') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * This is for Unscheduled Services
     */
    public function isDone()
    {
        if ($this->status == 'done') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function isConfirmed()
    {
        if ($this->status == 'confirmed' || !isset($this->status)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function isPending()
    {
        if ($this->status == 'pending') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function isWaitingApproval()
    {
        if ($this->status == 'waiting_approval') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getStatus()
    {
        if (!$this->status) {
            return __('confirmed', 'teambooking');
        } else {
            return $this->status;
        }
    }

    public function setCancellationReason($text)
    {
        $this->cancellation_reason = $text;
    }

    public function getCancellationReason()
    {
        if (!$this->cancellation_reason) {
            return '';
        } else {
            return $this->cancellation_reason;
        }
    }

    public function setPendingReason($text)
    {
        $this->pending_reason = $text;
    }

    public function getPendingReason()
    {
        if (!$this->pending_reason) {
            return '';
        } else {
            return $this->pending_reason;
        }
    }

    //------------------------------------------------------------

    //////////////////////////
    //                      //
    //     PAYMENT DATA     //
    //                      //
    //////////////////////////

    public function setPaymentGateway($gateway_id)
    {
        $this->payment_gateway = $gateway_id;
    }

    public function getPaymentGateway()
    {
        return $this->payment_gateway;
    }

    public function setPaid()
    {
        if ($this->payment_must_be_manually_confimed_on_paypal) {
            $this->payment_must_be_manually_confimed_on_paypal = FALSE;
        }
        $this->paid = TRUE;
    }

    public function setNotPaid()
    {
        $this->paid = FALSE;
    }

    public function isPaid()
    {
        if (!$this->paid) {
            return FALSE;
        } else {
            return $this->paid;
        }
    }

    public function setPaymentDetails(array $details)
    {
        $this->payment_details = $details;
    }

    public function getPaymentDetails()
    {
        return $this->payment_details;
    }

    public function setPaymentMustBeManuallyConfirmedOnPayPalOn()
    {
        $this->payment_must_be_manually_confimed_on_paypal = TRUE;
    }

    public function setPaymentMustBeManuallyConfirmedOnPayPalOff()
    {
        $this->payment_must_be_manually_confimed_on_paypal = FALSE;
    }

    public function getPaymentMustBeManuallyConfirmedOnPayPal()
    {
        if (!$this->payment_must_be_manually_confimed_on_paypal) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function setIpnUniqueId($unique_id)
    {
        $this->ipn_unique_id = $unique_id;
    }

    public function getIpnUniqueId()
    {
        return $this->ipn_unique_id;
    }

    public function setCurrencyCode($code)
    {
        $this->currency_code = $code;
    }

    public function getCurrencyCode()
    {
        return $this->currency_code;
    }

    //------------------------------------------------------------

    //////////////////////////
    //                      //
    //   FILES REFERENCES   //
    //                      //
    //////////////////////////

    public function addFileReference($hook, array $returned_handle)
    {
        $this->files[$hook] = $returned_handle;
    }

    public function removeFileReference($hook)
    {
        if (is_array($this->files)) {
            unset($this->files[$hook]);
        }
    }

    public function getFilesReferences()
    {
        if (is_array($this->files)) {
            return $this->files;
        } else {
            return array();
        }
    }

    //------------------------------------------------------------

    //////////////////////////
    //                      //
    //   ADDITIONAL DATA    //
    //                      //
    //////////////////////////

    public function isFromContainer()
    {
        if (!$this->getGoogleCalendarEventParent()) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function setPostId($id)
    {
        $this->post_id = $id;
    }

    public function getPostId()
    {
        return $this->post_id;
    }

    public function setPostTitle($title)
    {
        $this->post_title = $title;
    }

    public function getPostTitle()
    {
        return $this->post_title;
    }

    //------------------------------------------------------------

    /**
     * Returns the hooks array, included the always-present hooks:
     * [start_datetime]
     * [end_datetime]
     * [tickets_quantity]
     * [unit_price]
     * [total_price]
     * [post_id]
     * [post_title]
     * [service_location]
     * [coworker_name]
     *
     * @return array
     */
    public function getHooksArray()
    {
        $hooks_values_array = array(
            'tickets_quantity' => $this->getTickets(),
            'unit_price'       => tbCurrencyCodeToSymbol(tbGetSettings()->getCurrencyCode(), $this->getPrice()),
            'total_price'      => tbCurrencyCodeToSymbol(tbGetSettings()->getCurrencyCode(), $this->getPrice() * $this->getTickets()),
            'post_id'          => $this->getPostId(),
            'post_title'       => $this->getPostTitle(),
            'service_location' => $this->getServiceLocation(),
            'coworker_name'    => tbGetSettings()->getCoworkerData($this->getCoworker())->getDisplayName(),
        );
        if ($this->getServiceClass() != 'service') {
            $timezone = tbGetTimezone();
            $start_time_obj = DateTime::createFromFormat('U', $this->getStart());
            $start_time_obj->setTimezone($timezone);
            $start_time_value = date_i18n_tb(get_option('date_format'), $start_time_obj->getTimestamp() + $start_time_obj->getOffset()) . " " . date_i18n_tb(get_option('time_format'), $start_time_obj->getTimestamp() + $start_time_obj->getOffset());
            $end_time_obj = DateTime::createFromFormat('U', $this->getEnd());
            $end_time_obj->setTimezone($timezone);
            $end_time_value = date_i18n_tb(get_option('date_format'), $end_time_obj->getTimestamp() + $end_time_obj->getOffset()) . " " . date_i18n_tb(get_option('time_format'), $end_time_obj->getTimestamp() + $end_time_obj->getOffset());
            $hooks_values_array['start_datetime'] = $start_time_value;
            $hooks_values_array['end_datetime'] = $end_time_value;
        }
        $return = array_merge($hooks_values_array, $this->getFieldsArray());

        return $return;
    }

    //------------------------------------------------------------

    public function getChecksum()
    {
        $back_start = $this->start;
        $back_end = $this->end;
        $this->start = NULL;
        $this->end = NULL;
        $return = md5(serialize($this));
        $this->start = $back_start;
        $this->end = $back_end;

        return $return;
    }

    //------------------------------------------------------------

    public function verifyChecksum(TeamBooking_ReservationData $reservation)
    {
        $back_start = $reservation->getStart();
        $back_end = $reservation->getEnd();
        $reservation->setStart(NULL);
        $reservation->setEnd(NULL);
        $hash_guest = md5(serialize($reservation));
        $reservation->setStart($back_start);
        $reservation->setEnd($back_end);
        if ($hash_guest === $this->getChecksum()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
