<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

/**
 * Class TeamBooking_Fetcher
 *
 * The role of the fetcher is to collect all the events
 * from Google Calendars and map them to TeamBooking slots.
 *
 * The fetcher also scans the database for pending or booked reservations.
 */
class TeamBooking_Fetcher
{
    //------------------------------------------------------------

    private $services_requested;
    private $coworkers_requested;
    private $min_time_requested;
    private $max_time_requested;

    private $timezone;

    private $slots_booked;
    private $slots_free;
    private $slots_containers;

    private $reservations_in_database;
    private $reservations_in_database_pending;

    /**
     * Prepare a dirty check for Google Apps for Work issues.
     *
     * It's not clear yet (perhaps a Google API  bug?), but
     * Google Apps for Work environment produces strange events
     * duplication, even when fetching from a single token.
     * At the moment, we're keeping track of already fetched events
     * (whatever the cause is) to avoid double slots in frontend calendar.
     */
    private $already_fetched;

    /** @var  Google_Client */
    private static $client;

    /** @var  Google_Service_Calendar */
    private static $service;

    /** @var TeamBookingSettings */
    private static $settings;

    public function __construct($reservations_in_database, $reservations_in_database_pending)
    {
        $this->slots_booked = array();
        $this->slots_containers = array();
        $this->reservations_in_database = $reservations_in_database;
        $this->reservations_in_database_pending = $reservations_in_database_pending;
        static::$settings = tbGetSettings();
        // Create a new Google Client object
        static::$client = new Google_Client();
        static::$client->addScope('https://www.googleapis.com/auth/calendar');
        static::$client->setApplicationName(static::$settings->getApplicationProjectName());
        static::$client->setClientId(static::$settings->getApplicationClientId());
        static::$client->setClientSecret(static::$settings->getApplicationClientSecret());
        static::$client->setAccessType('offline');
        // Has to be set before the auth call
        static::$service = new Google_Service_Calendar(static::$client);
        $this->setRequestedCoworkers(); // default
        $this->timezone = tbGetTimezone();
    }

    //------------------------------------------------------------

    public function setRequestedServices(array $services_ids = array())
    {
        $this->services_requested = $services_ids;
    }

    public function setRequestedCoworkers(array $coworkers_ids = array())
    {
        // Getting the authorized coworkers id list
        $authorized_coworkers = tbGetAuthCoworkersIdList();
        // If requested coworkers are specified, let's filter them
        if (!empty($coworkers_ids)) {
            $authorized_coworkers = array_intersect($authorized_coworkers, $coworkers_ids);
        }
        $this->coworkers_requested = $authorized_coworkers;
    }

    public function setRequestedMinTime($time_unix = NULL)
    {
        /**
         * Applying a cut to min time.
         *
         * We're going to do this, to avoid useless fetching of
         * past Google Calendar events (this would happen if the
         * function is called to retrieve current month slots).
         */
        if (strtotime($time_unix) < current_time('timestamp')) {
            $now = new DateTime();
            $time_unix = $now->format('c');
        }
        $this->min_time_requested = $time_unix;
    }

    public function setRequestedMaxTime($time_unix = NULL)
    {
        $this->max_time_requested = $time_unix;
    }

    //------------------------------------------------------------

    public function getSlots()
    {
        $this->already_fetched = array();

        $this->mapSlotsFromGcalResults($this->fetchEvents());

        $this->fetchBookedFromDatabase();

        $this->fetchPendingFromDatabase();

        $this->splitContainers();

        $return = array_merge($this->slots_free, $this->slots_booked);

        return $return;

    }

    /**
     * Return an array with service titles to search with GData Full-text query string
     *
     * @return array
     * @throws Exception
     */
    private function prepareQueries()
    {
        // Declaring the queries return array
        $queries = array();
        // Starting the coworkers loop
        foreach ($this->coworkers_requested as $coworker_id) {
            // Retrieving the coworker data
            $coworker_data = static::$settings->getCoworkerData($coworker_id);
            // Starting the services loop
            foreach ($this->services_requested as $service_id) {
                // Let's skip the service, if the coworker doesn't participate
                if (!$coworker_data->getCustomEventSettings($service_id)->isParticipate()) {
                    continue;
                }
                // Let's skip the service, if the service is inactive
                if (!static::$settings->getService($service_id)->isActive()) {
                    continue;
                }
                // If the service class is "Service", we must skip it
                if (static::$settings->getService($service_id)->isClass('service')) {
                    continue;
                }
                /**
                 * Let's put into the array the search term for FREE slots
                 */
                $queries[$coworker_id][$service_id]['free'] = $coworker_data->getCustomEventSettings($service_id)->getLinkedEventTitle();
                /**
                 * Now, the search term for BOOKED slots (Appointment class only)
                 *
                 * BOOKED slots are necessary if the setting to show the soldout slots
                 * is active. They are necessary too, for the container mode to work.
                 * We can avoid that query if we're sure that the container mode can't
                 * be used and at the same time the soldout slots are hidden.
                 */
                if (static::$settings->getService($service_id)->isClass('appointment')
                    && (
                        static::$settings->getService($service_id)->getShowSoldout()
                        || static::$settings->getService($service_id)->getDurationRule() == 'fixed'
                        || (
                            static::$settings->getService($service_id)->getDurationRule() == 'coworker'
                            && $coworker_data->getCustomEventSettings($service_id)->getDurationRule() != 'inherited'
                        )
                    )
                ) {
                    $queries[$coworker_id][$service_id]['booked'] = $coworker_data->getCustomEventSettings($service_id)->getAfterBookedTitle();
                } else {
                    $queries[$coworker_id][$service_id]['booked'] = NULL;
                }
            }
        }

        return $queries;
    }

    //------------------------------------------------------------

    private function fetchEvents()
    {
        /**
         * Get the full text queries to query the Google Calendars.
         *
         * We're using the "search" function of the Google Calendar API
         * in order to fetch only those events that we actually need,
         * rather than the whole Google Calendar data.
         */
        $queries = $this->prepareQueries();

        /**
         * Preparing the batch requests.
         *
         * We're using an "helper" array to mark with his serialization
         * each batch request.
         * Later on, after looping through the services
         * and the calendar ids, we can easily identify
         * the service, the calendar ID and the coworker
         * for each result.
         */
        static::$client->setUseBatch(TRUE);
        $batch = new Google_Http_Batch(static::$client);
        $helper_array = array(
            'service_id'  => '',
            'calendar_id' => '',
            'coworker_id' => '',
        );

        /**
         * Defining the results array in the form of
         * $results[coworker_id]
         */
        $results = array();

        /*
         * Start looping on a coworker-level.
         *
         * Google Calendar API are not able to batch requests with different
         * auth tokens. This means that we can't fetch all the coworker calendars
         * in a unique HTTP call. Having said that, the first looping level must
         * be the coworker level.
         * This is reflected in the way the getFullTextQueries()
         * method returns the queries array.
         */
        foreach ($queries as $coworker_id => $coworker_queries) {
            $coworker_data = static::$settings->getCoworkerData($coworker_id);
            static::$client->setAccessToken($coworker_data->getAccessToken());
            // Set the current coworker ID
            $helper_array['coworker_id'] = $coworker_id;

            /**
             * Start looping on a service level.
             *
             * We're building a batch requests with all the services,
             * to query.
             */
            foreach ($coworker_queries as $service_id => $query) {
                // Set the query params
                $event_list_params = array(
                    'timeMin'      => $this->min_time_requested,
                    'timeMax'      => $this->max_time_requested,
                    'singleEvents' => TRUE,
                    'q'            => $query['free'],
                    'orderBy'      => 'startTime',
                    // That's really important!
                    'maxResults'   => static::$settings->getMaxGoogleApiResults(),
                );

                // Set the current service ID
                $helper_array['service_id'] = $service_id;
                // Set the requested strings for later use
                $helper_array['query_free'] = $query['free'];
                $helper_array['query_booked'] = $query['booked'];

                /**
                 * Start looping on a Google Calendar ID level.
                 *
                 * We're collecting all the Google Calendar IDs provided by
                 * coworker, in order to query each of them for this service.
                 */
                foreach ($coworker_data->getCalendarId() as $key => $calendar_id) {
                    // Set the current Google Calendar ID
                    $helper_array['calendar_id'] = $calendar_id;

                    // Preparing the request
                    try {
                        $request = static::$service->events->listEvents($calendar_id, $event_list_params);
                    } catch (Exception $e) {
                        if ($e->getCode() != 0) {
                            $original_error = tb_looking_for_JSON_in_string($e->getMessage());
                            if (!is_null($original_error)) {
                                if (!is_null(json_decode($original_error[0]))) {
                                    $array_message = json_decode($original_error[0]);
                                    if ($array_message->error_description == 'Token has been revoked.') {
                                        continue 3; // this coworker has revoked the token, let's skip it
                                    }
                                }
                            }
                            $error_log = new TeamBooking_ErrorLog();
                            $error_log->setErrorCode($e->getCode());
                            $error_log->setCoworkerId($coworker_id);
                            $error_log->setCalendarId($calendar_id);
                            $error_log->setMessage($e->getMessage());
                            tbErrorLog($error_log);
                            if (!static::$settings->getSilentDebug()) {
                                echo 'ERROR: ' . $e->getMessage();
                                echo '<br>';
                                echo 'Calendar ID: ' . $calendar_id;
                            }
                        }
                        // We should continue to the next calendar_id, in case of exception
                        continue;
                    }
                    // Adding the request to the batch call
                    $batch->add($request, base64_encode(serialize($helper_array)));

                    /**
                     * If there is a query for the booked slots, add it too.
                     *
                     * This is for Appointment Class service only.
                     */
                    if (!is_null($query['booked'])) {
                        // Cloning the query params (we shouldn't modify the original one)
                        $event_list_params_booked = $event_list_params;
                        // Changing the query free text with the booked one
                        $event_list_params_booked['q'] = $query['booked'];
                        // Preparing the request
                        $request = static::$service->events->listEvents($calendar_id, $event_list_params_booked);
                        // Adding the request to the batch call, appending the "booked" marker
                        $batch->add($request, base64_encode(serialize($helper_array)) . "_booked");
                    }
                }
            }

            /**
             * The batch request for the coworker is ready, let's execute it.
             */
            try {
                $results[$coworker_id] = $batch->execute();
            } catch (Exception $e) {
                // Log the error
                if ($e->getCode() != 0) {
                    $error_log = new TeamBooking_ErrorLog();
                    $error_log->setErrorCode($e->getCode());
                    $error_log->setCoworkerId($coworker_id);
                    $error_log->setMessage($e->getMessage());
                    tbErrorLog($error_log);
                    if (!static::$settings->getSilentDebug()) {
                        echo 'ERROR: ' . $e->getMessage();
                    }
                }
                // We should continue to the next coworker, in case of exception
                continue;
            }

        }

        return $results;
    }

    //------------------------------------------------------------

    private function mapSlotsFromGcalResults(array $results)
    {
        /**
         * Let's start the coworker loop and sort the Slots
         */

        foreach ($results as $coworker_id => $result) {

            /**
             * Let's start the events loop
             */
            foreach ($result as $id => $events) {

                /** @var $events Google_Service_Calendar_Events */

                // Looking for API errors
                if ($events instanceof Google_Service_Exception) {
                    // Error found, let's log it
                    $error_log = new TeamBooking_ErrorLog();
                    $error_log->setCoworkerId($coworker_id);
                    $error_log->setErrorCode($events->getCode());
                    $error_log->setMessage($events->getMessage());
                    tbErrorLog($error_log);
                    if (!static::$settings->getSilentDebug()) {
                        echo $events->getMessage();
                    }
                    // In case of API error, we should skip this results record and drop it
                    unset($result[$id]);
                    continue;
                }

                // Google Apps for Work dirty hack
                if (in_array($id, $this->already_fetched)) {
                    // This results record was already fetched, skipping it
                    continue;
                } else {
                    $this->already_fetched[] = $id;
                }

                /**
                 * Retrieving important informations about this results record,
                 * and getting all the slots from it.
                 *
                 * The results record is identified by an ID that is equal to
                 * the marker we've set for the relative request, plus the
                 * "response-" string in front. The marker, was the serialized helper
                 * array, so in this stage we can easily know which service
                 * and which Google Calendar ID are related to this results
                 * record.
                 */
                $serialized_helper_array = substr($id, 9); // Get rid of "response-"

                /**
                 * Once the "response-" part is cutted down, we must check if
                 * we are in front of a booked slot results record.
                 * Relative requests were identified by the "_booked" marker,
                 * so let's look for it.
                 */

                if (substr($serialized_helper_array, -7) == '_booked') {
                    /**
                     * This is a booked appointments slots result record
                     * Let's strip the "_booked" marker.
                     */
                    $serialized_helper_array = substr($serialized_helper_array, 0, -7);

                    /**
                     * In order to know the service and the Google Calendar ID
                     * of this results record, we should unserialize the helper array.
                     */
                    $helper_array = unserialize(base64_decode($serialized_helper_array));

                    /**
                     * Let's call the mapper for "booked" results
                     */
                    $this->mapBookedResult($helper_array, $events);

                } else {

                    $helper_array = unserialize(base64_decode($serialized_helper_array));

                    /**
                     * Let's call the mapper for "free" results
                     */
                    $this->mapFreeResult($helper_array, $events);
                }

            }
        }
    }

    //------------------------------------------------------------

    private function mapFreeResult(array $helper_array, Google_Service_Calendar_Events $events)
    {
        // Getting the service id
        $service_id = $helper_array['service_id'];
        // Getting the coworker data
        $coworker_data = static::$settings->getCoworkerData($helper_array['coworker_id']);

        /**
         * Sorting Loop
         */
        foreach ($events->getItems() as $event) {

            /** @var $event Google_Service_Calendar_Event */

            $slot = new TeamBooking_Slot();

            // Check for All Day event and set start/end time
            if ($event->getStart()->getDateTime()) {
                $slot->setAllDayFalse();
                $start = $event->getStart()->getDateTime();
                $end = $event->getEnd()->getDateTime();
            } else {
                $slot->setAllDayTrue();
                $start = $event->getStart()->getDate();
                $end = $event->getEnd()->getDate();
            }

            /**
             * Is the slot a container one? If yes, we should
             * mark it (so we can split it into single slots later).
             *
             * PLEASE NOTE: we must allow the container ONLY IF
             * the duration rule is NOT "inherited", otherwise the
             * container slot must be skipped.
             */
            $container_title_check = $this->containerTitleCheck($event->getSummary(), $coworker_data->getCustomEventSettings($service_id)->getLinkedEventTitle());

            if ($container_title_check) {
                /**
                 * Let's check if the duration rule is set to "inherited",
                 * both in admin level or in coworker level.
                 */
                if (static::$settings->getService($service_id)->getDurationRule() == 'inherited'
                    || (
                        static::$settings->getService($service_id)->getDurationRule() == 'coworker'
                        && $coworker_data->getCustomEventSettings($service_id)->getDurationRule() == 'inherited'
                    )
                ) {
                    // It is "inherited", skip the container
                    continue;
                } else {
                    // It is not "inherited", mark the slot as valid container
                    $slot->setContainerTrue();

                    /**
                     * Is it a multiple service container?
                     * If yes, let's put the service names array inside the slot
                     */
                    if (is_array($container_title_check)
                        && count($container_title_check) > 1
                    ) {
                        $all_services_list = static::$settings->getServiceIdList();
                        $multiple_services_array = array();
                        foreach ($all_services_list as $the_id) {
                            if (static::$settings->getService($the_id)->isActive()
                                && $coworker_data->getCustomEventSettings($the_id)->isParticipate()
                            ) {
                                foreach ($container_title_check as $names_value) {
                                    if (strtolower(trim($coworker_data->getCustomEventSettings($the_id)->getLinkedEventTitle())) == strtolower($names_value)) {
                                        $multiple_services_array[] = $the_id;
                                    }
                                }
                            }
                        }
                        $slot->setMultipleServices($multiple_services_array);
                    }
                }
            } else {
                // It's not a container
                $slot->setContainerFalse();
            }

            /**
             * If the slot IS NOT a container, we should check that
             * the reference time (start or end) is not less than the
             * user min_time, or we should skip the slot(*).
             *
             * If the slot IS a container AND the start time is less than
             * the user min_time, we should simply let it pass, and do the previous
             * check on the derived slots, later.
             *
             * (*)This is necessary, due timeMin behaviour in Google Calendar API
             * (it's always relative to the event's end time).
             */
            $min_time = $this->getCoworkerMinTime($helper_array['coworker_id'], $service_id, $this->min_time_requested);

            if (!$slot->isContainer()) {
                /**
                 * Is the event relative to the service requested?
                 *
                 * We do this check, due to loose behaviour of the
                 * Google full text queries, and we do it if NOT a container
                 */
                if (strtolower(trim($event->getSummary())) != strtolower($helper_array['query_free'])) {
                    // Event title do not match perfectly, and it's not a container, let's skip it
                    continue;
                }
                if ($min_time->reference == 'end') {
                    // The reference for the min_time is the slot's end time
                    if (strtotime($end) < strtotime($min_time->min_time)) {
                        // Skip this slot
                        continue;
                    }
                } else {
                    // The reference for the min_time is the slot's start time
                    if (strtotime($start) < strtotime($min_time->min_time)) {
                        // Skip this slot
                        continue;
                    }
                }
            }

            /**
             * We're setting the slot's location
             */
            $location_setting = static::$settings->getService($service_id)->getLocationSetting();
            if ($location_setting == 'inherited') {
                $slot->setLocation($event->getLocation());
            } elseif ($location_setting == 'fixed') {
                $slot->setLocation(static::$settings->getService($service_id)->getLocationAddress());
            } else {
                $slot->setLocation(''); //empty
            }

            // Other settings
            $slot->setCoworkerId($helper_array['coworker_id']);
            $slot->setEndTime($end);
            $slot->setStartTime($start);
            $slot->setEventId($event->getID());
            $slot->setServiceId($service_id);
            $slot->setCalendarId($helper_array['calendar_id']);
            $slot->setServiceName(static::$settings->getService($service_id)->getName());
            $slot->setServiceInfo(static::$settings->getService($service_id)->getServiceInfo());

            // add the slot to the "free" list, or to the "container" list
            if ($slot->isContainer()) {
                $this->slots_containers[$event->getId() . '_' . $service_id] = $slot;
            } else {
                $this->slots_free[$event->getId()] = $slot;
            }

        }
    }

    //------------------------------------------------------------

    private function mapBookedResult(array $helper_array, Google_Service_Calendar_Events $events)
    {
        // Getting the service id
        $service_id = $helper_array['service_id'];

        /**
         * Sorting Loop
         */
        foreach ($events->getItems() as $event) {

            /** @var $event Google_Service_Calendar_Event */

            /**
             * Is the event relative to the service requested?
             *
             * We do this check, due to loose behaviour of the
             * Google full text queries
             */
            if (strtolower(trim($event->getSummary())) != strtolower($helper_array['query_booked'])) {
                continue;
            }

            /**
             * Is the event cancelled?
             *
             * We do this check to avoid strange behaviours with
             * cancelled recurring instances that are somehow kept
             * in the response array...
             */
            if ($event->getStatus() == 'cancelled') {
                continue;
            }

            // Preparing the Slot Object
            $slot = new TeamBooking_Slot();

            // Check for All Day event and set start/end time
            if ($event->getStart()->getDateTime()) {
                $slot->setAllDayFalse();
                $start = $event->getStart()->getDateTime();
                $end = $event->getEnd()->getDateTime();
            } else {
                $slot->setAllDayTrue();
                $start = $event->getStart()->getDate();
                $end = $event->getEnd()->getDate();
            }

            /**
             * We're setting the slot's location
             */
            $location_setting = static::$settings->getService($service_id)->getLocationSetting();
            if ($location_setting == 'inherited') {
                $slot->setLocation($event->getLocation());
            } elseif ($location_setting == 'fixed') {
                $slot->setLocation(static::$settings->getService($service_id)->getLocationAddress());
            } else {
                $slot->setLocation(''); //empty
            }

            // Mark the slot as "soldout"
            $slot->setSoldoutTrue();
            // Other settings
            $slot->setCoworkerId($helper_array['coworker_id']);
            $slot->setEndTime($end);
            $slot->setStartTime($start);
            $slot->setEventId($event->getID());
            $slot->setServiceId($service_id);
            $slot->setCalendarId($helper_array['calendar_id']);
            $slot->setServiceName(static::$settings->getService($service_id)->getName());
            $slot->setServiceInfo(static::$settings->getService($service_id)->getServiceInfo());

            // add the slot to the "booked" list
            $this->slots_booked[$event->getId()] = $slot;
        }

    }

    //------------------------------------------------------------

    private function fetchBookedFromDatabase()
    {
        foreach ($this->reservations_in_database as $id => $reservation) {

            /* @var $reservation TeamBooking_ReservationData */
            if ($reservation->isWaitingApproval()
                && static::$settings->getService($reservation->getServiceId())->isActive()
                && !static::$settings->getService($reservation->getServiceId())->getFreeUntilApproval()
                && (
                    (
                        $reservation->getGoogleCalendarEventParent()
                        && $reservation->getGoogleCalendarEvent() == NULL
                    )
                    || (!$reservation->getGoogleCalendarEventParent()
                        && $reservation->getGoogleCalendarEvent()
                    )
                )
            ) {
                $slot = tbReservationDataToSlot($reservation);
                $key_string = $slot->getServiceId() . $slot->getEventIdParent() . $slot->getEventId() . $slot->getStartTime() . $slot->getEndTime();
                if (static::$settings->getService($slot->getServiceId())->isClass('event')) {
                    if ($reservation->getGoogleCalendarEventParent()) {
                        $this->slots_free[$key_string] = $slot;
                    }
                } else {
                    $this->slots_booked[$key_string] = $slot;
                    // discard the free one in case of slot mode, very important
                    unset($this->slots_free[$slot->getEventId()]);
                }
            }

        }
    }

    //------------------------------------------------------------

    private function fetchPendingFromDatabase()
    {
        foreach ($this->reservations_in_database_pending as $id => $reservation) {

            /* @var $reservation TeamBooking_ReservationData */

            if (!tbIsReservationTimedOut($reservation, static::$settings)
                && static::$settings->getService($reservation->getServiceId())->isActive()
                && (
                    (
                        $reservation->getGoogleCalendarEventParent()
                        && $reservation->getGoogleCalendarEvent() == NULL
                    )
                    || (
                        !$reservation->getGoogleCalendarEventParent()
                        && $reservation->getGoogleCalendarEvent()
                    )
                )
            ) {
                $slot = tbReservationDataToSlot($reservation);
                $key_string = $slot->getServiceId() . $slot->getEventIdParent() . $slot->getEventId() . $slot->getStartTime() . $slot->getEndTime();
                if (static::$settings->getService($slot->getServiceId())->isClass('event')) {
                    if ($reservation->getGoogleCalendarEventParent()) {
                        $this->slots_free[$key_string] = $slot;
                    }
                } else {
                    $this->slots_booked[$key_string] = $slot;
                    // discard the free one in case of slot mode, very important
                    unset($this->slots_free[$slot->getEventId()]);
                }
            }
        }
    }

    //------------------------------------------------------------

    private function splitContainers()
    {
        /**
         * Let'split the containers into slots.
         *
         * During the reservation, the absence of an event ID
         * will be used to tell TeamBooking to create a new event
         * in Google Calendar instead of updating
         * (until a first reservation took place, all the slots in
         * a container are just mere abstractions).
         */

        $slots_to_be_added = array();

        foreach ($this->slots_containers as $slot) {

            /** @var $slot TeamBooking_Slot */

            // Get the service id
            $service_id = $slot->getServiceId();
            // Get the coworker data
            $coworker_data = static::$settings->getCoworkerData($slot->getCoworkerId());
            // Collect the buffer duration value
            $buffer = $coworker_data->getCustomEventSettings($service_id)->getBufferDuration(); // seconds

            /**
             * We're looking for slots possibly "contained" in this
             * container. This is done using "booked" array
             * for the "Appointment" class, and "free"
             * for the "Event" class.
             *
             * Let's set the time limits for this container.
             */
            $start_time_obj = new DateTime($slot->getStartTime());
            $start_time_obj->setTimezone($this->timezone);
            $end_time_obj = new DateTime($slot->getEndTime());
            $end_time_obj->setTimezone($this->timezone);

            // Declare the contained slots array
            $contained_booked_slots = array();

            /* Getting the multiple service id array */
            $multiple_services = $slot->getMultipleServices();

            if (static::$settings->getService($service_id)->getClass() == 'appointment'
                || !empty($multiple_services)
            ) {
                /**
                 * Let's browse the "booked" array.
                 */
                foreach ($this->slots_booked as $booked_slot) {
                    /**
                     * Check for the service id(s)
                     *
                     * If the multiple services array is not empty
                     * it means that the container is a multiple service one.
                     * So we need to collect all the contained booked slots
                     * of all the services.
                     *
                     * If the array is empty, the container is a single service one.
                     * So it's sufficient to check against the service id returned by
                     * getServiceId() method.
                     */
                    if (!empty($multiple_services)) {
                        if (!in_array($booked_slot->getServiceId(), $multiple_services)) {
                            // The booked slot service id is not contained in the array, skip
                            continue;
                        }
                    } else {
                        if ($service_id != $booked_slot->getServiceId()) {
                            // The booked slot service id doesn't match, skip
                            continue;
                        }
                    }
                    // Let's set the current booked slot's time limits
                    $start_time_obj_booked = new DateTime($booked_slot->getStartTime());
                    $start_time_obj_booked->setTimezone($this->timezone);
                    $end_time_obj_booked = new DateTime($booked_slot->getEndTime());
                    $end_time_obj_booked->setTimezone($this->timezone);

                    /**
                     * How can we determine if the booked slot is contained
                     * in this container? We're gonna do this by checking two
                     * conditions:
                     *
                     * 1. The start time of the booked slot must be greater than
                     * or equal to the container's start time.
                     *
                     * 2. The end time of the booked slot must be less than
                     * or equal to the container's end time.
                     *
                     * Why aren't we considering the possibility of partially-contained
                     * slots (which meet only one of the conditions)? At this stage,
                     * we're assuming that a contained slot is there because it was
                     * originated by this very container.
                     *
                     * TODO: what if the container was modified, in the meantime?
                     * TODO: What if the reservations are manually adjusted?
                     */
                    if ($start_time_obj_booked->format('U') >= $start_time_obj->format('U')
                        && $end_time_obj_booked->format('U') <= $end_time_obj->format('U')
                    ) {
                        // Conditions are both verified
                        $contained_booked_slots[] = $booked_slot;
                    }
                }
            }

            if (static::$settings->getService($service_id)->getClass() == 'event'
                || !empty($multiple_services)
            ) {
                /**
                 * Let's browse the $return_free array
                 */
                foreach ((array)$this->slots_free as $potential_booked_slot) {

                    // Check for the service id
                    if (empty($multiple_services)) {
                        if ($service_id != $potential_booked_slot->getServiceId()) {
                            // The service id is not the same, skipping
                            continue;
                        }
                    } else {
                        if (!in_array($potential_booked_slot->getServiceId(), $multiple_services)) {
                            continue;
                        }
                    }

                    // Let's set the current booked slot's time limits
                    $start_time_obj_booked = new DateTime($potential_booked_slot->getStartTime());
                    $start_time_obj_booked->setTimezone($this->timezone);
                    $end_time_obj_booked = new DateTime($potential_booked_slot->getEndTime());
                    $end_time_obj_booked->setTimezone($this->timezone);

                    // the potential booked slot is contained in this container?
                    if ($start_time_obj_booked->format('U') >= $start_time_obj->format('U')
                        && $end_time_obj_booked->format('U') <= $end_time_obj->format('U')
                    ) {
                        // Conditions are both verified
                        $contained_booked_slots[] = $potential_booked_slot;
                    }
                }
            }
            /**
             * Now, the $contained_booked_slots array is filled with
             * unordered contained slots relative to this container
             * and this service id. Let's order the slots by time.
             */
            $contained_booked_slots = $this->sortSlotsByTime($contained_booked_slots);

            /**
             * We need to know the duration of the slots inside the
             * containers. Remember, if the duration rule is set to "inherit",
             * the container would never exists in the first place,
             * so the $duration variable should never stay FALSE, actually.
             */
            $duration = FALSE;
            if (static::$settings->getService($service_id)->getDurationRule() == 'fixed') {
                $duration = static::$settings->getService($service_id)->getDefaultDuration(); // seconds
            } elseif (static::$settings->getService($service_id)->getDurationRule() == 'coworker') {
                if ($coworker_data->getCustomEventSettings($service_id)->getDurationRule() == 'fixed') {
                    $duration = $coworker_data->getCustomEventSettings($service_id)->getFixedDuration(); // seconds
                }
            }

            if (!$duration) {
                /**
                 * This is not a possibility. Let's skip the container
                 * for security pourposes. Below, there is a split loop
                 * commented, for further development in case of dynamic
                 * duration, or customer-defined duration.
                 */
                continue;
                //  $start_time_spool = $slot->getStartTime();
                //  foreach ($contained_booked_slots as $booked_slot) {
                //      if (strtotime($booked_slot->getStartTime()) > strtotime($start_time_spool)) {
                //          $slot_to_add = clone $slot;
                //          $slot_to_add->setEventIdParent($slot->getEventId());
                //          $slot_to_add->setEventId(FALSE);
                //          $slot_to_add->setContainerFalse();
                //          $slot_to_add->setStartTime($start_time_spool);
                //          $slot_to_add->setEndTime($booked_slot->getStartTime());
                //          $slots_to_be_added[] = $slot_to_add;
                //          // Set a new time spool, taking buffer into account
                //          $start_time_spool = date_i18n('c', strtotime($booked_slot->getEndTime()) + $buffer);
                //      } else {
                //          if (strtotime($booked_slot->getEndTime()) > strtotime($start_time_spool)) {
                //              $start_time_spool = $booked_slot->getEndTime();
                //          }
                //      }
                //  }
                //  // residual
                //  if (strtotime($start_time_spool) < strtotime($slot->getEndTime())) {
                //      $slot_to_add = clone $slot;
                //      $slot_to_add->setContainerFalse();
                //      $slot_to_add->setEventIdParent($slot->getEventId());
                //      $slot_to_add->setEventId(FALSE);
                //      $slot_to_add->setStartTime($start_time_spool);
                //      $slots_to_be_added[] = $slot_to_add;
                //  }
            } else {

                /**
                 * Let's start the split loop, based on
                 * duration and buffer values.
                 */
                $start_time_spool = $slot->getStartTime();
                $min_time = $this->getCoworkerMinTime($slot->getCoworkerId(), $slot->getServiceId(), $this->min_time_requested);

                foreach ($contained_booked_slots as $booked_slot) {

                    if (strtotime($booked_slot->getStartTime()) >= strtotime($start_time_spool)) {
                        while ((strtotime($booked_slot->getStartTime()) - strtotime($start_time_spool)) >= ($buffer + $duration)) {
                            $slot_to_add = clone $slot;
                            $slot_to_add->setEventIdParent($slot->getEventId());
                            $slot_to_add->setEventId(FALSE);
                            $slot_to_add->setContainerFalse();
                            $slot_to_add->setStartTime($start_time_spool);
                            $slot_to_add->setEndTime(date_i18n('c', strtotime($start_time_spool) + $duration));
                            $must_skip = FALSE;
                            if ($min_time->reference == 'end') {
                                // The reference for the min_time is the slot's end time
                                if (strtotime($slot_to_add->getEndTime()) < strtotime($min_time->min_time)) {
                                    // Skip this slot
                                    $must_skip = TRUE;
                                }
                            } else {
                                // The reference for the min_time is the slot's start time
                                if (strtotime($slot_to_add->getStartTime()) < strtotime($min_time->min_time)) {
                                    // Skip this slot
                                    $must_skip = TRUE;
                                }
                            }
                            if (!$must_skip) {
                                $slots_to_be_added[] = $slot_to_add;
                            }
                            // Set a new time spool, taking buffer into account
                            $start_time_spool = date_i18n('c', strtotime($start_time_spool) + $duration + $buffer);
                        }
                        // Set a new time spool, taking buffer into account
                        $start_time_spool = date_i18n('c', strtotime($booked_slot->getEndTime()) + $buffer);
                    } else {
                        if (strtotime($booked_slot->getEndTime()) > strtotime($start_time_spool)) {
                            $start_time_spool = $booked_slot->getEndTime();
                        }
                    }
                }

                // residual
                if (strtotime($start_time_spool) < strtotime($slot->getEndTime())) {
                    while ((strtotime($slot->getEndTime()) - strtotime($start_time_spool)) >= $duration) {
                        $slot_to_add = clone $slot;
                        $slot_to_add->setEventIdParent($slot->getEventId());
                        $slot_to_add->setEventId(FALSE);
                        $slot_to_add->setContainerFalse();
                        $slot_to_add->setStartTime($start_time_spool);
                        $slot_to_add->setEndTime(date_i18n('c', strtotime($start_time_spool) + $duration));
                        $must_skip = FALSE;
                        if ($min_time->reference == 'end') {
                            // The reference for the min_time is the slot's end time
                            if (strtotime($slot_to_add->getEndTime()) < strtotime($min_time->min_time)) {
                                // Skip this slot
                                $must_skip = TRUE;
                            }
                        } else {
                            // The reference for the min_time is the slot's start time
                            if (strtotime($slot_to_add->getStartTime()) < strtotime($min_time->min_time)) {
                                // Skip this slot
                                $must_skip = TRUE;
                            }
                        }
                        if (!$must_skip) {
                            $slots_to_be_added[] = $slot_to_add;
                        }
                        // Set a new time spool, taking buffer into account
                        $start_time_spool = date_i18n('c', strtotime($start_time_spool) + $duration + $buffer);
                    }
                }
            }
        }

        $this->slots_free = array_merge((array)$this->slots_free, $slots_to_be_added);
    }

    //------------------------------------------------------------

    /**
     * Checks if a slot is a container one, for the given service title
     *
     * @param string $event_summary
     * @param string $linked_event_title
     * @return mixed
     */
    private function containerTitleCheck($event_summary, $linked_event_title)
    {
        if (substr(strtolower(trim($event_summary)), -10) == ' container') {
            $return = FALSE;
            $services_array = array_map('trim', explode('+', substr(trim($event_summary), 0, -10)));
            foreach ($services_array as $service_name) {
                if (strtolower($service_name) == strtolower(trim($linked_event_title))) {
                    $return = $services_array;
                }
            }

            return $return;
        } else {

            return FALSE;
        }
    }

    //------------------------------------------------------------

    /**
     * Sort slots by time.
     *
     * @param array $slots
     * @return array
     */
    private function sortSlotsByTime(array $slots)
    {
        uasort($slots, function ($a, $b) {
            return strtotime($a->getStartTime()) > strtotime($b->getStartTime());
        });

        return $slots;
    }

    //------------------------------------------------------------

    /**
     * Retrieves a minimum getting time for the Google Calendar
     * events, tweaked with the specific coworker's min reservation
     * time limit for the specified service.
     *
     * @param int $coworker_id
     * @param string $service_id
     * @param string $min_get_time
     * @return stdClass
     */
    protected function getCoworkerMinTime($coworker_id, $service_id, $min_get_time)
    {
        $now = new DateTime();
        /**
         * Let's get the min time string for the given
         * coworker id and service id combination.
         *
         * The string is in the DateInterval format, with the
         * exception of *mid appended strings. These strings
         * means that the interval must be cut at midnight.
         *
         */
        $min_time_string = static::$settings->getCoworkerData($coworker_id)->getCustomEventSettings($service_id)->getMinTime();

        /**
         * Let's get the reference for the min time interval
         * specified by the given coworker for the given
         * service.
         *
         * It could be the START of the slot, or the END.
         */
        $reference = static::$settings->getCoworkerData($coworker_id)->getCustomEventSettings($service_id)->getMinTimeReference();

        /**
         * If the min time interval is a *mid appended one,
         * we should add the prepend interval to the
         * $now object AND set his time to midnight.
         */
        if (substr($min_time_string, -3) == 'mid') {
            // Get rid of 'mid' appendix
            $min_time_string = substr($min_time_string, 0, -3);
            // Add the interval
            $now->add(new DateInterval($min_time_string));
            // Set the time to midnight
            $now->setTime(0, 0);
        } else {
            // Just add the interval
            $now->add(new DateInterval($min_time_string));
        }

        /**
         * Let's return a formatted min time
         */
        if (strtotime($min_get_time) < strtotime($now->format('c'))) {
            /**
             * The min requesting time is less than the tweaked
             * min retrieving time, so we must return the tweaked
             * retrieving time (formatted).
             */
            $min_time = $now->format('c');
        } else {
            /**
             * The min requesting time is greater than or
             * equal to the tweaked min retrieving time,
             * so we can just return the first.
             */
            $min_time = $min_get_time;
        }

        /**
         * Preparing the object to return.
         *
         * We're using a stdClass for convenience,
         * so we can pass the reference along with it.
         */
        $return = new stdClass();
        $return->min_time = $min_time;
        $return->reference = $reference;

        return $return;
    }

}