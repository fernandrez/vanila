<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Install
{

    //------------------------------------------------------------

    /**
     * The installer method.
     *
     * @return boolean TRUE if all is done
     */
    public function install()
    {
        /*
         * extra safe-guard, the current user must have
         * activate_plugins capability
         */
        if (!current_user_can('activate_plugins')) {
            return TRUE;
        }
        /*
         * checking requirements
         */
        $php_min_version = '5.3.3';
        $php_current_version = phpversion();
        if (version_compare($php_min_version, $php_current_version, '>')) {
            trigger_error(sprintf(__('Team Booking requires PHP > 5.3.3, your server has PHP %s, please upgrade it before activate the plugin'), $php_current_version), E_USER_ERROR);
        }
        /*
         * generate options object, if not present
         */
        if (!$this->check_options()) {
            // the Team Booking option record exists but it's not the right object
            return FALSE;
        }
        /*
         * get the "administrator" role object
         */
        $role = get_role('administrator');
        /*
         * add "tb_can_sync_calendar" to this role object
         */
        $role->add_cap('tb_can_sync_calendar');
        /*
         *  Create the reservations tables
         */
        $this->createReservationsTable();
        $this->createPendingReservationsTable();

        return TRUE;
    }

    //------------------------------------------------------------

    /**
     * Deactivation method
     *
     * @return type
     */
    public function deactivate()
    {
        /*
         * extra safe-guard, the current user must have
         * activate_plugins capability
         */
        if (!current_user_can('activate_plugins')) {
            return;
        }
    }

    //------------------------------------------------------------

    /**
     * Uninstall method
     *
     * @global type $wp_roles
     * @global type $wpdb
     * @return boolean
     */
    public function uninstall()
    {
        /*
         * extra safe-guard, the current user must have
         * activate_plugins capability
         */
        if (!current_user_can('activate_plugins')) {
            return;
        }
        /*
         * remove custom capabilities
         */
        $delete_caps = array('tb_can_sync_calendar');
        global $wp_roles;
        foreach ($delete_caps as $cap) {
            foreach (array_keys($wp_roles->roles) as $role) {
                $wp_roles->remove_cap($role, $cap);
            }
        }
        /*
         * revoke all tokens programmatically
         */
        if (tbGetSettings()) {
            /*
             * get the coworkers data array
             */
            $coworkers_data = tbGetSettings()->getCoworkersData();
            if (!is_null($coworkers_data)) {
                /*
                 * looping through coworkers data
                 */
                foreach ($coworkers_data as $coworker_data) {
                    /* @var $coworker_data TeamBookingCoworker */
                    if (isset(json_decode($coworker_data->getAccessToken())->refresh_token)) {
                        /*
                         * a refresh token appears to be set, let's revoke it
                         */
                        $token = json_decode($coworker_data->getAccessToken())->refresh_token;
                        $client = new Google_Client();
                        try {
                            $client->revokeToken($token);
                        } catch (Exception $e) {
                            echo $e->getMessage();
                        }
                    }
                }
            }
        }

        /*
         * remove database tables, if selected
         */
        if (tbGetSettings()) {
            if (tbGetSettings()->getDropTablesOnUninstall()) {
                global $wpdb;
                $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}teambooking_reservations");
                $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}teambooking_reservations_pending");
            }
        }

        /*
         * remove plugin settings
         */
        delete_option('team_booking');

        return TRUE;
    }

    //------------------------------------------------------------

    /**
     * Creates the settings object
     *
     * @return boolean
     */
    private function check_options()
    {
        /*
         * check if settings record is already present
         */
        if (!get_option('team_booking')) {
            /*
             * not present, so add it
             */
            $option = new TeamBookingSettings();
            add_option('team_booking', $option);

            return TRUE;
        } elseif (!get_option('team_booking') instanceof TeamBookingSettings) {
            /*
             * the settings object is not the right one!
             */
            return FALSE;
        } else {
            /*
             * the right settings object is present
             */
            return TRUE;
        }
    }

    //------------------------------------------------------------

    /**
     * Creates the reservation database table
     *
     * @global type $wpdb
     * @return string
     */
    public function createReservationsTable()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "teambooking_reservations";
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            $charset_collate = $wpdb->get_charset_collate();
            $sql = "CREATE TABLE $table_name (
                        id int NOT NULL AUTO_INCREMENT,
                        created datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                        updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                        service_id tinytext NOT NULL,
                        coworker_id int NOT NULL,
                        data_object text not null,
                        UNIQUE KEY id (id)
                      ) $charset_collate;";
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }

        return $table_name;
    }

    //------------------------------------------------------------

    /**
     * Creates the pending reservation database table
     *
     * @global type $wpdb
     * @return string
     */
    public function createPendingReservationsTable()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "teambooking_reservations_pending";
        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            $charset_collate = $wpdb->get_charset_collate();
            $sql = "CREATE TABLE $table_name (
                        id int NOT NULL AUTO_INCREMENT,
                        created datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                        updated timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                        service_id tinytext NOT NULL,
                        coworker_id int NOT NULL,
                        data_object text not null,
                        UNIQUE KEY id (id)
                      ) $charset_collate;";
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }

        return $table_name;
    }

}
