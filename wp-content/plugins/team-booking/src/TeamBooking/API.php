<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

interface TeamBooking_API
{

    /**
     * This method sets the reservation data.
     * A private $data parameter must be defined
     * in the interface implementation.
     */
    public function setData(TeamBooking_ReservationData $data);

    /**
     * What to do after the reservation took place
     */
    public function after_reservation();

    /**
     * What to do before the reservation took place.
     *
     * This will be called exactly just before the Google event
     * updating, and then before emails are sent.
     */
    public function before_reservation();
}
