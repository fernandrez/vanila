<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

/**
 * This class defines the specific coworker settings
 * for a specific service. Mostly, those settings are
 * about the relationship between the service and the
 * coworker's Google Calendar(s)
 */
class TeamBookingCustomBTSettings
{

    //------------------------------------------------------------

    ////////////////////////////
    // Service-GCal relations //
    ////////////////////////////
    private $linked_event_title;
    private $after_booked_title;
    private $min_time;
    private $min_time_reference;
    private $booked_color;
    private $fixed_duration;
    private $duration_rule;
    private $buffer_duration;
    private $reminder;
    ////////////////////
    // Email settings //
    ////////////////////
    private $email;
    private $get_details_by_email;
    private $include_uploaded_files_as_attachment;
    //////////////////////
    // Service settings //
    //////////////////////
    private $participate;

    public function __construct()
    {
        $this->email = array(
            'email_text' => array(
                'subject' => "",
                'body'    => "",
            ),
        );
    }

    //------------------------------------------------------------

    public function getFixedDuration()
    {
        if (!$this->fixed_duration) {
            return 3600; // default, seconds
        } else {
            return $this->fixed_duration;
        }
    }

    public function setFixedDuration($seconds)
    {
        $this->fixed_duration = $seconds;
    }

    //------------------------------------------------------------

    public function setMinTimeReferenceStart()
    {
        $this->min_time_reference = 'start';
    }

    public function setMinTimeReferenceEnd()
    {
        $this->min_time_reference = 'end';
    }

    public function getMinTimeReference()
    {
        if (!$this->min_time_reference) {
            return 'start';
        } else {
            return $this->min_time_reference;
        }
    }

    //------------------------------------------------------------

    public function getBufferDuration()
    {
        if (!$this->buffer_duration) {
            return 0;
        } else {
            return $this->buffer_duration;
        }
    }

    public function setBufferDuration($seconds)
    {
        $this->buffer_duration = $seconds;
    }

    //------------------------------------------------------------

    public function getDurationRule()
    {
        if (!$this->duration_rule) {
            return 'inherited';
        } else {
            return $this->duration_rule;
        }
    }

    public function setDurationRule($rule)
    {
        $this->duration_rule = $rule;
    }

    //------------------------------------------------------------

    public function getLinkedEventTitle()
    {
        return $this->linked_event_title;
    }

    public function setLinkedEventTitle($title)
    {
        $this->linked_event_title = $title;
    }

    //------------------------------------------------------------

    public function getAfterBookedTitle()
    {
        return $this->after_booked_title;
    }

    public function setAfterBookedTitle($title)
    {
        $this->after_booked_title = $title;
    }

    //------------------------------------------------------------

    public function getMinTime()
    {
        return $this->min_time;
    }

    public function setMinTime($interval)
    {
        $this->min_time = $interval;
    }

    //------------------------------------------------------------

    public function setGetDetailsByEmailOn()
    {
        $this->get_details_by_email = TRUE;
    }

    public function setGetDetailsByEmailOff()
    {
        $this->get_details_by_email = FALSE;
    }

    public function getGetDetailsByEmail()
    {
        return $this->get_details_by_email;
    }

    //------------------------------------------------------------

    public function setParticipateOn()
    {
        $this->participate = TRUE;
    }

    public function setParticipateOff()
    {
        $this->participate = FALSE;
    }

    public function isParticipate()
    {
        if (!isset($this->participate)) {
            return TRUE;
        } else {
            return $this->participate;
        }
    }

    //------------------------------------------------------------

    public function setIncludeFilesAsAttachmentOn()
    {
        $this->include_uploaded_files_as_attachment = TRUE;
    }

    public function setIncludeFilesAsAttachmentOff()
    {
        $this->include_uploaded_files_as_attachment = FALSE;
    }

    public function getIncludeFilesAsAttachment()
    {
        if (!$this->include_uploaded_files_as_attachment) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    //------------------------------------------------------------

    public function setBookedEventColor($color)
    {
        $color = (int)$color;
        $this->booked_color = $color;
    }

    public function getBookedEventColor()
    {
        return $this->booked_color;
    }

    //------------------------------------------------------------

    public function getReminder()
    {
        return $this->reminder;
    }

    public function setReminder($seconds)
    {
        $seconds = (int)$seconds;
        $this->reminder = $seconds;
    }

    //------------------------------------------------------------

    public function setNotificationEmailSubject($text)
    {
        $this->email['email_text']['subject'] = tbFilterInput($text);
    }

    public function getNotificationEmailSubject()
    {
        return tbUnfilterInput($this->email['email_text']['subject']);
    }

    //------------------------------------------------------------

    public function setNotificationEmailBody($text)
    {
        $this->email['email_text']['body'] = tbFilterInput($text);
    }

    public function getNotificationEmailBody()
    {
        return tbUnfilterInput($this->email['email_text']['body']);
    }

}
