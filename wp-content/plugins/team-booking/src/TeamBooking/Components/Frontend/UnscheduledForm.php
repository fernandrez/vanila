<?php

// Blocks direct access to this file

defined('ABSPATH') or die("No script kiddies please!");



class TeamBooking_Components_Frontend_UnscheduledForm

{



    //------------------------------------------------------------



    private $settings;



    /** @var TeamBookingType */

    private $service;

    private $service_id;

    private $service_location;

    private $params;



    public function __construct($settings, TeamBooking_RenderParameters $params)

    {

        /* @var $settings TeamBookingSettings */

        $this->settings = $settings;

        $this->params = $params;

        $service_id = $params->getRequestedServiceIds();

        $this->service_id = reset($service_id);

        /* @var $service TeamBookingType */

        $this->service = $settings->getService($this->service_id);

        $this->service_location = $this->service->getLocationAddress();

    }



    //------------------------------------------------------------



    public function getContent()

    {

        // Strings

        $string_reservation_for = __('Reservation for', 'teambooking');

        $string_total_tickets = __('Total tickets:', 'teambooking');

        $string_total_price = __('Reservation Deposit:', 'teambooking');

        $string_book_now = __('Book now', 'teambooking');

        $string_book_and_pay = __('Book and pay', 'teambooking');

        $form_id = "tb-reservation-form-" . $this->service_id . "-" . $this->params->getInstance();

        $unscheduled_service_submit_id = "tb-unscheduled-service-submit-" . $this->service_id . "-" . $this->params->getInstance();

        if (is_user_logged_in()) {

            // TODO: use only wp methods

            $user = $this->settings->getCoworkerData(get_current_user_id());

        }

        if ($this->params->getIsWidget() === TRUE) {

            $widget_class = 'tb-widget';

            $input_size = 'mini';

            $header_class = 'tiny';

            $form_class = 'small';

        } else {

            $widget_class = '';

            $input_size = '';

            $header_class = '';

            $form_class = '';

        }

        // Store form fields on variable

        $form_fields_builtin = $this->service->getFormFields()->getBuiltInFields();

        $form_fields_custom = $this->service->getFormFields()->getCustomFields();

        // Merge the fields

        $form_fields = array_merge($form_fields_builtin, $form_fields_custom);

        // Keep only the active ones

        foreach ($form_fields as $key => $field) {

            /* @var $field TeamBookingFormTextField */

            if (!$field->getIsActive()) {

                unset($form_fields[$key]);

            }

        }



        $there_are_files = FALSE;

        $form_fields_markup = array();

        $form_fields_hidden_markup = array();



        foreach ($form_fields as $field) {

            // Check for file upload fields

            if (get_class($field) == 'TeamBookingFormFileUpload') {

                $there_are_files = TRUE;

            }

            $is_hidden = FALSE;

            /* @var $field TeamBookingFormTextField */

            switch ($field->getHook()) {

                case "first_name":

                    $default_value = (isset($user) && $this->settings->getAutofillReservationForm() ? $user->getFirstName() : '');

                    if ($this->settings->getAutofillReservationForm() === 'hide' && !empty($default_value)) {

                        $is_hidden = TRUE;

                    }

                    break;

                case "second_name":

                    $default_value = (isset($user) && $this->settings->getAutofillReservationForm() ? $user->getLastName() : '');

                    if ($this->settings->getAutofillReservationForm() === 'hide' && !empty($default_value)) {

                        $is_hidden = TRUE;

                    }

                    break;

                case "email":

                    $default_value = (isset($user) && $this->settings->getAutofillReservationForm() ? $user->getEmail() : '');

                    if ($this->settings->getAutofillReservationForm() === 'hide' && !empty($default_value)) {

                        $is_hidden = TRUE;

                    }

                    break;

                default:

                    $default_value = $field->getValue();

            }

            if (get_class($field) == 'TeamBookingFormCheckbox') {

                $field->setValue(__('Selected', 'teambooking'));

            } else {

                $field->setValue($default_value);

            }



            ob_start();

            if ($is_hidden) {

                $field->getHiddenMarkup();

                $result = ob_get_clean();

                $form_fields_hidden_markup[] = $result;

            } else {

                $field->getMarkup($input_size);

                // Address autocomplete by Google

                if ($field->getHook() == "address") {

                    ?>

                    <style>

                        .pac-container {

                            z-index: 99999999;

                            -webkit-filter: none !important;

                            filter: none !important;

                        }

                    </style>

                    <?php

                }

                $result = ob_get_clean();

                $form_fields_markup[] = $result;

            }

        }



        ob_start();

        if ($this->params->getIsAjaxCall() !== TRUE) {

            $calendar = new TeamBooking_Components_Frontend_Calendar($this->settings, new TeamBooking_RenderParameters());

            echo $calendar->getCalendarStyle();

        }

        ?>

        <div class="ui form grid <?= $widget_class ?> tb-frontend-calendar" data-params="<?= $this->params->encode() ?>"

             data-instance="<?= $this->params->getInstance() ?>">

            <style>

                .tb-service-form,

                .tb-service-form .tbk-divider,

                .tb-service-form .tbk-header,

                .tb-service-form .field > label {

                    color: <?= tbGetRightTextColor($this->settings->getColorBackground()) ?> !important;

                }

            </style>

            <div class="ui sixteen wide tbk-column tb-service-form">

                <div class="ui center aligned basic segment">

                    <h2 class="ui tbk-header <?= $header_class ?>">

                        <span style="font-weight: 300;font-style: italic;text-transform: none;">

                            <?= esc_html($string_reservation_for) ?>

                        </span>

                        <span>

                            <?= esc_html($this->service->getName()) ?>

                        </span>

                    </h2>

                </div>

                <?php if ($this->service->getServiceInfo()) { ?>

                    <div class="ui stacked segment">

                        <p><?= $this->service->getServiceInfo() ?></p>

                    </div>

                <?php } ?>

                <?php

                if ($there_are_files) {

                    $form_enctype = 'enctype="multipart/form-data"';

                } else {

                    $form_enctype = '';

                }

                ?>

                <?php

                if ($this->service->getLocationAddress() && $this->service->getLocationSetting() == 'fixed') {

                    $style = json_encode($this->settings->getMapStyle());

                    ?>

                    <div class="ui horizontal tbk-divider tb-map">

                        <i class="marker tb-icon"></i>

                    </div>

                    <p class="tb-map" style="text-align: center;font-weight: 300;font-style: italic;">

                        <?= esc_html(ucwords($this->service->getLocationAddress())) ?>

                    </p>

                    <div class="ui segment tb-map map-<?= $form_id ?>" style="height:200px;">

                    </div>

                    <script>

                        jQuery(document).ready(function () {

                            jQuery('.map-<?= $form_id ?>').gmap3({

                                marker: {

                                    address: '<?= $this->service->getLocationAddress() ?>'

                                },

                                map: {

                                    options: {

                                        zoom: 14,

                                        mapTypeId: 'style',

                                        scrollwheel: false,

                                        mapTypeControl: false

                                    }

                                },

                                styledmaptype: {

                                    id: "style",

                                    options: {

                                        name: "Map"

                                    },

                                    styles: <?= $style ?>

                                }

                            });

                        });

                    </script>

                <?php } ?>



                <div class="ui horizontal tbk-divider">

                    <i class="info letter tb-icon"></i>

                </div>

                <form id="<?= $form_id ?>"

                      class="ui <?= $this->params->getIsWidget() ? 'one' : 'two' ?> tbk-column grid stackable <?= $form_class ?>"

                      method="POST" action="" <?= $form_enctype ?>>

                    <?php wp_nonce_field('teambooking_submit_reservation', 'nonce') ?>

                    <input type="hidden" name="tickets" value="1">

                    <input type="hidden" name="service" value="<?= esc_attr($this->service_id) ?>">

                    <input type="hidden" name="post_id" value="">

                    <input type="hidden" name="service_location" value="<?= $this->service_location ?>">

                    <input type="hidden" name="customer_wp_id"

                           value="<?= isset($user) ? esc_attr($user->getId()) : '' ?>">

                    <?php

                    // Hidden fields, if any

                    foreach ($form_fields_hidden_markup as $field_markup) {

                        echo $field_markup;

                    }

                    ?>

                    <?php

                    foreach ($form_fields_markup as $field_markup) {

                        ?>

                        <div class="ui tbk-column"><?= $field_markup ?></div>

                        <?php

                    }

                    ?>

                    <!-- tickets and price segment -->

                    <?php if ($this->service->isClass('event')) { ?>

                        <div id="total-tickets-<?= $form_id ?>"

                             class="ui tbk-header <?= !$this->params->getIsWidget() ? 'floated left' : '' ?>" style="

                             font-weight: 300;

                             font-style: italic;

                             padding: 5px;

                             "><?= $string_total_tickets ?> 1

                        </div>

                    <?php } ?>

                    <?php if ($this->service->getPrice() > 0) { ?>

                        <div class="ui sixteen wide tbk-column" style="text-align:center;">

                            <div class="ui message">

                                <div id="total-price-<?= $form_id ?>" style="

                                     font-weight: 300;

                                     font-style: italic;

                                     ">

                                    <?= esc_html($string_total_price) ?>

                                    <?= tbCurrencyCodeToSymbol($this->settings->getCurrencyCode(), $this->service->getPrice()) ?>

                                </div>

                            </div>

                        </div>

                    <?php } ?>

                    <!-- confirm buttons -->

                    <div class="ui sixteen wide tbk-column">

                        <div class="ui <?= $this->params->getIsWidget() === TRUE ? '' : 'right floated tbk-buttons' ?>">

                            <button

                                class="ui positive <?= $this->params->getIsWidget() === TRUE ? 'tiny compact fluid' : '' ?> tbk-button"

                                type="submit"

                                id="<?= $unscheduled_service_submit_id ?>">

                                <?= count($this->settings->getPaymentGatewaysActive()) == 1

                                && $this->service->getPaymentMustBeDone() == 'immediately'

                                && $this->service->getPrice() > 0

                                    ? esc_html($string_book_and_pay)

                                    : esc_html($string_book_now) ?>

                            </button>

                        </div>

                    </div>

                </form>

            </div>

            <script>

                jQuery('#<?= $unscheduled_service_submit_id ?>').click(function (e) {

                    e.preventDefault();

                    if (jQuery(this).hasClass('loading') === false) {

                        tbUnscheduledServiceSubmit('<?= $this->service_id ?>', '<?= $this->params->getInstance() ?>'<?= $there_are_files ? ', true' : '' ?>);

                    }

                });

            </script>

        </div>



        <?php

        return ob_get_clean();

    }



    //------------------------------------------------------------



    public function getContentRegisterAdvice()

    {

        // Strings

        $string_must_be_logged = __('You must be logged-in to book this!', 'teambooking');

        $string_not_registered = __("I'm not registered...", 'teambooking');

        $string_login = __("Login", 'teambooking');

        if ($this->params->getIsWidget() === TRUE) {

            $widget_class = 'tb-widget';

            $widget_button_column_wide = 'sixteen';

        } else {

            $widget_class = '';

            $widget_button_column_wide = 'eight';

        }

        ob_start();

        ?>

        <div class="ui form grid stackable <?= $widget_class ?> tb-frontend-calendar"

             data-params="<?= $this->params->encode() ?>" data-instance="<?= $this->params->getInstance() ?>">

            <div class="ui sixteen wide tbk-column tb-service-form">

                <div class="ui centered <?= $widget_class ?> tbk-header">

                    <?= esc_html($string_must_be_logged) ?>

                </div>

            </div>

            <div class="tbk-row">

                <div class="ui <?= $widget_button_column_wide ?> wide tbk-column">

                    <a href="<?= esc_url($this->settings->getRegistrationUrl()) ?>"

                       class="ui fluid <?= $this->params->getIsWidget() === TRUE ? 'tiny compact' : '' ?> negative tbk-button">

                        <?= esc_html($string_not_registered) ?>

                    </a>

                </div>

                <?php if ($this->params->getIsWidget() === TRUE) { ?>

            </div>

            <div class="tbk-row">

                <?php } ?>

                <div class="ui <?= $widget_button_column_wide ?> wide tbk-column">

                    <a href="<?= wp_login_url(get_permalink()) ?>"

                       class="ui fluid <?= $this->params->getIsWidget() === TRUE ? 'tiny compact' : '' ?> positive tbk-button">

                        <?= $string_login ?>

                    </a>

                </div>

            </div>

        </div>



        <?php

        return ob_get_clean();

    }



}

