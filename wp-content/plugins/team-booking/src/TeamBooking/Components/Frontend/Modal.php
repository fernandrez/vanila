<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Components_Frontend_Modal
{

    //------------------------------------------------------------

    public $instance;
    public $start_end_times;
    public $coworker_string;

    /** @var TeamBookingType */
    private $service;
    private $coworker_id;
    private $calendar_id;
    private $event_id;
    private $service_id;
    private $service_location;
    private $tickets_left;
    private $event_id_parent;
    private $slot_start;
    private $slot_end;

    /** @var TeamBookingSettings */
    private $settings;

    public function __construct(TeamBooking_Slot $slot, TeamBookingSettings $settings)
    {
        try {
            if (!$settings->getService($slot->getServiceId())->isClass('service')) {
                $this->event_id = $slot->getEventId();
                $this->service_id = $slot->getServiceId();
                $this->coworker_id = $slot->getCoworkerId();
                $this->calendar_id = $slot->getCalendarId();
                $this->event_id_parent = $slot->getEventIdParent();
                $this->slot_start = $slot->getStartTime();
                $this->slot_end = $slot->getEndTime();
                $this->service = $settings->getService($slot->getServiceId());
                $this->tickets_left = $settings->getService($slot->getServiceId())->getAttendees() - $slot->getAttendeesNumber();
                $this->service_location = $slot->getLocation();
            }
        } catch (Exception $e) {
            // do nothing
        }

        $this->settings = $settings;
    }

    //------------------------------------------------------------

    public function getContent()
    {
        $form_id = "tb-reservation-form-" . $this->event_id . "-" . $this->instance . "-" . strtotime($this->slot_start);
        $modal_close_id = "tb-modal-close-" . $this->event_id . "-" . $this->instance . "-" . strtotime($this->slot_start);
        $modal_submit_id = "tb-modal-submit-" . $this->event_id . "-" . $this->instance . "-" . strtotime($this->slot_start);
        // Strings
        $string_reservation_for = __('Reservation for', 'teambooking');
        $string_total_tickets = __('Tickets', 'teambooking') . ':';
        $string_total_price = __('Total price', 'teambooking') . ':';
        $string_book_now = __('Book now', 'teambooking');
        $string_book_and_pay = __('Book and pay', 'teambooking');
        $string_or = __('or', 'teambooking');
        $string_close = __('Close', 'teambooking');

        if (is_user_logged_in()) {
            $user = $this->settings->getCoworkerData(get_current_user_id());
        }
        // Store form fields on variable
        $form_fields_builtin = $this->service->getFormFields()->getBuiltInFields();
        $form_fields_custom = $this->service->getFormFields()->getCustomFields();
        // Merge the fields
        $form_fields = array_merge($form_fields_builtin, $form_fields_custom);
        // Keep only the active ones
        foreach ($form_fields as $key => $field) {
            if (is_object($field)) {
                if (!$field->getIsActive()) {
                    unset($form_fields[$key]);
                }
            } else {
                unset($form_fields[$key]);
            }
        }
        $there_are_files = FALSE;
        $form_fields_markup = array();
        $form_fields_hidden_markup = array();

        foreach ($form_fields as $field) {
            // Check for file upload fields
            if (get_class($field) == 'TeamBookingFormFileUpload') {
                $there_are_files = TRUE;
            }
            $is_hidden = FALSE;
            /* @var $field TeamBookingFormTextField */
            switch ($field->getHook()) {
                case "first_name":
                    $default_value = (isset($user) && $this->settings->getAutofillReservationForm() ? $user->getFirstName() : '');
                    if ($this->settings->getAutofillReservationForm() === 'hide' && !empty($default_value)) {
                        $is_hidden = TRUE;
                    }
                    break;
                case "second_name":
                    $default_value = (isset($user) && $this->settings->getAutofillReservationForm() ? $user->getLastName() : '');
                    if ($this->settings->getAutofillReservationForm() === 'hide' && !empty($default_value)) {
                        $is_hidden = TRUE;
                    }
                    break;
                case "email":
                    $default_value = (isset($user) && $this->settings->getAutofillReservationForm() ? $user->getEmail() : '');
                    if ($this->settings->getAutofillReservationForm() === 'hide' && !empty($default_value)) {
                        $is_hidden = TRUE;
                    }
                    break;
                default:
                    $default_value = $field->getValue();
            }
            if (get_class($field) == 'TeamBookingFormCheckbox') {
                $field->setValue(__('Selected', 'teambooking'));
            } else {
                $field->setValue($default_value);
            }

            ob_start();
            if ($is_hidden) {
                $field->getHiddenMarkup();
                $result = ob_get_clean();
                $form_fields_hidden_markup[] = $result;
            } else {
                $field->getMarkup();
                // Address autocomplete by Google
                if ($field->getHook() == "address") {
                    ?>
                    <style>
                        .pac-container {
                            z-index: 99999999;
                            -webkit-filter: none !important;
                            filter: none !important;
                        }
                    </style>
                    <?php
                }
                $result = ob_get_clean();
                $form_fields_markup[] = $result;
            }
        }

        $timezone = tbGetTimezone();
        $start_date_time_object = new DateTime($this->slot_start);
        $start_date_time_object->setTimezone($timezone);

        ob_start();
        ?>
        <!-- modal header -->
        <div class="ui centered tbk-header">
            <div class="ui horizontal statistic">
                <div class="value">
                    <?= date_i18n_tb(get_option('date_format'), $start_date_time_object->getTimestamp() + $start_date_time_object->getOffset()) ?>
                </div>
                <div class="ui transparent label">
                    <span style="font-weight: 300;font-style: italic;"><?= esc_html($string_reservation_for) ?></span>
                    <?= esc_html($this->service->getName()) ?>
                    <!-- times string -->
                    <p class="ui tbk-modal-times-header">
                        <?= $this->start_end_times ?>
                    </p>
                    <!-- coworker string -->
                    <?php if (isset($this->coworker_string)) { ?>
                        <p class="ui tbk-modal-coworker-header">
                            <?= $this->coworker_string ?>
                        </p>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- modal content -->
        <div class="tbk-content" style="box-sizing: border-box;border: none;">
            <!-- service description section -->
            <?php if ($this->service->getServiceInfo()) { ?>
                <div class="ui stacked segment">
                    <p><?= $this->service->getServiceInfo() ?></p>
                </div>
            <?php } ?>
            <!-- map section -->
            <div class="ui horizontal tbk-divider tb-map" style="display:none">
                <i class="marker tb-icon"></i>
            </div>
            <p class="tb-map" style="display:none"></p>

            <div class="ui segment tb-map" style="display:none"></div>
            <!-- form section -->
            <div class="ui horizontal tbk-divider">
                <i class="info letter tb-icon"></i>
            </div>
            <?php
            /**
             * Now we're setting up the form
             */
            if ($there_are_files) {
                $form_enctype = 'enctype="multipart/form-data"';
            } else {
                $form_enctype = '';
            }
            ?>
            <form id="<?= $form_id ?>" class="ui form" method="POST" action="" <?= $form_enctype ?>>
                <?php wp_nonce_field('teambooking_submit_reservation', 'nonce') ?>
                <input type="hidden" name="tickets" value="1">
                <input type="hidden" name="owner" value="<?= esc_attr($this->coworker_id) ?>">
                <input type="hidden" name="calendar_id" value="<?= tb_base64_url_encode($this->calendar_id) ?>">
                <input type="hidden" name="event_id" value="<?= $this->event_id ?>">
                <input type="hidden" name="service" value="<?= esc_attr($this->service_id) ?>">
                <input type="hidden" name="post_id" value="">
                <input type="hidden" name="event_id_parent" value="<?= $this->event_id_parent ?>">
                <input type="hidden" name="slot_start" value="<?= $this->slot_start ?>">
                <input type="hidden" name="slot_end" value="<?= $this->slot_end ?>">
                <input type="hidden" name="service_location" value="<?= $this->service_location ?>">
                <input type="hidden" name="customer_wp_id"
                       value="<?= (isset($user)) ? esc_attr($user->getId()) : '' ?>">
                <?php
                /**
                 * Let's render the hidden fields (pre-filled user data)
                 * if any
                 */
                foreach ($form_fields_hidden_markup as $field_markup) {
                    echo $field_markup;
                }
                ?>
                <?php
                // Start grouping (max two, for now)
                $group_limit = NULL;
                $number_of_fields = count($form_fields_markup);
                if ($number_of_fields > 3 && $number_of_fields < 700) {
                    $group_limit = 2;
                    $group_limit_textual = "two";
                    echo "<div class='$group_limit_textual fields'>";
                } elseif ($number_of_fields >= 700) {
                    $group_limit = 3;
                    $group_limit_textual = "three";
                    echo "<div class='$group_limit_textual fields'>";
                }
                $i = 1;
                ?>
                <?php
                foreach ($form_fields_markup as $field) {
                    echo $field;
                    // Close grouping
                    if ($i == $group_limit) {
                        $i = 1; // reset
                        echo "</div><div class='$group_limit_textual fields'>";
                    } else {
                        $i++;
                    }
                }
                if ($number_of_fields > 3) {
                    echo "</div>";
                }
                ?>
            </form>
            <!-- tickets and price section -->
            <?php if ($this->service->isClass('event') || $this->service->getPrice() > 0) { ?>
                <div class="ui message tbk-tickets-price-section">
                    <?php if ($this->service->isClass('event')) { ?>
                        <span class="tbk-tickets-span">
                            <i class="ticket tb-icon"></i>
                            <?= esc_html($string_total_tickets) ?>
                            <?php
                            if ($this->service->getMaxReservationsPerUser() <= 1) {
                                echo '1';
                            } else {
                                ?>
                                <div class="ui inline scrolling dropdown" id="total-tickets-<?= $form_id ?>">
                                    <input type="hidden">

                                    <div class="default text">1</div>
                                    <i class="dropdown tb-icon"></i>

                                    <div class="menu">
                                        <?php
                                        if ($this->tickets_left < $this->service->getMaxReservationsPerUser()) {
                                            $i_max = $this->tickets_left;
                                        } else {
                                            $i_max = $this->service->getMaxReservationsPerUser();
                                        }
                                        for ($i = 1; $i <= $i_max; $i++) {
                                            ?>
                                            <div class="item"
                                                 data-value="<?= tbCurrencyCodeToSymbol($this->settings->getCurrencyCode(), $this->service->getPrice() * $i) ?>">
                                                <?= $i ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <script>
                                    jQuery('#total-tickets-<?= $form_id ?>')
                                        .uiDropdown({
                                            onChange: function (value, text, $selectedItem) {
                                                jQuery('#<?= $form_id ?> input[name=tickets]').val(text);
                                                jQuery('#<?= $form_id ?> input[name=quantity]').val(text);
                                                jQuery('#total-price-<?= $form_id ?>').html("<?= $string_total_price ?> " + value);
                                            }
                                        });
                                </script>
                            <?php } ?>
                        </span>
                    <?php } ?>
                    <?php if ($this->service->getPrice() > 0) { ?>
                        <span id="total-price-<?= $form_id ?>" class="" style="
                              font-weight: 300;
                              font-style: italic;
                              padding: 5px;
                              ">
                                  <?= esc_html($string_total_price) ?> <?= tbCurrencyCodeToSymbol($this->settings->getCurrencyCode(), $this->service->getPrice()) ?>
                        </span>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <!-- modal footer -->
        <div class="actions">
            <div class="ui tbk-buttons">
                <!-- deny button -->
                <div class="ui deny black tbk-button"
                     id="<?= $modal_close_id ?>"><?= esc_html($string_close) ?></div>
                <!-- divider -->
                <div class="or" data-text="<?= esc_attr($string_or) ?>"></div>
                <!-- confirm button -->
                <button class="ui positive tbk-button <?= $modal_submit_id ?>"
                        type="submit">
                    <?= count($this->settings->getPaymentGatewaysActive()) == 1
                    && $this->service->getPaymentMustBeDone() == 'immediately'
                    && $this->service->getPrice() > 0
                        ? esc_html($string_book_and_pay)
                        : esc_html($string_book_now) ?>
                </button>
            </div>
            <script>
                jQuery('.<?= $modal_submit_id ?>').click(function (e) {
                    e.preventDefault();
                    if (jQuery(this).hasClass('loading') === false) {
                        var files = <?= $there_are_files ? 'true' : '""' ?>;
                        tbScheduledServiceSubmit('<?= $this->event_id ?>', '<?= $this->instance ?>', '<?= strtotime($this->slot_start) ?>', files);
                    }
                });
            </script>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    public function getContentRegisterAdvice()
    {
        $registration_url = $this->settings->getRegistrationUrl();
        // Strings
        $string_close = __('Close', 'teambooking');
        $string_must_be_logged = __('You must be logged-in to book this!', 'teambooking');
        $string_not_registered = __("I'm not registered...", 'teambooking');
        $string_login = __("Login", 'teambooking');
        ob_start();
        ?>
        <div class="ui tbk-header">
            <?= esc_html($string_must_be_logged) ?>
        </div>
        <div class="tbk-content" style="display:block;width: initial;">
            <div class="ui stackable grid">
                <div class="ui eight wide tbk-column">
                    <a href="<?= esc_url($registration_url) ?>"
                       class="ui fluid negative tbk-button"><?= esc_html($string_not_registered) ?></a>
                </div>
                <div class="ui eight wide tbk-column">
                    <a href="<?= wp_login_url(get_permalink()) ?>"
                       class="ui fluid positive tbk-button"><?= esc_html($string_login) ?></a>
                </div>
            </div>
        </div>
        <div class="actions">
            <div class="ui black deny tbk-button"><?= esc_html($string_close) ?></div>
        </div>

        <?php
        return ob_get_clean();
    }

}
