<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Components_Frontend_ErrorMessages
{

    //------------------------------------------------------------

    private function basicTemplate($string)
    {
        ob_start();
        ?>
        <div class="ui error message">
            <div class="tbk-header">
                <?= __('Cannot make the reservation!', 'teambooking') ?>
            </div>
            <p><?= $string ?></p>
        </div>
        <?php
        $return = ob_get_clean();

        return $return;
    }

    //------------------------------------------------------------

    public function invalidAttendeeEmail()
    {
        $string = __('Your email is invalid, please check it and retry!', 'teambooking');

        return $this->basicTemplate($string);
    }

    //------------------------------------------------------------

    public function alreadyBooked()
    {
        $string = __('You have already booked this...', 'teambooking');

        return $this->basicTemplate($string);
    }

    //------------------------------------------------------------

    public function eventFull()
    {
        $string = __('Sorry, this service is full! Maybe someone booked last availability right now, just a moment before you :(', 'teambooking');

        return $this->basicTemplate($string);
    }

    //------------------------------------------------------------

    public function eventNotAvailableAnymore()
    {
        $string = __('Sorry, this slot is not available anymore. Probably it was booked by someone else or cancelled right now, just before you :(', 'teambooking');

        return $this->basicTemplate($string);
    }

    //------------------------------------------------------------

    public function coworkersRevokedAuth()
    {
        $string = __('Sorry, you should contact the administrator', 'teambooking');

        return $this->basicTemplate($string);
    }

    //------------------------------------------------------------

    public function genericGoogleApiError($error_message)
    {
        $string = __('Sorry, you should contact the administrator providing these informations:', 'teambooking') . " " . $error_message;

        return $this->basicTemplate($string);
    }

    //------------------------------------------------------------

    public function customerMaxCumulativeTicketsOvercome($error_message = '')
    {
        $string = __('Sorry, you are trying to book a number of tickets that, together with the ones you have already booked for this slot, overcomes the maximum number allowed per customer. Please retry with a lower number of tickets.', 'teambooking') . " " . $error_message;

        return $this->basicTemplate($string);
    }

}
