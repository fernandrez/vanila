<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Components_Frontend_Calendar
{

    //------------------------------------------------------------

    /** @var TeamBooking_SlotsResults */
    public $slots_obj;
    private $today;
    private $settings;
    private $params;

    public function __construct(TeamBookingSettings $settings, TeamBooking_RenderParameters $params)
    {
        // Day number of today
        $this->today = date_i18n('j');
        $this->settings = $settings;
        $this->params = $params;
    }

    //------------------------------------------------------------

    public function getContent()
    {
        // Start of week (day), 0 = Sunday
        $week_starting_day = get_option('start_of_week');
        // This should be plain English, not localized
        $week_starting_day_textual = date('D', strtotime("Sunday +{$week_starting_day} days"));
        // Week day of month's first day
        $first_month_day_position_from_week_start = date_i18n('N', mktime(0, 0, 0, $this->params->getMonth(), 1, $this->params->getYear())) - $week_starting_day;
        // If negative, add 7
        if ($first_month_day_position_from_week_start < 0) {
            $first_month_day_position_from_week_start += 7;
        }
        // Number of days in month
        $number_of_month_days = date_i18n("t", strtotime($this->params->getYear() . '-' . $this->params->getMonth() . '-01'));
        // Number of graphic calendar rows needed
        $rows = ceil(($number_of_month_days + $first_month_day_position_from_week_start) / 7);
        // Matrix indexes init
        $j = 0;
        $i = 0;
        ob_start();
        // Let's avoid double styling and filtering renders due to ajax calls
        if (!$this->params->getIsAjaxCall()) {
            echo $this->getCalendarStyle();
            if (count($this->params->getServiceIds()) > 1 && !$this->params->getNoFilter()) {
                ?>
                <div class="ui grid tb-filtering-dock" style="margin-bottom:0;">
                    <div class="tbk-column">
                        <?php $this->getFilterButton() ?>
                        <?php $this->getMonthFastSelector() ?>
                        <?php $this->getYearFastSelector() ?>
                    </div>
                </div>
                <?php
            }
        }
        ?>
        <div class="ui form grid <?= ($this->params->getIsWidget() === TRUE) ? "tb-widget" : "" ?> tb-frontend-calendar"
             data-params="<?= $this->params->encode() ?>" data-instance='<?= $this->params->getInstance() ?>'>
            <div class="tbk-row">
                <div class="three wide tbk-column tb-change-month"
                     data-month="<?= date_i18n('n', strtotime($this->params->getYear() . '-' . $this->params->getMonth() . '-01 - 1 month')) ?>"
                     data-year="<?= date_i18n('Y', strtotime($this->params->getYear() . '-' . $this->params->getMonth() . '-01 - 1 month')) ?>">
                    <span class="dashicons dashicons-arrow-left-alt2"></span>
                </div>
                <div class="ten wide tbk-column">
                    <?php
                    if ($this->params->getIsWidget() === TRUE) {
                        echo strtoupper(date_i18n('M Y', strtotime($this->params->getYear() . '-' . $this->params->getMonth() . '-01')));
                    } else {
                        echo strtoupper(date_i18n('F, Y', strtotime($this->params->getYear() . '-' . $this->params->getMonth() . '-01')));
                    }
                    ?>
                </div>
                <div class="three wide tbk-column tb-change-month"
                     data-month="<?= date_i18n('n', strtotime($this->params->getYear() . '-' . $this->params->getMonth() . '-01 + 1 month')) ?>"
                     data-year="<?= date_i18n('Y', strtotime($this->params->getYear() . '-' . $this->params->getMonth() . '-01 + 1 month')) ?>">
                    <span class="dashicons dashicons-arrow-right-alt2"></span>
                </div>
            </div>
            <div class="tb-calendar-line equal width tbk-column tbk-row">
                <?php
                while ($i < 7) {
                    ?>
                    <div class="tbk-column">
                        <div class="tb-weekline-day">
                            <?php
                            if ($this->params->getIsWidget() === TRUE) {
                                if (extension_loaded('mbstring')) {
                                    echo mb_substr(date_i18n('D', strtotime("last $week_starting_day_textual + $i day")), 0, 1);
                                } else {
                                    echo substr(date_i18n('D', strtotime("last $week_starting_day_textual + $i day")), 0, 1);
                                }
                            } else {
                                echo date_i18n('D', strtotime("last $week_starting_day_textual + $i day"));
                            }
                            ?>
                        </div>
                    </div>
                    <?php
                    $i++;
                }
                ?>

            </div>

            <?php
            while ($j < $rows) {
                $i = 0;
                ?>
                <div class="equal width tbk-column tbk-row tb-days">
                    <?php
                    while ($i < 7) {
                        // Grid index
                        $absolute_counter = $i + 1 + $j * 7;
                        // Day index
                        $relative_counter = $absolute_counter - $first_month_day_position_from_week_start;
                        $css_classes = $this->getDayCssClass($relative_counter);
                        // Get slots to include (to serve the slots list)
                        $slots_to_include = $this->slots_obj->getSlotsByDate(str_pad($relative_counter, 2, '0', STR_PAD_LEFT), str_pad($this->params->getMonth(), 2, '0', STR_PAD_LEFT), $this->params->getYear());
                        $slots_to_include = base64_encode(serialize($slots_to_include));
                        ?>
                        <div class="tbk-column"><?php
                            if ($relative_counter >= 1 && $relative_counter <= $number_of_month_days) {
                                ?>
                                <div class="ui tb-day <?= (isset($css_classes) ? $css_classes : '') ?>"
                                     data-day="<?= $relative_counter ?>" data-slots="<?= $slots_to_include ?>">
                                    <div>
                                        <?= $relative_counter ?>
                                    </div>
                                    <?php
                                    if ($this->params->getIsWidget() != TRUE) {
                                        $this->getReservationsLeftLabel($relative_counter);
                                    }
                                    ?>
                                </div>
                            <?php } ?>
                        </div>
                        <?php
                        $i++;
                    }
                    ?></div>
                <?php
                $j++;
            }
            ?>
            <div class="ui tbk-modal" id='tb-modal-list-<?= $this->params->getInstance() ?>'>
            </div>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    /**
     * Renders the small numbered dots label
     *
     * @param $relative_counter
     * @throws Exception
     */
    private function getReservationsLeftLabel($relative_counter)
    {
        if ($this->settings->getNumberedDotsLogic() == 'hide') {
            return;
        }
        $slots = $this->slots_obj->getSlotsByDate(str_pad($relative_counter, 2, '0', STR_PAD_LEFT), str_pad($this->params->getMonth(), 2, '0', STR_PAD_LEFT), $this->params->getYear());
        if (!empty($slots)) {
            $reservations = "";
            $sorted_slots = array();
            foreach ($slots as $slot) {
                /* @var $slot TeamBooking_Slot */
                if ($slot->isSoldout()) {
                    continue;
                }
                try {
                    if (isset($sorted_slots[$slot->getServiceId()])) {
                        if ($this->settings->getNumberedDotsLogic() == 'slots' ||
                            $this->settings->getService($slot->getServiceId())->isClass('appointment')
                        ) {
                            $sorted_slots[$slot->getServiceId()]++;
                        } elseif ($this->settings->getNumberedDotsLogic() == 'tickets') {
                            $sorted_slots[$slot->getServiceId()] += $this->settings->getService($slot->getServiceId())->getAttendees() - $slot->getAttendeesNumber();
                        }
                    } else {
                        if ($this->settings->getNumberedDotsLogic() == 'slots' ||
                            $this->settings->getService($slot->getServiceId())->isClass('appointment')
                        ) {
                            $sorted_slots[$slot->getServiceId()] = 1;
                        } elseif ($this->settings->getNumberedDotsLogic() == 'tickets') {
                            $sorted_slots[$slot->getServiceId()] = $this->settings->getService($slot->getServiceId())->getAttendees() - $slot->getAttendeesNumber();
                        }
                    }
                } catch (Exception $e) {
                    continue;
                }
            }
            foreach ($sorted_slots as $service_id => $number) {
                $reservations .= $this->getReservationsLeftDot($number, $service_id);
            }
            if (!empty($reservations)) {
                ?>
                <div class="ui small pointing above label computer only tbk-column">
                    <?= $reservations ?>
                </div>
                <?php
            }
        }
    }

    //------------------------------------------------------------

    /**
     * Renders a single numbered slot
     *
     * @param $number
     * @param $service_id
     * @return string
     * @throws Exception
     */
    private function getReservationsLeftDot($number, $service_id)
    {
        try {
            $color = $this->settings->getService($service_id)->getServiceColor();
            $name = $this->settings->getService($service_id)->getName();
        } catch (Exception $e) {
            return NULL;
        }
        if ($number < $this->settings->getNumberedDotsLowerBound()) {
            $number = NULL;
        }
        ob_start();
        ?>
        <span class='ui mini circular label tb-pointing-label-dots'
              style='background-color:<?= $color ?>;color:<?= tbGetRightTextColor($color, TRUE) ?>;'
              title='<?= $name ?>'>
            <?= $number ?>
        </span>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    /**
     * @param $relative_counter
     * @return string
     */
    private function getDayCssClass($relative_counter)
    {
        $slots = $this->slots_obj->getSlotsByDate(str_pad($relative_counter, 2, '0', STR_PAD_LEFT), str_pad($this->params->getMonth(), 2, '0', STR_PAD_LEFT), $this->params->getYear());
        $css_classes = "";
        if ($relative_counter == $this->today && $this->params->getMonth() == date_i18n('m') && $this->params->getYear() == date_i18n('Y')) {
            $css_classes = 'today';
        }
        if (($relative_counter >= $this->today && $this->params->getMonth() == date_i18n('m') && $this->params->getYear() == date_i18n('Y')) || ($this->params->getMonth() > date_i18n('m') && $this->params->getYear() >= date_i18n('Y')) || ($this->params->getYear() > date_i18n('Y'))) {
            if (!empty($slots)) {
                $free_places_detected = FALSE;
                foreach ($slots as $slot) {
                    /* @var $slot TeamBooking_Slot */
                    if (!$slot->isSoldout()) {
                        $free_places_detected = TRUE;
                        break;
                    }
                }
                if ($free_places_detected) {
                    $css_classes .= ' slots';
                } else {
                    $css_classes .= ' slots soldout';
                }
            }
        } else {
            $css_classes .= ' pastday';
        }

        return $css_classes;
    }

    //------------------------------------------------------------

    private function getMonthFastSelector()
    {
        $string_month = __('Month', 'teambooking');
        $string_reset_filter = __('Current', 'teambooking');
        ?>
        <div
            class="ui right floated <?= $this->params->getIsWidget() ? "mini" : "tiny" ?> basic dropdown tbk-button tbk-fast-month-dropdown"
            style="opacity:1;">
            <span class="text"><?= $string_month ?></span>

            <div class="menu">
                <?php for ($i = 1; $i <= 12; $i++) { ?>
                    <div class="item tbk-month-selector"
                         data-month="<?= date_i18n('m', mktime(0, 0, 0, $i, 1, date('Y'))) ?>"
                         data-instance="<?= $this->params->getInstance() ?>">
                        <?php
                        $value = date_i18n('F', mktime(0, 0, 0, $i, 1, date('Y')));
                        echo $value;
                        ?>
                    </div>
                    <?php
                }
                ?>
                <div class="tbk-divider"></div>
                <div class="item tbk-month-selector" data-month="<?= date_i18n('m') ?>"
                     data-instance="<?= $this->params->getInstance() ?>">
                    <?= $string_reset_filter . ' (' . date_i18n('F') . ')' ?>
                </div>
            </div>
        </div>
        <?php
    }

    //------------------------------------------------------------

    private function getYearFastSelector()
    {
        // Strings
        $string_year = __('Year', 'teambooking');
        $string_reset_filter = __('Current', 'teambooking');
        ?>
        <div class="ui right floated <?= $this->params->getIsWidget() ? "mini" : "tiny" ?> basic dropdown tbk-button tbk-fast-year-dropdown"
             style="opacity:1;"">
            <span class="text"><?= $string_year ?></span>

            <div class="menu">
                <?php
                for ($i = 0; $i <= 3; $i++) {
                    $value = date_i18n('Y', mktime(0, 0, 0, 1, 1, date('Y') + $i));
                    ?>
                    <div class="item tbk-year-selector" data-year="<?= $value ?>"
                         data-instance="<?= $this->params->getInstance() ?>">
                        <?= $value ?>
                    </div>
                    <?php
                }
                ?>
                <div class="tbk-divider"></div>
                <div class="item tbk-year-selector" data-year="<?= date('Y') ?>"
                     data-instance="<?= $this->params->getInstance() ?>">
                    <?= $string_reset_filter . ' (' . date_i18n('Y') . ')' ?>
                </div>
            </div>
        </div>
        <?php
    }

    //------------------------------------------------------------

    private function getFilterButton()
    {
        // Strings
        $string_filter = __('Services', 'teambooking');
        $string_reset_filter = __('All', 'teambooking');
        ?>
        <div class="ui left floated <?= $this->params->getIsWidget() ? "mini" : "tiny" ?> basic dropdown tbk-button tbk-service-filter-dropdown"
             style="opacity:1;">
            <span class="text"><?= $string_filter ?></span>

            <div class="menu">
                <?php
                if (is_array($this->params->getRequestedServiceIds())) {
                    $services = $this->params->getRequestedServiceIds();
                } else {
                    $services = $this->params->getServiceIds();
                }
                ?>
                <div class="scrolling menu">
                    <?php foreach ($services as $service_id) { ?>
                        <div class="item tbk-service-selector" data-service="<?= $service_id ?>"
                             data-instance="<?= $this->params->getInstance() ?>">
                            <div class="ui empty circular label"
                                 style="background-color:<?= $this->settings->getService($service_id)->getServiceColor() ?>"></div>
                            <?php
                            echo $this->settings->getService($service_id)->getName();
                            if ($this->settings->getService($service_id)->getPrice() > 0) {
                                $price = tbCurrencyCodeToSymbol($this->settings->getCurrencyCode(), $this->settings->getService($service_id)->getPrice());
                                ?>
                                <span class="description"><?= $price ?></span>
                            <?php } ?>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="tbk-divider"></div>
                <div class="item tbk-service-selector tb-reset-filter"
                     data-services="<?= base64_encode(serialize($services)) ?>">
                    <?= $string_reset_filter ?>
                </div>
            </div>
        </div>
        <?php
    }

    //------------------------------------------------------------

    public function getCalendarStyle()
    {
        $border = $this->settings->getBorder();
        $pattern = $this->settings->getPattern();
        ob_start();
        ?>
        <style>
            .tb-frontend-calendar {
                background: <?= $this->settings->getColorBackground() ?>;
                background-image: url(<?= tb_get_pattern($pattern['calendar'], $this->settings->getColorBackground())?>);
                border: <?= $border['size'] ?>px solid <?= $border['color'] ?>;
                border-radius: <?= $border['radius'] ?>px;
            }

            .tb-frontend-calendar > .tbk-row {
                color: <?= tbGetRightTextColor($this->settings->getColorBackground()) ?>;
            }

            .tb-frontend-calendar .ui.tb-day.pastday {
                color: <?= tbGetRightHoverColor($this->settings->getColorBackground()) ?>;
            }

            .tb-frontend-calendar .ui.tb-day.today {
                background: <?= tbGetRightHoverColor($this->settings->getColorBackground()) ?>;
            }

            .tb-change-month:hover {
                background-color: <?= tbGetRightHoverColor($this->settings->getColorBackground()) ?>;
            }

            .tbk-column .tb-day:not(.pastday):hover {
                background-color: <?= tbGetRightHoverColor($this->settings->getColorBackground()) ?>;
            }

            .tb-frontend-calendar .ui.tb-day.slots {
                background-color: <?= $this->settings->getColorFreeSlot() ?>;
                color: <?= tbGetRightTextColor($this->settings->getColorFreeSlot()) ?>;
            }

            .tb-frontend-calendar .ui.tb-day.slots.soldout {
                background-color: <?= $this->settings->getColorSoldoutSlot() ?>;
                color: <?= tbGetRightTextColor($this->settings->getColorSoldoutSlot()) ?>;
            }

            .tb-frontend-calendar .ui.tb-day.slots:hover {
                background: <?= $this->settings->getColorFreeSlot() ?>;
                background: linear-gradient(to bottom, rgba(0, 0, 0, 0.15) 0%, rgba(0, 0, 0, 0.15) 100%),
                linear-gradient(to bottom,  <?= $this->settings->getColorFreeSlot() ?> 0%,<?= $this->settings->getColorFreeSlot() ?> 100%); /* W3C */
            }

            .tb-frontend-calendar .ui.tb-day.slots.soldout:hover {
                background: <?= $this->settings->getColorSoldoutSlot() ?>;
                background: linear-gradient(to bottom, rgba(0, 0, 0, 0.15) 0%, rgba(0, 0, 0, 0.15) 100%),
                linear-gradient(to bottom,  <?= $this->settings->getColorSoldoutSlot() ?> 0%,<?= $this->settings->getColorSoldoutSlot() ?> 100%); /* W3C */
            }
            .tb-calendar-line.equal.width.tbk-column.tbk-row {
                background:<?= $this->settings->getColorWeekLine() ?>;
                background-image: url(<?= tb_get_pattern($pattern['weekline'], $this->settings->getColorWeekLine())?>);
                color:<?= tbGetRightTextColor($this->settings->getColorWeekLine()) ?>;
            }
        </style>

        <?php
        return ob_get_clean();
    }

}
