<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Components_Frontend_Schedule
{

    //------------------------------------------------------------

    private $slots;
    private $time_format;
    private $date_format;
    private $read_only;
    private $slots_obj;
    private $modal_id;

    /** @var TeamBookingSettings */
    private $settings;

    /** @var TeamBooking_RenderParameters */
    private $params;
    private $timezone;

    public function __construct(TeamBookingSettings $settings, TeamBooking_RenderParameters $parameters)
    {
        $this->time_format = get_option('time_format');
        $this->date_format = get_option('date_format');
        $this->settings = $settings;
        $this->params = $parameters;
        $this->timezone = tbGetTimezone();
        // Check if the instance is read_only
        $this->read_only = (strlen($parameters->getInstance()) == 8) ? FALSE : TRUE;
        $this->slots_obj = new TeamBooking_SlotsResults();
        $this->modal_id = 'tb-modal-list-' . $this->params->getInstance();
    }

    //------------------------------------------------------------

    public function getContent()
    {
        // Strings
        $string_close = __('Close', 'teambooking');
        ob_start();
        ?>
        <!-- day header -->
        <div class="ui tbk-header">
            <div class="tbk-content basic segment" style="width:100%;">
                <div class="ui horizontal small statistic" style="margin: 0;">
                    <div class="value">
                        <?= date_i18n(get_option('date_format'), mktime(0, 0, 0, $this->params->getMonth(), $this->params->getDay(), $this->params->getYear())) ?>
                    </div>
                </div>
                <?php
                $coworkers_list = $this->slots_obj->getShownCoworkerServiceIds();
                if (!empty($coworkers_list) && !$this->settings->isGroupSlotsByCoworker()) {
                    $this->getCoworkerFilter();
                }
                unset($coworkers_list);
                $locations_list = $this->slots_obj->getLocationsList();
                if (!empty($locations_list)) {
                    $this->getLocationFilter();
                }
                unset($locations_list);
                $this->getTimeFilter();
                ?>
            </div>
        </div>
        <!-- schedule list -->
        <div class="tbk-content" style="box-sizing: border-box;border: none; padding-top:0;">
            <div class="tbk-column tb-schedule-list">
                <?php
                if (!empty($this->slots)) {
                    if ($this->settings->isGroupSlotsByTime()) {
                        ?>
                        <div class="ui items tbk-column tbk-container">
                            <?= $this->sortSlots(); ?>
                        </div>
                        <?php
                    } else {
                        // Grouping
                        ?>
                        <div class="ui items two tbk-column equal width grid stackable tbk-container">
                            <?php
                            if ($this->settings->isGroupSlotsByService()) {
                                foreach ($this->slots_obj->getServiceIds() as $service_id) {
                                    $this->slots = $this->slots_obj->getSlotsByService(array($service_id));
                                    ?>
                                    <div class="tbk-column">
                                        <div class="ui horizontal tbk-divider"
                                             style="font-weight: 300;font-style: italic;">
                                            <span class="ui mini circular label tb-pointing-label-dots"
                                                  style="background-color:<?= $this->settings->getService($service_id)->getServiceColor() ?>"></span>
                                            <?= esc_html($this->settings->getService($service_id)->getName()) ?>
                                        </div>
                                        <div class="item">
                                            <?php
                                            echo $this->sortSlots();
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } elseif ($this->settings->isGroupSlotsByCoworker()) {
                                foreach ($this->slots_obj->getCoworkerIds() as $coworker_id) {
                                    $this->slots = $this->slots_obj->getSlotsByCoworker(array($coworker_id));
                                    ?>
                                    <div class="tbk-column">
                                        <div class="ui horizontal tbk-divider"
                                             style="font-weight: 300;font-style: italic;">
                                            <i class='user tb-icon'></i> <?= esc_html($this->settings->getCoworkerData($coworker_id)->getFirstName()) ?> <?= esc_html($this->settings->getCoworkerData($coworker_id)->getLastName()) ?>
                                        </div>
                                        <div class="item">
                                            <?php
                                            echo $this->sortSlots();
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
            <!-- reservation modal -->
            <div class="ui tbk-modal" id='tb-modal-<?= $this->params->getInstance() ?>'>
            </div>
        </div>
        <div class="actions">
            <div class="ui tbk-buttons">
                <!-- deny button -->
                <div class="ui deny black tbk-button"><?= esc_html($string_close) ?></div>
            </div>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getReservationsLeft($slot)
    {
        // Strings
        $string_left = __('left:', 'teambooking');
        $string_available = __('available', 'teambooking');
        /* @var $slot TeamBooking_Slot */
        if ($this->settings->getService($slot->getServiceId())->getShowReservationsLeft()) {
            $max_attendees = $this->settings->getService($slot->getServiceId())->getAttendees();
            $reservations_left = $max_attendees - $slot->getAttendeesNumber();
            if ($reservations_left <= 0) {
                return FALSE;
            }
            $percentage = 20;
            $label_color = (($reservations_left / $max_attendees) * 100 <= $percentage || $reservations_left == 1) ? "orange" : "green";
            ob_start();
            ?>
            <div class="ui horizontal <?= $label_color ?> label">
                <?= esc_html($string_left) ?>
                <div class="detail"><?= $reservations_left ?></div>
            </div>
            <?php
            return ob_get_clean();
        } else {
            if ($this->settings->getService($slot->getServiceId())->isClass('appointment') &&
                !$slot->isSoldout()
            ) {
                ob_start();
                ?>
                <div class="ui horizontal green label">
                    <?= esc_html($string_available) ?>
                </div>
                <?php
                return ob_get_clean();
            } else {
                return FALSE;
            }
        }
    }

    //------------------------------------------------------------

    private function getPriceTag($slot)
    {
        /* @var $slot TeamBooking_Slot */
        if ($this->settings->getService($slot->getServiceId())->getPrice() > 0) {
            ?>
            <div
                class="ui horizontal <?= $this->settings->getPriceTagColor() ?> label"><?= tbCurrencyCodeToSymbol($this->settings->getCurrencyCode(), $this->settings->getService($slot->getServiceId())->getPrice()) ?></div>
            <?php
        }
    }

    //------------------------------------------------------------

    /**
     * @param $slot
     * @return null|string
     * @throws Exception
     */
    private function getStartEnd($slot)
    {
        // Strings
        $string_all_day = __('All day', 'teambooking');
        $content_icons = '<i class="wait tb-icon"></i>';
        /* @var $slot TeamBooking_Slot */
        $start_date_time_object = new DateTime($slot->getStartTime());
        $start_date_time_object->setTimezone($this->timezone);
        $end_date_time_object = new DateTime($slot->getEndTime());
        $end_date_time_object->setTimezone($this->timezone);
        try {
            switch ($this->settings->getService($slot->getServiceId())->getShowTimes()->getValue()) {
                case "yes":
                    if ($slot->isAllDay()) {
                        return $content_icons . esc_html($string_all_day);
                    } else {
                        return $content_icons . $start_date_time_object->format($this->time_format) . " - " . $end_date_time_object->format($this->time_format);
                    }
                case "no" :
                    return NULL;
                case "start_time_only":
                    if ($slot->isAllDay()) {
                        return $content_icons . esc_html($string_all_day);
                    } else {
                        return $content_icons . $start_date_time_object->format($this->time_format);
                    }
                default:
                    return NULL;
            }
        } catch (Exception $e) {
            return NULL;
        }
    }

    //------------------------------------------------------------

    private function getStartTimeOnly($slot, $return_object = FALSE)
    {
        /* @var $slot TeamBooking_Slot */
        $start_date_time_object = new DateTime($slot->getStartTime());
        $start_date_time_object->setTimezone($this->timezone);
        if (!$return_object) {
            return $start_date_time_object->format($this->time_format);
        } else {
            return $start_date_time_object;
        }
    }

    //------------------------------------------------------------

    /**
     * @param TeamBooking_Slot $slot
     * @return null|string
     * @throws Exception
     */
    private function getCoworkerName($slot)
    {
        $content_icons = '<i class="user tb-icon"></i>';
        /* @var $slot TeamBooking_Slot */
        try {
            if (!$this->settings->getService($slot->getServiceId())->getShowCoworker()) {
                return NULL;
            }
            $coworker = $this->settings->getCoworkerData($slot->getCoworkerId());
            if (!$this->settings->getService($slot->getServiceId())->getShowCoworkerUrl()) {
                return $content_icons . esc_html($coworker->getFirstName()) . " " . esc_html($coworker->getLastName());
            } else {
                return $content_icons . '<a style="text-decoration:underline;" href="' . $this->settings->getCoworkerUrl($slot->getCoworkerId()) . '" target="_blank">' . esc_html($coworker->getFirstName()) . " " . esc_html($coworker->getLastName()) . '</a>';
            }
        } catch (Exception $e) {
            return NULL;
        }
    }

    //------------------------------------------------------------

    private function sortSlots()
    {
        $how_many_services = count($this->slots_obj->getServiceIds());
        $how_many_coworkers = count($this->slots_obj->getCoworkerIds());
        $string_book_now = __('Book now', 'teambooking');
        ob_start();
        ?>
        <div class="ui stackable cards">
            <?php
            foreach ($this->slots as $slot) {
                $basic_classes = 'ui fluid card';
                /* @var $slot TeamBooking_Slot */
                if ($this->settings->getService($slot->getServiceId())->isClass('appointment')) {
                    $string_sold_out = __('booked', 'teambooking');
                } else {
                    $string_sold_out = __('sold out', 'teambooking');
                }
                if ($this->settings->getService($slot->getServiceId())->getLoggedOnly() && !is_user_logged_in() && !$slot->isSoldout()) {
                    $attributes_to_add = 'class="' . $basic_classes . ' '
                        . 'tb-book-advice' . '" '
                        . 'data-event="' . $slot->getEventId() . '" '
                        . 'data-instance="' . $this->params->getInstance() . '"';
                } else {
                    // Map logic
                    $location = $slot->getLocation();
                    if (!empty($location)) {
                        $style = htmlentities(json_encode($this->settings->getMapStyle()));
                    } else {
                        $style = '';
                    }
                    if ($slot->isSoldout()) {
                        $attributes_to_add = 'class="' . $basic_classes . '"';
                    } else {
                        if (!$this->read_only) {
                            $basic_classes .= ' tb-book';
                        }
                        $attributes_to_add = 'class="link ' . $basic_classes . '" '
                            . 'data-instance="' . $this->params->getInstance() . '" '
                            . 'data-mapstyle="' . $style . '" ';
                        if (!$this->read_only) {
                            $attributes_to_add .= 'data-slot="' . base64_encode(serialize($slot)) . '" ';
                        }
                    }
                    $start_end_value = $this->getStartEnd($slot);
                    $attributes_to_add .= 'data-address="' . $location . '" '
                        . 'data-times="' . base64_encode($start_end_value) . '" '
                        . 'data-timeint="' . (!is_null($start_end_value) ? $this->getStartTimeOnly($slot, TRUE)->format('G') : '24') . '" ';
                    if ($this->settings->getService($slot->getServiceId())->getShowCoworker()) {
                        $attributes_to_add .= 'data-coworkerstring="' . base64_encode($this->getCoworkerName($slot)) . '" '
                            . 'data-coworker="' . $slot->getCoworkerId() . '" ';
                    }
                }
                if (!$this->read_only && $slot->isSoldout()) {
                    $is_soldout = TRUE;
                } else {
                    $is_soldout = FALSE;
                }
                if (($how_many_services <= 1 && $this->settings->isGroupSlotsByService()) ||
                    ($how_many_coworkers <= 1 && $this->settings->isGroupSlotsByCoworker()) ||
                    $this->settings->isGroupSlotsByTime()
                ) {
                    if ($this->settings->getFix62dot5()) {
                        $attributes_to_add .= 'style="width:calc(50% - 1.6rem);"';
                    } else {
                        $attributes_to_add .= 'style="width:calc(50% - 1rem);"';
                    }
                }
                ?>
                <div <?= $attributes_to_add ?>>
                    <div class="tbk-content">

                        <!-- left content -->
                        <div class="right floated ui image">
                            <?= $this->getReservationsLeft($slot); ?>
                            <?php $this->getPriceTag($slot) ?>
                        </div>

                        <!-- main content -->
                        <div class="tbk-header">
                            <?php
                            if ($this->settings->isGroupSlotsByTime()) {
                                echo $this->getStartEnd($slot);
                            } else {
                                echo $this->getStartEnd($slot);
                            }
                            ?>
                        </div>

                        <!-- times meta -->
                        <div class="meta" style="text-transform:none;">
                            <span class="tb-service-info"><?= esc_html($slot->getServiceName()) ?></span>
                        </div>
                        <div class="description">
                            <?= $this->getCoworkerName($slot) ?>
                            <?php if ($slot->getLocation() != NULL) { ?>
                                <span>
                                    <i class="marker tb-icon"></i><?= esc_html(ucwords($slot->getLocation())) ?>
                                </span>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="extra tbk-content">
                        <?php if ($slot->isSoldout()) { ?>
                            <div class="ui mini disabled red fluid tbk-button"><?= esc_html($string_sold_out) ?></div>
                        <?php } else { ?>
                            <div class="ui mini fluid tbk-button"><?= esc_html($string_book_now) ?></div>
                        <?php } ?>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    public function setSlots($slots)
    {
        $this->slots = $slots;
        // Populating the TeamBooking_SlotsResults object for easy grouping
        $this->slots_obj->addSlotsFromArray($this->slots);
    }

    //------------------------------------------------------------

    private function getFilterTimesArray($lower_bound = 1, $upper_bound = 24, $increment = 'hour')
    {
        $start = date_create('midnight');
        //$return = array($start->format($this->time_format));
        $return = array();
        for ($i = $lower_bound; $i <= $upper_bound; $i++) {
            $to_add = clone $start;
            date_add($to_add, date_interval_create_from_date_string($i . ' ' . $increment));
            $return[$i] = $to_add->format($this->time_format);
        }

        return $return;
    }

    //------------------------------------------------------------

    private function getCoworkerFilter()
    {
        $string_all = __('All', 'teambooking');
        ?>
        <div class="ui small compact basic right floated labeled tb-icon dropdown tbk-button"
             id="tb-coworker-schedule-select-<?= $this->params->getInstance() ?>">
            <i class="user tb-icon"></i>

            <div class="default text"><?= esc_html($string_all) ?></div>
            <div class="menu">
                <?php
                foreach ($this->slots_obj->getCoworkerIds() as $coworker_id) {
                    /* @var $slot TeamBooking_Slot */
                    $coworker_data = $this->settings->getCoworkerData($coworker_id);
                    ?>
                    <div class="item" data-value="<?= esc_attr($coworker_id) ?>">
                        <?= esc_html($coworker_data->getFirstName()) ?> <?= esc_html($coworker_data->getLastName()) ?>
                    </div>
                <?php } ?>
                <div class="tbk-divider"></div>
                <div class="item" data-value="all">
                    <?= esc_html($string_all) ?>
                </div>
            </div>
        </div>
        <script>
            jQuery('#tb-coworker-schedule-select-<?= $this->params->getInstance() ?>')
                .uiDropdown({
                    onChange: function (value, text, $selectedItem) {
                        var identifier = '';
                        var params = {
                            timeint: jQuery('#tb-time-schedule-select-<?= $this->params->getInstance() ?>').uiDropdown('get value'),
                            coworker: value,
                            address: jQuery('#tb-location-schedule-select-<?= $this->params->getInstance() ?>').uiDropdown('get value')
                        };
                        for (var key in params) {
                            if (params.hasOwnProperty(key) && params[key] !== 'all' && params[key].length !== 0 && key !== 'timeint') {
                                identifier += '[data-' + key + '="' + params[key] + '"]';
                            }
                        }
                        var prev_styles = jQuery('#<?= $this->modal_id ?> .card').attr("style");
                        if (typeof prev_styles === 'undefined') {
                            prev_styles = '';
                        }
                        jQuery('#<?= $this->modal_id ?> .card').attr("style", prev_styles + "display: none !important;");
                        if (params.timeint !== 'all') {
                            jQuery('#<?= $this->modal_id ?> .card' + identifier)
                                .filter(function () {
                                    return jQuery(this).attr("data-timeint") >= params.timeint;
                                })
                                .show();
                        } else {
                            jQuery('#<?= $this->modal_id ?> .card' + identifier).show();
                        }
                    }
                })
            ;
        </script>
        <?php
    }

    //------------------------------------------------------------

    private function getLocationFilter()
    {
        $string_all = __('All', 'teambooking');
        ?>
        <div class="ui small compact basic right floated labeled tb-icon dropdown tbk-button"
             id="tb-location-schedule-select-<?= $this->params->getInstance() ?>">
            <i class="marker tb-icon"></i>

            <div class="default text"><?= esc_html($string_all) ?></div>
            <div class="menu">
                <?php
                foreach ($this->slots_obj->getLocationsList() as $location) {
                    /* @var $slot TeamBooking_Slot */
                    ?>
                    <div class="item" data-value="<?= esc_attr($location) ?>">
                        <?= esc_html(ucwords($location)) ?>
                    </div>
                <?php } ?>
                <div class="tbk-divider"></div>
                <div class="item" data-value="all">
                    <?= esc_html($string_all) ?>
                </div>
            </div>
        </div>
        <script>
            jQuery('#tb-location-schedule-select-<?= $this->params->getInstance() ?>')
                .uiDropdown({
                    onChange: function (value, text, $selectedItem) {
                        var identifier = '';
                        var params = {
                            timeint: jQuery('#tb-time-schedule-select-<?= $this->params->getInstance() ?>').uiDropdown('get value'),
                            address: value,
                            coworker: jQuery('#tb-coworker-schedule-select-<?= $this->params->getInstance() ?>').uiDropdown('get value')
                        };
                        for (var key in params) {
                            if (params.hasOwnProperty(key) && params[key] !== 'all' && params[key].length !== 0 && key !== 'timeint') {
                                identifier += '[data-' + key + '="' + params[key] + '"]';
                            }
                        }
                        var prev_styles = jQuery('#<?= $this->modal_id ?> .card').attr("style");
                        if (typeof prev_styles === 'undefined') {
                            prev_styles = '';
                        }
                        jQuery('#<?= $this->modal_id ?> .card').attr("style", prev_styles + "display: none !important;");
                        if (params.timeint !== 'all') {
                            jQuery('#<?= $this->modal_id ?> .card' + identifier)
                                .filter(function () {
                                    return jQuery(this).attr("data-timeint") >= params.timeint;
                                })
                                .show();
                        } else {
                            jQuery('#<?= $this->modal_id ?> .card' + identifier).show();
                        }
                    }
                })
            ;
        </script>
        <?php
    }

    //------------------------------------------------------------

    private function getTimeFilter()
    {
        $string_all = __('All', 'teambooking');
        $string_from = __('from', 'teambooking');
        $start_times = array();
        foreach ($this->slots_obj->getAllSlots() as $slot) {
            /* @var $slot TeamBooking_Slot */
            if (!$slot->isSoldout()) {
                $start_times[] = $this->getStartTimeOnly($slot, TRUE);
            }
        }
        if (empty($start_times)) {
            // All seems booked
            return;
        }
        $lower_bound = (int)min($start_times)->format('G');
        $upper_bound = (int)max($start_times)->format('G');
        unset($start_times);
        ?>
        <div class="ui small compact basic right floated labeled tb-icon dropdown tbk-button"
             id="tb-time-schedule-select-<?= $this->params->getInstance() ?>">
            <i class="wait tb-icon"></i>

            <div class="default text"><?= esc_html($string_all) ?></div>
            <div class="menu">
                <?php
                foreach ($this->getFilterTimesArray($lower_bound, $upper_bound) as $timeint => $time) {
                    ?>
                    <div class="item" data-value="<?= $timeint ?>">
                        <?= esc_html($string_from) ?> <?= $time ?>
                    </div>
                <?php } ?>
                <div class="tbk-divider"></div>
                <div class="item" data-value="all">
                    <?= esc_html($string_all) ?>
                </div>
            </div>
        </div>
        <script>
            jQuery('#tb-time-schedule-select-<?= $this->params->getInstance() ?>')
                .uiDropdown({
                    onChange: function (value, text, $selectedItem) {
                        var identifier = '';
                        var params = {
                            timeint: value,
                            address: jQuery('#tb-location-schedule-select-<?= $this->params->getInstance() ?>').uiDropdown('get value'),
                            coworker: jQuery('#tb-coworker-schedule-select-<?= $this->params->getInstance() ?>').uiDropdown('get value')
                        };
                        for (var key in params) {
                            if (params.hasOwnProperty(key) && params[key] !== 'all' && params[key].length !== 0 && key !== 'timeint') {
                                identifier += '[data-' + key + '="' + params[key] + '"]';
                            }
                        }
                        var prev_styles = jQuery('#<?= $this->modal_id ?> .card').attr("style");
                        if (typeof prev_styles === 'undefined') {
                            prev_styles = '';
                        }
                        jQuery('#<?= $this->modal_id ?> .card').attr("style", prev_styles + "display: none !important;");
                        if (params.timeint !== 'all') {
                            jQuery('#<?= $this->modal_id ?> .card' + identifier)
                                .filter(function () {
                                    return jQuery(this).attr("data-timeint") >= params.timeint;
                                })
                                .show();
                        } else {
                            jQuery('#<?= $this->modal_id ?> .card' + identifier).show();
                        }
                    }
                })
            ;
        </script>
        <?php
    }

}
