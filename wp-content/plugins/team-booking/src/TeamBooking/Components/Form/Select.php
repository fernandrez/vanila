<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

#TeamBookingFormSelect extends TeamBookingFormTextField
#TeamBooking_Components_Form_Select

class TeamBookingFormSelect extends TeamBookingFormTextField
{

    //------------------------------------------------------------

    protected $options = array();

    //------------------------------------------------------------

    public function addOption($option)
    {
        $this->options[] = $option;
    }

    //------------------------------------------------------------

    public function resetOptions()
    {
        $this->options = array();
    }

    //------------------------------------------------------------

    public function getOptions()
    {
        return $this->options;
    }

    //------------------------------------------------------------

    // HTML Semantic UI markup
    public function getMarkup($input_size = '')
    {
        $random = tbRandomNumber(6);
        ?>
        <div class="field <?= $this->getRequiredFieldClass() ?>">
            <label><?= $this->wrapStringForTranslations(tbUnfilterInput($this->label)) ?></label>

            <div class="ui dropdown fluid selection" id="tb-<?= $this->hook . $random ?>">
                <input type="hidden"
                       name="form_fields[<?= $this->hook ?>]" <?= $this->required ? "required='required'" : "" ?>>

                <div class="default text"></div>
                <i class="dropdown tb-icon"></i>

                <div class="menu">
                    <div class="item" data-value="<?= $this->value ?>"><?= $this->value ?></div>
                    <?php foreach ($this->options as $option) { ?>
                        <div class="item"
                             data-value="<?= $option ?>"><?= $this->wrapStringForTranslations($option) ?></div>
                    <?php } ?>
                </div>
            </div>
            <?php $this->getValidationMessageLabel() ?>
            <script>
                jQuery('#tb-<?= $this->hook . $random ?>')
                    .uiDropdown()
                ;
            </script>
        </div>
        <?php
    }

}
