<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

#TeamBookingFormRadio extends TeamBookingFormSelect
#TeamBooking_Components_Form_Radio

class TeamBookingFormRadio extends TeamBookingFormSelect
{

    // HTML Semantic UI markup
    public function getMarkup($input_size = '')
    {
        $random_append = substr(md5(rand()), 0, 8);
        ?>
        <div class="grouped field fields <?= $this->getRequiredFieldClass() ?>">
            <label
                style="display: block;"><?= $this->wrapStringForTranslations(tbUnfilterInput($this->label)) ?></label>
            <?php if (!empty($this->value)) { ?>
                <div class="field">
                    <div class="ui radio checkbox">
                        <input id="tb-radio-<?= $this->hook . "-" . $random_append ?>" type="radio"
                               value="<?= $this->value ?>" name="form_fields[<?= $this->hook ?>]" checked="checked">
                        <label
                            for="tb-radio-<?= $this->hook . "-" . $random_append ?>"><?= $this->wrapStringForTranslations($this->value) ?></label>
                    </div>
                </div>
                <?php
                $random_append = substr(md5(rand()), 0, 8);
            }
            foreach ($this->options as $option) {
                ?>
                <div class="field">
                    <div class="ui radio checkbox">
                        <input id="tb-radio-<?= $this->hook . "-" . $random_append ?>" type="radio"
                               value="<?= $option ?>" name="form_fields[<?= $this->hook ?>]">
                        <label
                            for="tb-radio-<?= $this->hook . "-" . $random_append ?>"><?= $this->wrapStringForTranslations($option) ?></label>
                    </div>
                </div>
                <?php
                $random_append = substr(md5(rand()), 0, 8);
            }
            ?>
            <?php $this->getValidationMessageLabel() ?>
        </div>
        <?php
    }

}

