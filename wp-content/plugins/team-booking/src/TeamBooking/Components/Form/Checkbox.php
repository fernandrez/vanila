<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

#TeamBookingFormCheckbox extends TeamBookingFormTextField
#TeamBooking_Components_Form_Checkbox

class TeamBookingFormCheckbox extends TeamBookingFormTextField
{
    //------------------------------------------------------------

    protected $checked;

    //------------------------------------------------------------

    public function setCheckedOn()
    {
        $this->checked = TRUE;
    }

    public function setCheckedOff()
    {
        $this->checked = FALSE;
    }

    public function isChecked()
    {
        return $this->checked;
    }

    //------------------------------------------------------------

    // HTML Semantic UI mapper
    public function getMarkup($input_size = '')
    {
        $random_append = substr(md5(rand()), 0, 8);
        ?>
        <div class="field <?= $this->getRequiredFieldClass() ?>">
            <span style="display: block;">&zwnj;</span>

            <div class="ui checkbox">
                <input id="form_fields[<?= $this->hook ?>]_<?= $random_append ?>" name="form_fields[<?= $this->hook ?>]"
                       type="checkbox"
                       value="<?= $this->value ?>" <?php checked(TRUE, $this->checked) ?> <?= $this->required ? "required='required'" : "" ?>>
                <label for="form_fields[<?= $this->hook ?>]_<?= $random_append ?>">
                    <?= $this->wrapStringForTranslations(tbUnfilterInput($this->label)) ?>
                </label>
            </div>
            <?php $this->getValidationMessageLabel() ?>
        </div>
        <?php
    }

}
