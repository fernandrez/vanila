<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

#TeamBookingFormTextarea extends TeamBookingFormTextField
#TeamBooking_Components_Form_TextArea

class TeamBookingFormTextarea extends TeamBookingFormTextField
{

    // HTML Semantic UI mapper
    public function getMarkup($input_size = '')
    {
        ?>
        <div class="field <?= $this->getRequiredFieldClass() ?>">
            <label><?= $this->wrapStringForTranslations(tbUnfilterInput($this->label)) ?></label>
            <textarea name="form_fields[<?= $this->hook ?>]"
                      style="height:inherit" <?= $this->required ? "required='required'" : "" ?>><?= $this->value ?></textarea>
            <?php $this->getValidationMessageLabel() ?>
        </div>
        <?php
    }

}
