<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

#TeamBookingFormFileUpload extends TeamBookingFormTextField
#TeamBooking_Components_Form_FileUpload

class TeamBookingFormFileUpload extends TeamBookingFormTextField
{

    //------------------------------------------------------------

    protected $file_extensions;
    protected $max_size;

    //------------------------------------------------------------

    public function setFileExtensions($extensions)
    {
        $this->file_extensions = $extensions;
    }

    public function getFileExtensions($backend = FALSE)
    {
        if (!$backend) {
            if (!$this->file_extensions) {
                // Defaults
                return ".jpg .png .jpeg .zip";
            } else {
                $array = explode(",", $this->file_extensions);
                foreach ($array as $value) {
                    if (empty($value)) {
                        continue;
                    }
                    $new_array[] = "." . trim($value) . " ";
                }

                return trim(implode($new_array));
            }
        } else {
            if (!$this->file_extensions) {
                // Defaults
                return "jpg, png, jpeg, zip";
            } else {
                return $this->file_extensions;
            }
        }
    }

    //------------------------------------------------------------


    public function setMaxSize($size)
    {
        if (is_numeric($size)) {
            $this->max_size(abs($size));
        }
    }

    public function getMaxSize()
    {
        if (!$this->max_size) {
            // default
            return 30;
        } else {
            return $this->max_size;
        }
    }

    //------------------------------------------------------------

    // HTML Semantic UI mapper
    public function getMarkup($input_size = '')
    {
        $random_append = substr(md5(rand()), 0, 8);
        ?>
        <div class="field <?= $this->getRequiredFieldClass() ?>">
            <label><?= $this->wrapStringForTranslations(tbUnfilterInput($this->label)) ?></label>

            <div class="ui action input <?= $input_size ?>">
                <input style="height:inherit;" type="text" id="_<?= $this->hook . $random_append ?>" readonly="">
                <label for="<?= $this->hook . $random_append ?>"
                       class="ui tb-icon tbk-button black tb-btn-file <?= $input_size ?>"
                       id="label_<?= $this->hook . $random_append ?>" style="display: table-cell;">
                    <input type="file" id="<?= $this->hook . $random_append ?>" name="<?= $this->hook ?>"
                           style="display: none;height: inherit;" <?= $this->required ? "required='required'" : "" ?>>
                    <i class="dashicons dashicons-upload"></i>
                </label>
            </div>
            <?php $this->getValidationMessageLabel() ?>
            <script>
                jQuery(document).on('change', '#label_<?= $this->hook . $random_append ?> :file', function () {
                    var input = jQuery(this);

                    if (navigator.appVersion.indexOf("MSIE") != -1) { // IE
                        var label = input.val();
                        input.trigger('fileselect', [label, 0]);
                    } else {
                        var label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                        var size = input.get(0).files[0].size;

                        input.trigger('fileselect', [label, size]);
                    }
                });
                jQuery('#label_<?= $this->hook . $random_append ?> :file').on('fileselect', function (event, label, size) {
                    var fileExtentionRange = '<?= $this->getFileExtensions() ?>';
                    var MAX_SIZE = <?= $this->getMaxSize() ?>; // MB
                    jQuery('#<?= $this->hook . $random_append ?>').attr('name', '<?= $this->hook ?>'); // allow upload.
                    var postfix = label.substr(label.lastIndexOf('.'));
                    if (fileExtentionRange.indexOf(postfix.toLowerCase()) > -1) {
                        if (size > 1024 * 1024 * MAX_SIZE) {
                            alert('max size: ' + MAX_SIZE + ' MB.');
                            jQuery('#<?= $this->hook . $random_append ?>').removeAttr('name'); // cancel upload file.
                        } else {
                            jQuery('#_<?= $this->hook . $random_append ?>').val(label);
                        }
                    } else {
                        alert('file type: ' + fileExtentionRange);
                        jQuery('#<?= $this->hook . $random_append ?>').removeAttr('name'); // cancel upload file.
                    }
                });
            </script>
        </div>
        <?php
    }

}

