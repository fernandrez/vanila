<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Components_Admin_ManageServices
{

    //------------------------------------------------------------

    public $action;
    private $services;
    private $settings;
    private $view;

    public function __construct($settings)
    {
        /* @var $settings TeamBookingSettings */
        $this->settings = $settings;
        $this->services = $settings->getServices();
        $this->view = new TeamBooking_Components_Admin_View();
    }

    //------------------------------------------------------------

    public function getPostBody()
    {
        ob_start();
        ?>
        <div class="tbk-wrapper">
            <div class="tbk-row">

                <div class="tbk-column tbk-span-12">
                    <div class="tbk-panel">
                        <?= $this->getServiceList() ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getServiceList()
    {
        // Grouping strings for maintainability
        $string_1 = esc_html__('assigned to you', 'teambooking');
        $string_2 = esc_html__('assigned to someone else', 'teambooking');
        $string_3 = esc_html__('disabled', 'teambooking');
        $string_4 = esc_html__('Services', 'teambooking');
        $string_6 = esc_html__('Are you sure?', 'teambooking');
        $string_7 = esc_html__('Clone service', 'teambooking');
        ob_start();
        ?>

        <div class="tbk-content">
            <?php $this->view->panelTitleAddNew($string_4, 'team-booking-new-service') ?>
            <?= $this->getNewServiceModal() ?>

            <table class="widefat" id="tbk-services-list">
                <tr class="alternate">
                    <th>
                        <input type="checkbox" id="select-all-services" style="margin:0;">
                    </th>
                    <th style="font-weight:bold"><?= esc_html__('Name', 'teambooking') ?></th>
                    <th style="font-weight:bold"><?= esc_html__('Class', 'teambooking') ?></th>
                    <th style="font-weight:bold"><?= esc_html__('Id', 'teambooking') ?></th>
                    <th style="font-weight:bold"><?= esc_html__('Color', 'teambooking') ?></th>

                    <?php if (current_user_can('manage_options')) { ?>
                        <th style="font-weight:bold"><?= esc_html__('Specific shortcode', 'teambooking') ?></th>
                        <th style="font-weight:bold"><?= esc_html__('Active', 'teambooking') ?></th>
                    <?php } ?>

                    <th style="font-weight:bold"><?= esc_html__('Participate', 'teambooking') ?></th>
                    <th style="font-weight:bold"><?= esc_html__('Actions', 'teambooking') ?></th>
                </tr>

                <?php
                foreach ($this->services as $service_id => $service) {
                    /* @var $service TeamBookingType */
                    $participation_label_id = "participation-label-" . $service->getId();
                    ?>
                    <tr>
                        <td>
                            <input type="checkbox" class="select-service"
                                   data-serviceid="<?= $service_id ?>">
                        </td>
                        <td>
                            <?= $service->getName() ?>
                        </td>

                        <td>
                            <?= ucfirst($service->getClass()) ?>
                        </td>

                        <td style="color: grey;font-style: italic;">
                            <?= $service->getId() ?>
                        </td>

                        <td>
                            <div class="ui circular empty label"
                                 style="background-color:<?= $service->getServiceColor() ?>"></div>
                        </td>

                        <?php if (current_user_can('manage_options')) { ?>
                            <td>
                                <code>[tb-calendar booking="<?= $service->getId() ?>"]</code>
                            </td>

                            <td>
                                <div class="ui slider checkbox" id="toggle-service-activation-<?= $service->getId() ?>">
                                    <input type="checkbox" name="toggle-service-activation-<?= $service->getId() ?>"
                                        <?= checked($service->isActive()) ?>>
                                </div>

                                <!-- toggle service activation script -->
                                <script>
                                    jQuery('#toggle-service-activation-<?= $service->getId() ?>')
                                        .checkbox({
                                            fireOnInit: false,
                                            onChecked: function () {
                                                tbToggleService('activate', '<?= $service->getId() ?>');
                                                jQuery('#toggle-service-participation-<?= $service->getId() ?>').show();
                                                jQuery('#<?= $participation_label_id ?>').hide();
                                            },
                                            onUnchecked: function () {
                                                tbToggleService('deactivate', '<?= $service->getId() ?>');
                                                jQuery('#toggle-service-participation-<?= $service->getId() ?>').hide();
                                                jQuery('#<?= $participation_label_id ?>').show();
                                            }
                                        })
                                    ;
                                </script>
                            </td>
                        <?php } ?>

                        <td>
                            <?php
                            if ($service->isActive()) {
                                $display_toggle = TRUE;
                            } else {
                                $display_toggle = FALSE;
                            }

                            if ($service->getCoworkerForDirectAssignment() == get_current_user_id() && $service->getAssignmentRule() == 'direct') {
                                ?>
                                <div class="ui mini green label"
                                     id="toggle-service-participation-<?= $service->getId() ?>" <?= !$display_toggle ? 'style="display:none;"' : '' ?>>
                                    <?= $string_1 ?>
                                </div>

                            <?php } elseif ($service->getAssignmentRule() == 'direct') { ?>
                                <div class="ui mini red label"
                                     id="toggle-service-participation-<?= $service->getId() ?>" <?= !$display_toggle ? 'style="display:none;"' : '' ?>>
                                    <?= $string_2 ?>
                                </div>
                            <?php

                            } else {
                            ?>
                                <div class="ui slider checkbox"
                                     id="toggle-service-participation-<?= $service->getId() ?>" <?= !$display_toggle ? 'style="display:none;"' : '' ?>>
                                    <input type="checkbox" name="toggle-service-participation-<?= $service->getId() ?>"
                                        <?= checked($this->settings->getCoworkerData(get_current_user_id())->getCustomEventSettings($service->getId())->isParticipate(), TRUE) ?>>
                                </div>

                                <!-- toggle service participation script -->
                                <script>
                                    jQuery('#toggle-service-participation-<?= $service->getId() ?>')
                                        .checkbox({
                                            fireOnInit: false,
                                            onChecked: function () {
                                                tbToggleService('activate', '<?= $service->getId() ?>', true);
                                            },
                                            onUnchecked: function () {
                                                tbToggleService('deactivate', '<?= $service->getId() ?>', true);
                                            }
                                        })
                                    ;
                                </script>
                                <?php
                            }
                            ?>

                            <div class="ui mini label"
                                 id="<?= $participation_label_id ?>" <?= $display_toggle ? 'style="display:none;"' : '' ?>>
                                <?= $string_3 ?>
                            </div>
                        </td>

                        <td style="padding:5px;">
                            <a class="tbk-service-action-email ui circular label mini horizontal"
                               title="<?= esc_html__('Email', 'teambooking') ?>"
                               href="<?= admin_url('admin.php?page=team-booking&tab=events&event=' . $service_id) . "&email=1" ?>">
                                <?php $this->view->dashicon('dashicons-email') ?>
                            </a>

                            <?php if (current_user_can('manage_options')) { ?>
                                <a class="tbk-service-action-form ui circular label mini horizontal"
                                   title="<?= esc_html__('Reservation form', 'teambooking') ?>"
                                   href="<?= admin_url('admin.php?page=team-booking&tab=events&event=' . $service_id) . "&form=1" ?>">
                                    <?php $this->view->dashicon('dashicons-editor-ul') ?>
                                </a>
                            <?php } ?>

                            <?php if (current_user_can('manage_options')) { ?>
                                <a class="tbk-service-action-delete ui circular label mini horizontal team-booking-delete-service"
                                   href="#"
                                   title="<?= esc_html__('Delete', 'teambooking') ?>"
                                   data-servicename="<?= $service->getName() ?>"
                                   data-serviceid="<?= $service_id ?>">
                                    <?php $this->view->dashicon('dashicons-trash') ?>
                                </a>

                                <a class="tbk-service-action-clone ui circular label mini horizontal team-booking-clone-service"
                                   href="#"
                                   title="<?= esc_html__('Clone', 'teambooking') ?>"
                                   data-serviceid="<?= $service_id ?>">
                                    <?php $this->view->dashicon('dashicons-admin-page') ?>
                                </a>

                                <a class="tbk-service-action-settings ui circular label mini horizontal"
                                   title="<?= esc_html__('Settings', 'teambooking') ?>"
                                   href="<?= admin_url('admin.php?page=team-booking&tab=events&event=' . $service_id) ?>">
                                    <?php $this->view->dashicon('dashicons-admin-tools') ?>
                                </a>
                            <?php } ?>

                            <?php if (!$service->isClass('service')) { ?>
                                <a class="tbk-service-action-calendar ui circular label mini horizontal"
                                   title="<?= esc_html__('Your Google Calendar', 'teambooking') ?>"
                                   href="<?= admin_url('admin.php?page=team-booking&tab=events&event=' . $service_id) . "&gcal=1" ?>">
                                    <?php $this->view->dashicon('dashicons-calendar') ?>
                                </a>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>

            <!-- Select services scripts -->
            <script>
                jQuery('#select-all-services').change(function () {
                    if (this.checked) {
                        jQuery('.select-service').prop('checked', true);
                    } else {
                        jQuery('.select-service').prop('checked', false);
                    }
                });
                jQuery('.select-service, #select-all-services').change(function () {
                    var checked = jQuery('.select-service:checked').length;
                    if (checked > 0) {
                        jQuery('#selected-services-actions').show();
                        jQuery('#selected-services-number').html(checked);
                    } else {
                        jQuery('#selected-services-actions').hide();
                        jQuery('#selected-services-number').html(checked);
                    }
                });
            </script>

            <!-- Select services notification -->
            <div class="tablenav">
                <div id="selected-services-actions" style="float:left;display: none;" class="displaying-num">
                    <span
                        id="selected-services-number"></span> <?= strtolower(esc_html__('selected', 'teambooking')) ?>
                    (<a
                        id="remove-selected-services"
                        href="#"><!--
                     --><?= strtolower(__('delete', 'teambooking')) ?><!--
                 --></a>)
                </div>
            </div>

        </div>

        <!--Confirmation Modal Markup-->
        <?php
        $service_name_span = "<span class='service-name'></span>";
        $modal_content = sprintf(esc_html__('You are going to permanently delete %s', 'teambooking'), $service_name_span);
        $this->view->small_modal(
            'tb-booking-delete-modal',
            $string_6,
            $modal_content,
            esc_html__('Yes', 'teambooking'),
            esc_html__('No', 'teambooking')
        );
        unset($modal_content);
        ?>

        <!--Confirmation Modal Markup (all services)-->
        <?php
        $service_name_span = "<span class='selected-services'>" . esc_html__('all the selected services', 'teambooking') . "</span>";
        $modal_content = sprintf(esc_html__('You are going to permanently delete %s', 'teambooking'), $service_name_span);
        $this->view->small_modal(
            'tb-service-delete-selected-modal',
            $string_6,
            $modal_content,
            esc_html__('Yes', 'teambooking'),
            esc_html__('No', 'teambooking')
        );
        unset($modal_content);
        ?>

        <!--Clone Service Modal Markup-->
        <?php ob_start() ?>
        <label><?= __('Please provide a new service id', 'teambooking') ?></label>
        <input class="large-text" type="text" id="tb-booking-clone-service-new-id" value="">

        <p id="tbk-clone-id-already-existant" style="display:none;">
            <?= esc_html__('This service id is already in use, please provide a fresh one.', 'teambooking') ?>
        </p>
        <?php
        $modal_content = ob_get_clean();
        $this->view->small_modal(
            'tb-booking-clone-service-modal',
            $string_7,
            $modal_content,
            esc_html__('Clone', 'teambooking'),
            esc_html__('Cancel', 'teambooking')
        );
        unset($modal_content);
        ?>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getNewServiceModal()
    {
        ob_start();
        ?>
        <!--New Service Modal Markup-->
        <div class="ui small modal" id="tb-booking-new-service-modal">
            <i class="close tb-icon"></i>

            <div class="header">
                <?= esc_html__('New service', 'teambooking') ?>
            </div>

            <div class="content" style="width:calc(100% - 4rem)">
                <ul class="tbk-list">
                    <li>
                        <h4><?= esc_html__('Name', 'teambooking') ?></h4>
                        <input class="large-text" type="text" id="tb-booking-new-service-name" value=""
                               placeholder="e.g. Dental Care">

                        <p id="tbk-new-name-already-existant" style="display:none;">
                            <?= esc_html__('This name is already in use, please provide a fresh one.', 'teambooking') ?>
                        </p>
                    </li>

                    <li>
                        <h4><?= esc_html__('Id', 'teambooking') ?></h4>
                        <input class="large-text" type="text" id="tb-booking-new-service-id" value=""
                               placeholder="e.g. dental-care">

                        <p id="tbk-new-id-already-existant" style="display:none;">
                            <?= esc_html__('This id is already in use, please provide a fresh one.', 'teambooking') ?>
                        </p>
                    </li>

                    <li>
                        <h4><?= esc_html__('Class', 'teambooking') ?></h4>
                        <fieldset>
                            <label>
                                <input type="radio" name="class" value="event" checked=""/>
                                <span><?= esc_html__('Event', 'teambooking') ?></span>

                                <p>
                                    <?= esc_html__('A conference, a music lesson, hotel rooms, and so on. Everything that involves tickets and/or attendees, is an Event Class.', 'teambooking') ?>
                                </p>
                            </label>
                            <label>
                                <input type="radio" name="class" value="appointment"/>
                                <span><?= esc_html__('Appointment', 'teambooking') ?></span>

                                <p>
                                    <?= esc_html__('Calendar Owners can be techicians, psychologists, medics. This Class is made for that.', 'teambooking') ?>
                                </p>
                            </label>
                            <label>
                                <input type="radio" name="class" value="service"/>
                                <span><?= esc_html__('Unscheduled service', 'teambooking') ?></span>

                                <p>
                                    <?= esc_html__('A service with no scheduling needs. Think about support tickets, estimate request, and so on. Instead of the calendar, just a plain request form will be shown.', 'teambooking') ?>
                                </p>
                            </label>
                        </fieldset>
                    </li>
                </ul>
            </div>

            <div class="actions">
                <div class="ui positive button" style="height:auto">
                    <?= esc_html__('Add', 'teambooking') ?>
                </div>
                <div class="ui deny black button" style="height:auto">
                    <?= esc_html__('Cancel', 'teambooking') ?>
                </div>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

}
