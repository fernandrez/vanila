<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Components_Admin_Overview
{

    //------------------------------------------------------------

    public $reservations;
    public $list_title;
    public $filter_service;
    public $new_reservation_ids;

    /** @var TeamBooking_AdminFilterObject */
    public $filter_object;
    private $settings;
    private $view;

    public function __construct($settings)
    {
        /* @var $settings TeamBookingSettings */
        $this->settings = $settings;
        $this->view = new TeamBooking_Components_Admin_View();
    }

    //------------------------------------------------------------

    public function getPostBody()
    {
        $this->filterSearchTerms();
        $this->sortList();
        ob_start();
        ?>
        <div class="tbk-wrapper" xmlns="http://www.w3.org/1999/html">

            <div class="tbk-row">
                <?= $this->getDataExportForms() ?>
                <div class="tbk-column tbk-span-9">

                    <?php if (!$this->filter_object->getPendingReservations()) { ?>
                        <div class="tbk-row">
                            <?= $this->getStats() ?>
                        </div>
                    <?php } ?>

                    <div class="tbk-row">
                        <div class="tbk-column tbk-span-12">
                            <div class="tbk-panel">
                                <?= $this->getReservationList() ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tbk-column tbk-span-3">
                    <div class="tbk-panel">
                        <?= $this->getFilters() ?>
                    </div>
                    <div class="tbk-panel">
                        <?= $this->getLastErrors() ?>
                    </div>
                </div>
            </div>

        </div>
        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    public function getFilters()
    {
        $warning_icon = '<span style="color: rgb(251, 175, 74)" class="dashicons dashicons-warning"></span>';
        ob_start();
        ?>

        <?=
        $this->view->panelTitleWithLabel(
            esc_html__('Filters', 'teambooking'),
            esc_html__('clean', 'teambooking'),
            'tbk-filters-clean'
        ) ?>

        <script>
            jQuery('.tbk-filters-clean').click(function (e) {
                e.preventDefault();
                window.location.replace("<?=admin_url('/admin.php?page=team-booking&tab=overview')?>");
            })
        </script>

        <ul class="tbk-list">
            <li>
                <?= $this->view->segmentHeader(
                    esc_html__('Services', 'teambooking'),
                    "",
                    tbCheckServiceIdExistance($this->filter_service) ? $warning_icon : ""
                ) ?>

                <select id="tbk-overview-filter-service-select">

                    <?php
                    foreach ($this->settings->getServices() as $service) {
                        echo $this->view->select_option($service->getName(), $service->getId(), $service->getId(), $this->filter_service);
                    }
                    echo '<option disabled>──────────</option>';
                    echo $this->view->select_option(esc_html__('All', 'teambooking'), '', tbCheckServiceIdExistance($this->filter_service), FALSE);
                    ?>
                </select>

                <script>
                    jQuery('#tbk-overview-filter-service-select').change(function () {
                        var service_id = this.value;
                        window.location.replace("<?=admin_url('/admin.php?page=team-booking&tab=overview&service=')?>" + service_id);
                    });
                </script>
            </li>

            <li>
                <?= $this->view->segmentHeader(
                    esc_html__('Status', 'teambooking'),
                    "",
                    $this->filter_object->getStatus() === NULL ? "" : $warning_icon
                ) ?>

                <?php $filter_entries = clone $this->filter_object; ?>

                <select id="tbk-overview-filter-status-select">
                    <?php $filter_entries->setStatus('confirmed') ?>
                    <?= $this->view->select_option(ucfirst(esc_html__('confirmed', 'teambooking')), $filter_entries->encode(), $this->filter_object->getStatus(), 'confirmed') ?>
                    <?php $filter_entries->setStatus('cancelled') ?>
                    <?= $this->view->select_option(ucfirst(esc_html__('cancelled', 'teambooking')), $filter_entries->encode(), $this->filter_object->getStatus(), 'cancelled') ?>
                    <?php $filter_entries->setStatus('waiting_approval') ?>
                    <?= $this->view->select_option(ucfirst(esc_html__('waiting for approval', 'teambooking')), $filter_entries->encode(), $this->filter_object->getStatus(), 'waiting_approval') ?>
                    <option disabled>──────────</option>
                    <?php $filter_entries->setStatus(NULL) ?>
                    <?= $this->view->select_option(esc_html__('All', 'teambooking'), $filter_entries->encode(), $this->filter_object->getStatus(), NULL) ?>
                </select>
                <script>
                    jQuery('#tbk-overview-filter-status-select').change(function () {
                        var service_id = jQuery('#tbk-overview-filter-service-select').val();
                        var filter = this.value;
                        window.location.replace("<?=admin_url('/admin.php?page=team-booking&tab=overview&service=')?>" + service_id + '&filter=' + filter);
                    });
                </script>
            </li>

            <?php if (current_user_can('manage_options')) { ?>
                <li>
                    <?= $this->view->segmentHeader(
                        esc_html__('Coworker', 'teambooking'),
                        "",
                        $this->filter_object->getCoworker() === NULL ? "" : $warning_icon
                    ) ?>

                    <?php $filter_entries = clone $this->filter_object; ?>

                    <select id="tbk-overview-filter-coworker-select">
                        <?php
                        foreach (tbGetAllCoworkersList() as $id => $coworker) {
                            $filter_entries->setCoworker($id);
                            echo $this->view->select_option($coworker['name'], $filter_entries->encode(), $this->filter_object->getCoworker(), $id);
                        }
                        ?>
                        <option disabled>──────────</option>
                        <?php $filter_entries->setCoworker(NULL) ?>
                        <?= $this->view->select_option(esc_html__('All', 'teambooking'), $filter_entries->encode(), $this->filter_object->getCoworker(), NULL) ?>
                    </select>
                    <script>
                        jQuery('#tbk-overview-filter-coworker-select').change(function () {
                            var service_id = jQuery('#tbk-overview-filter-service-select').val();
                            var filter = this.value;
                            window.location.replace("<?=admin_url('/admin.php?page=team-booking&tab=overview&service=')?>" + service_id + '&filter=' + filter);
                        });
                    </script>
                </li>
            <?php } ?>

            <li>
                <?= $this->view->segmentHeader(
                    esc_html__('Payment status', 'teambooking'),
                    "",
                    $this->filter_object->getPaymentStatus() === NULL ? "" : $warning_icon
                ) ?>

                <?php $filter_entries = clone $this->filter_object; ?>

                <select id="tbk-overview-filter-payment-select">
                    <?php $filter_entries->setPaymentStatus('paid') ?>
                    <?= $this->view->select_option(ucfirst(esc_html__('paid', 'teambooking')), $filter_entries->encode(), $this->filter_object->getPaymentStatus(), 'paid') ?>
                    <?php $filter_entries->setPaymentStatus('not_paid') ?>
                    <?= $this->view->select_option(ucfirst(esc_html__('not paid', 'teambooking')), $filter_entries->encode(), $this->filter_object->getPaymentStatus(), 'not_paid') ?>
                    <option disabled>──────────</option>
                    <?php $filter_entries->setPaymentStatus(NULL) ?>
                    <?= $this->view->select_option(esc_html__('All', 'teambooking'), $filter_entries->encode(), $this->filter_object->getPaymentStatus(), NULL) ?>
                </select>
                <script>
                    jQuery('#tbk-overview-filter-payment-select').change(function () {
                        var service_id = jQuery('#tbk-overview-filter-service-select').val();
                        var filter = this.value;
                        window.location.replace("<?=admin_url('/admin.php?page=team-booking&tab=overview&service=')?>" + service_id + '&filter=' + filter);
                    });
                </script>
            </li>

            <li>
                <?= $this->view->segmentHeader(esc_html__('Pending payments', 'teambooking')) ?>

                <?php
                $filter_entries = clone $this->filter_object;
                $filter_entries->setPendingReservations(!($this->filter_object->getPendingReservations()));
                $filter_entries->setIds(NULL);
                $filter_entries->setSearchTerm(NULL);
                ?>
                <a href="<?= admin_url('/admin.php?page=team-booking&tab=overview&service=' . $this->filter_service) . '&filter=' . $filter_entries->encode() ?>"
                   class="ui mini <?= $this->filter_object->getPendingReservations() ? 'green' : '' ?> label"
                   style="text-decoration: none;">
                    <?= esc_html__('show pending payments', 'teambooking') ?>
                </a>

            </li>

            <li>
                <?= $this->view->segmentHeader(esc_html__('Show records per page', 'teambooking')) ?>

                <table style="width:100%">
                    <tr>
                        <?php
                        $filter_entries = clone $this->filter_object;
                        $filter_entries->setMaxEntries(15);
                        ?>
                        <td>
                            <a class="<?= ($this->filter_object->getMaxEntries() == 15) ? 'button-primary' : 'button-secondary' ?>"
                               style="width: 100%;text-align: center;"
                               href="<?= admin_url('/admin.php?page=team-booking&tab=overview&service=' . $this->filter_service) . '&filter=' . $filter_entries->encode() ?>">15</a>
                        </td>
                        <?php
                        $filter_entries->setMaxEntries(30);
                        ?>
                        <td>
                            <a class="<?= ($this->filter_object->getMaxEntries() == 30) ? 'button-primary' : 'button-secondary' ?>"
                               style="width: 100%;text-align: center;"
                               href="<?= admin_url('/admin.php?page=team-booking&tab=overview&service=' . $this->filter_service) . '&filter=' . $filter_entries->encode() ?>">30</a>
                        </td>
                        <?php
                        $filter_entries->setMaxEntries(60);
                        ?>
                        <td>
                            <a class="<?= ($this->filter_object->getMaxEntries() == 60) ? 'button-primary' : 'button-secondary' ?>"
                               style="width: 100%;text-align: center;"
                               href="<?= admin_url('/admin.php?page=team-booking&tab=overview&service=' . $this->filter_service) . '&filter=' . $filter_entries->encode() ?>">60</a>

                        </td>
                        <?php
                        unset($filter_entries);
                        ?>
                    </tr>
                </table>
            </li>
        </ul>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    public function getReservationList()
    {
        $timezone = tbGetTimezone();
        ob_start();
        ?>
        <div class="tbk-content">
            <h2 style="display: inline-block;">
                <?= $this->list_title ?>

                <a href="<?= admin_url('admin.php?page=team-booking&tab=events&add_event=1') ?>"
                   class="add-new-h2 tb-get-csv">
                    <?= esc_html__('Export CSV', 'teambooking') ?>
                </a>

                <?php if (class_exists('ZipArchive')) { ?>
                    <a href="<?= admin_url('admin.php?page=team-booking&tab=events&add_event=1') ?>"
                       class="add-new-h2 tb-get-xlsx">
                        <?= esc_html__('Export XLSX', 'teambooking') ?>
                    </a>

                <?php } else { ?>
                    <div class="add-new-h2 inactive">
                        <?= esc_html__('Export XLSX', 'teambooking') ?>
                    </div>
                <?php } ?>

                <a href="#" class="add-new-h2"
                   id="delete_all_tb_reservations" <?= $this->filter_object->getPendingReservations() ? 'data-pending="1"' : '' ?>>
                    <?= esc_html__('Delete All', 'teambooking') ?>
                </a>
            </h2>

            <script>
                jQuery('#delete_all_tb_reservations').click(function (e) {
                    e.preventDefault();
                });
            </script>

            <form style="display:inline;float: right;margin-top: 10px;" method="POST"
                  id="tbk-overview-search"
                  action="<?= admin_url('admin-post.php') ?>">
                <?php
                if ($this->filter_object->getSearchTerm() != NULL) {
                    $clean_search_filter = clone $this->filter_object;
                    $clean_search_filter->setSearchTerm(NULL);
                    $clean_search_filter->setIds(NULL);
                    ?>
                    <span class="tbk-setting-alert"
                          style="padding: 3px 3px 3px 10px;vertical-align: middle;margin-right: 10px;">
                        <?= esc_html__('Results for', 'teambooking') ?>
                        <span>
                            <?= $this->filter_object->getSearchTerm() ?>
                        </span>
                        <a href="<?= admin_url('/admin.php?page=team-booking&tab=overview&service=' . $this->filter_service) . '&filter=' . $clean_search_filter->encode() ?>"
                           class="dashicons dashicons-no-alt" style="vertical-align:text-top;font-size: 19px;"></a>
                    </span>
                <?php } ?>
                <input type="hidden" name="filter" value="<?= $this->filter_object->encode() ?>">
                <input type="hidden" name="action" value="option_submit">
                <?php wp_nonce_field('team_booking_options_verify') ?>
                <span class="dashicons dashicons-search" style="vertical-align: middle;"></span>
                <input type="text" class="regular-text" name="reservation_search" style="vertical-align: middle;"
                       placeholder="<?= esc_html__('Search...', 'teambooking') ?>">
            </form>

            <?php if ($this->filter_object->getPendingReservations()) { ?>
                <div class="tbk-setting-notice">
                    <span><?= esc_html__('Currently showing pending payments reservations.', 'teambooking') ?></span>
                    <?= esc_html__("All the reservations for which an immediate payment is required but not done yet, will be listed here. A pending reservation will be automatically removed according to the maximum pending time setting. If you manually remove it, the slot/tickets will be available again for booking. No communication will be sent back to customer.", 'teambooking') ?>
                </div>
            <?php } ?>


            <!--Confirmation Modal Markup-->
            <?php ob_start() ?>
            <p>
                <?= esc_html__('All the reservations for all the services, regardless of current filtering options, will be removed from the database. This action is permanent.', 'teambooking') ?>
            </p>

            <p>
                <?= esc_html__('Please note: non-expired-nor-cancelled Appointments Classes will stay booked, non-expired-nor-cancelled Events Classes will regain the tickets, but customers and coworkers will not be notified of that.', 'teambooking') ?>
            </p>
            <?php
            $modal_content = ob_get_clean();
            $this->view->small_modal(
                'tb-delete-all-reservations-modal',
                esc_html__('Are you sure?', 'teambooking'),
                $modal_content,
                esc_html__('Yes', 'teambooking'),
                esc_html__('No', 'teambooking')
            );
            unset($modal_content)
            ?>

            <table class="widefat" id="tbk-reservations-overview">
                <tbody>
                <tr class="alternate">
                    <th>
                        <input type="checkbox" id="select-all-reservations" style="margin:0;">
                    </th>
                    <th style="font-weight:bold"><?= esc_html__('Service', 'teambooking') ?></th>
                    <th style="font-weight:bold"><?= esc_html__('Who', 'teambooking') ?></th>

                    <?php
                    $filter_entries = clone $this->filter_object;
                    $filter_entries->setSortBy('when');
                    ?>
                    <th style="font-weight:bold">
                        <?php if ($this->filter_object->getSortBy() == 'when') { ?>
                            <?= esc_html__('When', 'teambooking') ?>
                            <span class="dashicons dashicons-arrow-down"
                                  style="line-height:inherit;height:auto;"></span>
                        <?php } else { ?>
                            <a href="<?= admin_url('/admin.php?page=team-booking&tab=overview&service=' . $this->filter_service) . '&filter=' . $filter_entries->encode() ?>">
                                <?= esc_html__('When', 'teambooking') ?>
                            </a>
                        <?php } ?>
                    </th>

                    <?php $filter_entries->setSortBy('booked'); ?>
                    <th style="font-weight:bold">
                        <?php if ($this->filter_object->getSortBy() == 'booked') { ?>
                            <?= esc_html__('Date of reservation', 'teambooking') ?>
                            <span class="dashicons dashicons-arrow-down"
                                  style="line-height:inherit;height:auto;"></span>
                        <?php } else { ?>
                            <a href="<?= admin_url('/admin.php?page=team-booking&tab=overview&service=' . $this->filter_service) . '&filter=' . $filter_entries->encode() ?>">
                                <?= esc_html__('Date of reservation', 'teambooking') ?>
                            </a>
                        <?php } ?>
                    </th>

                    <th style="font-weight:bold"><?= esc_html__('Status', 'teambooking') ?></th>
                    <th style="font-weight:bold"><?= esc_html__('Payment Status', 'teambooking') ?></th>

                    <?php if (current_user_can('manage_options')) { ?>
                        <th style="font-weight:bold"><?= esc_html__('Coworker', 'teambooking') ?></th>
                    <?php } ?>

                    <th style="font-weight:bold"><?= esc_html__('Actions', 'teambooking') ?></th>
                </tr>

                <?php
                // Skipping reservations if necessary
                $this->keepOnlyFilteredReservations();
                // Pagination parameters
                $last_page = count($this->reservations) == 0 ? 1 : ceil(count($this->reservations) / $this->filter_object->getMaxEntries());
                if ($this->filter_object->getPage() > $last_page) {
                    $this->filter_object->setPage($last_page);
                }
                $first_listed_log = ($this->filter_object->getPage() - 1) * $this->filter_object->getMaxEntries() + 1;
                $last_listed_log = $first_listed_log + $this->filter_object->getMaxEntries() - 1;
                $counter = 1;
                // Log list
                foreach ($this->reservations as $id => $log) {
                    // This skips old reservations before v.1.2 if present
                    if (!$log instanceof TeamBooking_ReservationData) {
                        continue;
                    }
                    /** @var $log TeamBooking_ReservationData */
                    $time = $log->getCreationInstant(); // UTC, seconds
                    // Pagination logic
                    if ($counter < $first_listed_log || $counter > $last_listed_log) {
                        $counter++;
                        continue;
                    } else {
                        $counter++;
                    }
                    if (!is_null($log->getSlotStart())) {
                        $date_time_object = new DateTime($log->getSlotStart());
                        $date_time_object->setTimezone($timezone);
                        $when_value = date_i18n_tb(get_option('date_format'), $date_time_object->getTimestamp() + $date_time_object->getOffset())
                            . " "
                            . date_i18n_tb(get_option('time_format'), $date_time_object->getTimestamp() + $date_time_object->getOffset());
                    } else {
                        $when_value = esc_html__('Unscheduled', 'teambooking');
                    }
                    $date_time_object = DateTime::createFromFormat('U', $time);
                    $date_time_object->setTimezone($timezone);
                    $date_time_of_reservation_value = date_i18n_tb(get_option('date_format'), $date_time_object->getTimestamp() + $date_time_object->getOffset())
                        . " "
                        . date_i18n_tb(get_option('time_format'), $date_time_object->getTimestamp() + $date_time_object->getOffset());
                    ?>
                    <tr <?= in_array($id, $this->new_reservation_ids) ? 'class="new-reservation"' : '' ?>>
                        <td>
                            <input type="checkbox" class="select-reservation"
                                   data-reservationid="<?= $id ?>" <?= $this->filter_object->getPendingReservations() ? 'data-pending="1"' : '' ?>>
                        </td>
                        <!-- Service name column -->
                        <td>
                            <?php if (tbCheckServiceIdExistance($log->getServiceId())) { ?>
                                <a href="<?= get_admin_url() . 'admin.php?page=team-booking&tab=events&event=' . $log->getServiceId() ?>">
                                    <?= $log->getServiceName() ?>
                                </a>
                            <?php } else { ?>
                                <?= $log->getServiceName() ?>
                            <?php } ?>
                        </td>
                        <!-- Who column -->
                        <td>
                            <?= $log->getCustomerDisplayName() ?>
                        </td>
                        <!-- When column -->
                        <td>
                            <?= $when_value ?>
                        </td>
                        <!-- Date of reservation column -->
                        <td>
                            <?= $date_time_of_reservation_value ?>
                        </td>
                        <!-- Reservation status column -->
                        <td>
                            <?php
                            try {
                                $this->settings->getService($log->getServiceId());
                                if ($this->filter_object->getPendingReservations()) {
                                    echo '<div class="ui mini yellow label">' . esc_html__('pending', 'teambooking') . '</div>';
                                } else {
                                    echo $log->isConfirmed() && $log->getServiceClass() != 'service' ? '<div class="ui mini label">' . esc_html__('confirmed', 'teambooking') . '</div>' : '';
                                    echo $log->isConfirmed() && $log->getServiceClass() == 'service' ? '<div class="ui mini blue label">' . esc_html__('todo', 'teambooking') . '</div>' : '';
                                    echo $log->isDone() ? '<div class="ui mini label">' . esc_html__('done', 'teambooking') . '</div>' : '';
                                    echo $log->isPending() ? '<div class="ui mini yellow label">' . esc_html__('pending', 'teambooking') . '</div>' : '';
                                    echo $log->isCancelled() ? '<div class="ui mini red label">' . esc_html__('cancelled', 'teambooking') . '</div>' : '';
                                    echo $log->isWaitingApproval() ? '<div class="ui mini yellow label">' . esc_html__('waiting for approval', 'teambooking') . '</div>' : '';
                                }
                            } catch (Exception $ex) {
                                echo '<div class="ui mini black label">' . esc_html__('Service removed', 'teambooking') . '</div>';
                            }
                            ?>
                        </td>
                        <!-- Payment status column -->
                        <td>
                            <?php
                            if ($this->filter_object->getPendingReservations()) {
                                echo '<div class="ui mini yellow label">' . esc_html__('pending', 'teambooking') . '</div>';
                                if ($log->getPaymentGateway()) {
                                    $this->settings->getPaymentGatewaySettingObject($log->getPaymentGateway())->getLabel();
                                }
                            } else {
                                if ($log->isPaid()) {
                                    echo '<div class="ui mini green label">' . esc_html__('paid', 'teambooking') . '</div>';
                                    if ($log->getPaymentGateway()) {
                                        $this->settings->getPaymentGatewaySettingObject($log->getPaymentGateway())->getLabel();
                                    }
                                } elseif ($log->getPrice() > 0 && $this->settings->thereIsAtLeastOneActivePaymentGateway()) {
                                    if ($log->getPaymentMustBeManuallyConfirmedOnPayPal()) {
                                        echo '<div class="ui mini yellow label">' . esc_html__('must be manually confirmed on PayPal', 'teambooking') . '</div>';
                                    } else {
                                        echo '<div class="ui mini red label">' . esc_html__('not paid', 'teambooking') . '</div>';
                                    }
                                } else {
                                    echo '<div class="ui mini label">' . esc_html__('unhandled', 'teambooking') . '</div>';
                                }
                            }
                            ?>
                        </td>
                        <?php if (current_user_can('manage_options')) { ?>
                            <!-- Coworker column -->
                            <td>
                                <?= ucwords($this->settings->getCoworkerData($log->getCoworker())->getDisplayName()) ?>
                                <?= ($log->getCoworker() == get_current_user_id()) ? "(" . esc_html__('you', 'teambooking') . ")" : "" ?>
                            </td>
                        <?php } ?>
                        <!-- Actions column -->
                        <td>
                            <?= $this->getReservationActions($id, $log, $this->filter_object->getPendingReservations()) ?>
                        </td>
                    </tr>
                <?php $this->getReservationDetailsModal($log, $time, $when_value) ?>
                <?php
                if (
                (
                    $log->getPrice() > 0
                    && !$this->filter_object->getPendingReservations()
                )
                || $log->getServiceClass() == 'service'
                )
                { ?>

                    <!-- modify reservation modal markup -->
                    <div class="ui small modal" id='tb-modal-modify-<?= $time ?>'>
                        <i class="close tb-icon"></i>

                        <div class="header">
                            <?= esc_html__('Modify reservation', 'teambooking') ?>
                        </div>
                        <form action="<?= admin_url('admin-post.php') ?>" method="POST"
                              id="tb-modal-modify-form-<?= $time ?>">
                            <input type="hidden" name="action" value="option_submit">
                            <?php wp_nonce_field('team_booking_options_verify') ?>
                            <input type="hidden" name="reservation_database_id" value="<?= $id ?>">

                            <div class="content" style="width: calc(100% - 4rem);">

                                <?php if (current_user_can('manage_options') && !$this->filter_object->getPendingReservations()) { ?>
                                    <div style="display: inline-block;padding-right: 30px;">
                                        <h4 style="margin: 0">
                                            <?= esc_html__('Payment status', 'teambooking') ?>
                                        </h4>

                                        <div style="font-style: italic">
                                            <?= $this->view->radio(esc_html__('paid', 'teambooking'), 'reservation_payment_status', 1, $log->isPaid(), TRUE) ?>
                                            <br>
                                            <?= $this->view->radio(esc_html__('not paid', 'teambooking'), 'reservation_payment_status', 0, $log->isPaid(), FALSE) ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($log->getServiceClass() == 'service'
                                    && (
                                        $log->isConfirmed()
                                        ||
                                        $log->isDone()
                                    )
                                ) { ?>
                                    <div style="display: inline-block;margin-right: 20px">
                                        <h4 style="margin: 0">
                                            <?= esc_html__('Status', 'teambooking') ?>
                                        </h4>

                                        <div style="font-style: italic">
                                            <?= $this->view->radio(esc_html__('done', 'teambooking'), 'reservation_unscheduled_status', 1, $log->isDone(), TRUE) ?>
                                            <br>
                                            <?= $this->view->radio(esc_html__('todo', 'teambooking'), 'reservation_unscheduled_status', 0, $log->isDone(), FALSE) ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="actions">
                                <div class="ui deny black button"
                                     style="height:auto"><?= esc_html__('Cancel', 'teambooking') ?></div>
                                <button type="submit" name="tb_modify_reservation_save" class="ui positive button"
                                        style="height:auto">
                                    <?= esc_html__('Save changes', 'teambooking') ?>
                                </button>
                            </div>
                        </form>
                    </div>
                    <script>
                        jQuery('#tb-modal-modify-<?= $time ?>')
                            .uiModal({
                                blurring: true,
                                transition: 'fade up'
                            })
                            .uiModal('attach events', '#tb-modify-<?= $time ?>', 'show')
                            .uiModal('setting', 'onApprove', function ($clicked) {
                                $clicked.addClass('loading');
                                return false;
                            })
                        ;
                    </script>
                <?php } ?>
                    <script>
                        jQuery('#tb-modify-<?= $time ?>').click(function (event) {
                            event.preventDefault();
                        });
                    </script>
                <?php } ?>
                </tbody>
            </table>

            <!-- Select reservations scripts -->
            <script>
                jQuery('#select-all-reservations').change(function () {
                    if (this.checked) {
                        jQuery('.select-reservation').prop('checked', true);
                    } else {
                        jQuery('.select-reservation').prop('checked', false);
                    }
                });
                jQuery('.select-reservation, #select-all-reservations').change(function () {
                    var checked = jQuery('.select-reservation:checked').length;
                    if (checked > 0) {
                        jQuery('#selected-reservations-actions').show();
                        jQuery('#selected-reservation-number').html(checked);
                    } else {
                        jQuery('#selected-reservations-actions').hide();
                        jQuery('#selected-reservation-number').html(checked);
                    }
                });
            </script>

            <?php if (current_user_can('manage_options')) { ?>

                <!--Delete Modal Markup-->
                <?php ob_start() ?>
                <?= esc_html__('The reservation(s) will be removed from the database.', 'teambooking') ?>
                <p>
                    <?= esc_html__('Please note: non-expired-nor-cancelled Appointments Classes will stay booked, non-expired-nor-cancelled Events Classes will regain the tickets, but customers and coworkers will not be notified of that.', 'teambooking') ?>
                </p>
                <?php $modal_content = ob_get_clean();
                $this->view->small_modal(
                    'tb-reservation-delete-modal',
                    esc_html__('Are you sure?', 'teambooking'),
                    $modal_content,
                    esc_html__('Yes', 'teambooking'),
                    esc_html__('No', 'teambooking')
                );
                unset($modal_content); ?>

                <!--Cancellation Modal Markup-->
                <?php ob_start() ?>
                <span class="previously-confirmed">
                            <?= esc_html__('The reservation will be revoked, and the slot will be freed.', 'teambooking') ?>
                </span>
                <div>
                    <p><?= esc_html__('Add a reason (optional, will be added to the cancellation email):', 'teambooking') ?></p>
                    <textarea style="width:100%;" id="reason"></textarea>
                </div>
                <?php $modal_content = ob_get_clean();
                $this->view->small_modal(
                    'tb-reservation-cancel-modal',
                    esc_html__('Are you sure?', 'teambooking'),
                    $modal_content,
                    esc_html__('Yes', 'teambooking'),
                    esc_html__('No', 'teambooking')
                );
                unset($modal_content); ?>

            <?php } ?>

            <!--Pagination Markup-->
            <?php
            if (!is_null($this->filter_service)) {
                $pagination_root = admin_url('/admin.php?page=team-booking&tab=overview&service=' . $this->filter_service);
            } else {
                $pagination_root = admin_url('/admin.php?page=team-booking&tab=overview');
            }
            $filter_first_page = clone $this->filter_object;
            $filter_first_page->setPage(1);
            $filter_prev_page = clone $this->filter_object;
            $filter_prev_page->setPage($this->filter_object->getPage() - 1);
            $filter_next_page = clone $this->filter_object;
            $filter_next_page->setPage($this->filter_object->getPage() + 1);
            $filter_last_page = clone $this->filter_object;
            $filter_last_page->setPage($last_page);
            ?>
            <div class="tablenav">

                <div id="selected-reservations-actions" style="float:left;display: none;" class="displaying-num">
                    <span
                        id="selected-reservation-number"></span> <?= strtolower(esc_html__('selected', 'teambooking')) ?>
                    (<a
                        id="remove-selected-reservations"
                        href="#" <?= $this->filter_object->getPendingReservations() ? 'data-pending="1"' : '' ?>><!--
                     --><?= strtolower(__('delete', 'teambooking')) ?><!--
                 --></a>)
                </div>

                <div class="tablenav-pages">
                    <span class="displaying-num"><?= esc_html__('Pages', 'teambooking') ?></span>
                    <a class="first-page <?= $this->filter_object->getPage() == 1 ? "disabled" : "" ?>"
                       href="<?= $this->filter_object->getPage() == 1 ? "" : $pagination_root . '&filter=' . $filter_first_page->encode() ?>">«</a>
                    <a class="prev-page <?= $this->filter_object->getPage() == 1 ? "disabled" : "" ?>"
                       href="<?= $this->filter_object->getPage() == 1 ? "" : $pagination_root . '&filter=' . $filter_prev_page->encode() ?>">‹</a>
                    <span class="paging-input"><span
                            class="current-page"><?= $this->filter_object->getPage() ?></span> <?= esc_html__('of', 'teambooking') ?>
                        <span class="total-pages"><?= $last_page ?></span></span>
                    <a class="next-page <?= $this->filter_object->getPage() == $last_page ? "disabled" : "" ?>"
                       href="<?= $this->filter_object->getPage() == $last_page ? "" : $pagination_root . '&filter=' . $filter_next_page->encode() ?>">›</a>
                    <a class="last-page <?= $this->filter_object->getPage() == $last_page ? "disabled" : "" ?>"
                       href="<?= $this->filter_object->getPage() == $last_page ? "" : $pagination_root . '&filter=' . $filter_last_page->encode() ?>">»</a>
                </div>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    /**
     * @param $database_id
     * @param TeamBooking_ReservationData $reservation
     * @param bool|FALSE $pending
     * @return string
     * @throws Exception
     */
    public function getReservationActions($database_id, TeamBooking_ReservationData $reservation, $pending = FALSE)
    {
        if (!tbCheckServiceIdExistance($reservation->getServiceId())) {
            /**
             * The service is not present anymore
             * so we'll use the pending reservations actions
             * as base
             */
            $pending = TRUE;
        }
        ob_start();
        ?>
        <a class="tbk-reservations-action-details ui circular label mini horizontal"
           title="<?= esc_html__('Details', 'teambooking') ?>"
           href="#"
           id="tb-<?= $reservation->getCreationInstant() ?>">
            <?php $this->view->dashicon('dashicons-editor-alignleft') ?>
        </a>
        <?php
        if (current_user_can('manage_options')) { ?>
            <a class="tbk-reservations-action-delete ui circular label mini horizontal team-booking-delete-reservation"
               title="<?= esc_html__('Delete', 'teambooking') ?>"
               href="#"
               data-reservationid="<?= $database_id ?>" <?= $pending ? 'data-pending="1"' : '' ?>>
                <?php $this->view->dashicon('dashicons-trash') ?>
            </a>
        <?php } ?>
        <?php
        if ($pending && current_user_can('manage_options')) {
            // Pending reservation actions
            if (tbCheckServiceIdExistance($reservation->getServiceId())) { ?>
                <a class="tbk-reservations-action-approve ui circular label mini horizontal team-booking-confirm-pending-reservation"
                   title="<?= esc_html__('Confirm', 'teambooking') ?>"
                   href="#"
                   data-reservationid="<?= $database_id ?>">
                    <?php $this->view->dashicon('dashicons-yes') ?>
                </a>
            <?php } ?>
            <!-- Confirm pending Modal Markup-->
            <div class="ui small modal" id="tb-reservation-confirm-pending-modal-<?= $database_id ?>">
                <i class="close tb-icon"></i>

                <div class="header">
                    <?= esc_html__('Are you sure you want to confirm this pending reservation?', 'teambooking') ?>
                </div>

                <div class="content" style="width: calc(100% - 4rem);">
                    <?= esc_html__('Accordingly to the service settings, the confirmation and notification emails will be sent.', 'teambooking') ?>
                    <p class="error">
                        <?= esc_html__('The pending reservation cannot be confirmed due the following error:', 'teambooking') ?>
                        <span></span>
                    </p>
                </div>

                <div class="actions">
                    <div class="ui positive button setpaid" style="height:auto">
                        <?= esc_html__('Yes and set as paid', 'teambooking') ?>
                    </div>
                    <div class="ui approve blue button" style="height:auto;">
                        <?= esc_html__('Yes but leave unpaid', 'teambooking') ?>
                    </div>
                    <div class="ui deny black button" style="height:auto">
                        <?= esc_html__('No', 'teambooking') ?>
                    </div>
                </div>
            </div>
            <!-- ENDOF Confirm pending Modal Markup-->
            <?php
        } else {
            // Normal reservation actions
            if ($reservation->isWaitingApproval()) {
                if (($this->settings->getService($reservation->getServiceId())->getApproveRule() === 'coworker'
                        && get_current_user_id() == $reservation->getCoworker())
                    || (($this->settings->getService($reservation->getServiceId())->getApproveRule() !== 'coworker')
                        && current_user_can('manage_options'))
                ) {
                    ?>
                    <a class="tbk-reservations-action-approve ui circular label mini horizontal team-booking-approve-reservation"
                       title="<?= esc_html__('Approve', 'teambooking') ?>"
                       href="#"
                       data-reservationid="<?= $database_id ?>">
                        <?php $this->view->dashicon('dashicons-yes') ?>
                    </a>

                    <!--Approval Modal Markup-->
                    <?php ob_start() ?>
                    <?= esc_html__('Accordingly to the service settings, the confirmation and notification emails will be sent.', 'teambooking') ?>
                    <p class="error">
                        <?= esc_html__('The reservation cannot be approved due the following error:', 'teambooking') ?>
                        <span></span>
                    </p>
                    <?php $modal_content = ob_get_clean();
                    $this->view->small_modal(
                        'tb-reservation-approve-modal-' . $database_id,
                        esc_html__('Are you sure you want to confirm this reservation?', 'teambooking'),
                        $modal_content,
                        esc_html__('Yes', 'teambooking'),
                        esc_html__('No', 'teambooking'));
                    unset($modal_content) ?>
                    <!-- ENDOF Approval Modal Markup-->
                <?php }
            } ?>

            <?php if (current_user_can('manage_options')) { ?>
                <?php if (!$reservation->isCancelled() && $reservation->getServiceClass() != 'service') { ?>
                    <a class="tbk-reservations-action-cancel ui circular label mini horizontal team-booking-cancel-reservation"
                       title="<?= esc_html__('Cancel', 'teambooking') ?>"
                       href="#"
                       data-reservationid="<?= $database_id ?>"
                       data-previouslyconfirmed="<?= $reservation->isWaitingApproval() ? 'no' : 'yes' ?>">
                        <?php $this->view->dashicon('dashicons-no-alt') ?>
                    </a>
                <?php }
            } ?>

            <?php if ($reservation->getPrice() > 0 || $reservation->getServiceClass() == 'service') { ?>
                <a class="tbk-reservations-action-modify ui circular label mini horizontal team-booking-modify-reservation"
                   title="<?= esc_html__('Modify', 'teambooking') ?>"
                   href="#"
                   id="tb-modify-<?= $reservation->getCreationInstant() ?>">
                    <?php $this->view->dashicon('dashicons-edit') ?>
                </a>
                <?php
            }

        }

        return ob_get_clean();
    }

//------------------------------------------------------------

    public
    function getLastErrors()
    {
        ob_start();
        ?>

        <?php
        $this->view->panelTitleWithLabel(
            esc_html__('Last errors', 'teambooking'),
            esc_html__('clean', 'teambooking'),
            'clean-error-logs',
            esc_html__('Hover for description', 'teambooking')) ?>

        <div class="tbk-content">
            <table class="widefat">
                <tbody>
                <tr class="alternate">
                    <th style="font-weight:bold"><?= esc_html__('Message', 'teambooking') ?></th>
                    <th style="font-weight:bold"><?= esc_html__('When', 'teambooking') ?></th>

                    <?php if (current_user_can('manage_options')) { ?>
                        <th style="font-weight:bold"><?= esc_html__('Coworker', 'teambooking') ?></th>
                    <?php } ?>
                </tr>

                <?php
                $error_logs = $this->settings->getErrorLogs();

                if (!empty($error_logs)) {

                    $error_logs = array_reverse($this->settings->getErrorLogs());

                    foreach ($error_logs as $log) {
                        /* @var $log TeamBooking_ErrorLog */
                        if (!current_user_can('manage_options') && get_current_user_id() != $log->getCoworkerId()) {
                            continue;
                        }
                        ?>
                        <tr>
                            <td>
                                <a href="#" title="<?= $log->getDescription() ?>"><?= $log->getMessage() ?></a>
                            </td>

                            <td>
                                <?= date_i18n_tb(get_option('date_format') . " " . get_option('time_format'), $log->getTimestamp()) ?>
                            </td>

                            <?php if (current_user_can('manage_options')) { ?>
                                <td>
                                    <?= $log->getCoworker() ?>
                                </td>
                            <?php } ?>

                        </tr>
                        <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>

        <?php
        return ob_get_clean();
    }

//------------------------------------------------------------

    public
    function getStats()
    {
        $timezone = tbGetTimezone();

        // retrieve some reservations count
        $non_expired_reservations_num = 0;
        $cancelled_reservations_num = 0;
        $waiting_for_approval_num = 0;
        $at_least_one_service_with_approval = FALSE;
        $total_amount = 0;
        $paid_amount = 0;
        foreach ($this->reservations as $id => $reservation) {
            /* @var $reservation TeamBooking_ReservationData */
            if (!tbCheckServiceIdExistance($reservation->getServiceId())) {
                continue;
            }
            $start_time = new DateTime($reservation->getSlotStart());
            $start_time->setTimezone($timezone);
            if ($start_time->format('U') > current_time('timestamp')
                ||
                $reservation->getServiceClass() == 'service' && $reservation->isConfirmed()
            ) {
                $non_expired_reservations_num++;
            }
            if ($reservation->isCancelled()) {
                $cancelled_reservations_num++;
            }
            if ($this->settings->getService($reservation->getServiceId())->getApproveRule() != 'none') {
                $at_least_one_service_with_approval = TRUE;
            }
            if ($reservation->isWaitingApproval()) {
                $waiting_for_approval_num++;
            }
            if (!$reservation->isCancelled()
                || (
                    $reservation->isCancelled()
                    && $reservation->isPaid()
                )
            ) {
                $total_amount += $reservation->getTickets() * $reservation->getPrice();
            }
            if ($reservation->isPaid()) {
                $paid_amount += $reservation->getTickets() * $reservation->getPrice();
            }
        }
        $currency_symbol = '$';
        foreach (tbGetCurrencies() as $code => $currency) {
            if ($code == $this->settings->getCurrencyCode()) {
                $currency_symbol = $currency['symbol'];
            }
        }
        $string_1 = __('Upcoming', 'teambooking');
        $string_2 = __('Cancelled', 'teambooking');
        $string_3 = __('Total amount', 'teambooking');
        $string_4 = __('Paid amount', 'teambooking');
        $string_5 = __('Waiting for approval', 'teambooking');

        ob_start();
        ?>
        <!-- Non-expired reservations -->
        <a class="tbk-stats tbk-panel tbk-span-2">
            <div class="tbk-content">
                <?= $this->view->segmentHeader($string_1) ?>
                <div class="tbk-stats-num">
                    <?= $non_expired_reservations_num ?>
                </div>
            </div>
        </a>
        <?php if ($at_least_one_service_with_approval) { ?>
        <!-- Waiting for approval -->
        <?php
        $filter_obj = new TeamBooking_AdminFilterObject();
        $filter_obj->setStatus('waiting_approval')
        ?>
        <a class="tbk-stats tbk-stats-link tbk-panel tbk-span-2"
           href="<?= admin_url('/admin.php?page=team-booking&tab=overview&filter=') . $filter_obj->encode() ?>">
            <div class="tbk-content">
                <?= $this->view->segmentHeader($string_5) ?>
                <div class="tbk-stats-num">
                    <?= $waiting_for_approval_num ?>
                </div>
            </div>
        </a>
    <?php } ?>
        <!-- Cancelled reservations -->
        <?php
        $filter_obj = new TeamBooking_AdminFilterObject();
        $filter_obj->setStatus('cancelled')
        ?>
        <a class="tbk-stats tbk-stats-link tbk-panel tbk-span-2"
           href="<?= admin_url('/admin.php?page=team-booking&tab=overview&filter=') . $filter_obj->encode() ?>">
            <div class="tbk-content">
                <?= $this->view->segmentHeader($string_2) ?>
                <div class="tbk-stats-num">
                    <?= $cancelled_reservations_num ?>
                </div>
            </div>
        </a>
        <?php if ($total_amount > 0) { ?>
        <!-- Total amount -->
        <a class="tbk-stats tbk-panel tbk-span-2">
            <div class="tbk-content">
                <?= $this->view->segmentHeader($string_3) ?>
                <div class="tbk-stats-num">
                    <?= $total_amount ?>
                    <span class="currency"><?= $currency_symbol ?></span>
                </div>
            </div>
        </a>
        <!-- Paid amount -->
        <a class="tbk-stats tbk-panel tbk-span-2">
            <div class="tbk-content">
                <?= $this->view->segmentHeader($string_4) ?>
                <div class="tbk-stats-num">
                    <?= $paid_amount ?>
                    <span class="currency"><?= $currency_symbol ?></span>
                </div>
            </div>
        </a>
    <?php } ?>

        <?php
        return ob_get_clean();
    }

//------------------------------------------------------------

    public
    function getDataExportForms()
    {
        ob_start();
        ?>
        <form id="tb-get-csv-form" method="POST" action="<?= admin_url() . 'admin-post.php' ?>">
            <input type="hidden" name="action" value="option_submit">
            <?php wp_nonce_field('team_booking_options_verify') ?>
            <input type="hidden" name="filter"
                   value="<?= (!is_null($this->filter_service)) ? $this->filter_service : "" ?>">
            <input type="hidden" name="get_csv_file_tb" value="1">
        </form>

        <form id="tb-get-xlsx-form" method="POST" action="<?= admin_url() . 'admin-post.php' ?>">
            <input type="hidden" name="action" value="option_submit">
            <?php wp_nonce_field('team_booking_options_verify') ?>
            <input type="hidden" name="filter"
                   value="<?= (!is_null($this->filter_service)) ? $this->filter_service : "" ?>">
            <input type="hidden" name="get_xlsx_file_tb" value="1">
        </form>

        <?php
        return ob_get_clean();
    }

//------------------------------------------------------------

    private
    function getReservationDetailsModal(TeamBooking_ReservationData $log, $time, $when_value)
    {
        $files = $log->getFilesReferences();
        ?>
        <div class="ui small modal" id='tb-modal-<?= $time ?>'>
            <i class="close tb-icon"></i>

            <div class="header">
                <a class="blue ui label" data-show="tb-customer-details">
                    <?= esc_html__('Customer', 'teambooking') ?>
                </a>
                <?php if ($log->isPaid() && NULL !== $log->getPaymentDetails()) { ?>
                    |
                    <a class="ui label" data-show="tb-payment-details">
                        <?= esc_html__('Payment', 'teambooking') ?>
                    </a>
                <?php } ?>
                |
                <a class="ui label" data-show="tb-booking-details">
                    <?= esc_html__('Booking', 'teambooking') ?>
                </a>
                <?php if (!empty($files)) { ?>
                    |
                    <a class="ui label" data-show="tb-files-details">
                        <?= esc_html__('Files', 'teambooking') ?>
                    </a>
                <?php } ?>
                <?php if ($log->getCancellationReason() || ($log->getPendingReason() && $log->isPending())) { ?>
                    |
                    <a class="ui label" data-show="tb-other-details">
                        <?= esc_html__('Other', 'teambooking') ?>
                    </a>
                <?php } ?>
            </div>

            <div class="content" style="width: calc(100% - 4rem);">
                <!-- Other details part -->
                <div class="tb-data tb-other-details" style="display:none;">
                    <?php if ($log->getCancellationReason()) { ?>
                        <h4><?= esc_html__('Cancellation reason', 'teambooking') ?></h4>
                        <div style="font-style: italic;font-weight: 300;">
                            <?= $log->getCancellationReason() ?>
                        </div>
                    <?php } ?>
                    <?php if ($log->getPendingReason() && $log->isPending()) { ?>
                        <h4><?= esc_html__('Pending reason', 'teambooking') ?></h4>
                        <div style="font-style: italic;font-weight: 300;">
                            <?= $log->getPendingReason() ?>
                        </div>
                    <?php } ?>
                </div>

                <!-- Customer details part -->
                <div class="tb-data tb-customer-details">
                    <h4><?= esc_html__('Customer details', 'teambooking') ?></h4>
                    <?php
                    foreach ($log->getFieldsArray() as $key => $field) {
                        if ($field) {
                            ?>
                            <div style="font-style: italic;font-weight: 300;">
                                <?php
                                try {
                                    $label_from_hook = $this->settings->getService($log->getServiceId())->getFormFields()->getLabelFromHook($key);
                                } catch (Exception $ex) {
                                    $label_from_hook = FALSE;
                                }
                                if ($label_from_hook) {
                                    echo $label_from_hook;
                                } else {
                                    echo $key;
                                }
                                ?>
                            </div>
                            <input type="text" class="large-text" style='margin-bottom:10px;' value="<?= $field ?>"
                                   readonly="">
                            <?php
                        }
                    }
                    if ($log->getCustomerUserId()) {
                        ?>
                        <div style="font-style: italic;font-weight: 300;">
                            <?= esc_html__('WordPress User', 'teambooking') ?>
                        </div>
                        <input type="text" class="large-text" style='margin-bottom:10px;'
                               value="<?= get_userdata($log->getCustomerUserId())->user_nicename ?>" readonly="">

                    <?php } ?>

                </div>

                <?php
                if ($log->isPaid() && NULL !== $log->getPaymentDetails()) {
                    ?>
                    <!-- Payment details part -->
                    <div class="tb-data tb-payment-details" style="display:none;">
                        <h4><?= esc_html__('Payment details', 'teambooking') ?></h4>

                        <div style="font-style: italic;font-weight: 300;">
                            <?= esc_html__('Payment gateway', 'teambooking') ?>
                        </div>
                        <input type="text" readonly="" class="large-text" style='margin-bottom:10px;'
                               value="<?= $log->getPaymentGateway() ?>">
                        <?php foreach ($log->getPaymentDetails() as $key => $detail) { ?>
                            <div style="font-style: italic;font-weight: 300;">
                                <?= ucwords($key) ?>
                            </div>
                            <input type="text" readonly="" class="large-text" style='margin-bottom:10px;'
                                   value="<?= $detail ?>">
                        <?php } ?>
                    </div>
                <?php } ?>

                <!-- Booking details part -->
                <div class="tb-data tb-booking-details" style="display:none;">
                    <h4><?= esc_html__('Booking details', 'teambooking') ?></h4>

                    <div style="font-style: italic;font-weight: 300;">
                        <?= esc_html__('Service', 'teambooking') ?>
                    </div>
                    <input type="text" readonly="" class="large-text" style='margin-bottom:10px;'
                           value="<?= $log->getServiceName() ?>">
                    <?php if ($log->getServiceLocation() != '') { ?>
                        <div style="font-style: italic;font-weight: 300;">
                            <?= esc_html__('Location', 'teambooking') ?>
                        </div>
                        <input type="text" readonly="" class="large-text" style='margin-bottom:10px;'
                               value="<?= $log->getServiceLocation() ?>">
                    <?php } ?>
                    <?php if (current_user_can('manage_options')) { ?>
                        <div style="font-style: italic;font-weight: 300;">
                            <?= esc_html__('Coworker', 'teambooking') ?>
                        </div>
                        <input type="text" readonly="" class="large-text" style='margin-bottom:10px;'
                               value="<?= $this->settings->getCoworkerData($log->getCoworker())->getDisplayName() ?>">
                    <?php } ?>
                    <?php if ($log->getServiceClass() != 'service') { ?>
                        <div style="font-style: italic;font-weight: 300;">
                            <?= esc_html__('When', 'teambooking') ?>
                        </div>
                        <input type="text" readonly="" class="large-text" style='margin-bottom:10px;'
                               value="<?= $when_value ?>">
                        <div style="font-style: italic;font-weight: 300;">
                            <?= esc_html__('Tickets', 'teambooking') ?>
                        </div>
                        <input type="text" readonly="" class="large-text" style='margin-bottom:10px;'
                               value="<?= $log->getTickets() ?>">
                        <?php
                    }
                    ?>
                    <div style="font-style: italic;font-weight: 300;">
                        <?= esc_html__('Total price', 'teambooking') ?>
                    </div>
                    <input type="text" readonly="" class="large-text" style='margin-bottom:10px;'
                           value="<?= tbCurrencyCodeToSymbol($log->getCurrencyCode(), $log->getTickets() * $log->getPrice()) ?>">
                </div>

                <?php
                if (!empty($files)) {
                    ?>
                    <!-- Files details part -->
                    <div class="tb-data tb-files-details" style="display:none;">
                        <h4><?= esc_html__('Attached files', 'teambooking') ?></h4>
                        <?php foreach ($files as $hook => $file_info_array) { ?>
                            <div style="font-style: italic;font-weight: 300;">
                                <?= ucwords(str_replace("_", " ", $hook)) ?>
                            </div>
                            <a href="<?= $file_info_array['url'] ?>"><?= esc_html__('Open it', 'teambooking') ?></a>
                        <?php } ?>
                    </div>
                    <?php
                }
                ?>
            </div>

            <div class="actions">
                <div class="ui button deny black" style="height: auto;">
                    <?= esc_html__('Close', 'teambooking') ?>
                </div>
            </div>
        </div>
        <script>
            jQuery('#tb-modal-<?= $time ?>').on('click', '.header > .label', function () {
                jQuery('#tb-modal-<?= $time ?> > .header > .label').removeClass('blue');
                jQuery(this).addClass('blue');
                jQuery('#tb-modal-<?= $time ?> .tb-data').hide();
                var to_show = jQuery(this).data('show');
                jQuery('#tb-modal-<?= $time ?> .' + to_show).show();
            });
            jQuery('#tb-modal-<?= $time ?>')
                .uiModal({
                    blurring: true,
                    transition: 'fade up'
                })
                .uiModal('attach events', '#tb-<?= $time ?>', 'show')
            ;
            jQuery('#tb-<?= $time ?>').click(function (event) {
                event.preventDefault();
            });
        </script>
        <?php
    }

//------------------------------------------------------------

    private
    function keepOnlyFilteredReservations()
    {
        foreach ($this->reservations as $id => $log) {
            /** @var $log TeamBooking_ReservationData */
            // This skips reservations that does not belong to Coworker, if not Admin
            if (!current_user_can('manage_options') && $log->getCoworker() != get_current_user_id()) {
                unset($this->reservations[$id]);
            }
            // Apply filters
            if ($log->getServiceClass() == 'service' && $log->isDone()) {
                if ($this->filter_object->getStatus() !== NULL
                    && $this->filter_object->getStatus() !== 'confirmed'
                ) {
                    unset($this->reservations[$id]);
                }
            } else {
                if ($this->filter_object->getStatus() !== NULL
                    && $log->getStatus() != $this->filter_object->getStatus()
                ) {
                    unset($this->reservations[$id]);
                }
            }
            if ($this->filter_object->getPaymentStatus() !== NULL) {
                if ($log->isPaid() && $this->filter_object->getPaymentStatus() != 'paid') {
                    unset($this->reservations[$id]);
                }
                if (!$log->isPaid() && $this->filter_object->getPaymentStatus() != 'not_paid') {
                    unset($this->reservations[$id]);
                }
            }
            if ($this->filter_object->getCoworker() !== NULL
                && $this->filter_object->getCoworker() != $log->getCoworker()
            ) {
                unset($this->reservations[$id]);
            }
        }
    }

//------------------------------------------------------------

    private
    function filterSearchTerms()
    {
        if (!is_null($this->filter_object->getIds()) && ($this->filter_object->getSearchTerm()) != NULL) {
            if ($this->filter_object->getIds() != NULL) {
                $this->reservations = tbGetReservationsByIds($this->filter_object->getIds(), $this->filter_object->getPendingReservations());
            } else {
                $this->reservations = array();
            }
        }
    }

//------------------------------------------------------------

    private
    function sortList()
    {
        // Sorting the reservations
        if ($this->filter_object->getSortBy() == 'when') {
            uasort($this->reservations, function ($a, $b) {
                /* @var $a TeamBooking_ReservationData */
                return ($a->getStart() > $b->getStart()) ? -1 : (($a->getStart() < $b->getStart()) ? 1 : 0);
            });
        } else {
            krsort($this->reservations);
        }
    }

}
