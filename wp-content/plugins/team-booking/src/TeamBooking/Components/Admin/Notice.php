<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Components_Admin_Notice
{

    //------------------------------------------------------------

    private $_message;
    private $_additional_content;

    public function __construct($message, $additional_content = NULL)
    {
        $this->_message = $message;
        $this->_additional_content = $additional_content;
    }

    //------------------------------------------------------------

    public function getPositive()
    {
        return "
        <div class = 'notice updated is-dismissible'>
        <p>$this->_message</p>
        $this->_additional_content
        <button type='button' class='notice-dismiss'></button>
        </div>";
    }

    public function getNegative()
    {
        return "
        <div class = 'notice error is-dismissible'>
        <p>$this->_message</p>
        $this->_additional_content
        <button type='button' class='notice-dismiss'></button>
        </div>";
    }

}
