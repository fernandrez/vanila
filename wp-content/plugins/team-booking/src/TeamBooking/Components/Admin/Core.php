<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Components_Admin_Core
{

    //------------------------------------------------------------

    public $action;
    public $roles_all;
    public $roles_allowed;
    private $redirect_uri;
    private $js_origins;
    private $client_id_input_value;
    private $client_secret_input_value;
    private $project_name_input_value;
    private $max_pending_time;
    private $settings;
    private $view;

    public function __construct($settings)
    {
        /* @var $settings TeamBookingSettings */
        $this->settings = $settings;
        $this->client_id_input_value = $settings->getApplicationClientId();
        $this->client_secret_input_value = $settings->getApplicationClientSecret();
        $this->project_name_input_value = $settings->getApplicationProjectName();
        $this->max_pending_time = $settings->getMaxPendingTime();
        $this->redirect_uri = admin_url() . 'admin-ajax.php?action=teambooking_oauth_callback';
        $this->js_origins = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, strpos($_SERVER["SERVER_PROTOCOL"], '/'))) . '://' . $_SERVER['HTTP_HOST'];
        $this->view = new TeamBooking_Components_Admin_View();
    }

    //------------------------------------------------------------

    public function getPostBody()
    {
        ob_start();
        ?>

        <div class="tbk-wrapper">
            <div class="tbk-row">

                <div class="tbk-column tbk-span-10">
                    <form method="POST" action="<?= $this->action ?>">
                        <input type="hidden" name="action" value="option_submit">
                        <?php wp_nonce_field('team_booking_options_verify') ?>
                        <div class="tbk-column tbk-span-6">
                            <div class="tbk-panel">
                                <?= $this->getGoogleProjectSettings() ?>
                            </div>
                            <div class="tbk-panel">
                                <?= $this->getQuickGuide() ?>
                            </div>
                        </div>
                        <div class="tbk-column tbk-span-6">
                            <div class="tbk-panel">
                                <?= $this->getSettings() ?>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="tbk-column tbk-span-2">
                    <div class="tbk-panel">
                        <?= $this->getImportExport() ?>
                    </div>
                </div>

            </div>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getSettings()
    {
        // Grouping strings for maintainability
        $string_9 = esc_html__('Settings', 'teambooking');
        $string_10 = esc_html__('Roles allowed to be Coworkers', 'teambooking');
        $string_11 = esc_html__('Users with selected roles will be your Coworkers. Those who link a Google Calendar can participate to all service classes. Coworkers without a linked Google Calendar can participate to "Unscheduled services" only.', 'teambooking');
        $string_12 = esc_html__('Autofill reservation form fields for registered users', 'teambooking');
        $string_13 = esc_html__('If yes, a logged-in customer will found some reservation form fields pre-filled, based on his WordPress profile data. If yes and hide, the pre-filled fields will not be shown, but their data still pass.', 'teambooking');
        $string_14 = esc_html__('Yes', 'teambooking');
        $string_15 = esc_html__('No', 'teambooking');
        $string_16 = esc_html__('Yes, and hide fields', 'teambooking');
        $string_17 = esc_html__('Load the frontend calendar at the first month with an available slot', 'teambooking');
        $string_18 = esc_html__('If yes, the frontend calendar will be automatically loaded at the nearest month with at least one free slot. Please note: if yes, the first loading instance will be slower, since the data to retrieve from Google is not limited in time.', 'teambooking');
        $string_19 = esc_html__('Registration URL', 'teambooking');
        $string_20 = esc_html__('Logged-only services will invite users to register here', 'teambooking');
        $string_21 = esc_html__('Keep reservations in database for:', 'teambooking');
        $string_22 = esc_html__("Counting starts from reservation's date", 'teambooking');
        $string_23 = esc_html__("WARNING: a total of", 'teambooking');
        $string_24 = esc_html__("reservations will be deleted on save", 'teambooking');
        $string_25 = esc_html__('Max Google API results:', 'teambooking');
        $string_26 = esc_html__('Raise this number if you are experiencing issues like missing slots, or partially filled months. Keep in mind that a higher number means a slower response time due to increased data fetching.', 'teambooking');
        $string_27 = esc_html__('Debug:', 'teambooking');
        $string_28 = esc_html__('Silent (error logs on Overview tab only)', 'teambooking');
        $string_29 = esc_html__('Verbose (error messages on frontend calendar)', 'teambooking');
        $string_30 = esc_html__('Save changes', 'teambooking');
        $string_31 = esc_html__('Delete reservation database when the plugin is uninstalled', 'teambooking');
        $string_33 = esc_html__('Allow customers to download ICAL file after a reservation', 'teambooking');
        $string_34 = esc_html__('Skip Google Maps library loading', 'teambooking');
        $string_35 = esc_html__('Set it to YES if your theme or another plugin already loads the Google Maps js library, to avoid twice call issues', 'teambooking');
        $string_36 = esc_html__('Max pending time', 'teambooking');
        $string_37 = esc_html__('If payment is not made within this time, the reservation will be released. It affects only services where payment must be done immediately.', 'teambooking');
        $fieldname_4 = 'roles_allowed';
        $fieldname_5 = 'autofill';
        $fieldname_6 = 'first_month_automatic';
        $fieldname_7 = 'registration_url';
        $fieldname_8 = 'database_reservation_timeout';
        $fieldname_9 = 'max_google_api_results';
        $fieldname_10 = 'debug';
        $fieldname_11 = 'drop_tables';
        $fieldname_13 = 'show_ical';
        $fieldname_14 = 'skip_gmaps';
        $fieldname_15 = 'max_pending_time';
        ob_start();
        ?>

        <?php $this->view->panelTitle($string_9) ?>

        <div class="tbk-content">
            <ul class="tbk-list">

                <li>
                    <?php $this->view->segmentHeader($string_10, $string_11) ?>

                    <fieldset>
                        <?php
                        foreach ($this->roles_all as $name => $role) {
                            ?>
                            <?php $this->view->checkbox($role['name'], $fieldname_4 . '[' . $name . ']', $role['name'], 1, in_array($role['name'], $this->roles_allowed)) ?>
                            <br>
                        <?php } ?>
                    </fieldset>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_12, $string_13) ?>

                    <fieldset>
                        <?php $this->view->radio($string_14, $fieldname_5, 'yes', $this->settings->getAutofillReservationForm(), TRUE) ?>
                        <br/>
                        <?php $this->view->radio($string_16, $fieldname_5, 'hide', $this->settings->getAutofillReservationForm(), 'hide') ?>
                        <br/>
                        <?php $this->view->radio($string_15, $fieldname_5, 'no', $this->settings->getAutofillReservationForm(), FALSE) ?>
                    </fieldset>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_17, $string_18) ?>

                    <fieldset>
                        <?php $this->view->radio($string_14, $fieldname_6, 'yes', $this->settings->isFirstMonthWithFreeSlotShown(), TRUE) ?>
                        <br/>
                        <?php $this->view->radio($string_15, $fieldname_6, 'no', $this->settings->isFirstMonthWithFreeSlotShown(), FALSE) ?>
                    </fieldset>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_33) ?>

                    <fieldset>
                        <?php $this->view->radio($string_14, $fieldname_13, 'yes', $this->settings->getShowIcal(), TRUE) ?>
                        <br/>
                        <?php $this->view->radio($string_15, $fieldname_13, 'no', $this->settings->getShowIcal(), FALSE) ?>
                    </fieldset>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_19, $string_20) ?>

                    <?php $this->view->textfield($this->settings->getRegistrationUrl(), $fieldname_7) ?>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_21, $string_22) ?>

                    <p>
                        <select name="<?= $fieldname_8 ?>" id="tb_database_timeout_select">
                            <?php $this->view->select_option(
                                esc_html__('15 days', 'teambooking'),
                                15 * DAY_IN_SECONDS,
                                $this->settings->getDatabaseReservationTimeout(),
                                15 * DAY_IN_SECONDS,
                                "data-warning='" . tbCleanReservations(FALSE, TRUE, 15 * DAY_IN_SECONDS) . "'")
                            ?>
                            <?php $this->view->select_option(
                                esc_html__('30 days', 'teambooking'),
                                30 * DAY_IN_SECONDS,
                                $this->settings->getDatabaseReservationTimeout(),
                                30 * DAY_IN_SECONDS,
                                "data-warning='" . tbCleanReservations(FALSE, TRUE, 30 * DAY_IN_SECONDS) . "'")
                            ?>
                            <?php $this->view->select_option(
                                esc_html__('60 days', 'teambooking'),
                                60 * DAY_IN_SECONDS,
                                $this->settings->getDatabaseReservationTimeout(),
                                60 * DAY_IN_SECONDS,
                                "data-warning='" . tbCleanReservations(FALSE, TRUE, 60 * DAY_IN_SECONDS) . "'")
                            ?>
                            <?php $this->view->select_option(
                                esc_html__('120 days', 'teambooking'),
                                120 * DAY_IN_SECONDS,
                                $this->settings->getDatabaseReservationTimeout(),
                                120 * DAY_IN_SECONDS,
                                "data-warning='" . tbCleanReservations(FALSE, TRUE, 120 * DAY_IN_SECONDS) . "'")
                            ?>
                            <?php $this->view->select_option(
                                esc_html__('240 days', 'teambooking'),
                                240 * DAY_IN_SECONDS,
                                $this->settings->getDatabaseReservationTimeout(),
                                240 * DAY_IN_SECONDS,
                                "data-warning='" . tbCleanReservations(FALSE, TRUE, 240 * DAY_IN_SECONDS) . "'")
                            ?>
                            <?php $this->view->select_option(
                                esc_html__('Forever', 'teambooking'),
                                0,
                                $this->settings->getDatabaseReservationTimeout(),
                                0)
                            ?>
                        </select>

                        <span id="tb_database_timeout_warning" style="margin-left: 10px;color: #cc0000"></span>
                    </p>

                    <!-- This script handles the warning for reservations going to be deleted -->
                    <script>
                        jQuery('#tb_database_timeout_select').change(function () {
                            var number = jQuery('#tb_database_timeout_select').find('option:selected').data('warning');
                            if (number !== undefined && number !== '') {
                                jQuery('#tb_database_timeout_warning').html('<?= $string_23 ?>' + ' ' + number + ' ' + '<?= $string_24 ?>');
                            } else {
                                jQuery('#tb_database_timeout_warning').html('');
                            }
                        });
                    </script>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_36, $string_37) ?>

                    <p>
                        <select name="<?= $fieldname_15 ?>" style="display:block;">
                            <?php $this->view->select_option(esc_html__('15min', 'teambooking'), 900, $this->max_pending_time, 900) ?>
                            <?php $this->view->select_option(esc_html__('30min', 'teambooking'), 1800, $this->max_pending_time, 1800) ?>
                            <?php $this->view->select_option(esc_html__('1h', 'teambooking'), 3600, $this->max_pending_time, 3600) ?>
                            <?php $this->view->select_option(esc_html__('2h', 'teambooking'), 7200, $this->max_pending_time, 7200) ?>
                            <?php $this->view->select_option(esc_html__('3h', 'teambooking'), 10800, $this->max_pending_time, 10800) ?>
                            <?php $this->view->select_option(esc_html__('6h', 'teambooking'), 21600, $this->max_pending_time, 21600) ?>
                            <?php $this->view->select_option(esc_html__('12h', 'teambooking'), 43200, $this->max_pending_time, 43200) ?>
                            <?php $this->view->select_option(esc_html__('1day', 'teambooking'), 86400, $this->max_pending_time, 86400) ?>
                            <?php $this->view->select_option(esc_html__('2days', 'teambooking'), 172800, $this->max_pending_time, 172800) ?>
                            <?php $this->view->select_option(esc_html__('3days', 'teambooking'), 259200, $this->max_pending_time, 259200) ?>
                            <?php $this->view->select_option(esc_html__('4days', 'teambooking'), 345600, $this->max_pending_time, 345600) ?>
                        </select>
                    </p>

                    <p class="description">
                        <?= esc_html__('Values too low could, in extreme cases, lead to payments after reservation is released. Also consider that, in order to process IPN confirmation, your server must be up and running. PayPal example: if your server is down, IPN will be resent by PayPal for up to four days, with a maximum of 15 retries. The interval will increase after each fail attempt.', 'teambooking') ?>
                    </p>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_25, $string_26) ?>

                    <p>
                        <select name="<?= $fieldname_9 ?>">
                            <?php $this->view->select_option(600, 600, $this->settings->getMaxGoogleApiResults(), 600) ?>
                            <?php $this->view->select_option(700, 700, $this->settings->getMaxGoogleApiResults(), 700) ?>
                            <?php $this->view->select_option(800, 800, $this->settings->getMaxGoogleApiResults(), 800) ?>
                            <?php $this->view->select_option(900, 900, $this->settings->getMaxGoogleApiResults(), 900) ?>
                            <?php $this->view->select_option(1000, 1000, $this->settings->getMaxGoogleApiResults(), 1000) ?>
                            <?php $this->view->select_option(1100, 1100, $this->settings->getMaxGoogleApiResults(), 1100) ?>
                            <?php $this->view->select_option(1200, 1200, $this->settings->getMaxGoogleApiResults(), 1200) ?>
                            <?php $this->view->select_option(1300, 1300, $this->settings->getMaxGoogleApiResults(), 1300) ?>
                            <?php $this->view->select_option(1400, 1400, $this->settings->getMaxGoogleApiResults(), 1400) ?>
                            <?php $this->view->select_option(1500, 1500, $this->settings->getMaxGoogleApiResults(), 1500) ?>
                            <?php $this->view->select_option(1600, 1600, $this->settings->getMaxGoogleApiResults(), 1600) ?>
                        </select>
                    </p>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_31) ?>

                    <fieldset>
                        <?php $this->view->radio($string_14, $fieldname_11, 'yes', $this->settings->getDropTablesOnUninstall(), TRUE) ?>
                        <br/>
                        <?php $this->view->radio($string_15, $fieldname_11, 'no', $this->settings->getDropTablesOnUninstall(), FALSE) ?>
                    </fieldset>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_27) ?>

                    <fieldset>
                        <?php $this->view->radio($string_28, $fieldname_10, 'silent', $this->settings->getSilentDebug(), TRUE) ?>
                        <br/>
                        <?php $this->view->radio($string_29, $fieldname_10, 'verbose', $this->settings->getSilentDebug(), FALSE) ?>
                    </fieldset>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_34, $string_35) ?>

                    <fieldset>
                        <?php $this->view->radio($string_14, $fieldname_14, 'yes', $this->settings->getSkipGmapLibs(), TRUE) ?>
                        <br/>
                        <?php $this->view->radio($string_15, $fieldname_14, 'no', $this->settings->getSkipGmapLibs(), FALSE) ?>
                    </fieldset>
                </li>

                <li>
                    <?php submit_button($string_30, 'primary', 'team_booking_options_submit') ?>
                </li>
            </ul>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getImportExport()
    {
        // Grouping strings for maintainability
        $string1 = esc_html__('Import/Export settings', 'teambooking');
        $string2 = esc_html__('Export current settings', 'teambooking');
        $string3 = esc_html__('Import settings from file', 'teambooking');
        $string4 = esc_html__('Import from JSON file', 'teambooking');
        $string5 = esc_html__('Cancel', 'teambooking');
        $string6 = esc_html__('OK', 'teambooking');
        $string7 = esc_html__('The Authorized redirect URI and/or Authorized Javascript Origins seems to be incorrect, have you correctly pasted the values provided here in your Google Project console? Please double check, and retry with the new JSON file!', 'teambooking');
        $string8 = esc_html__('Sorry, this is not a JSON Google Project file, or it is not complete.', 'teambooking');
        $string9 = esc_html__('Please select a file!', 'teambooking');
        $fieldname1 = 'get_tb_settings_backup';
        $fieldname2 = 'set_tb_settings_backup';
        $fieldname3 = 'settings_backup_file';
        $fieldname_4 = 'settings_json_file';
        ob_start();
        ?>

        <?php $this->view->panelTitle($string1) ?>

        <div class="tbk-content" style="text-align: center">
            <p>
                <a class="button tbk-button-long-text" id="team-booking-export-settings">
                    <?= $string2 ?>
                </a>
            </p>

            <form id="team-booking-export-settings_form" method="POST" action="<?= admin_url() . 'admin-post.php' ?>">
                <input type="hidden" name="action" value="option_submit">
                <?php wp_nonce_field('team_booking_options_verify') ?>
                <input type="hidden" name="<?= $fieldname1 ?>" value="1">
            </form>


            <a class="button tbk-button-long-text" id="team-booking-import-settings">
                <?= $string3 ?>
            </a>
        </div>

        <!-- import settings modal markup -->
        <div class="ui modal" id="team-booking-import-settings_modal">
            <i class="close tb-icon"></i>

            <div class="header">
                <?= $string3 ?>
            </div>

            <div class="content" style="width:initial;">
                <form id="team-booking-import-settings_form" method="POST"
                      action="<?= admin_url() . 'admin-post.php' ?>" enctype="multipart/form-data">
                    <input type="hidden" name="action" value="option_submit">
                    <?php wp_nonce_field('team_booking_options_verify') ?>
                    <input type="hidden" name="<?= $fieldname2 ?>" value="1">
                    <input type="file" name="<?= $fieldname3 ?>">
                </form>
            </div>

            <div class="actions">
                <div class="ui deny black button" style="height: auto"><?= $string5 ?></div>
                <div class="ui positive button" style="height: auto"
                     id="team-booking-import-settings_form_submit"><?= $string6 ?></div>
            </div>
        </div>

        <!-- import JSON modal markup -->
        <div class="ui modal" id="team-booking-import-core-json_modal">
            <i class="close tb-icon"></i>

            <div class="header">
                <?= $string4 ?>
            </div>

            <div class="content" style="width:initial;">
                <form id="team-booking-import-core-json_form" method="POST" action="">
                    <?php wp_nonce_field('team_booking_options_verify') ?>
                    <input type="file" name="<?= $fieldname_4 ?>" data-ays-ignore="true">

                    <p class="json-errors uri_mismatch" style="display: none;font-weight: 700;color: darkred;">
                        <?= $string7 ?>
                    </p>

                    <p class="json-errors invalid_file" style="display: none;font-weight: 700;color: darkred;">
                        <?= $string8 ?>
                    </p>

                    <p class="json-errors no_file" style="display: none;font-weight: 700;color: darkred;">
                        <?= $string9 ?>
                    </p>
                </form>
            </div>

            <div class="actions">
                <div class="ui deny black button" style="height: auto"><?= $string5 ?></div>
                <div class="ui positive button" style="height: auto"
                     id="team-booking-import-core-json_form_submit"><?= $string6 ?></div>
            </div>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getQuickGuide()
    {
        $plugin_data = get_plugin_data(TEAMBOOKING_FILE_PATH);
        $string1 = esc_html__('Quick configuration guide', 'teambooking');
        $url = 'http://console.developers.google.com/';
        $url_doc = $plugin_data['PluginURI'] . '/docs';
        $string2 = esc_html__('To made Team Booking working, you need to start a new Project on your Google Developer Console', 'teambooking') . ' (<a href="' . $url . '" alt="Google Developer Console Link" target="_blank">link</a>)';
        $string3 = esc_html__('Then read the "Core Configuration" paragraph of the Team Booking Documentation', 'teambooking') . ' (<a href="' . $url_doc . '" alt="TeamBooking documentation" target="_blank">link</a>)';
        ob_start();
        ?>

        <?php $this->view->panelTitle($string1) ?>

        <div class="tbk-content">
            <p>
                <?= $string2 ?>
            </p>

            <p>
                <?= $string3 ?>
            </p>
            
        </div>
        <?php

        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getGoogleProjectSettings()
    {
        // Grouping strings for maintainability
        $string_1 = esc_html__('Google Project Data', 'teambooking');
        $string_2 = esc_html__('Client ID', 'teambooking');
        $string_3 = esc_html__('locked', 'teambooking');
        $string_4 = esc_html__('unlocked', 'teambooking');
        $string_5 = esc_html__('Client Secret', 'teambooking');
        $string_6 = esc_html__('Product name', 'teambooking');
        $string_7 = esc_html__('Authorized redirect URI', 'teambooking');
        $string_8 = esc_html__('Authorized Javascript Origins', 'teambooking');
        $string_30 = esc_html__('Save changes', 'teambooking');
        $string_9 = esc_html__('Import from JSON file', 'teambooking');
        $fieldname_1 = 'client_id';
        $fieldname_2 = 'client_secret';
        $fieldname_3 = 'project_name';
        ob_start();
        ?>

        <?php $this->view->panelTitle($string_1
        . '<a class="ui mini label horizontal" id="team-booking-import-core-json" style="margin-left: 10px;cursor: pointer;">'
        . $string_9
        . '</a>') ?>

        <div class="tbk-content">
            <ul class="tbk-list">
                <li>
                    <?php $this->view->segmentHeader($string_2) ?>

                    <p>
                        <input type="text" id="client_lock_input" name="<?= $fieldname_1 ?>"
                               value="<?= $this->client_id_input_value ?>"
                               class="regular-text" <?= !empty($this->client_id_input_value) ? 'readonly' : '' ?>/>
                    </p>

                    <?php if (!empty($this->client_id_input_value)) { ?>

                        <div class="ui toggle checkbox" id="client_lock" style="vertical-align: initial;">
                            <input type="checkbox" name="public">
                            <label><?= $string_3 ?></label>
                        </div>

                        <!-- lock/unlock script -->
                        <script>
                            jQuery('#client_lock').checkbox({
                                onChecked: function () {
                                    jQuery('#client_lock_input').attr("readonly", false);
                                    jQuery('#client_lock').find('label').html("<?= $string_4 ?>");
                                },
                                onUnchecked: function () {
                                    jQuery('#client_lock_input').attr("readonly", true);
                                    jQuery('#client_lock').find('label').html("<?= $string_3 ?>");
                                },
                                fireOnInit: false
                            });
                        </script>
                    <?php } ?>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_5) ?>

                    <p>
                        <input type="text" id="secret_lock_input" name="<?= $fieldname_2 ?>"
                               value="<?= $this->client_secret_input_value ?>"
                               class="regular-text" <?= !empty($this->client_secret_input_value) ? 'readonly' : '' ?> />
                    </p>

                    <?php if (!empty($this->client_secret_input_value)) { ?>

                        <div class="ui toggle checkbox" id="secret_lock" style="vertical-align: initial;">
                            <input type="checkbox" name="public">
                            <label><?= $string_3 ?></label>
                        </div>

                        <!-- lock/unlock script -->
                        <script>
                            jQuery('#secret_lock').checkbox({
                                onChecked: function () {
                                    jQuery('#secret_lock_input').attr("readonly", false);
                                    jQuery('#secret_lock').find('label').html("<?= $string_4 ?>");
                                },
                                onUnchecked: function () {
                                    jQuery('#secret_lock_input').attr("readonly", true);
                                    jQuery('#secret_lock').find('label').html("<?= $string_3 ?>");
                                },
                                fireOnInit: false
                            });
                        </script>
                    <?php } ?>
                </li>
                <li>

                    <?php $this->view->segmentHeader($string_6) ?>

                    <p>
                        <input type="text" id="project_lock_input" name="<?= $fieldname_3 ?>"
                               value="<?= $this->project_name_input_value ?>"
                               class="regular-text" <?= !empty($this->project_name_input_value) ? 'readonly' : '' ?> />
                    </p>

                    <?php if (!empty($this->project_name_input_value)) { ?>

                        <div class="ui toggle checkbox" id="project_lock" style="vertical-align: initial;">
                            <input type="checkbox" name="public">
                            <label><?= $string_3 ?></label>
                        </div>

                        <!-- lock/unlock script -->
                        <script>
                            jQuery('#project_lock').checkbox({
                                onChecked: function () {
                                    jQuery('#project_lock_input').attr("readonly", false);
                                    jQuery('#project_lock').find('label').html("<?= $string_4 ?>");
                                },
                                onUnchecked: function () {
                                    jQuery('#project_lock_input').attr("readonly", true);
                                    jQuery('#project_lock').find('label').html("<?= $string_3 ?>");
                                },
                                fireOnInit: false
                            });
                        </script>
                    <?php } ?>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_7, $this->redirect_uri) ?>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_8, $this->js_origins) ?>
                </li>

                <li>
                    <?php submit_button($string_30, 'primary', 'team_booking_options_submit') ?>
                </li>
            </ul>
        </div>

        <?php
        return ob_get_clean();
    }

}
