<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

include_once dirname(__FILE__) . '/preview.php';

class TeamBooking_Components_Admin_Style
{

    //------------------------------------------------------------

    public $action;
    private $settings;
    private $view;

    public function __construct($settings)
    {
        /* @var $settings TeamBookingSettings */
        $this->settings = $settings;
        $this->view = new TeamBooking_Components_Admin_View();
    }

    //------------------------------------------------------------

    public function getPostBody()
    {
        ob_start();
        ?>
        <form method="POST" action="<?= $this->action ?>">
            <input type="hidden" name="action" value="option_submit">
            <?php wp_nonce_field('team_booking_options_verify') ?>
            <div class="tbk-wrapper">
                <div class="tbk-row">

                    <div class="tbk-column tbk-span-3">
                        <div class="tbk-panel">
                            <?= $this->getColours() ?>
                        </div>
                    </div>

                    <div class="tbk-column tbk-span-3">
                        <div class="tbk-panel">
                            <?= $this->getSlotList() ?>
                        </div>

                        <div class="tbk-panel">
                            <?= $this->get62dot5fix() ?>
                        </div>
                    </div>

                    <div class="tbk-column tbk-span-6">
                        <div class="tbk-panel">
                            <?= $this->getCalendarPreview() ?>
                        </div>
                        <div class="tbk-panel">
                            <?= $this->getMapStyles() ?>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getColours()
    {
        $border = $this->settings->getBorder();
        $pattern = $this->settings->getPattern();

        ob_start();
        ?>
        <div class="tbk-panel-title">
            <h4><?= esc_html__('Frontend calendar', 'teambooking') ?></h4>
        </div>
        <div class="tbk-content">
            <ul class="tbk-list">
                <li>
                    <h4>
                        <?= esc_html__('Background', 'teambooking') ?>
                    </h4>

                    <p>
                        <input type="text" class="tb-color-field" name="tb-background-color"
                               value="<?= $this->settings->getColorBackground() ?>">
                    </p>

                    <p>
                        <?= esc_html__('Pattern overlay', 'teambooking') ?>
                    </p>
                    <?= $this->view->radio(esc_html__('None', 'teambooking'), 'tb-calendar-pattern', 0, $pattern['calendar'], 0) ?>

                    <?php for ($i = 1; $i <= 6; $i++) { ?>
                        <br>
                        <?= $this->view->radio('', 'tb-calendar-pattern', $i, $pattern['calendar'], $i) ?>
                        <span class="tbk-pattern-preview"
                              style="background:url(<?= tb_get_pattern($i, '#FFFFFF') ?>);"></span>
                    <?php } ?>
                </li>
                <li>
                    <h4>
                        <?= esc_html__('Week line', 'teambooking') ?>
                    </h4>

                    <p>
                        <input type="text" class="tb-color-field" name="tb-weekline-color"
                               value="<?= $this->settings->getColorWeekLine() ?>">
                    </p>

                    <p>
                        <?= esc_html__('Pattern overlay', 'teambooking') ?>
                    </p>
                    <?= $this->view->radio(esc_html__('None', 'teambooking'), 'tb-weekline-pattern', 0, $pattern['weekline'], 0) ?>

                    <?php for ($i = 1; $i <= 6; $i++) { ?>
                        <br>
                        <?= $this->view->radio('', 'tb-weekline-pattern', $i, $pattern['weekline'], $i) ?>
                        <span class="tbk-pattern-preview"
                              style="background:url(<?= tb_get_pattern($i, '#FFFFFF') ?>);"></span>
                    <?php } ?>

                </li>
                <li>
                    <h4>
                        <?= esc_html__('Free slot', 'teambooking') ?>
                    </h4>

                    <p>
                        <input type=" text" class="tb-color-field" name="tb-freeslot-color"
                               value="<?= $this->settings->getColorFreeSlot() ?>">
                    </p>
                </li>
                <li>
                    <h4>
                        <?= esc_html__('Soldout slot', 'teambooking') ?>
                    </h4>

                    <p>
                        <input type="text" class="tb-color-field" name="tb-soldoutslot-color"
                               value="<?= $this->settings->getColorSoldoutSlot() ?>">
                    </p>
                </li>
                <li>
                    <h4>
                        <?= esc_html__('Border', 'teambooking') ?>
                    </h4>

                    <p>
                        <input type="number" name="tb-border-size" min="0" max="20" step="1"
                               class="small-text"
                               value="<?= $border['size'] ?>">
                        <label><?= esc_html__('Border size', 'teambooking') ?> (px)</label>
                        <br>
                        <input type="number" name="tb-border-radius" min="0" max="100" step="1"
                               class="small-text"
                               value="<?= $border['radius'] ?>">
                        <label><?= esc_html__('Border radius', 'teambooking') ?> (px)</label>

                    <p><?= esc_html__('Border color', 'teambooking') ?></p>
                    <input type="text" class="tb-color-field" name="tb-border-color"
                           value="<?= $border['color'] ?>">
                    </p>
                </li>
                <li>
                    <h4>
                        <?= esc_html__('Numbered dots', 'teambooking') ?>
                    </h4>

                    <p>
                        <?= esc_html__('What do you want the dots to show for each service?', 'teambooking') ?>
                    </p>

                    <fieldset>
                        <?= $this->view->radio(
                            esc_html__('Total number of slots available', 'teambooking'),
                            'tb-numbered-dots-logic',
                            'slots',
                            'slots',
                            $this->settings->getNumberedDotsLogic()
                        ) ?>
                        <br/>
                        <?= $this->view->radio(
                            esc_html__('Total number of tickets available', 'teambooking'),
                            'tb-numbered-dots-logic',
                            'tickets',
                            'tickets',
                            $this->settings->getNumberedDotsLogic()
                        ) ?>
                        <br/>
                        <?= $this->view->radio(
                            esc_html__('Hide the dots', 'teambooking'),
                            'tb-numbered-dots-logic',
                            'hide',
                            'hide',
                            $this->settings->getNumberedDotsLogic()
                        ) ?>
                    </fieldset>

                    <p style="margin-top:20px;">
                        <?= esc_html__('Do you want the number inside the dots to disappear under a certain threshold? (Put here a very big value, if you want to hide the numbers, while keeping the dots)', 'teambooking') ?>
                    </p>

                    <input type="number" name="tb-numbered-dots-lower-bound" min="0" step="1" class="small-text"
                           value="<?= $this->settings->getNumberedDotsLowerBound() ?>">
                </li>

                <li>
                    <?php submit_button(esc_html__('Save changes', 'teambooking'), 'primary', 'team_booking_frontend_style_submit') ?>
                </li>
            </ul>
        </div>
        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getSlotList()
    {
        ob_start();
        ?>
        <div class="tbk-panel-title">
            <h4><?= esc_html__('Frontend schedule list', 'teambooking') ?></h4>
        </div>
        <div class="tbk-content">
            <ul class="tbk-list">
                <li>
                    <h4>
                        <?= esc_html__('Sort slots', 'teambooking') ?>
                    </h4>

                    <fieldset>
                        <?= $this->view->radio(
                            esc_html__('By time (default)', 'teambooking'),
                            'tb-group-slots-by',
                            'bytime',
                            TRUE,
                            $this->settings->isGroupSlotsByTime()
                        ) ?>
                        <br/>
                        <?= $this->view->radio(
                            esc_html__('By coworker', 'teambooking'),
                            'tb-group-slots-by',
                            'bycoworker',
                            TRUE,
                            $this->settings->isGroupSlotsByCoworker()
                        ) ?>
                        <br/>
                        <?= $this->view->radio(
                            esc_html__('By service', 'teambooking'),
                            'tb-group-slots-by',
                            'byservice',
                            TRUE,
                            $this->settings->isGroupSlotsByService()
                        ) ?>
                    </fieldset>
                </li>

                <li>
                    <h4>
                        <?= esc_html__('Price label color', 'teambooking') ?>
                    </h4>
                    <style>
                        .price-label-colours .label {
                            margin-bottom: 2px;
                        }
                    </style>
                    <fieldset class="price-label-colours">
                        <?= $this->view->radio(
                            esc_html__('Red', 'teambooking'),
                            'tb-price-tag-color',
                            'red',
                            'red',
                            $this->settings->getPriceTagColor(),
                            'ui mini label red'
                        ) ?>
                        <br/>
                        <?= $this->view->radio(
                            esc_html__('Orange', 'teambooking'),
                            'tb-price-tag-color',
                            'orange',
                            'orange',
                            $this->settings->getPriceTagColor(),
                            'ui mini label orange'
                        ) ?>
                        <br/>
                        <?= $this->view->radio(
                            esc_html__('Yellow', 'teambooking'),
                            'tb-price-tag-color',
                            'yellow',
                            'yellow',
                            $this->settings->getPriceTagColor(),
                            'ui mini label yellow'
                        ) ?>
                        <br/>
                        <?= $this->view->radio(
                            esc_html__('Olive', 'teambooking'),
                            'tb-price-tag-color',
                            'olive',
                            'olive',
                            $this->settings->getPriceTagColor(),
                            'ui mini label olive'
                        ) ?>
                        <br/>
                        <?= $this->view->radio(
                            esc_html__('Green', 'teambooking'),
                            'tb-price-tag-color',
                            'green',
                            'green',
                            $this->settings->getPriceTagColor(),
                            'ui mini label green'
                        ) ?>
                        <br/>
                        <?= $this->view->radio(
                            esc_html__('Teal', 'teambooking'),
                            'tb-price-tag-color',
                            'teal',
                            'teal',
                            $this->settings->getPriceTagColor(),
                            'ui mini label teal'
                        ) ?>
                        <br/>
                        <?= $this->view->radio(
                            esc_html__('Blue', 'teambooking'),
                            'tb-price-tag-color',
                            'blue',
                            'blue',
                            $this->settings->getPriceTagColor(),
                            'ui mini label blue'
                        ) ?>
                        <br/>
                        <?= $this->view->radio(
                            esc_html__('Violet', 'teambooking'),
                            'tb-price-tag-color',
                            'violet',
                            'violet',
                            $this->settings->getPriceTagColor(),
                            'ui mini label violet'
                        ) ?>
                        <br/>
                        <?= $this->view->radio(
                            esc_html__('Purple', 'teambooking'),
                            'tb-price-tag-color',
                            'purple',
                            'purple',
                            $this->settings->getPriceTagColor(),
                            'ui mini label purple'
                        ) ?>
                        <br/>
                        <?= $this->view->radio(
                            esc_html__('Pink', 'teambooking'),
                            'tb-price-tag-color',
                            'pink',
                            'pink',
                            $this->settings->getPriceTagColor(),
                            'ui mini label pink'
                        ) ?>
                        <br/>
                        <?= $this->view->radio(
                            esc_html__('Brown', 'teambooking'),
                            'tb-price-tag-color',
                            'brown',
                            'brown',
                            $this->settings->getPriceTagColor(),
                            'ui mini label brown'
                        ) ?>
                        <br/>
                        <?= $this->view->radio(
                            esc_html__('Grey', 'teambooking'),
                            'tb-price-tag-color',
                            'grey',
                            'grey',
                            $this->settings->getPriceTagColor(),
                            'ui mini label grey'
                        ) ?>
                        <br/>
                        <?= $this->view->radio(
                            esc_html__('Black', 'teambooking'),
                            'tb-price-tag-color',
                            'black',
                            'black',
                            $this->settings->getPriceTagColor(),
                            'ui mini label black'
                        ) ?>
                    </fieldset>
                </li>
                <li>
                    <?php submit_button(esc_html__('Save changes', 'teambooking'), 'primary', 'team_booking_frontend_style_submit') ?>
                </li>
            </ul>
        </div>
        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getCalendarPreview()
    {
        ob_start();
        ?>
        <div class="tbk-panel-title">
            <h4>
                <?= esc_html__('Frontend calendar preview', 'teambooking') ?>
            </h4>
        </div>
        <div class="tbk-content" id="tb-frontend-preview">
            <div class="tbk-setting-notice">
                <span><?= esc_html__('Note', 'teambooking') ?></span>
                <?= esc_html__('This is a rough preview. Colors only are updated in real-time. For accurate results, please save the changes.', 'teambooking') ?>
            </div>
            <?php tbRenderPreview() ?>
        </div>
        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function get62dot5fix()
    {
        ob_start();
        ?>
        <div class="tbk-panel-title">
            <h4>
                <?= esc_html__('CSS fix for small fonts', 'teambooking') ?>
            </h4>
        </div>
        <div class="tbk-content" id="tb-css-fix">
            <p>
                <?= esc_html__("Some of calendar fonts appear too small? This is often due the so-called 62.5% hack used by some themes. If your theme's stylesheet applies a 62.5% font size to the html element, then you must activate this option.", 'teambooking') ?>
            </p>

            <?= $this->view->checkbox(__("Active", 'teambooking'), 'tb-css-fix', 1, $this->settings->getFix62dot5(), 1) ?>

            <div class="tbk-setting-notice">
                <span><?= esc_html__('Note', 'teambooking') ?></span>
                <?= esc_html__('Remember to empty the cache after activating or deactivating this option, or you may not be able to see any change', 'teambooking') ?>
            </div>

            <?php submit_button(esc_html__('Save changes', 'teambooking'), 'primary', 'team_booking_frontend_style_submit') ?>
        </div>
        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getMapStyles()
    {
        ob_start();
        ?>
        <div class="tbk-panel-title">
            <h4>
                <?= esc_html__('Map styles', 'teambooking') ?>
                <span style="font-weight:300;font-style: italic;">- by <a href="https://snazzymaps.com">Snazzy Maps</a></span>
            </h4>
        </div>
        <div class="tbk-content" id="tb-frontend-mapstyles">
            <ul class="tbk-list">
                <fieldset>
                    <li>
                        <label>
                            <input type="radio" name="tb-map-style"
                                   value="0" <?php checked($this->settings->getMapStyle(TRUE), 0) ?> />

                            <p>Light Gray <span>(ColorByt)</span></p>

                            <div class="tbk-map-style-preview"
                                 style="background-image: url(<?= TEAMBOOKING_URL ?>/images/mapstyle-light-gray.png);"></div>
                        </label>
                    </li>

                    <li>
                        <label>
                            <input type="radio" name="tb-map-style"
                                   value="1" <?php checked($this->settings->getMapStyle(TRUE), 1) ?> />

                            <p>Unsaturated Browns <span>(Simon Goellner)</span></p>

                            <div class="tbk-map-style-preview"
                                 style="background-image: url(<?= TEAMBOOKING_URL ?>/images/mapstyle-unsaturated-browns.png);"></div>
                        </label>
                    </li>

                    <li>
                        <label>
                            <input type="radio" name="tb-map-style"
                                   value="2" <?php checked($this->settings->getMapStyle(TRUE), 2) ?>/>

                            <p>Pale Dawn <span>(Adam Krogh)</span></p>

                            <div class="tbk-map-style-preview"
                                 style="background-image: url(<?= TEAMBOOKING_URL ?>/images/mapstyle-pale-dawn.png);"></div>
                        </label>
                    </li>

                    <li>
                        <label>
                            <input type="radio" name="tb-map-style"
                                   value="3" <?php checked($this->settings->getMapStyle(TRUE), 3) ?>/>

                            <p>Orange <span>(bjorn)</span></p>

                            <div class="tbk-map-style-preview"
                                 style="background-image: url(<?= TEAMBOOKING_URL ?>/images/mapstyle-orange.png);"></div>
                        </label>
                    </li>

                    <li>
                        <label>
                            <input type="radio" name="tb-map-style"
                                   value="4" <?php checked($this->settings->getMapStyle(TRUE), 4) ?>/>

                            <p>Coy Beauty <span>(Danika Pariseau)</span></p>

                            <div class="tbk-map-style-preview"
                                 style="background-image: url(<?= TEAMBOOKING_URL ?>/images/mapstyle-coy-beauty.png);"></div>
                        </label>
                    </li>

                    <li>
                        <?php submit_button(esc_html__('Save changes', 'teambooking'), 'primary', 'team_booking_frontend_style_submit') ?>
                    </li>
                </fieldset>
            </ul>
        </div>
        <?php
        return ob_get_clean();
    }

}
