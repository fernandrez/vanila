<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Components_Admin_Service
{

    //------------------------------------------------------------

    public $action;
    private $service_id;
    private $service_name;
    private $view;

    /** @var TeamBookingCoworker */
    private $coworker;

    /** @var TeamBookingType */
    private $service;

    public function __construct($settings, $service)
    {
        /* @var $settings TeamBookingSettings */
        $this->coworker = $settings->getCoworkerData(get_current_user_id());
        /* @var $service TeamBookingType */
        $this->service = $service;
        $this->service_name = $service->getName();
        $this->service_id = $service->getId();
        $this->view = new TeamBooking_Components_Admin_View();
    }

    //------------------------------------------------------------

    public function getPostBody()
    {
        $string_1 = esc_html__('Class', 'teambooking');
        $string_2 = esc_html__('Id', 'teambooking');
        $string_3 = strtoupper($string_1)
            . ' '
            . ucfirst($this->service->getClass())
            . ' - ' .
            strtoupper($string_2)
            . ' '
            . $this->service_id;
        ob_start();
        ?>
        <div class="tbk-wrapper">
            <div class="tbk-row">
                <div class="tbk-column tbk-span-12">
                    <?= $this->getHeaderBlock($string_3) ?>
                </div>
            </div>

            <form method="POST" action="<?= $this->action ?>">
                <input type="hidden" name="action" value="option_submit">
                <?php wp_nonce_field('team_booking_options_verify') ?>
                <input type="hidden" name="service_id" value="<?= $this->service_id ?>">
                <input type="hidden" name="service_settings" value="general">

                <div class="tbk-row">
                    <div class="tbk-column tbk-span-4">
                        <div class="tbk-panel">
                            <?= $this->getGeneralEdit() ?>
                        </div>
                        <div class="tbk-panel">
                            <?= $this->getGeneralAccess() ?>

                        </div>
                    </div>

                    <div class="tbk-column tbk-span-4">
                        <div class="tbk-panel">
                            <?= $this->getGeneralSlotsAppearance() ?>
                        </div>
                        <div class="tbk-panel">
                            <?= $this->getGeneralPayments() ?>
                        </div>
                        <div class="tbk-panel">
                            <?= $this->getGeneralRedirect() ?>
                        </div>
                    </div>

                    <div class="tbk-column tbk-span-4">
                        <div class="tbk-panel">
                            <?= $this->getGeneralApproveDeny() ?>
                        </div>
                        <?php if (!$this->service->isClass('service')) { ?>
                            <div class="tbk-panel">
                                <?= $this->getCancellationSettings() ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </form>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    public function getPostBodyEmail()
    {
        $string_1 = esc_html__('Email settings', 'teambooking');
        $hooks = $this->service->getFormFields()->getHookList();
        if (!$this->service->isClass('service')) {
            $hooks[] = "start_datetime";
            $hooks[] = "end_datetime";
            if (!$this->service->isClass('appointment')) {
                $hooks[] = "tickets_quantity";
                $hooks[] = "total_price";
            }
        }
        $hooks[] = "unit_price";
        $hooks[] = "post_id";
        $hooks[] = "post_title";
        $hooks[] = "coworker_name";
        if ($this->service->getLocationSetting() != 'none') {
            $hooks[] = "service_location";
        }
        ob_start();
        ?>
        <div class="tbk-wrapper">
            <div class="tbk-row">
                <div class="tbk-column tbk-span-12">
                    <?= $this->getHeaderBlock($string_1) ?>
                </div>
            </div>

            <div class="tbk-row">
                <form method="POST" action="<?= $this->action ?>">
                    <input type="hidden" name="action" value="option_submit">
                    <?php wp_nonce_field('team_booking_options_verify') ?>
                    <input type="hidden" name="service_id" value="<?= $this->service_id ?>">
                    <input type="hidden" name="service_settings" value="email">

                    <?php if (current_user_can('manage_options')) { ?>
                        <div class="tbk-column tbk-span-4">
                            <div class="tbk-panel">
                                <?= $this->getEmailAdminSettings($hooks) ?>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="tbk-column tbk-span-4">
                        <div class="tbk-panel">
                            <?= $this->getEmailCoworkerSettings($hooks) ?>
                        </div>
                    </div>

                    <?php if (current_user_can('manage_options')) { ?>
                        <div class="tbk-column tbk-span-4">
                            <div class="tbk-panel">
                                <?= $this->getEmailCancellationSettings($hooks) ?>
                            </div>
                        </div>
                    <?php } ?>
                </form>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    public function getPostBodyGcalSettings()
    {
        $string_1 = esc_html__('Your Google Calendar settings', 'teambooking');
        ob_start();
        // Get booking data
        ?>
        <div class="tbk-wrapper">
            <div class="tbk-row">
                <div class="tbk-column tbk-span-12">
                    <?= $this->getHeaderBlock($string_1) ?>
                </div>
            </div>

            <div class="tbk-row">
                <div class="tbk-column tbk-span-8">
                    <form method="POST" action="<?= $this->action ?>">
                        <input type="hidden" name="action" value="option_submit">
                        <input type="hidden" name="service_id" value="<?= $this->service_id ?>">
                        <?php wp_nonce_field('team_booking_options_verify') ?>
                        <div class="tbk-panel">
                            <?= $this->getGCalSettingsContent() ?>
                        </div>
                    </form>
                </div>

                <div class="tbk-column tbk-span-4">
                    <div class="tbk-panel">
                        <?= $this->getAvailabilityModes() ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    public function getPostBodyReservationForm()
    {
        $string_1 = ucfirst(esc_html__('Reservation form', 'teambooking'));
        ob_start();
        ?>
        <div class="tbk-wrapper">
            <div class="tbk-row">
                <div class="tbk-column tbk-span-12">
                    <?= $this->getHeaderBlock($string_1) ?>
                </div>
            </div>
            <div class="tbk-row">
                <div class="tbk-column tbk-span-4">
                    <div class="tbk-panel">
                        <?= $this->getBuiltInFieldsSettings() ?>
                    </div>
                </div>
                <div class="tbk-column tbk-span-8">
                    <div class="tbk-panel">
                        <?= $this->getCustomFieldsSettings() ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getEmailAdminSettings($hooks)
    {
        $string_1 = ucfirst(esc_html__('Admin notification and customer confirmation', 'teambooking'));
        $string_2 = esc_html__('Send:', 'teambooking');
        $string_3 = esc_html__('Notification email to admin', 'teambooking');
        $string_4 = esc_html__('Confirmation email to customer', 'teambooking');
        $string_5 = esc_html__('Admin address for receiving notifications', 'teambooking');
        $string_6 = esc_html__('This address will be also the sender of confirmation email to customers', 'teambooking');
        $string_8 = esc_html__('Confirmation email subject', 'teambooking');
        $string_9 = esc_html__('Confirmation email body', 'teambooking');
        $string_10 = esc_html__('Save changes', 'teambooking');
        $string_11 = esc_html__('Include uploaded files as attachment', 'teambooking');
        $string_12 = esc_html__('Notification email subject', 'teambooking');
        $string_13 = esc_html__('Notification email body', 'teambooking');
        $string_14 = esc_html__('Yes', 'teambooking');
        $string_15 = esc_html__('No', 'teambooking');
        $string_16 = esc_html__('Available form hooks (click to insert at cursor point):', 'teambooking');
        $fieldname_1 = 'event[email_to_email]';
        $fieldname_2 = 'event[email_to_customer]';
        $fieldname_3 = 'event[email_for_notifications]';
        $fieldname_4 = 'event[include_files_as_attachment]';
        $fieldname_5 = 'event[back_end_email][subject]';
        $fieldname_6 = 'event[back_end_email][body]';
        $fieldname_7 = 'event[front_end_email][subject]';
        $fieldname_8 = 'event[front_end_email][body]';
        ob_start();
        ?>

        <?php $this->view->panelTitle($string_1) ?>

        <div class="tbk-content">
            <ul class="tbk-list">
                <li>
                    <?php $this->view->segmentHeader($string_2) ?>

                    <fieldset>

                        <?php $this->view->checkbox($string_3, $fieldname_1, 1, 1, $this->service->getNotify()) ?>
                        <br>
                        <?php $this->view->checkbox($string_4, $fieldname_2, 1, 1, $this->service->getConfirm()) ?>

                    </fieldset>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_5, $string_6) ?>

                    <p>
                        <?php $this->view->textfield($this->service->getEmailForNotifications(), $fieldname_3, '', TRUE) ?>
                    </p>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_11) ?>

                    <fieldset>

                        <?php $this->view->radio($string_14, $fieldname_4, 1, $this->service->getIncludeFilesAsAttachment(), TRUE) ?>
                        <br/>
                        <?php $this->view->radio($string_15, $fieldname_4, 0, $this->service->getIncludeFilesAsAttachment(), FALSE) ?>

                    </fieldset>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_12) ?>

                    <p>
                        <?php $this->view->textfield($this->service->getBackendEmail()->getSubject(), $fieldname_5) ?>
                    </p>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_13, $string_16) ?>

                    <p class="description">
                        <?php
                        foreach ($hooks as $hook) {
                            $this->view->ui_mini_label('[' . $hook . ']', '', '', 'tb-hook-placeholder', 'a');
                        } ?>
                    </p>

                    <?php
                    wp_editor(
                        $this->service->getBackendEmail()->getBody(),
                        'event_back_end_email_body',
                        array(
                            'media_buttons' => FALSE,
                            'textarea_name' => $fieldname_6,
                            'tinymce'       => FALSE,
                        )
                    ); ?>

                </li>

                <li>
                    <?php $this->view->segmentHeader($string_8) ?>

                    <p>
                        <?php $this->view->textfield($this->service->getFrontendEmail()->getSubject(), $fieldname_7) ?>
                    </p>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_9, $string_6) ?>

                    <p class="description">
                        <?php
                        foreach ($hooks as $hook) {
                            $this->view->ui_mini_label('[' . $hook . ']', '', '', 'tb-hook-placeholder', 'a');
                        } ?>
                    </p>

                    <?php
                    wp_editor(
                        $this->service->getFrontendEmail()->getBody(),
                        'event_front_end_email_body',
                        array(
                            'media_buttons' => FALSE,
                            'textarea_name' => $fieldname_8,
                            'tinymce'       => FALSE,
                        )
                    ); ?>
                </li>

                <li>
                    <?php submit_button($string_10, 'primary', 'team_booking_update_booking_type') ?>
                </li>
            </ul>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getEmailCoworkerSettings($hooks)
    {
        $custom_booking_type_settings = $this->coworker->getCustomEventSettings($this->service_id);
        $string_1 = esc_html__('Get reservation details by email', 'teambooking');
        $string_2 = esc_html__('Even if the relative Google Calendar event description will still be filled with reservation data, is recommended to keep this option active.', 'teambooking');
        $string_3 = esc_html__('Include uploaded files as attachment', 'teambooking');
        $string_4 = esc_html__('Notification email subject', 'teambooking');
        $string_5 = esc_html__('Notification email body', 'teambooking');
        $string_6 = esc_html__('Available form hooks (click to insert at cursor point):', 'teambooking');
        $string_7 = esc_html__('Yes', 'teambooking');
        $string_8 = esc_html__('No', 'teambooking');
        $string_9 = esc_html__('Save changes', 'teambooking');
        $string_10 = ucfirst(esc_html__('Coworker notification', 'teambooking'));
        $fieldname_1 = 'data[get_details_by_email]';
        $fieldname_2 = 'data[include_uploaded_files_as_attachment]';
        $fieldname_3 = 'data[email][email_text][subject]';
        $fieldname_4 = 'data[email][email_text][body]';
        ob_start();
        ?>

        <?php $this->view->panelTitle($string_10) ?>

        <div class="tbk-content">
            <ul class="tbk-list">
                <li>
                    <?php $this->view->segmentHeader($string_1, $this->service->isClass('appointment') ? $string_2 : '') ?>

                    <fieldset>

                        <?php $this->view->radio($string_7, $fieldname_1, 'true', $custom_booking_type_settings->getGetDetailsByEmail(), TRUE) ?>
                        <br/>
                        <?php $this->view->radio($string_8, $fieldname_1, 'false', $custom_booking_type_settings->getGetDetailsByEmail(), FALSE) ?>

                    </fieldset>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_3) ?>

                    <fieldset>

                        <?php $this->view->radio($string_7, $fieldname_2, 'true', $custom_booking_type_settings->getIncludeFilesAsAttachment(), TRUE) ?>
                        <br/>
                        <?php $this->view->radio($string_8, $fieldname_2, 'false', $custom_booking_type_settings->getIncludeFilesAsAttachment(), FALSE) ?>

                    </fieldset>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_4) ?>

                    <p>
                        <?php $this->view->textfield($custom_booking_type_settings->getNotificationEmailSubject(), $fieldname_3) ?>
                    </p>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_5, $string_6) ?>

                    <p class="description">
                        <?php foreach ($hooks as $hook) {
                            $this->view->ui_mini_label('[' . $hook . ']', '', '', 'tb-hook-placeholder', 'a');
                        } ?>
                    </p>

                    <?php
                    wp_editor(
                        $custom_booking_type_settings->getNotificationEmailBody(),
                        'email_text_body',
                        array(
                            'media_buttons' => FALSE,
                            'textarea_name' => $fieldname_4,
                            'tinymce'       => FALSE,
                        )
                    ); ?>
                </li>

                <li>
                    <?php submit_button($string_9, 'primary', 'team_booking_update_booking_type') ?>
                </li>
            </ul>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getEmailCancellationSettings($hooks)
    {
        $string_6 = esc_html__('Available form hooks (click to insert at cursor point):', 'teambooking');
        $string_7 = esc_html__('Yes', 'teambooking');
        $string_8 = esc_html__('No', 'teambooking');
        $string_20 = esc_html__('Save changes', 'teambooking');
        $string_21 = ucfirst(esc_html__('Cancellation notification', 'teambooking'));
        $string_22 = esc_html__('Send an email back to customer when a reservation is cancelled', 'teambooking');
        $string_23 = esc_html__('Cancellation email subject', 'teambooking');
        $string_24 = esc_html__('Cancellation email body', 'teambooking');
        $fieldname_13 = 'event[send_cancellation_email]';
        $fieldname_14 = 'event[cancellation_email][subject]';
        $fieldname_15 = 'event[cancellation_email][body]';
        ob_start();
        ?>

        <?php $this->view->panelTitle($string_21) ?>

        <div class="tbk-content">
            <ul class="tbk-list">
                <li>
                    <?php $this->view->segmentHeader($string_22) ?>

                    <fieldset>

                        <?php $this->view->radio($string_7, $fieldname_13, 1, $this->service->getSendCancellation(), TRUE) ?>
                        <br/>
                        <?php $this->view->radio($string_8, $fieldname_13, 0, $this->service->getSendCancellation(), FALSE) ?>

                    </fieldset>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_23) ?>

                    <p>
                        <?php $this->view->textfield($this->service->getCancellationEmail()->getSubject(), $fieldname_14) ?>
                    </p>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_24, $string_6) ?>

                    <p class="description">
                        <?php foreach ($hooks as $hook) {
                            $this->view->ui_mini_label('[' . $hook . ']', '', '', 'tb-hook-placeholder', 'a');
                        } ?>
                    </p>

                    <?php
                    wp_editor(
                        $this->service->getCancellationEmail()->getBody(),
                        'cancellation_email_body',
                        array(
                            'media_buttons' => FALSE,
                            'textarea_name' => $fieldname_15,
                            'tinymce'       => FALSE,
                        )
                    ); ?>

                </li>

                <li>
                    <?php submit_button($string_20, 'primary', 'team_booking_update_booking_type') ?>
                </li>
            </ul>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getHeaderBlock($description)
    {
        $string_1 = esc_html__('Back to list', 'teambooking');
        ob_start();
        ?>
        <div class="tbk-panel tbk-content">
            <div class="tbk-settings-title">
                <a href="<?= admin_url('admin.php?page=team-booking&tab=events') ?>"
                   class="button button-hero button-primary"><span
                        class="dashicons dashicons-arrow-left-alt"></span><?= $string_1 ?></a>

                <h3 class="tbk-heading">
                    <?= $this->service_name ?>
                </h3>

                <p class="tbk-excerpt">
                    <?= $description ?>
                </p>
            </div>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getGeneralEdit()
    {
        $string_1 = esc_html__('General settings', 'teambooking');
        $string_4 = esc_html__('Name', 'teambooking');
        $string_5 = esc_html__('Service description', 'teambooking');
        $string_6 = esc_html__('This description will be shown on reservation modal. You can use html formatting.', 'teambooking');
        $string_7 = esc_html__('Service color', 'teambooking');
        $string_8 = esc_html__('Max reservations per slot', 'teambooking');
        $string_9 = esc_html__('Max value = 200', 'teambooking');
        $string_10 = esc_html__('Max tickets per reservation', 'teambooking');
        $string_11 = esc_html__('For "event" services, a customer (identified by email address) can place one reservation per slot. If you want to allow the customer to book multiple tickets, provide the maximum number here.', 'teambooking');
        $string_12 = esc_html__('Slot duration rule', 'teambooking');
        $string_13 = esc_html__('Let Coworkers decide', 'teambooking');
        $string_14 = esc_html__('Inherited from Google Calendar event', 'teambooking');
        $string_15 = esc_html__('Fixed:', 'teambooking');
        $string_16 = esc_html__('Hours', 'teambooking');
        $string_17 = esc_html__('Minutes', 'teambooking');
        $string_18 = esc_html__("Assignment rule", 'teambooking');
        $string_19 = esc_html__('Equal', 'teambooking');
        $string_20 = esc_html__('Direct', 'teambooking');
        $string_21 = esc_html__('Random', 'teambooking');
        $string_23 = esc_html__("Location", 'teambooking');
        $string_24 = esc_html__("If a location is set, a map will appear on the reservation form. If inherited from Google Calendar slot, but the slot has no location, the map won't show.", 'teambooking');
        $string_25 = esc_html__('No location', 'teambooking');
        $string_26 = esc_html__('Inherited from Google Calendar slot', 'teambooking');
        $string_27 = esc_html__('Fixed location:', 'teambooking');
        $string_28 = esc_html__('Save changes', 'teambooking');
        $string_29 = esc_html__('Note: if set to "no location" and the reservation form has the built-in address field active, then the Google Calendar event will be updated with the address of the customer, if given. Only for Appointment Class.', 'teambooking');
        $string_30 = esc_html__('In case of multiple coworkers giving the availability to this service, it needs a rule to decide which of them will be assigned to a new reservation, time after time.', 'teambooking');
        $string_31 = esc_html__('A new reservation will be assigned to the coworker with less of them assigned (for this service), based on reservation history in the database.', 'teambooking');
        $string_32 = esc_html__('A new reservation will be always assigned to the coworker specified below.', 'teambooking');
        $string_33 = esc_html__('Picks a random coworker.', 'teambooking');
        $fieldname_1 = 'event[booking_type_name]';
        $fieldname_2 = 'event[info]';
        $fieldname_3 = 'event[service_color]';
        $fieldname_4 = 'event[max_attendees]';
        $fieldname_5 = 'event[max_tickets_per_reservation]';
        $fieldname_6 = 'event[duration_rule]';
        $fieldname_7 = 'event[default_duration_hours]';
        $fieldname_8 = 'event[default_duration_minutes]';
        $fieldname_9 = 'event[assignment_rule]';
        $fieldname_10 = 'event[direct_coworker]';
        $fieldname_11 = 'event[location_setting]';
        $fieldname_12 = 'event[location_address]';
        ob_start();
        ?>

        <?php $this->view->panelTitle($string_1) ?>

        <div class="tbk-content">
            <ul class="tbk-list">
                <li>
                    <?php $this->view->segmentHeader($string_4) ?>

                    <p>
                        <?php $this->view->textfield($this->service_name, $fieldname_1, '', TRUE, 'e.g. House Surveys') ?>
                    </p>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_5, $string_6) ?>

                    <?php
                    wp_editor(
                        $this->service->getServiceInfo(),
                        'service_info',
                        array(
                            'media_buttons' => FALSE,
                            'textarea_name' => $fieldname_2,
                            'tinymce'       => FALSE,
                            'textarea_rows' => 8,
                        )
                    ); ?>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_7) ?>

                    <p>
                        <?php $this->view->colorField($fieldname_3, $this->service->getServiceColor()) ?>
                    </p>
                </li>

                <?php if ($this->service->getAttendees() != NULL) { ?>
                    <li>
                        <?php $this->view->segmentHeader($string_8, $string_9) ?>

                        <p>
                            <input type="number" name="<?= $fieldname_4 ?>" min="1" max="200" class="small-text"
                                   value="<?= $this->service->getAttendees() ?>">
                        </p>
                    </li>

                    <li>
                        <?php $this->view->segmentHeader($string_10, $string_11) ?>

                        <p>
                            <input type="number" name="<?= $fieldname_5 ?>" min="1"
                                   max="<?= $this->service->getAttendees() ?>" class="small-text"
                                   value="<?= $this->service->getMaxReservationsPerUser() ?>">
                        </p>
                    </li>
                <?php } ?>

                <?php
                if (!$this->service->isClass('service')) {
                    $service_hours = floor($this->service->getDefaultDuration() / 3600);
                    $service_minutes = ceil($this->service->getDefaultDuration() - $service_hours * 3600) / 60;
                    ?>
                    <li>
                        <?php $this->view->segmentHeader($string_12) ?>

                        <fieldset>

                            <?php $this->view->radio($string_13, $fieldname_6, 'coworker', $this->service->getDurationRule(), 'coworker') ?>
                            <br/>
                            <?php $this->view->radio($string_14, $fieldname_6, 'inherited', $this->service->getDurationRule(), 'inherited') ?>
                            <br/>
                            <?php $this->view->radio($string_15, $fieldname_6, 'fixed', $this->service->getDurationRule(), 'fixed') ?>

                            <select name="<?= $fieldname_7 ?>">
                                <?php for ($i = 0; $i < 24; $i++) {
                                    $this->view->select_option($i, $i, $service_hours, $i);
                                } ?>
                            </select>

                            <span><?= $string_16 ?></span>

                            <select name="<?= $fieldname_8 ?>">
                                <?php for ($i = 0; $i < 60; $i++) {
                                    $this->view->select_option($i, $i, $service_minutes, $i);
                                } ?>
                            </select>

                            <span><?= $string_17 ?></span>

                        </fieldset>
                    </li>
                <?php } ?>

                <?php if ($this->service->isClass('service')) { ?>
                    <li>
                        <?php $this->view->segmentHeader($string_18, $string_30) ?>

                        <fieldset>

                            <div class="tbk-light-divider"></div>
                            <?php $this->view->radio($string_19, $fieldname_9, 'equal', $this->service->getAssignmentRule(), 'equal') ?>
                            <p>
                                <?= $string_31 ?>
                            </p>

                            <div class="tbk-light-divider"></div>
                            <?php $this->view->radio($string_20, $fieldname_9, 'direct', $this->service->getAssignmentRule(), 'direct') ?>

                            <p>
                                <?= $string_32 ?>
                            </p>

                            <p>
                                <select
                                    name="<?= $fieldname_10 ?>" <?php disabled($this->service->getAssignmentRule() == 'direct', FALSE) ?>>
                                    <?php
                                    $coworkers = tbGetCoworkersIdList();
                                    $actual_direct_coworker = $this->service->getCoworkerForDirectAssignment();

                                    foreach ($coworkers as $id) {
                                        $user_data = get_userdata($id);
                                        if ($id == $actual_direct_coworker) {
                                            ?>
                                            <option selected="selected" value="<?= $id ?>">
                                                <?= $user_data->user_firstname ?>
                                                <?= $user_data->user_lastname ?>
                                                (<?= $user_data->user_email ?>)
                                            </option>
                                        <?php } else { ?>
                                            <option value="<?= $id ?>">
                                                <?= $user_data->user_firstname ?>
                                                <?= $user_data->user_lastname ?>
                                                (<?= $user_data->user_email ?>)
                                            </option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </p>

                            <div class="tbk-light-divider"></div>
                            <?php $this->view->radio($string_21, $fieldname_9, 'random', $this->service->getAssignmentRule(), 'random') ?>
                            <p>
                                <?= $string_33 ?>
                            </p>

                        </fieldset>

                        <!-- script to disable/enable the coworker dropdown -->
                        <script>
                            jQuery('input[name="<?= $fieldname_9 ?>"]').on('click', function () {
                                if (this.value === 'direct') {
                                    jQuery('select[name="<?= $fieldname_10 ?>"]').prop('disabled', false);
                                } else {
                                    jQuery('select[name="<?= $fieldname_10 ?>"]').prop('disabled', 'disabled');
                                }
                            });
                        </script>
                    </li>
                <?php } ?>

                <li>
                    <?php $this->view->segmentHeader($string_23, $string_24) ?>

                    <fieldset>

                        <?php $this->view->radio($string_25, $fieldname_11, 'none', $this->service->getLocationSetting(), 'none') ?>
                        <br/>

                        <?php if (!$this->service->isClass('service')) { ?>
                            <?php $this->view->radio($string_26, $fieldname_11, 'inherited', $this->service->getLocationSetting(), 'inherited') ?>
                            <br/>
                        <?php } ?>

                        <?php $this->view->radio($string_27, $fieldname_11, 'fixed', $this->service->getLocationSetting(), 'fixed') ?>
                        <br/>
                        <input type="text" name="<?= $fieldname_12 ?>" class="regular-text"
                               value="<?= $this->service->getLocationAddress() ?>"
                               placeholder="e.g. 1234, Main Street, Anytown, USA" <?php disabled($this->service->getLocationSetting() == 'fixed', FALSE) ?>>

                    </fieldset>

                    <?php if ($this->service->isClass('appointment')) { ?>
                        <div class="tbk-setting-alert">
                            <?= $string_29 ?>
                        </div>
                    <?php } ?>
                </li>

                <li>
                    <?php submit_button($string_28, 'primary', 'team_booking_update_booking_type') ?>
                </li>
            </ul>
            <!-- script to disable/enable address textfield -->
            <script>
                jQuery('input[name="<?= $fieldname_11 ?>"]').on('click', function () {
                    if (this.value === 'fixed') {
                        jQuery('input[name="<?= $fieldname_12 ?>"]').prop('disabled', false);
                    } else {
                        jQuery('input[name="<?= $fieldname_12 ?>"]').prop('disabled', 'disabled');
                    }
                });
            </script>
        </div>
        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getGeneralPayments()
    {
        $string_28 = esc_html__('Payments settings', 'teambooking');
        $string_29 = esc_html__("Price for reservation", 'teambooking');
        $string_30 = esc_html__("If zero, won't appears. To change the currency, go to Payments Gateway tab.", 'teambooking');
        $string_31 = esc_html__('Payment must be done:', 'teambooking');
        $string_32 = esc_html__('Immediately', 'teambooking');
        $string_33 = esc_html__('The customer is redirect to the payment gateway after the reservation. If no payment will be done within the Max Pending Time specified in Payments gateway section, the reservation will be released.', 'teambooking');
        $string_34 = esc_html__('Later', 'teambooking');
        $string_35 = esc_html__('The customer will pay on a later time. Team Booking will not handle the payment, and you should manually set the reservation as "Paid", eventually.', 'teambooking');
        $string_36 = esc_html__("At the customer's discretion", 'teambooking');
        $string_37 = esc_html__('The customer choose whether redirect to the payment gateway after the reservation, or pay locally/later. If he wants to pay on a later time and/or cancel the payment, the reservation will still be in place, as "Not paid". You should manually set the reservation as "Paid", eventually.', 'teambooking');
        $string_38 = esc_html__('Save changes', 'teambooking');
        $fieldname_13 = 'event[service_price]';
        $fieldname_14 = 'event[payment_must_be_done]';
        ob_start();
        ?>

        <?php $this->view->panelTitle($string_28) ?>

        <div class="tbk-content">
            <ul class="tbk-list">
                <li>
                    <?php $this->view->segmentHeader($string_29, $string_30) ?>

                    <p>
                        <input type="number" name="<?= $fieldname_13 ?>" min="0" step="0.01" class="small-text"
                               value="<?= $this->service->getPrice() ?>">
                        <?php
                        echo tbCurrencyCodeToSymbol(tbGetSettings()->getCurrencyCode(), NULL);
                        ?>
                    </p>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_31) ?>

                    <fieldset>
                        <label>
                            <table>
                                <tr>
                                    <td style="vertical-align: baseline;padding: 0;">
                                        <input type="radio" name="<?= $fieldname_14 ?>"
                                               value="immediately" <?php checked($this->service->getPaymentMustBeDone(), 'immediately') ?> />
                                    </td>
                                    <td style="vertical-align: baseline;padding: 0;">
                                        <span>
                                            <strong><?= $string_32 ?>.</strong>
                                            <?= $string_33 ?>
                                        </span></td>
                                </tr>
                            </table>
                        </label>
                        <br/>
                        <label>
                            <table>
                                <tr>
                                    <td style="vertical-align: baseline;padding: 0;">
                                        <input type="radio" name="<?= $fieldname_14 ?>"
                                               value="later" <?php checked($this->service->getPaymentMustBeDone(), 'later') ?> />
                                    </td>
                                    <td style="vertical-align: baseline;padding: 0;">
                                        <span>
                                            <strong><?= $string_34 ?>.</strong>
                                            <?= $string_35 ?>
                                        </span></td>
                                </tr>
                            </table>
                        </label>
                        <br/>
                        <label>
                            <table>
                                <tr>
                                    <td style="vertical-align: baseline;padding: 0;">
                                        <input type="radio" name="<?= $fieldname_14 ?>"
                                               value="discretional" <?php checked($this->service->getPaymentMustBeDone(), 'discretional') ?> />
                                    </td>
                                    <td style="vertical-align: baseline;padding: 0;">
                                        <span>
                                            <strong><?= $string_36 ?>.</strong>
                                            <?= $string_37 ?>
                                        </span></td>
                                </tr>
                            </table>
                        </label>
                    </fieldset>
                </li>

                <li>
                    <?php submit_button($string_38, 'primary', 'team_booking_update_booking_type') ?>
                </li>
            </ul>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getGeneralRedirect()
    {
        $string_1 = esc_html__('Save changes', 'teambooking');
        $string_2 = esc_html__('Redirect', 'teambooking');
        $string_3 = esc_html__('If activated, the customer will be redirect to the specified URL after a successful reservation for this service. This is meant for tracking conversions. If an offsite payment gateway like PayPal is chosen, then this URL will override the one specified in the gateway settings and will be called after the payment (but if the customer decides not to be redirected, then an eventual conversion code will not be triggered).', 'teambooking');
        $string_4 = esc_html__('Active', 'teambooking');
        $string_5 = esc_html__('Redirect URL', 'teambooking');
        $string_6 = esc_html__('Note', 'teambooking');
        $string_7 = esc_html__("About payments: when redirect is active, while the immediate and later payment settings will work just fine, the customer's discretional payment setting otherwise won't work if selected, so no payment option will be presented to the customer after the reservation.", 'teambooking');
        $string_8 = esc_html__('Redirect and conversion tracking', 'teambooking');
        $fieldname_1 = 'event[redirect]';
        $fieldname_2 = 'event[redirect_url]';
        ob_start();
        ?>

        <?php $this->view->panelTitle($string_8) ?>

        <div class="tbk-content">
            <ul class="tbk-list">

                <li>
                    <?php $this->view->segmentHeader($string_2, $string_3) ?>

                    <?php $this->view->checkbox($string_4, $fieldname_1, 1, $this->service->getRedirect(), TRUE) ?>
                    <p style="margin-top: 20px"><?= $string_5 ?></p>
                    <?php $this->view->textfield($this->service->getRedirectUrl(), $fieldname_2, 'tbk-redirect-url-field', FALSE, 'http://, https://', !$this->service->getRedirect()) ?>
                    <div class="tbk-setting-alert">
                        <span><?= $string_6 ?> </span><?= $string_7 ?>
                    </div>
                </li>

                <li>
                    <?php submit_button($string_1, 'primary', 'team_booking_update_booking_type') ?>
                </li>
            </ul>
            <!-- script to disable/enable redirect URL textfield -->
            <script>
                jQuery('input[name="<?= $fieldname_1 ?>"]').on('click', function () {
                    if (this.checked) {
                        jQuery('#tbk-redirect-url-field').prop('disabled', false);
                    } else {
                        jQuery('#tbk-redirect-url-field').prop('disabled', 'disabled');
                    }
                });
            </script>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getGeneralSlotsAppearance()
    {
        $string_38 = esc_html__('Frontend slot settings', 'teambooking');
        $string_39 = esc_html__('Show reservations left', 'teambooking');
        $string_40 = esc_html__('Yes', 'teambooking');
        $string_41 = esc_html__('No', 'teambooking');
        $string_42 = esc_html__('Show sold-out slots', 'teambooking');
        $string_43 = esc_html__('Show start/end times', 'teambooking');
        $string_44 = esc_html__("Show coworker's name", 'teambooking');
        $string_45 = esc_html__("Link coworker's profile page", 'teambooking');
        $string_46 = esc_html__("If yes, and if Coworker's name is shown, then it becomes a link pointing to Coworker's profile page. You can set the profile page for each of your Coworkers in the Manage Coworkers tab.", 'teambooking');
        $string_47 = esc_html__('Save changes', 'teambooking');
        $fieldname_15 = 'event[show_reservations_left]';
        $fieldname_16 = 'event[show_soldout]';
        $fieldname_17 = 'event[show_times]';
        $fieldname_18 = 'event[show_coworker]';
        $fieldname_19 = 'event[show_coworker_url]';
        ob_start();
        ?>

        <?php $this->view->panelTitle($string_38) ?>

        <div class="tbk-content">
            <ul class="tbk-list">
                <?php if (!$this->service->isClass('service')) { ?>
                    <?php if ($this->service->isClass('event')) { ?>
                        <li>
                            <?php $this->view->segmentHeader($string_39) ?>

                            <fieldset>

                                <?php $this->view->radio($string_40, $fieldname_15, 1, $this->service->getShowReservationsLeft(), TRUE) ?>
                                <br/>
                                <?php $this->view->radio($string_41, $fieldname_15, 0, $this->service->getShowReservationsLeft(), FALSE) ?>

                            </fieldset>
                        </li>
                    <?php } ?>

                    <li>
                        <?php $this->view->segmentHeader($string_42) ?>

                        <fieldset>

                            <?php $this->view->radio($string_40, $fieldname_16, 1, $this->service->getShowSoldout(), TRUE) ?>
                            <br/>
                            <?php $this->view->radio($string_41, $fieldname_16, 0, $this->service->getShowSoldout(), FALSE) ?>

                        </fieldset>
                    </li>

                    <li>
                        <?php $this->view->segmentHeader($string_43) ?>

                        <fieldset>
                            <?php
                            foreach ($this->service->getShowTimes()->getOptionsList() as $var => $value) {
                                $this->view->radio($this->service->getShowTimes()->getOptionDescription($var), $fieldname_17, $var, $value, TRUE);
                                echo '<br>';
                            }
                            ?>
                        </fieldset>
                    </li>
                <?php } ?>

                <li>
                    <?php $this->view->segmentHeader($string_44) ?>

                    <fieldset>

                        <?php $this->view->radio($string_40, $fieldname_18, 1, $this->service->getShowCoworker(), TRUE) ?>
                        <br/>
                        <?php $this->view->radio($string_41, $fieldname_18, 0, $this->service->getShowCoworker(), FALSE) ?>

                    </fieldset>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_45, $string_46) ?>

                    <fieldset>

                        <?php $this->view->radio($string_40, $fieldname_19, 1, $this->service->getShowCoworkerUrl(), TRUE) ?>
                        <br/>
                        <?php $this->view->radio($string_41, $fieldname_19, 0, $this->service->getShowCoworkerUrl(), FALSE) ?>

                    </fieldset>
                </li>

                <li>
                    <?php submit_button($string_47, 'primary', 'team_booking_update_booking_type') ?>
                </li>
            </ul>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getGeneralApproveDeny()
    {
        $string_1 = esc_html__('Approval settings', 'teambooking');
        $string_2 = esc_html__('Choose wheter the reservations require approval or not', 'teambooking');
        $string_3 = esc_html__('If approval is required, then the reservation will be shown in the overview panel in "approval required" status. No actions like sending confirmation email, or updating Google Calendar will be taken, until the reservation is confirmed.', 'teambooking');
        $string_4 = esc_html__('Do not require approval', 'teambooking');
        $string_5 = esc_html__('Require Admin approval', 'teambooking');
        $string_6 = esc_html__('Require Coworker approval', 'teambooking');
        $string_7 = esc_html__('Save changes', 'teambooking');
        $string_8 = esc_html__('Note', 'teambooking');
        $string_9 = esc_html__('you can activate approval only if the "payment must be done" setting for this service is set to "later"!', 'teambooking');
        $string_10 = esc_html__('Until approval:', 'teambooking');
        $string_11 = esc_html__('keep the slot/tickets free (could lead to overbooking)', 'teambooking');
        $string_12 = esc_html__('set the slot/tickets in a booked status', 'teambooking');
        $string_13 = esc_html__('Notification', 'teambooking');
        $string_14 = esc_html__('Send an email notification to admin/coworker when a reservation requires approval', 'teambooking');
        $string_15 = esc_html__('Email subject', 'teambooking');
        $string_16 = esc_html__('Email text', 'teambooking');
        $fieldname_1 = 'event[approve_rule]';
        $fieldname_2 = 'event[approve_until]';
        $fieldname_3 = 'event[approve_notification]';
        $fieldname_4 = 'event[approve_notification_subject]';
        $fieldname_5 = 'event[approve_notification_text]';
        ob_start();
        ?>

        <?php $this->view->panelTitle($string_1) ?>

        <div class="tbk-content">
            <ul class="tbk-list">
                <li>
                    <?php $this->view->segmentHeader($string_2, $string_3) ?>

                    <?php if ($this->service->getPaymentMustBeDone() !== 'later') { ?>
                        <div class="tbk-setting-alert">
                            <span><?= $string_8 ?></span>
                            <?= $string_9 ?>
                        </div>
                    <?php } ?>

                    <fieldset>

                        <?php $this->view->radio($string_4, $fieldname_1, 'none', $this->service->getApproveRule(), 'none') ?>
                        <br/>
                        <label><input type="radio" name="<?= $fieldname_1 ?>"
                                      value="admin" <?php checked($this->service->getApproveRule(), 'admin') ?> <?php disabled($this->service->getPaymentMustBeDone() === 'later', FALSE) ?>/>
                            <span><?= $string_5 ?></span></label><br/>
                        <label><input type="radio" name="<?= $fieldname_1 ?>"
                                      value="coworker" <?php checked($this->service->getApproveRule(), 'coworker') ?> <?php disabled($this->service->getPaymentMustBeDone() === 'later', FALSE) ?>/>
                            <span><?= $string_6 ?></span></label>

                    </fieldset>
                </li>

                <?php if (!$this->service->isClass('service')) { ?>
                    <li>
                        <?php $this->view->segmentHeader($string_10) ?>

                        <fieldset>

                            <?php $this->view->radio($string_11, $fieldname_2, 'yes', $this->service->getFreeUntilApproval(), TRUE) ?>
                            <br/>
                            <?php $this->view->radio($string_12, $fieldname_2, 'no', $this->service->getFreeUntilApproval(), FALSE) ?>

                        </fieldset>
                    </li>
                <?php } ?>

                <li>
                    <?php $this->view->segmentHeader($string_13) ?>
                    <?php $this->view->checkbox($string_14, $fieldname_3, 'approve_notification', $this->service->getSendApprovalNotification(), TRUE) ?>
                    <br/><br/>

                    <p>
                        <?= $string_15 ?>
                    </p>
                    <?php $this->view->textfield($this->service->getApprovalNotificationSubject(), $fieldname_4) ?>
                    <p>
                        <?= $string_16 ?>
                    </p>
                    <?php
                    wp_editor(
                        $this->service->getApprovalNotificationText(),
                        $fieldname_5,
                        array(
                            'media_buttons' => FALSE,
                            'textarea_name' => $fieldname_5,
                            'tinymce'       => FALSE,
                            'textarea_rows' => 8,
                        )
                    ); ?>
                </li>

                <li>
                    <?php submit_button($string_7, 'primary', 'team_booking_update_booking_type') ?>
                </li>
            </ul>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getCancellationSettings()
    {
        $string_1 = esc_html__('Cancellation settings', 'teambooking');
        $string_2 = esc_html__('Cancellation by customer', 'teambooking');
        $string_3 = esc_html__('Allow or deny the customer to cancel a reservation for this service. If allowed, customers can cancel their reservations for this service by visiting the page where [tb-reservations] shortcode is placed. They must be logged users.', 'teambooking');
        $string_4 = esc_html__('Allow', 'teambooking');
        $string_5 = esc_html__('Deny', 'teambooking');
        $string_6 = esc_html__('Cancellation time limit', 'teambooking');
        $string_7 = esc_html__("Choose a time limit, relative to the reservation's start time, after which the cancellation will be not possible anymore by the customer.", 'teambooking');
        $string_8 = esc_html__('minutes', 'teambooking');
        $string_9 = esc_html__('hours', 'teambooking');
        $string_10 = esc_html__('days', 'teambooking');
        $fieldname_1 = 'event[allow_customer_cancellation]';
        $fieldname_2 = 'event[allow_customer_cancellation_until_value]';
        $fieldname_3 = 'event[allow_customer_cancellation_until_unit]';

        $mins = 0;
        $hours = 0;
        $days = 0;
        $allow_customer_cancellation_until_value = $this->service->getAllowCustomerCancellationUntil() / 60;
        if (floor($allow_customer_cancellation_until_value) > 0) {
            $mins = floor($allow_customer_cancellation_until_value);
        }
        if (floor($mins / 60) > 0 && $mins / 60 - floor($mins / 60) == 0) {
            $hours = floor($mins / 60);
            $mins = 0;
        }
        if (floor($hours / 24) > 0 && $hours / 24 - floor($hours / 24) == 0) {
            $days = floor($hours / 24);
            $hours = 0;
            $mins = 0;
        }
        if ($days > 0) {
            $unit = 'days';
            $unit_value = $days;
        } elseif ($hours > 0) {
            $unit = 'hours';
            $unit_value = $hours;
        } else {
            $unit = 'mins';
            $unit_value = $mins;
        }

        ob_start();
        ?>
        <?php $this->view->panelTitle($string_1) ?>
        <div class="tbk-content">
            <ul class="tbk-list">
                <li>
                    <?php $this->view->segmentHeader($string_2, $string_3) ?>

                    <fieldset>
                        <?php $this->view->radio($string_4, $fieldname_1, 1, $this->service->getAllowCustomerCancellation(), TRUE) ?>
                        <br/>
                        <?php $this->view->radio($string_5, $fieldname_1, 0, $this->service->getAllowCustomerCancellation(), FALSE) ?>

                    </fieldset>
                </li>

                <li>
                    <?php $this->view->segmentHeader($string_6, $string_7) ?>

                    <input type="number" name="<?= $fieldname_2 ?>" min="0" step="1" class="small-text"
                           value="<?= $unit_value ?>">

                    <fieldset>
                        <?php $this->view->radio($string_8, $fieldname_3, MINUTE_IN_SECONDS, $unit, 'mins') ?>
                        <br/>
                        <?php $this->view->radio($string_9, $fieldname_3, HOUR_IN_SECONDS, $unit, 'hours') ?>
                        <br/>
                        <?php $this->view->radio($string_10, $fieldname_3, DAY_IN_SECONDS, $unit, 'days') ?>

                    </fieldset>
                </li>
            </ul>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getGeneralAccess()
    {
        $string_47 = esc_html__('Access settings', 'teambooking');
        $string_48 = esc_html__('Who can make reservations', 'teambooking');
        $string_49 = esc_html__('If "Logged user only" is selected, then an advice with link to registration page will be shown. The default link can be changed in the "Core settings" tab.', 'teambooking');
        $string_50 = esc_html__('Everyone', 'teambooking');
        $string_51 = esc_html__('Logged users only', 'teambooking');
        $string_52 = esc_html__('Save changes', 'teambooking');
        $fieldname_20 = 'event[allow_reservation]';
        ob_start();
        ?>

        <?php $this->view->panelTitle($string_47) ?>

        <div class="tbk-content">
            <ul class="tbk-list">
                <li>
                    <?php $this->view->segmentHeader($string_48, $string_49) ?>

                    <fieldset>

                        <?php $this->view->radio($string_50, $fieldname_20, 0, $this->service->getLoggedOnly(), FALSE) ?>
                        <br/>
                        <?php $this->view->radio($string_51, $fieldname_20, 1, $this->service->getLoggedOnly(), TRUE) ?>

                    </fieldset>
                </li>

                <li>
                    <?php submit_button($string_52, 'primary', 'team_booking_update_booking_type') ?>
                </li>
            </ul>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getCustomFieldsSettings()
    {
        $string_6 = ucfirst(esc_html__('Custom fields', 'teambooking'));
        $string_7 = esc_html__('Add Text Field', 'teambooking');
        $string_8 = esc_html__('Add Select', 'teambooking');
        $string_9 = esc_html__('Add Textarea', 'teambooking');
        $string_10 = esc_html__('Add Checkbox', 'teambooking');
        $string_11 = esc_html__('Add Radio Group', 'teambooking');
        $string_12 = esc_html__('Add File Upload', 'teambooking');
        $string_13 = esc_html__('No custom fields for this service yet.', 'teambooking');
        ob_start();
        ?>

        <?php $this->view->panelTitle($string_6) ?>

        <div class="tbk-content">

            <a class="ui mini horizontal label tb-add-custom-field" data-field="text"
               data-serviceid="<?= $this->service_id ?>">
                <span class="dashicons dashicons-plus"></span>
                <?= $string_7 ?>
            </a>
            <a class="ui mini horizontal label tb-add-custom-field" data-field="select"
               data-serviceid="<?= $this->service_id ?>" href="#">
                <span class="dashicons dashicons-plus"></span>
                <?= $string_8 ?>
            </a>
            <a class="ui mini horizontal label tb-add-custom-field" data-field="textarea"
               data-serviceid="<?= $this->service_id ?>" href="#">
                <span class="dashicons dashicons-plus"></span>
                <?= $string_9 ?>
            </a>
            <a class="ui mini horizontal label tb-add-custom-field" data-field="checkbox"
               data-serviceid="<?= $this->service_id ?>" href="#">
                <span class="dashicons dashicons-plus"></span>
                <?= $string_10 ?>
            </a>
            <a class="ui mini horizontal label tb-add-custom-field" data-field="radio"
               data-serviceid="<?= $this->service_id ?>" href="#">
                <span class="dashicons dashicons-plus"></span>
                <?= $string_11 ?>
            </a>
            <a class="ui mini horizontal label tb-add-custom-field" data-field="file"
               data-serviceid="<?= $this->service_id ?>" href="#">
                <span class="dashicons dashicons-plus"></span>
                <?= $string_12 ?>
            </a>

            <hr>

            <?php
            $custom_fields = $this->service->getFormFields()->getCustomFields();
            if (empty($custom_fields)) {
                ?>
                <div class='tb-no-custom-fields'>
                    <?= $string_13 ?>
                </div>
                <?php
            }
            ?>
            <ul class="tb-custom-fields-list sortable">
                <?php
                foreach ($custom_fields as $field) {
                    if (is_object($field)) {
                        ?>
                        <li>
                            <?= tbAdminFormCustomFieldsMapper($field, $this->service->getId()) ?>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getBuiltInFieldsSettings()
    {
        $string_2 = esc_html__('Built-in fields', 'teambooking');
        $string_3 = esc_html__('White checkbox = show', 'teambooking');
        $string_4 = esc_html__('Red checkbox = required', 'teambooking');
        $string_5 = esc_html__('Save changes', 'teambooking');
        $string_6 = esc_html__('Field validation rule', 'teambooking');
        $string_14 = esc_html__('Update field translations', 'teambooking');
        $fieldname_1 = 'form_fields';
        ob_start();
        ?>
        <form method="POST" action="<?= $this->action ?>">
            <input type="hidden" name="action" value="option_submit">
            <?php wp_nonce_field('team_booking_options_verify') ?>
            <input type="hidden" name="service_id" value="<?= $this->service_id ?>">
            <input type="hidden" name="service_settings" value="form">

            <?php $this->view->panelTitle(ucfirst($string_2)) ?>

            <div class="tbk-content">
                <ul class="tbk-list">
                    <li>
                        <?php $this->view->segmentHeader($string_2, $string_3 . '<br>' . $string_4) ?>

                        <fieldset>
                            <?php
                            $fields = $this->service->getFormFields()->getBuiltInFields();
                            echo $this->getMozCheckboxCSSTarget('EF6060');

                            foreach ($fields as $field) {
                                /* @var $field TeamBookingFormTextField */
                                ?>
                                <div style="margin-bottom:5px;">
                                    <label for="<?= $fieldname_1 ?>[<?= $field->getHook() ?>]" class="ui label"
                                           style="cursor:auto;">
                                        <input name="<?= $fieldname_1 ?>[<?= $field->getHook() ?>]"
                                               style="vertical-align: text-bottom;" type="checkbox"
                                               value="<?= $field->getHook() ?>" <?php checked(1, $field->getIsActive()) ?> />
                                        <input name="<?= $fieldname_1 ?>[<?= $field->getHook() ?>_required]"
                                               style="vertical-align: text-bottom;" type="checkbox"
                                               value="<?= $field->getHook() ?>" <?php checked(1, $field->getIsRequired()) ?>
                                               class="tb_required _EF6060"/>
                                        <span><?= $field->getLabel() ?></span><span
                                            class="tbk-builtin-field-hook"> [<?= $field->getHook() ?>]</span>
                                    </label>

                                    <a title="<?= $string_6 ?>" id="tb-field-regex-open-<?= $field->getHook() ?>"
                                       class="ui <?= ($field->getValidationRegex()) ? 'poor-red' : '' ?> horizontal label">
                                        <span class="dashicons dashicons-editor-spellcheck"></span>
                                    </a>
                                    <?= tbGetValidationModal($field, $fieldname_1) ?>
                                </div>
                                <?php
                            }
                            ?>

                        </fieldset>
                    </li>

                    <li>
                        <div data-serviceid="<?= $this->service_id ?>" id="team_booking_update_translations"
                             class="button button-secondary"><?= $string_14 ?></div>
                    </li>
                    <li>
                        <?php submit_button($string_5, 'primary', 'team_booking_update_booking_type') ?>
                    </li>
                </ul>
            </div>
        </form>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getAvailabilityModes()
    {
        $string_1 = esc_html__('Availability modes', 'teambooking');
        $string_2 = esc_html__('The image below describes the two modes of placing availability in Google Calendar. You can use both, but please note that the Container Mode is NOT available if the slot duration is inherited from Google Calendar event.', 'teambooking');
        $string_3 = esc_html__('Slot mode', 'teambooking');
        $string_4 = esc_html__('Event name', 'teambooking');
        $string_5 = esc_html__('Event title (free slot)', 'teambooking');
        $string_6 = esc_html__('Container mode', 'teambooking');
        $string_7 = esc_html__('Container mode (multiple services)', 'teambooking');
        ob_start();
        ?>

        <?php $this->view->panelTitle($string_1) ?>

        <div class="tbk-content">
            <p>
                <?= $string_2 ?>
            </p>

            <div class="tbk-row">
                <div class="tbk-panel-title">
                    <h4><?= $string_3 ?></h4>

                    <p><?= $string_4 ?> = <?= $string_5 ?></p>
                </div>
                <div class="tbk-column tbk-span-5">
                    <div class="tbk-content" style="padding:10px;">
                        <div style="
                             background: #888888;
                             min-height: 35px;
                             border-radius: 5px;
                             color: white;
                             font-size: 0.6vw;
                             line-height: 0.7vw;
                             padding: 4px;">
                            11:00 - 12:00<br>
                            Service
                        </div>
                    </div>
                </div>
                <div class="tbk-column tbk-span-2">
                    <div class="tbk-content" style="padding-right: 0;padding-left: 0;text-align: center;">
                        <span class="dashicons dashicons-arrow-right-alt"></span>
                    </div>
                </div>
                <div class="tbk-column tbk-span-5">
                    <div class="tbk-content" style="padding:10px;">
                        <div style="background: #43B555;
                             min-height: 35px;
                             border-radius: 5px;
                             color: white;
                             font-size: 0.6vw;
                             line-height: 0.7vw;
                             padding: 4px;">
                            11:00 - 12:00<br>
                            New reservation for Service
                        </div>
                    </div>
                </div>
            </div>
            <div class="tbk-row">
                <div class="tbk-panel-title">
                    <h4><?= $string_6 ?></h4>

                    <p><?= $string_4 ?> = <?= $string_5 ?> + "container"</p>
                </div>
                <div class="tbk-column tbk-span-5">
                    <div class="tbk-content" style="padding:10px;">
                        <div style="
                             background: #888888;
                             min-height: 180px;
                             border-radius: 5px;
                             color: white;
                             font-size: 0.6vw;
                             line-height: 0.7vw;
                             padding: 4px;">
                            14:00 - 18:00<br>
                            Service container
                        </div>
                    </div>
                </div>
                <div class="tbk-column tbk-span-2">
                    <div class="tbk-content" style="padding-right: 0;padding-left: 0;text-align: center;">
                        <span class="dashicons dashicons-arrow-right-alt"></span>
                    </div>
                </div>
                <div class="tbk-column tbk-span-5">
                    <div class="tbk-content" style="padding:10px;">
                        <div style="
                             background: #888888;
                             min-height: 180px;
                             border-radius: 5px;
                             color: white;
                             font-size: 0.6vw;
                             line-height: 0.7vw;
                             padding: 4px;">
                            14:00 - 18:00<br>
                            Service container
                            <div class="tbk-column tbk-span-12" style="position:absolute;">
                                <div class="tbk-content" style="padding:10px;">
                                    <div style="background: #43B555;
                                         min-height: 35px;
                                         border-radius: 5px;
                                         color: white;
                                         font-size: 0.6vw;
                                         line-height: 0.7vw;
                                         padding: 4px;">
                                        15:00 - 16:00<br>
                                        New reservation for Service
                                    </div>
                                </div>
                            </div>
                            <div class="tbk-column tbk-span-12" style="position:absolute;top: 100px;">
                                <div class="tbk-content" style="padding:10px;">
                                    <div style="background: #43B555;
                                         min-height: 35px;
                                         border-radius: 5px;
                                         color: white;
                                         font-size: 0.6vw;
                                         line-height: 0.7vw;
                                         padding: 4px;">
                                        16:15 - 17:15<br>
                                        New reservation for Service
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tbk-row">
                <div class="tbk-panel-title">
                    <h4><?= $string_7 ?></h4>

                    <p><?= $string_4 ?> = <?= $string_5 ?> 1 + "+" + <?= $string_5 ?> 2 + "+" ... + "container"</p>
                </div>
                <div class="tbk-column tbk-span-5">
                    <div class="tbk-content" style="padding:10px;">
                        <div style="
                             background: #888888;
                             min-height: 180px;
                             border-radius: 5px;
                             color: white;
                             font-size: 0.6vw;
                             line-height: 0.7vw;
                             padding: 4px;">
                            14:00 - 18:00<br>
                            Service 1 + Service 2 + Service 3 container
                        </div>
                    </div>
                </div>
                <div class="tbk-column tbk-span-2">
                    <div class="tbk-content" style="padding-right: 0;padding-left: 0;text-align: center;">
                        <span class="dashicons dashicons-arrow-right-alt"></span>
                    </div>
                </div>
                <div class="tbk-column tbk-span-5">
                    <div class="tbk-content" style="padding:10px;">
                        <div style="
                             background: #888888;
                             min-height: 180px;
                             border-radius: 5px;
                             color: white;
                             font-size: 0.6vw;
                             line-height: 0.7vw;
                             padding: 4px;">
                            14:00 - 18:00<br>
                            Service 1 + Service 2 + Service 3 container
                            <div class="tbk-column tbk-span-12" style="position:absolute;">
                                <div class="tbk-content" style="padding:10px;">
                                    <div style="background: #43B555;
                                         min-height: 35px;
                                         border-radius: 5px;
                                         color: white;
                                         font-size: 0.6vw;
                                         line-height: 0.7vw;
                                         padding: 4px;">
                                        15:00 - 16:00<br>
                                        New reservation for Service 1
                                    </div>
                                </div>
                            </div>
                            <div class="tbk-column tbk-span-12" style="position:absolute;top: 100px;">
                                <div class="tbk-content" style="padding:10px;">
                                    <div style="background: #43B555;
                                         min-height: 55px;
                                         border-radius: 5px;
                                         color: white;
                                         font-size: 0.6vw;
                                         line-height: 0.7vw;
                                         padding: 4px;">
                                        16:15 - 17:15<br>
                                        New reservation for Service 3
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getGCalSettingsContent()
    {
        // Getting booking type custom settings
        $custom_booking_type_settings = $this->coworker->getCustomEventSettings($this->service_id);
        // Grouping fieldnames and strings for mantainability
        $fieldname_1 = 'data[linked_event_title]';
        $fieldname_2 = 'data[booked_title]';
        $fieldname_3 = 'data[min_time]';
        $fieldname_4 = 'data[booked_color]';
        $fieldname_5 = 'data[reminder]';
        $fieldname_6 = 'data[duration_rule]';
        $fieldname_7 = 'data[default_duration_hours]';
        $fieldname_8 = 'data[default_duration_minutes]';
        $fieldname_9 = 'data[buffer_hours]';
        $fieldname_10 = 'data[buffer_minutes]';
        $fieldname_11 = 'data[min_time_reference]';
        $string_1 = esc_html__('Event title (free slot)', 'teambooking');
        $string_2 = esc_html__('Case insensitive. This is the name that events in your Google Calendar must have, in order to become free slots for this service.', 'teambooking');
        $string_3 = esc_html__('Event title (booked slot)', 'teambooking');
        $string_4 = esc_html__('Case insensitive. This is the name that a free slot in your Google Calendar will get, once booked. If you manually give this name to an event, it will appear as a booked slot.', 'teambooking');
        $string_5 = esc_html__('When reservations should be closed?', 'teambooking');
        $string_6 = esc_html__('10 min before', 'teambooking');
        $string_7 = esc_html__('1 hour before', 'teambooking');
        $string_8 = esc_html__('6 hours before', 'teambooking');
        $string_9 = esc_html__('12 hours before', 'teambooking');
        $string_10 = esc_html__('24 hours before', 'teambooking');
        $string_11 = esc_html__('1 day before (until midnight)', 'teambooking');
        $string_12 = esc_html__('2 days before (until midnight)', 'teambooking');
        $string_13 = esc_html__('Yes', 'teambooking');
        $string_14 = esc_html__('No', 'teambooking');
        $string_15 = esc_html__('Event color after reservation', 'teambooking');
        $string_16 = esc_html__('Visual aid to distinguish between free and reserved slots in your Google Calendar', 'teambooking');
        $string_17 = esc_html__('Calendar default color', 'teambooking');
        $string_18 = esc_html__('Set a reminder for reserved slot', 'teambooking');
        $string_19 = esc_html__('If active, you will receive a reminder by email for any reserved slot for this service.', 'teambooking');
        $string_20 = esc_html__('No reminder', 'teambooking');
        $string_21 = esc_html__('10 min before', 'teambooking');
        $string_22 = esc_html__('30 min before', 'teambooking');
        $string_23 = esc_html__('1h before', 'teambooking');
        $string_24 = esc_html__('Save changes', 'teambooking');
        $string_25 = esc_html__('15 days before (until midnight)', 'teambooking');
        $string_26 = esc_html__('Slot duration rule', 'teambooking');
        $string_27 = esc_html__('If inherited, the container mode will not be available', 'teambooking');
        $string_28 = esc_html__('Fixed:', 'teambooking');
        $string_29 = esc_html__('Inherited from Google Calendar event', 'teambooking');
        $string_30 = esc_html__('Set by Admin', 'teambooking');
        $string_31 = esc_html__('Hours', 'teambooking');
        $string_32 = esc_html__('Minutes', 'teambooking');
        $string_33 = esc_html__('Buffer between consecutive slots', 'teambooking');
        $string_34 = esc_html__('This applies only inside container slots', 'teambooking');
        $string_36 = esc_html__('slot start time', 'teambooking');
        $string_37 = esc_html__('slot end time', 'teambooking');
        $string_38 = esc_html__('30 min before', 'teambooking');
        $string_39 = esc_html__('3 days before (until midnight)', 'teambooking');
        $string_40 = esc_html__('7 days before (until midnight)', 'teambooking');
        $string_41 = esc_html__('Choose the minimum time to book prior to the slot start or end time', 'teambooking');
        $string_42 = esc_html__('10 days before (until midnight)', 'teambooking');
        $string_43 = esc_html__('30 days before (until midnight)', 'teambooking');
        $string_44 = esc_html__('2 hours before', 'teambooking');
        $string_45 = esc_html__('3 hours before', 'teambooking');
        ob_start();
        ?>

        <div class="tbk-content">
            <ul class="tbk-list">

                <?php if (!$this->service->isClass('service')) { ?>
                    <li>
                        <?php $this->view->segmentHeader($string_1, $string_2) ?>

                        <p>
                            <?php $this->view->textfield($custom_booking_type_settings->getLinkedEventTitle(), $fieldname_1) ?>
                        </p>
                    </li>

                    <?php if ($this->service->isClass('appointment')) { ?>
                        <li>
                            <?php $this->view->segmentHeader($string_3, $string_4) ?>

                            <p>
                                <?php $this->view->textfield($custom_booking_type_settings->getAfterBookedTitle(), $fieldname_2) ?>
                            </p>
                        </li>
                    <?php } ?>

                    <li>
                        <?php $this->view->segmentHeader($string_5, $string_41) ?>

                        <p>
                            <select name="<?= $fieldname_3 ?>">
                                <?php $this->view->select_option($string_6, 'PT10M', 'PT10M', $custom_booking_type_settings->getMinTime()) ?>
                                <?php $this->view->select_option($string_38, 'PT30M', 'PT30M', $custom_booking_type_settings->getMinTime()) ?>
                                <?php $this->view->select_option($string_7, 'PT1H', 'PT1H', $custom_booking_type_settings->getMinTime()) ?>
                                <?php $this->view->select_option($string_44, 'PT2H', 'PT2H', $custom_booking_type_settings->getMinTime()) ?>
                                <?php $this->view->select_option($string_45, 'PT3H', 'PT3H', $custom_booking_type_settings->getMinTime()) ?>
                                <?php $this->view->select_option($string_8, 'PT6H', 'PT6H', $custom_booking_type_settings->getMinTime()) ?>
                                <?php $this->view->select_option($string_9, 'PT12H', 'PT12H', $custom_booking_type_settings->getMinTime()) ?>
                                <?php $this->view->select_option($string_10, 'P1D', 'P1D', $custom_booking_type_settings->getMinTime()) ?>
                                <?php $this->view->select_option($string_11, 'P1Dmid', 'P1Dmid', $custom_booking_type_settings->getMinTime()) ?>
                                <?php $this->view->select_option($string_12, 'P2Dmid', 'P2Dmid', $custom_booking_type_settings->getMinTime()) ?>
                                <?php $this->view->select_option($string_39, 'P3Dmid', 'P3Dmid', $custom_booking_type_settings->getMinTime()) ?>
                                <?php $this->view->select_option($string_40, 'P7Dmid', 'P7Dmid', $custom_booking_type_settings->getMinTime()) ?>
                                <?php $this->view->select_option($string_42, 'P10Dmid', 'P10Dmid', $custom_booking_type_settings->getMinTime()) ?>
                                <?php $this->view->select_option($string_25, 'P15Dmid', 'P15Dmid', $custom_booking_type_settings->getMinTime()) ?>
                                <?php $this->view->select_option($string_43, 'P30Dmid', 'P30Dmid', $custom_booking_type_settings->getMinTime()) ?>
                            </select>

                            <select name="<?= $fieldname_11 ?>">
                                <?php $this->view->select_option($string_36, 'start', 'start', $custom_booking_type_settings->getMinTimeReference()) ?>
                                <?php $this->view->select_option($string_37, 'end', 'end', $custom_booking_type_settings->getMinTimeReference()) ?>
                            </select>
                        </p>
                    </li>

                    <li>
                        <?php $this->view->segmentHeader($string_26, $string_27) ?>

                        <?php
                        if ($this->service->getDurationRule() != 'coworker') {
                            $service_hours = floor($this->service->getDefaultDuration() / 3600);
                            $service_minutes = ceil($this->service->getDefaultDuration() - $service_hours * 3600) / 60;
                            if ($this->service->getDurationRule() == 'fixed') {
                                $rule_string = $string_28 . " " . $service_hours . "h " . $service_minutes . "m";
                            }
                            if ($this->service->getDurationRule() == 'inherited') {
                                $rule_string = $string_29;
                            }
                            ?>
                            <div class="ui mini blue label"><?= $rule_string ?>
                                <div class="detail"><?= "(" . $string_30 . ")" ?></div>
                            </div>

                        <?php } else { ?>

                            <fieldset>
                                <?php
                                $service_hours = floor($custom_booking_type_settings->getFixedDuration() / 3600);
                                $service_minutes = ceil($custom_booking_type_settings->getFixedDuration() - $service_hours * 3600) / 60;
                                ?>
                                <label><input type="radio" name="<?= $fieldname_6 ?>"
                                              value="inherited" <?php checked($custom_booking_type_settings->getDurationRule(), 'inherited') ?> />
                                    <span><?= $string_29 ?></span></label><br/>
                                <label><input type="radio" name="<?= $fieldname_6 ?>"
                                              value="fixed" <?php checked($custom_booking_type_settings->getDurationRule(), 'fixed') ?>/>
                                    <span><?= $string_28 ?></span></label>

                                <select name="<?= $fieldname_7 ?>">
                                    <?php for ($i = 0; $i < 24; $i++) { ?>
                                        <option
                                            value="<?= $i ?>" <?php selected($service_hours, $i, TRUE); ?>><?= $i ?></option>
                                    <?php } ?>
                                </select>

                                <span><?= $string_31 ?></span>

                                <select name="<?= $fieldname_8 ?>">
                                    <?php for ($i = 0; $i < 60; $i++) { ?>
                                        <option
                                            value="<?= $i ?>" <?php selected($service_minutes, $i, TRUE); ?>><?= $i ?></option>
                                    <?php } ?>
                                </select>

                                <span><?= $string_32 ?></span>

                            </fieldset>
                        <?php } ?>
                    </li>

                    <li>
                        <?php $this->view->segmentHeader($string_33, $string_34) ?>

                        <fieldset>

                            <?php
                            $buffer_hours = floor($custom_booking_type_settings->getBufferDuration() / 3600);
                            $buffer_minutes = ceil($custom_booking_type_settings->getBufferDuration() - $buffer_hours * 3600) / 60;
                            ?>

                            <select name="<?= $fieldname_9 ?>">
                                <?php for ($i = 0; $i < 24; $i++) { ?>
                                    <option
                                        value="<?= $i ?>" <?php selected($buffer_hours, $i, TRUE); ?>><?= $i ?></option>
                                <?php } ?>
                            </select>

                            <span><?= $string_31 ?></span>

                            <select name="<?= $fieldname_10 ?>">
                                <?php for ($i = 0; $i < 60; $i++) { ?>
                                    <option
                                        value="<?= $i ?>" <?php selected($buffer_minutes, $i, TRUE); ?>><?= $i ?></option>
                                <?php } ?>
                            </select>

                            <span><?= $string_32 ?></span>

                        </fieldset>

                    </li>
                <?php } ?>

                <?php if (!$this->service->isClass('service')) { ?>
                    <li>
                        <?php $this->view->segmentHeader($string_15, $string_16) ?>

                        <fieldset>

                            <label for="<?= $fieldname_4 ?>">
                                <input name="<?= $fieldname_4 ?>" type="radio"
                                       value="0" <?php checked('0', $custom_booking_type_settings->getBookedEventColor()) ?>/><?= $string_17 ?>
                            </label><br/><br/>
                            <?= $this->getMozRadioCSSTarget('a4bdfc') ?>
                            <input style="background: #a4bdfc" class="_a4bdfc" name="<?= $fieldname_4 ?>" type="radio"
                                   value="1" <?php checked('1', $custom_booking_type_settings->getBookedEventColor()) ?>/>
                            <?= $this->getMozRadioCSSTarget('7ae7bf') ?>
                            <input style="background: #7ae7bf" class="_7ae7bf" name="<?= $fieldname_4 ?>" type="radio"
                                   value="2" <?php checked('2', $custom_booking_type_settings->getBookedEventColor()) ?>/>
                            <?= $this->getMozRadioCSSTarget('dbadff') ?>
                            <input style="background: #dbadff" class="_dbadff" name="<?= $fieldname_4 ?>" type="radio"
                                   value="3" <?php checked('3', $custom_booking_type_settings->getBookedEventColor()) ?>/>
                            <?= $this->getMozRadioCSSTarget('ff887c') ?>
                            <input style="background: #ff887c" class="_ff887c" name="<?= $fieldname_4 ?>" type="radio"
                                   value="4" <?php checked('4', $custom_booking_type_settings->getBookedEventColor()) ?>/>
                            <?= $this->getMozRadioCSSTarget('fbd75b') ?>
                            <input style="background: #fbd75b" class="_fbd75b" name="<?= $fieldname_4 ?>" type="radio"
                                   value="5" <?php checked('5', $custom_booking_type_settings->getBookedEventColor()) ?>/>
                            <?= $this->getMozRadioCSSTarget('ffb878') ?>
                            <input style="background: #ffb878" class="_ffb878" name="<?= $fieldname_4 ?>" type="radio"
                                   value="6" <?php checked('6', $custom_booking_type_settings->getBookedEventColor()) ?>/>
                            <?= $this->getMozRadioCSSTarget('46d6db') ?>
                            <input style="background: #46d6db" class="_46d6db" name="<?= $fieldname_4 ?>" type="radio"
                                   value="7" <?php checked('7', $custom_booking_type_settings->getBookedEventColor()) ?>/>
                            <?= $this->getMozRadioCSSTarget('e1e1e1') ?>
                            <input style="background: #e1e1e1" class="_e1e1e1" name="<?= $fieldname_4 ?>" type="radio"
                                   value="8" <?php checked('8', $custom_booking_type_settings->getBookedEventColor()) ?>/>
                            <?= $this->getMozRadioCSSTarget('5484ed') ?>
                            <input style="background: #5484ed" class="_5484ed" name="<?= $fieldname_4 ?>" type="radio"
                                   value="9" <?php checked('9', $custom_booking_type_settings->getBookedEventColor()) ?>/>
                            <?= $this->getMozRadioCSSTarget('51b749') ?>
                            <input style="background: #51b749" class="_51b749" name="<?= $fieldname_4 ?>" type="radio"
                                   value="10" <?php checked('10', $custom_booking_type_settings->getBookedEventColor()) ?>/>
                            <?= $this->getMozRadioCSSTarget('dc2127') ?>
                            <input style="background: #dc2127" class="_dc2127" name="<?= $fieldname_4 ?>" type="radio"
                                   value="11" <?php checked('11', $custom_booking_type_settings->getBookedEventColor()) ?>/>

                        </fieldset>
                    </li>

                    <li>
                        <?php $this->view->segmentHeader($string_18, $string_19) ?>

                        <fieldset>

                            <label>
                                <input name="<?= $fieldname_5 ?>" type="radio"
                                       value="0" <?php checked('0', $custom_booking_type_settings->getReminder()) ?>/><?= $string_20 ?>
                            </label><br/>
                            <label>
                                <input name="<?= $fieldname_5 ?>" type="radio"
                                       value="10" <?php checked('10', $custom_booking_type_settings->getReminder()) ?>/><?= $string_21 ?>
                            </label><br/>
                            <label>
                                <input name="<?= $fieldname_5 ?>" type="radio"
                                       value="30" <?php checked('30', $custom_booking_type_settings->getReminder()) ?>/><?= $string_22 ?>
                            </label><br/>
                            <label>
                                <input name="<?= $fieldname_5 ?>" type="radio"
                                       value="60" <?php checked('60', $custom_booking_type_settings->getReminder()) ?>/><?= $string_23 ?>
                            </label>

                        </fieldset>
                    </li>
                <?php } ?>

                <li>
                    <?= submit_button($string_24, 'primary', 'team_booking_save_personal_event_settings') ?>
                </li>
            </ul>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getMozRadioCSSTarget($hex_color)
    {
        ob_start();
        ?>
        <style type="text/css">
            @-moz-document url-prefix() {
            <?= "input[type='radio']._" . $hex_color ?> {
                outline: 2px solid # <?= $hex_color ?>;
            }
            }
        </style>
        <?php
        return ob_get_clean();
    }

    private function getMozCheckboxCSSTarget($hex_color)
    {
        ob_start();
        ?>
        <style type="text/css">
            @-moz-document url-prefix() {
            <?= "input[type='checkbox']._" . $hex_color ?> {
                outline: 2px solid # <?= $hex_color ?>;
            }
            }
        </style>
        <?php
        return ob_get_clean();
    }

}
