<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Components_Admin_ManageCoworkers
{

    //------------------------------------------------------------

    private $auth_coworkers;
    private $settings;
    private $view;

    public function __construct($settings)
    {
        /* @var $settings TeamBookingSettings */
        $this->auth_coworkers = tbGetAllCoworkersList();
        $this->settings = $settings;
        $this->view = new TeamBooking_Components_Admin_View();
    }

    //------------------------------------------------------------

    public function getPostBody()
    {
        ob_start();
        ?>
        <div class="tbk-wrapper">
            <div class="tbk-row">

                <div class="tbk-column tbk-span-12">
                    <div class="tbk-panel">
                        <?= $this->getCoworkerList() ?>
                    </div>
                </div>
            </div>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getCoworkerList()
    {
        // Strings and fieldnames for mantainability
        $string_1 = esc_html__('Coworkers', 'teambooking');
        $string_2 = esc_html__('you', 'teambooking');
        $string_3 = esc_html__('WP role no longer allowed', 'teambooking');
        $string_4 = esc_html__('not set', 'teambooking');
        $string_5 = esc_html__('ready', 'teambooking');
        $string_6 = esc_html__('temporarily authorized', 'teambooking');
        $string_7 = esc_html__('not authorized', 'teambooking');
        $string_8 = esc_html__('Settings', 'teambooking');
        $string_9 = esc_html__('Profile URL (leave it blank for WordPress default)', 'teambooking');
        $string_10 = esc_html__('Remove data', 'teambooking');
        $string_11 = esc_html__('Revoke Authorization', 'teambooking');
        $string_12 = esc_html__('Are you sure?', 'teambooking');
        $string_13 = esc_html__('You are going to revoke authorization given to TeamBooking by the Google Account of:', 'teambooking');
        $string_14 = esc_html__("He won't be able to place availability to your services until he gives authorization again", 'teambooking');
        ob_start();
        ?>

        <?php $this->view->panelTitle($string_1) ?>

        <div class="tbk-content">
            <table class="widefat" id="tbk-coworkers-overview">
                <tr class="alternate">
                    <th style="font-weight:bold"><?= esc_html__('Name', 'teambooking') ?></th>
                    <th style="font-weight:bold"><?= esc_html__('Email', 'teambooking') ?></th>
                    <th style="font-weight:bold"><?= esc_html__('WP Roles', 'teambooking') ?></th>
                    <th style="font-weight:bold"><?= esc_html__('Specific shortcode', 'teambooking') ?></th>
                    <th style="font-weight:bold"><?= esc_html__('Google Calendar', 'teambooking') ?></th>
                    <th style="font-weight:bold"><?= esc_html__('Actions', 'teambooking') ?></th>
                </tr>

                <?php
                foreach ($this->auth_coworkers as $id => $coworker) {
                    ?>
                    <tr <?= ($id == get_current_user_id()) ? 'style="background:rgb(255, 229, 229);"' : '' ?>>

                        <td>
                            <?= ucwords($coworker['name']) ?>
                            <?= ($id == get_current_user_id()) ? "(" . $string_2 . ")" : "" ?>
                        </td>

                        <td><?= $coworker['email'] ?></td>
                        <td><?= ucwords(implode(", ", $coworker['roles'])) ?></td>
                        <td><code>[tb-calendar coworker="<?= $id ?>"]</code></td>

                        <td>
                            <?php if (isset($coworker['allowed_no_more'])) {
                                $this->view->ui_mini_label($string_3, 'red');
                            } elseif (empty($coworker['calendar']) && $coworker['token'] == 'refresh') {
                                $this->view->ui_mini_label($string_4, 'yellow');
                            } elseif (!empty($coworker['calendar']) && $coworker['token'] == 'refresh') {
                                $this->view->ui_mini_label($string_5, 'green');
                            } elseif ($coworker['token'] == 'access') {
                                $this->view->ui_mini_label($string_6, 'red');
                            } else {
                                $this->view->ui_mini_label($string_7);
                            } ?>
                        </td>

                        <td>
                            <?php if (isset($coworker['allowed_no_more'])) { ?>
                                <a class="tbk-coworkers-action-clean tb-remove-residual-data ui circular label mini horizontal"
                                   title="<?= $string_10 ?>"
                                   href="#"
                                   data-coworker="<?= $id ?>">
                                    <?php $this->view->dashicon('dashicons-no') ?>
                                </a>

                            <?php } else { ?>
                                <a class="tbk-coworkers-action-settings ui circular label mini horizontal"
                                   title="<?= $string_8 ?>"
                                   href="#"
                                   data-coworker="<?= $id ?>"
                                   id="team_booking_coworker_settings_<?= $id ?>">
                                    <?php $this->view->dashicon('dashicons-admin-tools') ?>
                                </a>

                                <!-- Coworker settings modal markup -->
                                <?php ob_start() ?>
                                <form id="team-booking-coworker-settings-form-<?= $id ?>" action="" method="POST">
                                    <div style="font-style: italic;font-weight: 300;">
                                        <?= $string_9 ?>
                                    </div>
                                    <input type="text" data-ays-ignore="true" class="large-text"
                                           style="margin-bottom:10px;" name="coworker_url"
                                           value="<?= $this->settings->getCoworkerUrl($id) ?>">
                                </form>
                                <?php
                                $modal_content = ob_get_clean();
                                $this->view->small_modal('tb-coworker-settings-modal-' . $id, $string_8, $modal_content);
                                unset($modal_content);
                                ?>

                                <?php if (!empty($coworker['token'])) { ?>
                                    <a class="tbk-coworkers-action-unlink tbk-revoke-coworker-token ui circular label mini horizontal"
                                       title="<?= $string_11 ?>"
                                       href="#"
                                       data-coworker="<?= $id ?>"
                                       data-name="<?= ucwords($coworker['name']) ?>">
                                        <?php $this->view->dashicon('dashicons-editor-unlink') ?>
                                    </a>

                                    <?php
                                } else {
                                    // TODO: give the possibility to authorize Coworkers (with a CLEAR DISCLAIMER)
                                }
                                ?>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </table>

            <!-- Revoke coworker modal markup -->
            <?php ob_start() ?>
            <p>
                <?= $string_13 ?>
                <span id="revoke-coworker-name"></span>
            </p>

            <p>
                <?= $string_14 ?>
            </p>
            <?php
            $modal_content = ob_get_clean();
            $this->view->small_modal('tb-coworker-token-revoke-modal', $string_12, $modal_content);
            unset($modal_content);
            ?>
        </div>

        <?php
        return ob_get_clean();
    }

}
