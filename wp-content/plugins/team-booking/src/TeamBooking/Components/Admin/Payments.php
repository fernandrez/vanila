<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Components_Admin_Payments
{

    //------------------------------------------------------------

    public $action;
    private $currency;
    private $gateways;

    public function __construct($settings)
    {
        /* @var $settings TeamBookingSettings */
        $this->currency = $settings->getCurrencyCode();
        $this->gateways = $settings->getPaymentGatewaySettingObjects();
    }

    //------------------------------------------------------------

    public function getPostBody()
    {
        ob_start();
        ?>
        <div class="tbk-wrapper">
            <form method="POST" action="<?= $this->action ?>">
                <input type="hidden" name="action" value="option_submit">
                <?php wp_nonce_field('team_booking_options_verify') ?>

                <div class="tbk-column tbk-span-12">
                    <div class="tbk-panel tbk-content">
                        <div class="tbk-settings-title">
                            <h3 class="tbk-heading">
                                <?= esc_html__('Currency', 'teambooking') ?>
                            </h3>

                            <p class="tbk-excerpt">
                                <select name="currency_code">
                                    <option value=""><?= esc_html__('Select currency...', 'teambooking') ?></option>
                                    <?php foreach (tbGetCurrencies() as $code => $data) { ?>

                                        <option value="<?= $code ?>" <?php selected($this->currency, $code, TRUE); ?>>
                                            <?= $code ?> (<?= $data['label'] ?>)
                                        </option>
                                    <?php } ?>
                                </select>
                            </p>
                            <input type="submit" name="team_booking_payment_options_submit"
                                   class="button button-hero button-primary"
                                   value="<?= esc_html__('Save changes', 'teambooking') ?>">
                        </div>
                    </div>
                </div>

                <div class="tbk-row">
                    <?php foreach ($this->gateways as $gateway) { ?>
                        <div class="tbk-column tbk-span-6">
                            <div class="tbk-panel">
                                <?= $gateway->getBackendSettingsTab() ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </form>
        </div>

        <?php
        return ob_get_clean();
    }

}
