<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Components_Admin_View
{

    public function segmentHeader($text, $description = '', $icon = '')
    {
        ?>

        <h4>
            <?= $text ?> <?= $icon ?>
        </h4>

        <?php
        if (!empty($description)) {
            ?>

            <p>
                <?= $description ?>
            </p>

            <?php
        }
    }

    public function panelTitle($text)
    {
        ?>
        <div class="tbk-panel-title">
            <h4>
                <?= $text ?>
            </h4>
        </div>
        <?php
    }

    public function panelTitleAddNew($text, $add_new_class)
    {
        ?>
            <h2>
                <?= $text ?>
                <a href="#" class="add-new-h2 <?= $add_new_class ?>">
                    <?= __('Add new', 'teambooking') ?>
                </a>
            </h2>
        <?php
    }

    public function panelTitleWithLabel($text, $label_text, $label_class, $description = '')
    {
        ?>
        <div class="tbk-panel-title">
            <h4>
                <?= $text ?>
                <a href="#" class="ui mini horizontal label <?= $label_class ?>"
                   style="text-decoration: none;margin-left: 5px;">
                    <?= $label_text ?>
                </a>
                <?php if (!empty($description)) { ?>
                    <p><?= $description ?></p>
                <?php } ?>
            </h4>
        </div>
        <?php
    }

    public function textfield($value, $name, $id = '', $required = FALSE, $placeholder = '', $disabled = FALSE)
    {
        ?>
        <p>
            <input type="text" value="<?= $value ?>" class="regular-text"
                   name="<?= $name ?>"
                   <?= !empty($id) ? 'id="' . $id . '"' : '' ?>
                   <?= $required ? 'required="required"' : '' ?>
                   <?= $placeholder ? 'placeholder="'.$placeholder.'"' : '' ?>
                   <?= $disabled ? 'disabled="disabled"' : '' ?>
                />
        </p>
        <?php
    }

    public function checkbox($text, $name, $value, $checked_cond_1, $checked_cond_2)
    {
        ?>
        <label for="<?= $name ?>">
            <input name="<?= $name ?>" type="checkbox"
                   value="<?= $value ?>" <?php checked($checked_cond_1, $checked_cond_2) ?> >
            <?= $text ?>
        </label>

        <?php
    }

    public function radio($text, $name, $value, $checked_cond_1, $checked_cond_2, $text_classes = "")
    {
        ?>
        <label title='<?= $text ?>'>
            <input type="radio" name="<?= $name ?>"
                   value="<?= $value ?>" <?php checked($checked_cond_1, $checked_cond_2) ?> />
            <span class="<?= $text_classes ?>">
                <?= $text ?>
            </span>
        </label>
        <?php
    }

    public function select_option($text, $value, $selected_cond_1, $selected_cond_2, $optional_data = '')
    {
        ?>
        <option
            value="<?= $value ?>" <?php selected($selected_cond_1, $selected_cond_2, TRUE); ?>
            <?php if (!empty($optional_data)) { ?>
                <?= $optional_data ?>
            <?php } ?>
            >
            <?= $text ?>
        </option>
        <?php
    }

    public function ui_mini_label($text, $color = '', $id = '', $class = '', $element = 'div')
    {
        ?>
        <<?=$element?> class="ui mini <?= $color ?> <?= $class ?> label" <?= empty($id) ? '' : $id ?>><?= $text ?></<?=$element?>>
        <?php
    }

    public function dashicon($name)
    {
        ?>
        <span class="dashicons <?= $name ?>"></span>
        <?php
    }

    public function small_modal($id, $header_text, $content, $approve_txt = '', $deny_txt = '')
    {
        ?>
        <div class="ui small modal" id="<?= $id ?>">

            <i class="close tb-icon"></i>

            <div class="header">
                <?= $header_text ?>
            </div>

            <div class="content" style="width: calc(100% - 4rem);">
                <?= $content ?>
            </div>

            <div class="actions">
                <div class="ui positive button" style="height:auto">
                    <?= empty($approve_txt) ? __('Save', 'teambooking') : $approve_txt ?>
                </div>
                <div class="ui deny black button" style="height:auto">
                    <?= empty($deny_txt) ? __('Close', 'teambooking') : $deny_txt ?>
                </div>
            </div>
        </div>
        <?php
    }

    public function colorField($name, $value)
    {
        ?>
        <input type="text" class="tb-color-field" name="<?= $name ?>" value="<?= $value ?>">
        <?php
    }

}