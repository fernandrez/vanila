<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Components_Admin_Personal
{

    //------------------------------------------------------------

    public $action;

    /** @var TeamBookingCoworker $coworker */
    private $coworker;
    private $services;
    private $settings;
    private $access_token;
    private $calendar_service;

    public function __construct($settings)
    {
        /* @var $settings TeamBookingSettings */
        $this->settings = $settings;
        $this->services = $settings->getServices();
        $this->coworker = $settings->getCoworkerData(get_current_user_id());
        $this->access_token = $this->coworker->getAccessToken();
        $this->calendar_service = new TeamBooking_Calendar();
    }

    //------------------------------------------------------------

    public function getPostBody()
    {
        ob_start();
        ?>
        <div class="tbk-wrapper">
            <div class="tbk-row">
                <div class="tbk-column tbk-span-8">
                    <div class="tbk-panel">
                        <?= $this->getCalendarList() ?>
                    </div>
                </div>
                <div class="tbk-column tbk-span-2">
                    <div class="tbk-panel">
                        <?= $this->getStatusBlock() ?>
                    </div>
                </div>
                <div class="tbk-column tbk-span-2">
                    <div class="tbk-panel">
                        <?= $this->getHints() ?>
                    </div>
                </div>
            </div>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getCalendarList()
    {
        // Grouping fieldnames and strings for mantainability
        $string_1 = esc_html__('Your Google Calendars', 'teambooking');
        $string_2 = esc_html__('Google Calendar name', 'teambooking');
        $string_4 = esc_html__('Add new', 'teambooking');
        $string_5 = esc_html__('Add a Google Calendar', 'teambooking');
        $string_6 = esc_html__('Cancel', 'teambooking');
        $string_7 = esc_html__('Are you sure?', 'teambooking');
        $string_8 = esc_html__('Yes', 'teambooking');
        $string_9 = esc_html__('You are going to remove this Google Calendar, that means it will not be used anymore to look into it for availability. You can always re-add it, no event will be touched.', 'teambooking');
        ob_start();
        ?>
        <div class="tbk-panel-title">
            <h2>
                <?= $string_1 ?>
                <?php if (empty($this->access_token)) { ?>
                    <div class="add-new-h2 inactive"><?= $string_4 ?></div>
                <?php
                } else {
                $calendar_list = $this->calendar_service->getGoogleCalendarList($this->access_token);
                ?>
                    <a href="#" class="add-new-h2" id="tb-add-calendar"><?= $string_4 ?></a>
                    <script>
                        jQuery('#tb-add-calendar').click(function (e) {
                            jQuery('#tbk-gcal-not-found').hide();
                            jQuery('#tb-personal-add-calendar-id').val('');
                            e.preventDefault();
                        })
                    </script>
                    <!-- Add calendar modal markup -->
                    <div class="ui small modal" id="tb-personal-add-calendar-modal">
                        <i class="close tb-icon"></i>

                        <div class="header">
                            <?= $string_5 ?>
                        </div>
                        <div class="content" style="display:block;width: initial;">
                            <label><?= $string_2 ?></label>
                            <select style="width: 100%" id="tb-personal-add-calendar-id">
                                <option value='' disabled selected style='display:none;'>
                                    <?= esc_html__('Please select one of your owned calendars', 'teambooking'); ?>
                                </option>
                                <?php
                                foreach ($calendar_list as $calendar_entry) {
                                    /* @var $calendar_entry Google_Service_Calendar_CalendarListEntry */
                                    if (in_array($calendar_entry->getId(), $this->coworker->getCalendarId())) {
                                        continue;
                                    }
                                    ?>
                                    <option
                                        value='<?= $calendar_entry->getId() ?>'><?= $calendar_entry->getSummary() ?></option>
                                <?php } ?>
                            </select>

                            <p id="tbk-gcal-not-found" style="display: none;">
                                <?= esc_html__('This Google Calendar ID cannot be found in your authorized Google Account. Are you sure it was not removed a little while ago? Please refresh the page.', 'teambooking'); ?>
                            </p>
                        </div>
                        <div class="actions">
                            <div class="ui positive button" style="height:auto">
                                <?= $string_4 ?>
                            </div>
                            <div class="ui deny black button" style="height:auto">
                                <?= $string_6 ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </h2>
        </div>
        <?php if (!empty($this->access_token)) { ?>
        <div class="tbk-content">
            <table class="form-table">
                <tbody>
                <?php
                foreach ($this->coworker->getCalendarId() as $key => $calendar_id) {
                    ?>
                    <tr>
                        <th scope="row" style="width:350px;">
                            <?php
                            $found = FALSE;
                            foreach ($calendar_list as $calendar_entry) {
                                if ($calendar_entry->getId() == $calendar_id) {
                                    ?>
                                    <div
                                        class="ui mini horizontal label tbk-gcal-<?= $calendar_entry->getColorId() ?>"></div>
                                    <?php
                                    echo $calendar_entry->getSummary();
                                    $found = TRUE;
                                }
                            }
                            if (!$found) {
                                ?>
                                <div class="ui mini red label"><?= esc_html__('Not found anymore!', 'teambooking'); ?></div>
                            <?php } ?>
                        </th>
                        <td style="width: 1px">
                            <?= $calendar_id ?>
                        </td>
                        <td>
                            <a href="#" class="dashicons dashicons-trash tb-remove-calendar-id"
                               data-key="<?= $key ?>"></a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <script>
                jQuery('.tb-remove-calendar-id').click(function (e) {
                    e.preventDefault();
                })
            </script>
        </div>
    <?php } ?>
        <!-- Remove calendar modal markup -->
        <div class="ui small modal" id="tb-personal-remove-calendar-modal">
            <i class="close tb-icon"></i>

            <div class="header">
                <?= $string_7 ?>
            </div>
            <div class="content" style="width: initial;">
                <p><?= $string_9 ?></p>
            </div>
            <div class="actions">
                <div class="ui positive button" style="height:auto">
                    <?= $string_8 ?>
                </div>
                <div class="ui deny black button" style="height:auto">
                    <?= $string_6 ?>
                </div>
            </div>
        </div>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getHints()
    {
        $string_1 = esc_html__('Useful links', 'teambooking');
        $string_2 = esc_html__('Your Google authorized apps page', 'teambooking');
        ob_start();
        ?>
        <div class="tbk-panel-title">
            <h4><?= $string_1 ?></h4>
        </div>
        <ul class="tbk-list">
            <li>
                <ol>
                    <li><?php $url = 'https://www.google.com/calendar/render' ?>
                        <a href="<?= $url ?>" target="_blank">Google Calendar</a>
                    </li>
                    <li>
                        <a href="https://security.google.com/settings/security/permissions?pli=1" target="_blank">
                            <?= $string_2 ?>
                        </a>
                    </li>
                </ol>
            </li>
        </ul>

        <?php
        return ob_get_clean();
    }

    //------------------------------------------------------------

    private function getStatusBlock()
    {
        $string_1 = esc_html__('Authorized', 'teambooking');
        $string_2 = esc_html__('Temporarily Authorized', 'teambooking');
        $string_3 = esc_html__('Your authorization process had issues! Please revoke the authorization manually in your Google Account first', 'teambooking');
        $string_4 = esc_html__('here', 'teambooking');
        $string_5 = esc_html__('Revoke authorization', 'teambooking');
        $string_6 = esc_html__('Authorize!', 'teambooking');
        $string_7 = esc_html__('Not authorized', 'teambooking');
        $string_8 = esc_html__('Status', 'teambooking');
        $string_9 = esc_html__('Are you sure?', 'teambooking');
        $string_10 = esc_html__('Yes', 'teambooking');
        $string_11 = esc_html__('No', 'teambooking');
        $string_12 = esc_html__('You are going to revoke your authorization of your Google Account', 'teambooking');
        $string_13 = esc_html__('All the Google Calendar IDs you have added, will be unlinked. In order to place availability in the future, you should go through the authorization step again. You can reauthorize with another Google Account, if not already used by another coworker.', 'teambooking');
        ob_start();
        ?>
        <div class="tbk-panel-title">
            <h4><?= $string_8 ?></h4>
        </div>
        <div class="tbk-content">
            <?php
            if (!empty($this->access_token)) {
                $coworker_google_email = $this->calendar_service->getTokenEmailAccount($this->access_token, $this->coworker->getId());
                ?>
                <div class="tbk-coworker-gmail-account">
                    <?php
                    if ($coworker_google_email instanceof Google_Auth_Exception) {
                        echo $coworker_google_email->getMessage();
                    } else {
                        echo $coworker_google_email;
                    }
                    ?>
                </div>
            <?php
            if (isset(json_decode($this->access_token)->refresh_token)) {
            ?>
                <span class="tbk-status tbk-success"><?= $string_1 ?></span>
            <?php } else { ?>
                <span class="tbk-status tbk-partial-success"><?= $string_2 ?></span>
                <p><?= $string_3 ?> (<a href="https://security.google.com/settings/security/permissions"
                                        target="_blank"><?= $string_4 ?></a>)</p>
            <?php
            }
            ?>
                <p>
                    <a id="team_booking_revoke_personal_token" class="button tbk-button-long-text">
                        <?= $string_5 ?>
                    </a>
                </p>
                <script>
                    jQuery('#team_booking_revoke_personal_token').click(function (e) {
                        e.preventDefault();
                    })
                </script>
                <!-- Revoke Auth modal markup -->
                <div class="ui small modal" id="tb-personal-token-revoke-modal">
                    <i class="close tb-icon"></i>

                    <div class="header">
                        <?= $string_9 ?>
                    </div>
                    <div class="content" style="width: initial;">
                        <p>
                            <?= $string_12 ?>
                        </p>
                        <h4>
                            <?php
                            if (!$coworker_google_email instanceof Google_Auth_Exception) {
                                echo $coworker_google_email;
                            }
                            ?>
                        </h4>

                        <p>
                            <?= $string_13 ?>
                        </p>
                    </div>
                    <div class="actions">
                        <div class="ui positive button" style="height:auto">
                            <?= $string_10 ?>
                        </div>
                        <div class="ui deny black button" style="height:auto">
                            <?= $string_11 ?>
                        </div>
                    </div>
                </div>
            <?php
            } else {
            $calendar = new TeamBooking_Calendar();
            $authorization_url = $calendar->createAuthUrl();
            ?>
                <span class="tbk-status tbk-unsuccess"><?= $string_7 ?></span>
                <?php
                // prior to PHP 5.5 empty() only supports variables
                $client_id = $this->settings->getApplicationClientId();
                $client_secret = $this->settings->getApplicationClientSecret();
            if (!empty($client_id) && !empty($client_secret)) {
                ?>
                <p>
                    <a class='button button-primary tbk-button-long-text' href='<?= $authorization_url ?>'>
                        <span class="dashicons dashicons-googleplus"> </span>
                        <?= $string_6 ?>
                    </a>
                </p>
                <?php
            }
            }
            ?>
        </div>

        <?php
        return ob_get_clean();
    }

}
