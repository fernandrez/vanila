<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Components_Admin_Misc
{

    /**
     * @var TeamBooking_Components_Admin_Misc
     */
    private static $_instance;

    //------------------------------------------------------------

    public static function render()
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    //------------------------------------------------------------

    /**
     * Generate the tab wrapper for admin page
     *
     * @param array $tabs
     * @param string $active_tab
     */
    public function getTabWrapper($tabs, $active_tab)
    {
        $dashicons_array = array(
            'overview'  => 'dashicons-chart-area',
            'events'    => 'dashicons-clipboard',
            'personal'  => 'dashicons-businessman',
            'aspect'    => 'dashicons-art',
            'general'   => 'dashicons-admin-generic',
            'payments'  => 'dashicons-cart',
            'coworkers' => 'dashicons-groups',
        );
        $plugin_data = get_plugin_data(TEAMBOOKING_FILE_PATH);
        ?>
        <header class="tbk-header">
            <div class="tbk-version-docs">v. <?= $plugin_data['Version'] ?> |
                <a href="<?= $plugin_data['PluginURI'] ?>/docs" target="_blank">Docs</a>
            </div>
            <a href="<?= $plugin_data['PluginURI'] ?>" target="_blank" class="tbk-logo">TeamBooking</a>

            <h2 class="tbk-heading">Hi, <?= wp_get_current_user()->user_firstname ?></h2>
            <nav class="tbk-nav-horizontal">
                <ul>
                    <?php
                    foreach ($tabs as $key => $tab) {
                        ?>
                        <li> <?php
                            if ($key == $active_tab) {
                                ?>
                                <a href="<?= admin_url('admin.php?page=team-booking&tab=' . $key) ?>" class="active">
                                    <span class="dashicons <?= $dashicons_array[$key] ?>"
                                          style="line-height: inherit;"></span>
                                    <?= $tab ?>
                                </a>
                                <?php
                            } else {
                                ?>
                                <a href="<?= admin_url('admin.php?page=team-booking&tab=' . $key) ?>">
                                    <span class="dashicons <?= $dashicons_array[$key] ?>"
                                          style="line-height: inherit;"></span>
                                    <?= $tab ?>
                                </a>
                                <?php
                            }
                            ?> </li> <?php
                    }
                    ?>
                </ul>
            </nav>
        </header>
        <?php
    }

}
