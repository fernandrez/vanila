<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_SlotsResults
{

    //------------------------------------------------------------

    // This array is ordered by service id
    private $main_array = array();
    // Current sorting
    private $current_sorting = 'service';

    public function __construct()
    {
    }

    //------------------------------------------------------------

    public function addSlot(TeamBooking_Slot $slot)
    {
        $this->main_array[$slot->getServiceId()][] = $slot;
    }

    public function addSlotsFromArray(array $slots)
    {
        foreach ($slots as $slot) {
            $this->addSlot($slot);
        }
    }

    public function getSlotsByService(array $service_ids)
    {
        $results_array = array();
        $this->sortMainArrayByService();
        foreach ($service_ids as $service_id) {
            if (isset($this->main_array[$service_id])) {
                $results_array = array_merge($this->main_array[$service_id], $results_array);
            }
        }

        return $this->sort($results_array);
    }

    public function getSlotsByCoworker(array $coworker_ids)
    {
        $results_array = array();
        $this->sortMainArrayByCoworker();
        foreach ($coworker_ids as $coworker_id) {
            if (isset($this->main_array[$coworker_id])) {
                $results_array = array_merge($this->main_array[$coworker_id], $results_array);
            }
        }

        return $this->sort($results_array);
    }

    public function sortMainArrayByCoworker()
    {
        if ($this->current_sorting == 'coworker') {
            // Let's reorder based on numbers of slots
            uasort($this->main_array, function ($a, $b) {
                return count($b) - count($a);
            });

            return;
        }
        $results_array = array();
        foreach ($this->main_array as $slots_array) {
            foreach ($slots_array as $slot) {
                /* @var $slot TeamBooking_Slot */
                $results_array[$slot->getCoworkerId()][] = $slot;
            }
        }
        $this->main_array = $results_array;
        // Let's reorder based on numbers of slots
        uasort($this->main_array, function ($a, $b) {
            return count($b) - count($a);
        });
        $this->current_sorting = 'coworker';
    }

    public function sortMainArrayByService()
    {
        if ($this->current_sorting == 'service') {
            // Let's reorder based on numbers of slots
            uasort($this->main_array, function ($a, $b) {
                return count($b) - count($a);
            });

            return;
        }
        $results_array = array();
        foreach ($this->main_array as $slots_array) {
            foreach ($slots_array as $slot) {
                /* @var $slot TeamBooking_Slot */
                $results_array[$slot->getServiceId()][] = $slot;
            }
        }
        $this->main_array = $results_array;
        // Let's reorder based on numbers of slots
        uasort($this->main_array, function ($a, $b) {
            return count($b) - count($a);
        });
        $this->current_sorting = 'service';
    }

    public function sortMainArrayByDate()
    {
        if ($this->current_sorting == 'date') {
            return;
        }
        $results_array = array();
        $timezone = tbGetTimezone();
        foreach ($this->main_array as $slots_array) {
            foreach ($slots_array as $slot) {
                /* @var $slot TeamBooking_Slot */
                $start_time = new DateTime($slot->getStartTime());
                $start_time->setTimezone($timezone);
                $results_array[$start_time->format('d-m-Y')][] = $slot;
            }
        }
        $this->main_array = $results_array;
        $this->current_sorting = 'date';
    }

    public function getAllSlots()
    {
        $this->sortMainArrayByService();
        $return = array();
        foreach ($this->main_array as $slots_array) {
            foreach ($slots_array as $slot) {
                $return[] = $slot;
            }
        }

        return $return;
    }

    public function getServiceIds()
    {
        $this->sortMainArrayByService();

        return array_keys($this->main_array);
    }

    public function getCoworkerIds()
    {
        $this->sortMainArrayByCoworker();

        return array_keys($this->main_array);
    }

    public function getShownCoworkerServiceIds()
    {
        $services = $this->getServiceIds();
        $return = array();
        foreach ($services as $id) {
            if (tbGetSettings()->getService($id)->getShowCoworker()) {
                $return[] = $id;
            }
        }

        return $return;
    }

    public function getLocationsList()
    {
        $return = array();
        foreach ($this->getAllSlots() as $slot) {
            /* @var $slot TeamBooking_Slot */
            if ($slot->getLocation() !== '') {
                $return[$slot->getLocation()] = 1;
            }
        }

        return array_keys($return);
    }

    /**
     *
     * @return array The dates are in the form DD-MM-YYYY
     */
    public function getDates()
    {
        $this->sortMainArrayByDate();

        return array_keys($this->main_array);
    }

    /**
     *
     * @param string $day i.e. 24
     * @param string $month i.e. 05
     * @param string $year i.e. 2017
     */
    public function getSlotsByDate($day = FALSE, $month = FALSE, $year = FALSE)
    {
        $results_array = array();
        $this->sortMainArrayByDate();
        if (isset($this->main_array[$day . '-' . $month . '-' . $year])) {
            $results_array = array_merge($this->main_array[$day . '-' . $month . '-' . $year], $results_array);
        }

        return $this->sort($results_array);
    }

    public function sort(array $results_array)
    {
        /**
         * Let's sort the slots by the defined rule
         */
        if (tbGetSettings()->isGroupSlotsByTime()) {
            $return = $this->sortSlotsByTime($results_array);
        } elseif (tbGetSettings()->isGroupSlotsByService()) {
            $return = $this->sortSlotsByService($results_array);
        } elseif (tbGetSettings()->isGroupSlotsByCoworker()) {
            $return = $this->sortSlotsByCoworker($results_array);
        } else {
            $return = $this->sortSlotsByTime($results_array); // Default sorting
        }

        return $return;
    }

    /**
     * Sort slots by time.
     *
     * @param array $slots
     * @return array
     */
    private function sortSlotsByTime(array $slots)
    {
        uasort($slots, function ($a, $b) {
            return strtotime($a->getStartTime()) > strtotime($b->getStartTime());
        });

        return $slots;
    }

    /**
     * Sort slots by coworker.
     *
     * @param array $slots
     * @return array
     */
    private function sortSlotsByCoworker(array $slots)
    {
        uasort($slots, function ($a, $b) {
            if ($a->getCoworkerId() == $b->getCoworkerId()) {
                return strtotime($a->getStartTime()) > strtotime($b->getStartTime());
            }

            return $a->getCoworkerId() > $b->getCoworkerId();
        });

        return $slots;
    }

    /**
     * Sort slots by service name (alphabetically).
     *
     * @param array $slots
     * @return array Slots
     */
    private function sortSlotsByService(array $slots)
    {
        uasort($slots, function ($a, $b) {
            if ($a->getServiceName() == $b->getServiceName()) {
                return strtotime($a->getStartTime()) > strtotime($b->getStartTime());
            }

            return $a->getServiceName() > $b->getServiceName();
        });

        return $slots;
    }

    /**
     * Get the closest slot in time
     */
    public function getClosestSlot()
    {
        $results_array = array();
        foreach ($this->main_array as $slots_array) {
            $results_array = array_merge($results_array, $slots_array);
        }
        $results_array = $this->sortSlotsByTime($results_array);

        return $results_array[0];
    }

    public function thereAreSlots()
    {
        if (!empty($this->main_array)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function trimSlots($min_time, $max_time)
    {
        $timezone = tbGetTimezone();
        foreach ($this->main_array as $key_1 => $slots_array) {
            foreach ($slots_array as $key_2 => $slot) {
                /* @var $slot TeamBooking_Slot */
                $start_time = new DateTime($slot->getStartTime());
                $start_time->setTimezone($timezone);
                if ($start_time->format('U') > strtotime($max_time) && !is_null($min_time) && !is_null($max_time)) {
                    unset($this->main_array[$key_1][$key_2]);
                }
            }
        }
    }

}
