<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Widget extends WP_Widget
{

    /**
     * Sets up the widgets name etc
     */
    public function __construct()
    {
        parent::__construct(
            'teambooking_widget', // Base ID
            __('Team Booking Calendar', 'teambooking'), // Name
            array('description' => __('Shows a booking calendar', 'teambooking'),) // Args
        );
    }

    //------------------------------------------------------------

    /**
     * Outputs the content of the widget
     *
     * @param array $args
     * @param array $instance
     * @return bool
     * @throws Exception
     */
    public function widget($args, $instance)
    {
        add_action('wp_footer', 'enqueueTeamBookingStylesFooter');

        wp_enqueue_script('semantic-script');
        wp_enqueue_script('tb-base64-decoder');

        $settings = tbGetSettings();
        // Read-only mode is identified by lenght of istance id
        // This is the fastest way to keep things safe from exploits
        if ($instance['read_only'] == 'true') {
            $unique_id = tbRandomNumber(6);
        } else {
            $unique_id = tbRandomNumber(8);
        }
        if (isset($instance['no_filter']) && $instance['no_filter'] == 'true') {
            $filter = TRUE;
        } else {
            $filter = FALSE;
        }
        $title = apply_filters('widget_title', $instance['title']);
        echo $args['before_widget'];
        if (!empty($title)) {
            echo $args['before_title'] . $title . $args['after_title'];
        }

        // Picking all active services
        $all_services = $settings->getServices();
        // Remove inactive services
        foreach ($all_services as $key => $service) {
            if (!$service->isActive()) {
                unset($all_services[$key]);
            }
        }
        if (!empty($all_services)) {
            $checkboxes = array();
            foreach ($all_services as $booking_id => $booking) {
                $checkboxes[$booking_id] = isset($instance[$booking->getId()]) ? TRUE : FALSE;
            }
        } else {
            return _e('WARNING: no booking(s) found. Please add one first.', 'teambooking');
        }
        $widget_service_list = array_keys($checkboxes, TRUE);
        if (empty($widget_service_list)) {
            return _e('WARNING: no booking(s) selected. Please check this widget settings.', 'teambooking');
        }
        $calendar = new TeamBooking_Calendar();
        $parameters = new TeamBooking_RenderParameters();
        if (count($widget_service_list) == 1 && $settings->getService(reset($widget_service_list))->isClass('service')) {
            $parameters->setServiceIds($widget_service_list);
        } else {
            $parameters->setServiceIds(array_keys($all_services));
        }
        $parameters->setRequestedServiceIds($widget_service_list);
        $parameters->setCoworkerIds(array());
        $parameters->setInstance($unique_id);
        $parameters->setIsAjaxCallNo();
        if (!$filter) {
            $parameters->setNoFilterNo();
        } else {
            $parameters->setNoFilterYes();
        }
        $parameters->setIsWidgetYes();
        $post_id = get_the_ID();
        echo "<div class='ui calendar_widget_container' data-postid='" . $post_id . "'>";
        $calendar->getCalendar($parameters);
        echo "</div>";
        echo $args['after_widget'];

        return TRUE;
    }

    //------------------------------------------------------------

    /**
     * Outputs the options form on admin
     *
     * @param array $instance The widget options
     */
    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Widget title', 'teambooking');
        }
        if (!isset($instance['read_only'])) {
            $instance['read_only'] = "false";
        }
        if (!isset($instance['no_filter'])) {
            $instance['no_filter'] = "false";
        }
        ?>
        <p>
            <label for="<?= $this->get_field_id('title') ?>"><?= __('Title:') ?></label>
            <input class="widefat" id="<?= $this->get_field_id('title') ?>" name="<?= $this->get_field_name('title') ?>"
                   type="text" value="<?= esc_attr($title) ?>">
        </p>
        <p><?= __('Services:', 'teambooking') ?></p>
        <?php
        foreach (tbGetSettings()->getServices() as $booking) {
            $id = $this->get_field_id($booking->getId());
            ?>
            <label for="<?= $id ?>"></label>
            <input type="checkbox" id="<?= $id ?>" name="<?= $this->get_field_name($booking->getId()) ?>" <?php
            if (isset($instance[$booking->getId()])) {
                checked($instance[$booking->getId()], 'on');
            }
            ?>><span><?= $booking->getName() ?></span>
            <br>
        <?php } ?>
        <p><?= __('Read-only:', 'teambooking') ?></p>
        <input type="radio" name="<?= $this->get_field_name('read_only') ?>" value="true" <?=
    checked($instance['read_only'], "true")
        ?>><?= __('Yes', 'teambooking') ?>
        <input type="radio" name="<?= $this->get_field_name('read_only') ?>" value="false" <?=
    checked($instance['read_only'], "false")
        ?>><?= __('No', 'teambooking') ?>
        <p><?= __('Hide filter button:', 'teambooking') ?></p>
        <input type="radio" name="<?= $this->get_field_name('no_filter') ?>" value="true" <?=
    checked($instance['no_filter'], "true")
        ?>><?= __('Yes', 'teambooking') ?>
        <input type="radio" name="<?= $this->get_field_name('no_filter') ?>" value="false" <?=
    checked($instance['no_filter'], "false")
        ?>><?= __('No', 'teambooking') ?>
        <br>
        <br>
        <?php
    }

    //------------------------------------------------------------

    /**
     * Processing widget options on save
     *
     * @param array $new_instance The new options
     * @param array $old_instance The previous options
     * @return array
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        foreach (tbGetSettings()->getServices() as $service) {
            $instance[$service->getId()] = $new_instance[$service->getId()];
        }
        $instance['read_only'] = $new_instance['read_only'];
        $instance['no_filter'] = $new_instance['no_filter'];

        return $instance;
    }

}
