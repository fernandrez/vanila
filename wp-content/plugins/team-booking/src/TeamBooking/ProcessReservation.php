<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

/**
 * This class handles the reservation process
 * along with payments
 */
class TeamBooking_ProcessReservation
{

    //------------------------------------------------------------

    private $settings;

    public function __construct()
    {
        $this->settings = tbGetSettings();
        // Reservation form processing actions
        add_action(
            'wp_ajax_tb_submit_payment',
            array(
                $this,
                'submitPayment',
            ), 10, 0
        );
        add_action(
            'wp_ajax_nopriv_tb_submit_payment',
            array(
                $this,
                'submitPayment',
            ), 10, 0
        );
        add_action(
            'wp_ajax_tbajax_action_submit_form',
            array(
                $this,
                'submitReservation',
            )
        );
        add_action(
            'wp_ajax_nopriv_tbajax_action_submit_form',
            array(
                $this,
                'submitReservation',
            )
        );
        add_action(
            'wp_ajax_tb_process_onsite_payment',
            array(
                $this,
                'processOnsite',
            )
        );
        add_action(
            'wp_ajax_nopriv_tb_process_onsite_payment',
            array(
                $this,
                'processOnsite',
            )
        );
        // ICAL file
        add_action(
            'admin_post_tb_get_ical',
            array(
                $this,
                'getIcalFile',
            )
        );
        add_action(
            'admin_post_nopriv_tb_get_ical',
            array(
                $this,
                'getIcalFile',
            )
        );
    }

    //------------------------------------------------------------

    /**
     * Process onsite payments
     *
     * @return type
     */
    public function processOnsite()
    {
        $nonce = $_POST['nonce'];
        if (!wp_verify_nonce($nonce, 'teambooking_process_payment_onsite')) {
            $message = $this->tbGetExpiredNonceMessage();
            exit($message);
        }
        ///////////////////////
        // Reservation data  //
        ///////////////////////
        /* @var $reservation_data TeamBooking_ReservationData */
        $reservation_data = unserialize(base64_decode($_POST['reservation_data']));
        $reservation_database_id = filter_input(INPUT_POST, 'reservation_database_id');
        ///////////////////
        // Service data  //
        ///////////////////
        $service_id = $reservation_data->getServiceId();
        //////////////////////////////////
        // Reservation database record  //
        //////////////////////////////////
        if ($this->settings->getService($service_id)->getPaymentMustBeDone() == 'immediately'
            && !$reservation_data->isPaid()
        ) {
            // pending
            $retrieved_reservation_data = tbGetReservationById($reservation_database_id, TRUE);
        } else {
            // not pending
            $retrieved_reservation_data = tbGetReservationById($reservation_database_id);
        }
        ///////////////
        // Checksum  //
        ///////////////
        if ($reservation_data->getCreationInstant() != $retrieved_reservation_data->getCreationInstant()) {
            return; // corrupted data or wrong id
        }
        ////////////////////////////
        // Additional parameters  //
        ////////////////////////////
        $additional_parameter = filter_input(INPUT_POST, 'additional_parameter');
        ///////////////////
        // Gateway boot  //
        ///////////////////
        $gateway_id = filter_input(INPUT_POST, 'gateway_id');
        $response = $this->settings->getPaymentGatewaySettingObject($gateway_id)->prepareGateway($reservation_data, $additional_parameter);
        if (!($response instanceof TeamBooking_Error)) {
            /**
             * at this point, $response must be an array
             * with relevant payment details
             */
            $retrieved_reservation_data->setPaid();
            $retrieved_reservation_data->setPaymentGateway($gateway_id);
            $retrieved_reservation_data->setPaymentDetails($response);
            /**
             * Let's do the reservation only if it was pending,
             * and it was of course pending if the payment is
             * requested immediately
             */
            if ($this->settings->getService($service_id)->getPaymentMustBeDone() == 'immediately') {
                $reservation_class = new TeamBooking_Reservation($retrieved_reservation_data);
                $retrieved_reservation_data = $reservation_class->doReservation();
            }
            /**
             * Let's update the database
             */
            if (!$retrieved_reservation_data instanceof TeamBooking_Error) {
                if ($this->settings->getService($service_id)->getPaymentMustBeDone() == 'immediately') {
                    // the record is in pending table, let's move it
                    tbDeleteReservationById($reservation_database_id, TRUE);
                    tbInsertReservation($retrieved_reservation_data);
                } else {
                    // the record is in regular table, let's update it
                    tbUpdateReservationById($reservation_database_id, $retrieved_reservation_data);
                }
                // All is done, redirect or not?
                if ($this->settings->getService($reservation_data->getServiceId())->getRedirect()) {
                    echo $this->settings->getService($reservation_data->getServiceId())->getRedirectUrl();
                } else {
                    // start of HTML response >>>>
                    ?>
                    <div class="ui positive message">
                        <div class="tbk-header">
                            <?= esc_html__('Thank you!', 'teambooking') ?>
                        </div>
                        <p><?= esc_html__('Your payment was succesfull!', 'teambooking') ?></p>
                    </div>
                    <?php
                    // <<<< end of HTML response
                }
            } else {
                // Something went wrong, output a message
                ?>
                <div class="ui negative message">
                    <div class="tbk-header">
                        <?= esc_html($retrieved_reservation_data->getMessage()) ?>
                    </div>
                </div>
                <?php
            }
            // bye bye
            exit();
        } else {
            /*
             * $response is an error object
             */
            echo esc_html($response->getMessage());
            // bye bye
            exit;
        }
    }

    //------------------------------------------------------------

    /**
     * Process the very first payment action
     *
     * It needs:
     * - reservation data
     * - reservation database ID (if not pending)
     * - payment gateway ID
     *
     */
    public function submitPayment($reservation_data = NULL, $gateway_id = NULL, $reservation_database_id = NULL)
    {
        ///////////////////////
        // Reservation data  //
        ///////////////////////
        /* @var $reservation_data TeamBooking_ReservationData */
        if (is_null($reservation_data)) {
            // this function is called via Ajax, parameters are POSTed
            $reservation_data = unserialize(base64_decode($_POST['reservation_data']));
        }
        ///////////////////
        // Service data  //
        ///////////////////
        $service_id = $reservation_data->getServiceId();
        //////////////////////////////////
        // Reservation database record  //
        //////////////////////////////////
        if ($this->settings->getService($service_id)->getPaymentMustBeDone() == 'immediately'
            && !$reservation_data->isPaid()
        ) {
            /*
             * Let's insert the pending reservation
             *
             * If the service is unscheduled, then the coworker is not
             * defined at this point, we must choose it before inserting
             * in the database
             */
            if ($reservation_data->getServiceClass() == 'service' && !$reservation_data->getCoworker()) {
                $reservation_data->setCoworker(TeamBooking_Reservation::chooseCoworker($service_id));
            }
            $reservation_database_id = tbInsertReservation($reservation_data, TRUE);
        } else {
            // not pending, so already inserted
            if (is_null($reservation_database_id)) {
                // this function is called via Ajax, parameters are POSTed
                $reservation_database_id = filter_input(INPUT_POST, 'reservation_database_id');
            }
            $retrieved_reservation_data = tbGetReservationById($reservation_database_id);
            ///////////////
            // Checksum  //
            ///////////////
            if ($reservation_data->getCreationInstant() != $retrieved_reservation_data->getCreationInstant()) {
                return; // corrupted data or wrong id
            }
        }
        ///////////////////
        // Gateway boot  //
        ///////////////////
        if (is_null($gateway_id)) {
            // this function is called via Ajax, parameters are POSTed
            $gateway_id = filter_input(INPUT_POST, 'gateway_id');
        }
        if ($this->settings->getPaymentGatewaySettingObject($gateway_id)->isOffsite()) {
            // if the gateway is offsite, we call it directly
            $response = $this->settings->getPaymentGatewaySettingObject($gateway_id)->prepareGateway($reservation_data);
        } else {
            // otherwise, we call specific data collecting form
            $response = $this->settings->getPaymentGatewaySettingObject($gateway_id)->getDataForm($reservation_data, $reservation_database_id);
        }
        echo $response;
        // bye bye
        exit;
    }

    //------------------------------------------------------------

    /**
     * Ajax callback: submit form reservation on frontend calendar
     */
    public function submitReservation()
    {
        // Map the reservation data
        $reservation_data = tbModalFormMapper($_POST['data']);

        // Is nonce verification failed?
        if (!$reservation_data) {
            $message = $this->tbGetExpiredNonceMessage();
            exit($message);
        }

        // Checks for files
        if (!empty($_FILES)) {
            foreach ($_FILES as $hook => $file) {
                // TODO: serverside data validation
                $returned_handle = tbHandleFileUpload($file);
                $reservation_data->addFileReference($hook, $returned_handle);
            }
        }

        // If the service price is 0, then the reservation is inherently "paid"...
        if ($this->settings->getService($reservation_data->getServiceId())->getPrice() <= 0) {
            $reservation_data->setPaid();
        }

        /*
         * Let's save the reservation data.
         * 
         * If payment is requested immediately and the price is > 0
         * then we must save the data in PENDING table.
         */
        if ($this->settings->getService($reservation_data->getServiceId())->getPaymentMustBeDone() == 'immediately'
            && !$reservation_data->isPaid()
            && !current_user_can('manage_options') //Admins must skip the payment process
        ) {
            // pending
            $active_payment_gateways = $this->settings->getPaymentGatewaysActive();
            /**
             * We have only one payment gateway active, and the payment is
             * requested immediately? Then, instead of showing payment choices,
             * we would rather skip the step and go directly into the payment
             * process
             */
            if (count($active_payment_gateways) == 1) {
                /* @var $gateway TeamBooking_PaymentGateways_Settings */
                $gateway = reset($active_payment_gateways);
                $gateway_id = $gateway->getGatewayId();
                $this->submitPayment($reservation_data, $gateway_id, 0);
            } else {
                $this->showPaymentChoices(0, $reservation_data);
            }
            exit;

        } else {
            // not pending
            $reservation = new TeamBooking_Reservation($reservation_data);

            // confirm the reservation, or waiting for approval
            if ($this->settings->getService($reservation_data->getServiceId())->getApproveRule() === 'none') {
                // confirm
                $response = $reservation->doReservation();
                // Check for errors
                if ($response instanceof TeamBooking_Error) {
                    $template = new TeamBooking_Components_Frontend_ErrorMessages();
                    $code = $response->getCode();
                    switch ($code) {
                        case 1:
                            echo $template->coworkersRevokedAuth();
                            break;
                        case 2:
                            echo $template->eventNotAvailableAnymore();
                            break;
                        case 3:
                            echo $template->alreadyBooked();
                            break;
                        case 4:
                            echo $template->invalidAttendeeEmail();
                            break;
                        case 5:
                            echo $template->genericGoogleApiError($response->getMessage());
                            break;
                        case 6:
                            echo $template->eventFull();
                            break;
                        case 8:
                            echo $template->customerMaxCumulativeTicketsOvercome();
                            break;
                    }
                    // bye bye
                    exit;
                }
            } else {
                // put in waiting status
                $reservation_data->setStatusWaitingApproval();

                // send the notification email
                if ($this->settings->getService($reservation_data->getServiceId())->getSendApprovalNotification()) {
                    $email = new TeamBooking_Email();
                    $email->setSubject($this->settings->getService($reservation_data->getServiceId())->getApprovalNotificationSubject());
                    $email->setBody($this->settings->getService($reservation_data->getServiceId())->getApprovalNotificationText());
                    $email->setFrom($this->settings->getService($reservation_data->getServiceId())->getEmailForNotifications(), get_bloginfo('name'));
                    $email->setReplyTo(get_bloginfo('admin_email'));
                    if ($this->settings->getService($reservation_data->getServiceId())->getApproveRule() === 'coworker') {
                        $email->setTo($this->settings->getCoworkerData($reservation_data->getCoworker())->getEmail());
                    } elseif ($this->settings->getService($reservation_data->getServiceId())->getApproveRule() === 'admin') {
                        $email->setTo($this->settings->getService($reservation_data->getServiceId())->getEmailForNotifications());
                    }
                    $email->send();
                }
            }

            // Push the reservation into the database
            $reservation_database_id = tbInsertReservation($reservation_data);

            /**
             * TODO: since $response may not logically defined, should this
             * conditional be replaced by if(isset($response)) and the whole
             * subsequent block embraced by else{} ?
             */
            if ($reservation_data->isWaitingApproval()) {
                // let's check if we should redirect or not
                if ($this->settings->getService($reservation_data->getServiceId())->getRedirect()) {
                    echo $this->settings->getService($reservation_data->getServiceId())->getRedirectUrl();
                } else {
                    // start of HTML response >>>>
                    ?>
                    <div class="ui positive message">
                        <div class="tbk-header">
                            <?= esc_html__('Thank you for your reservation!', 'teambooking') ?>
                        </div>
                        <?php if ($this->settings->getService($reservation_data->getServiceId())->getConfirm()) { ?>
                            <p><?= esc_html__("We'll send you an email when the reservation gets approved!", 'teambooking') ?></p>
                        <?php } ?>
                    </div>
                    <br>
                    <?php
                    if (!$this->settings->getService($reservation_data->getServiceId())->isClass('service') &&
                        $this->settings->getShowIcal()
                    ) {
                        $this->getIcalForm($reservation_data);
                    }
                    // <<<< end of HTML response
                }
                exit;
            }

            // let's retrieve the updated reservation data
            $new_reservation_data = $response;

            if ($new_reservation_data instanceof TeamBooking_ReservationData) {
                // let's check if we should redirect or not
                if ($this->settings->getService($reservation_data->getServiceId())->getRedirect()) {
                    echo $this->settings->getService($reservation_data->getServiceId())->getRedirectUrl();
                } else {
                    // start of HTML response >>>>
                    ?>
                    <div class="ui positive message">
                        <div class="tbk-header">
                            <?= esc_html__('Thank you for your reservation!', 'teambooking') ?>
                        </div>
                        <?php if ($this->settings->getService($new_reservation_data->getServiceId())->getConfirm()) { ?>
                            <p><?= esc_html__('We have sent you an email with details.', 'teambooking') ?></p>
                        <?php } ?>
                    </div>
                    <?php
                    if (!$new_reservation_data->isPaid()
                        && $this->settings->getService($new_reservation_data->getServiceId())->getPrice() > 0
                        && $this->settings->thereIsAtLeastOneActivePaymentGateway()
                        && $this->settings->getService($new_reservation_data->getServiceId())->getPaymentMustBeDone() != 'later'
                        && !current_user_can('manage_options')
                    ) {
                        ?>
                        <div class="ui centered tbk-header">
                            <?= esc_html__('Do you want to pay right now?', 'teambooking') ?>
                        </div>
                        <?php
                        $this->showPaymentChoices($reservation_database_id, $new_reservation_data);
                    }
                    ?>
                    <br>
                    <?php
                    if (!$this->settings->getService($new_reservation_data->getServiceId())->isClass('service') &&
                        $this->settings->getShowIcal()
                    ) {
                        $this->getIcalForm($new_reservation_data);
                    }
                    // <<<< end of HTML response
                }
                // Update reservation
                tbUpdateReservationById($reservation_database_id, $new_reservation_data);
            }
            // bye bye
            exit;
        }
    }

    //------------------------------------------------------------

    /**
     * Shows the payment choices to the customer
     *
     * @param type $reservation_database_id
     * @param TeamBooking_ReservationData $reservation_data
     */
    private function showPaymentChoices($reservation_database_id, TeamBooking_ReservationData $reservation_data)
    {
        ?>
        <div class="ui basic segment" data-reservation="<?= base64_encode(serialize($reservation_data)) ?>"
             data-id="<?= $reservation_database_id ?>" <?= $reservation_data->getServiceClass() == 'service' ? 'data-isform' : '' ?>>
            <div class="ui stackable equal width center aligned grid">
                <?php
                $active_payment_gateways = $this->settings->getPaymentGatewaysActive();
                $i = 1;
                foreach ($active_payment_gateways as $gateway) {
                    /* @var $gateway TeamBooking_PaymentGateways_Settings */
                    ?>
                    <?php if ($i != 1) { ?>
                        <div class="ui vertical tbk-divider">
                            <?= esc_html__('or', 'teambooking') ?>
                        </div>
                    <?php } ?>
                    <div class="tbk-column">
                        <?= $gateway->getPayButton() ?>
                    </div>
                    <?php
                    $i++;
                }
                ?>
            </div>
            <script>
                tbSubmitPayment();
            </script>
        </div>
        <?php
    }

    //------------------------------------------------------------

    private function tbGetExpiredNonceMessage()
    {
        $message = __('Please refresh the page...', 'teambooking');

        return '<div class="ui warning message"><div class="tbk-header">Oops!</div>' . esc_html($message) . '</div>';
    }

    //------------------------------------------------------------

    private function getIcalForm(TeamBooking_ReservationData $data)
    {
        ?>
        <div class="tbk-buttons">
            <form id="tb-get-ical-form" method="POST" action="<?= admin_url() . 'admin-post.php' ?>">
                <input type="hidden" name="action" value="tb_get_ical">
                <?php wp_nonce_field('team_booking_options_verify') ?>
                <input type="hidden" name="start" value="<?= $data->getStart() ?>">
                <input type="hidden" name="end" value="<?= $data->getEnd() ?>">
				<input type="hidden" name="description" value="<?= esc_attr($this->settings->getService($data->getServiceId())->getServiceInfo()) ?>">
				<input type="hidden" name="summary" value="<?= esc_attr($data->getServiceName()) ?>">
                <input type="hidden" name="uri" value="<?= $data->getUri() ?>">
                <input type="hidden" name="address" value="<?= esc_attr($data->getServiceLocation()) ?>">
                <button type="submit" class="ui basic tbk-button tb-get-ical">
                    <i class="tb-icon calendar"></i>
                    <?= esc_html__('Save on my calendar', 'teambooking') ?>
                </button>
            </form>
        </div>
        <?php
    }

    //------------------------------------------------------------

    public function getIcalFile()
    {
        tbGenerateICSFile(
            tbFilterInput($_POST['summary'], TRUE) . '.ics',
            $_POST['start'],
            $_POST['end'],
            $_POST['address'],
            $_POST['description'],
            $_POST['uri'],
            $_POST['summary']
        );
        exit;
    }

}
