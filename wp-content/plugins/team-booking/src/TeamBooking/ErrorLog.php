<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

/**
 * This class defines an error object used to log API errors
 */
class TeamBooking_ErrorLog
{

    //------------------------------------------------------------

    private $error_code;
    private $message;
    private $timestamp;
    private $coworker_id;
    private $calendar_id;

    //------------------------------------------------------------

    public function getMessage()
    {
        if ($this->error_code == 502) {
            return "Error 502 (Server Error)!!"; // AVOID strange scramblin'css
        } else {
            return $this->message;
        }
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    //------------------------------------------------------------

    public function getErrorCode()
    {
        return $this->error_code;
    }

    public function setErrorCode($code)
    {
        $this->error_code = $code;
    }

    //------------------------------------------------------------

    public function getDescription()
    {
        switch ($this->error_code) {
            case 404:
                // TODO: remove (it should be useless as of v1.4.0)
                $return = "The Calendar ID provided by Coworker is neither existant nor belongs to his authorized Google Account";
                break;
            case 500:
                $return = "A Google internal temporary server error, don't worry too much about it";
                break;
            case 502:
                $return = "The server encountered a temporary error and could not complete the request. Just a temporary error.";
                break;
            default :
                $return = __('No description available', 'teambooking');
                break;
        }

        return $return;
    }

    //------------------------------------------------------------

    public function getTimestamp()
    {
        return $this->timestamp;
    }

    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    //------------------------------------------------------------

    public function getCoworkerId()
    {
        return $this->coworker_id;
    }

    public function setCoworkerId($id)
    {
        $this->coworker_id = $id;
    }

    //------------------------------------------------------------

    public function getCoworker()
    {
        $coworker = new TeamBookingCoworker($this->coworker_id);
        $return = $coworker->getFirstName() . " " . $coworker->getLastName();

        return $return;
    }

    //------------------------------------------------------------

    public function getCalendarId()
    {
        if (isset($this->calendar_id)) {
            return $this->calendar_id;
        } else {
            return NULL;
        }
    }

    public function setCalendarId($id)
    {
        $this->calendar_id = $id;
    }

}
