<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Loader
{

    //------------------------------------------------------------

    /**
     * The main load method.
     *
     * It should be called from the plugin main file in order to init the plugin
     */
    public function load()
    {

        // load text domain for translations
        add_action('init', array(
            $this,
            'load_textdomain',
        ));

        // add oAuth handler
        add_action('wp_ajax_teambooking_oauth_callback', 'teambooking_oauth_callback');

        // add IPN listener
        add_action('wp_ajax_teambooking_ipn_listener', 'teambooking_ipn_listener');
        add_action('wp_ajax_nopriv_teambooking_ipn_listener', 'teambooking_ipn_listener');

        // add TinyMCE vars getter
        add_action('wp_ajax_teambooking_get_tinymce_vars', 'teambooking_get_tinymce_vars');

        new TeamBooking_ProcessReservation();

        /*
         * load admin classes and resources
         */
        if (is_admin() && (!defined('DOING_AJAX') || !DOING_AJAX)) {

            // activation hook
            register_activation_hook(TEAMBOOKING_FILE_PATH, array(
                $this,
                'install',
            ));
            // deactivation hook
            register_deactivation_hook(TEAMBOOKING_FILE_PATH, array(
                $this,
                'deactivate',
            ));
            // uninstall hook ($this can't work, TeamBooking_Loader class must be called directly)
            register_uninstall_hook(TEAMBOOKING_FILE_PATH, array(
                'TeamBooking_Loader',
                'uninstall',
            ));

            /*
             * Check settings integrity
             * 
             * If the settings are not the right object
             * tries to deactivate the plugin
             */
            if (!tbGetSettings() instanceof TeamBookingSettings) {
                add_action('init', function () {
                    deactivate_plugins(plugin_basename(TEAMBOOKING_FILE_PATH));
                    wp_redirect(admin_url('plugins.php'));
                });
            } else {
                /*
                 * TGM Plugin Activation loader
                 */
                include_once dirname(TEAMBOOKING_FILE_PATH) . '/libs/tgm/class-tgm-plugin-activation.php';
                add_action('tgmpa_register', array(
                    $this,
                    'requiredPlugins',
                ));
            }

            // update method
            if (tbGetSettings() instanceof TeamBookingSettings) {
                if (tbGetSettings()->getVersion() < TEAMBOOKING_VERSION) {
                    add_action('init', array(
                        $this,
                        'update',
                    ));
                }
            }

            // load admin class
            new TeamBooking_Admin();
        } else {

            // hook the shortcode register method
            add_action('init', array(
                $this,
                'register_shortcodes',
            ));

            // enqueue resources
            add_action('wp_enqueue_scripts', array(
                $this,
                'tb_frontend_resources_enqueue',
            ));

            // register Ajax Callbacks
            add_action('wp_ajax_tbajax_action_change_month', 'tbajax_action_change_month_callback');
            add_action('wp_ajax_tbajax_action_show_day_schedule', 'tbajax_action_show_day_schedule_callback');
            add_action('wp_ajax_tbajax_action_filter_calendar', 'tbajax_action_filter_calendar_callback');
            add_action('wp_ajax_tbajax_action_get_reservation_modal', 'tbajax_action_get_reservation_modal_callback');
            add_action('wp_ajax_tbajax_action_get_register_modal', 'tbajax_action_get_register_modal_callback');
            add_action('wp_ajax_tbajax_action_fast_month_selector', 'tbajax_action_fast_month_selector_callback');
            add_action('wp_ajax_tbajax_action_fast_year_selector', 'tbajax_action_fast_year_selector_callback');
            add_action('wp_ajax_tbajax_action_cancel_reservation', 'tbajax_action_cancel_reservation_callback');

            add_action('wp_ajax_nopriv_tbajax_action_change_month', 'tbajax_action_change_month_callback');
            add_action('wp_ajax_nopriv_tbajax_action_show_day_schedule', 'tbajax_action_show_day_schedule_callback');
            add_action('wp_ajax_nopriv_tbajax_action_filter_calendar', 'tbajax_action_filter_calendar_callback');
            add_action('wp_ajax_nopriv_tbajax_action_get_reservation_modal', 'tbajax_action_get_reservation_modal_callback');
            add_action('wp_ajax_nopriv_tbajax_action_get_register_modal', 'tbajax_action_get_register_modal_callback');
            add_action('wp_ajax_nopriv_tbajax_action_fast_month_selector', 'tbajax_action_fast_month_selector_callback');
            add_action('wp_ajax_nopriv_tbajax_action_fast_year_selector', 'tbajax_action_fast_year_selector_callback');
            add_action('wp_ajax_nopriv_tbajax_action_cancel_reservation', 'tbajax_action_cancel_reservation_callback');
        }

        // register the widget
        add_action('widgets_init', function () {
            register_widget('TeamBooking_Widget');
        });

        // load fonts
        add_action('wp_enqueue_scripts', array(
            $this,
            'load_fonts',
        ));
    }

    //------------------------------------------------------------

    /**
     * Load fonts
     *
     * @param bool|FALSE $always
     */
    public static function load_fonts()
    {
        wp_register_style('teambooking_fonts', '//fonts.googleapis.com/css?family=Oswald|Open+Sans:300italic,400,300,700|Josefin+Sans:400,700,300,300italic', array(), '1.0.0');
    }

    //------------------------------------------------------------

    /**
     * Load frontend resources
     */
    public function tb_frontend_resources_enqueue()
    {
        ////////////////////
        //      CSS       //
        ////////////////////

        if (tbGetSettings()->getFix62dot5()) {
            wp_register_style('semantic-style', TEAMBOOKING_URL . 'libs/semantic/semantic-fix-min.css');
        } else {
            wp_register_style('semantic-style', TEAMBOOKING_URL . 'libs/semantic/semantic-min.css');
        }
        wp_register_style('teambooking-style-frontend', TEAMBOOKING_URL . 'css/frontend.css');

        //////////////////
        //  Dashicons   //
        //////////////////
        wp_enqueue_style('dashicons');

        //////////////
        // scripts  //
        //////////////
        wp_register_script('semantic-script', TEAMBOOKING_URL . 'libs/semantic/semantic.js', array('jquery'));
        wp_register_script('tb-base64-decoder', TEAMBOOKING_URL . 'libs/base64/base64decode.js', array('jquery'));
        wp_register_script('tb-frontend-script', TEAMBOOKING_URL . 'js/frontend.js', array('jquery'), FALSE, TRUE);
        // in javascript, object properties are accessed as ajax_object.some_value
        wp_localize_script('tb-frontend-script', 'TB_Ajax', array('ajax_url' => admin_url('admin-ajax.php')));

        ////////////////////
        // Google Places  //
        ////////////////////
        wp_register_script('google-places-script', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places', array('jquery'));
        wp_register_script('tb-geocomplete-script', TEAMBOOKING_URL . 'js/assets/jquery.geocomplete.min.js', array('jquery'));

        ////////////
        // GMap3  //
        ////////////
        wp_register_script('tb-gmap3-script', TEAMBOOKING_URL . 'libs/gmap3/gmap3.min.js', array('jquery'));

        /////////////////
        //   Stripe    //
        /////////////////
        if (tbGetSettings()->getPaymentGatewaySettingObject('stripe')->isActive()
            && tbGetSettings()->getPaymentGatewaySettingObject('stripe')->isLoadLibrary()
        ) {
            wp_register_script('stripejs', "https://js.stripe.com/v2/");
        }

    }

    //------------------------------------------------------------

    /**
     * The plugin Install method
     * Creates an instance of the install-class and fires the installation method
     * It is used only during the activation of the plugin
     */
    public function install()
    {
        $install = new TeamBooking_Install();
        $return = $install->install();

        return $return;
    }

    /**
     * The plugin Deactivate method
     * Creates an instance of the install-class and fires the deactivation method
     * It is used only during the deactivation of the plugin
     */
    public function deactivate()
    {
        $install = new TeamBooking_Install();
        $install->deactivate();
    }

    /**
     * The plugin Uninstall method
     * Creates an instance of the install-class and fires the unistallation method
     * It is used only during the uninstallation of the plugin
     */
    public static function uninstall()
    {
        $install = new TeamBooking_Install();
        $install->uninstall();
    }

    //------------------------------------------------------------

    /**
     * The plugin Update method
     * Handles changes eventually needed when not a clean-install
     *
     * NOTES:
     *
     * - version < 1.2.5: it repairs the hooks for builtin fields
     *                    due to visibility properties change.
     *                    It resets the show/required settings.
     * - version < 1.3.0: creates the database tables for all
     *                    the reservations records, and transfer
     *                    the logs there.
     * - version < 1.4.0: updates the PayPal gateway by the new
     *                    interface, adds Stripe gateway too.
     */
    public function update()
    {
        $settings = tbGetSettings();
        $stored_version = $settings->getVersion();
        if ($stored_version < '1.2.5') {
            $services = $settings->getServices();
            $old_hooks_and_labels = array(
                'first_name'  => __('First name', 'teambooking'),
                'second_name' => __('Last name', 'teambooking'),
                'email'       => __('Email', 'teambooking'),
                'address'     => __('Address', 'teambooking'),
                'phone'       => __('Phone number', 'teambooking'),
                'note'        => __('Notes', 'teambooking'),
            );
            $old_hooks = array_keys($old_hooks_and_labels);
            if (empty($services)) {
                return;
            }
            foreach ($services as $service) {
                /* @var $service TeamBookingType */
                $fields = $service->getFormFields();
                $i = 0;
                foreach ($fields->getBuiltInFields() as $field) {
                    $field->setHook($old_hooks[$i]);
                    $field->setLabel($old_hooks_and_labels[$old_hooks[$i]]);
                    $i++;
                }
                $service->setFormFields($fields);
                $settings->updateService($service, $service->getId());
            }
        }
        if ($stored_version < '1.3.0') {
            $install = new TeamBooking_Install();
            $install->createPendingReservationsTable();
            $table_name = $install->createReservationsTable();
            $logs = $settings->getLogs();
            global $wpdb;
            foreach ($logs as $key => $log) {
                if ($log instanceof TeamBooking_ReservationData) {
                    /* @var $log TeamBooking_ReservationData */
                    $log->setCreationInstant($key);
                    $created = current_time("Y-m-d H:i:s");
                    $data_object = serialize($log);
                    $wpdb->insert($table_name, array(
                        'created'     => $created,
                        'service_id'  => $log->getServiceId(),
                        'coworker_id' => $log->getCoworker(),
                        'data_object' => $data_object,
                    ));
                    $settings->dropLog($key);
                }
            }
        }
        if ($stored_version < '1.4.0') {
            $payment_gateways = $settings->getPaymentGatewaySettingObjects();
            if (empty($payment_gateways)) {
                $settings->addPaymentGatewaySettingObject(new TeamBooking_PaymentGateways_Stripe_Settings());
                $settings->addPaymentGatewaySettingObject(new TeamBooking_PaymentGateways_PayPal_Settings());
            }
            $settings->clean();
        }
        /*
         *  Update the version flag
         */
        $settings->setVersion(TEAMBOOKING_VERSION);
        $settings->save();

        return;
    }

    //------------------------------------------------------------

    /**
     * Load the text domain on plugin load.
     *
     * Hooked to the plugins_loaded via the load method
     */
    public function load_textdomain()
    {
        $domain = 'teambooking';
        $locale = apply_filters('plugin_locale', get_locale(), $domain);
        load_textdomain($domain, WP_LANG_DIR . '/teambooking/' . $locale . '.mo');
        load_plugin_textdomain($domain, FALSE, plugin_basename(TEAMBOOKING_PATH) . '/languages/');
    }

    //------------------------------------------------------------

    public function requiredPlugins()
    {
        /*
         * Array of plugin arrays. Required keys are name and slug.
         * If the source is NOT from the .org repo, then source is also required.
         */
        $plugins = array(
            array(
                'name'         => 'Envato Market',
                // The plugin name.
                'slug'         => 'envato-market',
                // The plugin slug (typically the folder name).
                'source'       => 'http://envato.github.io/wp-envato-market/dist/envato-market.zip',
                // The plugin source.
                'required'     => FALSE,
                // If false, the plugin is only 'recommended' instead of required.
                'external_url' => '',
            ),
        );

        $config = array(
            'id'           => 'tgmpa',
            // Unique ID for hashing notices for multiple instances of TGMPA.
            'default_path' => '',
            // Default absolute path to bundled plugins.
            'menu'         => 'tgmpa-install-plugins',
            // Menu slug.
            'parent_slug'  => 'plugins.php',
            // Parent menu slug.
            'capability'   => 'manage_options',
            // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
            'has_notices'  => TRUE,
            // Show admin notices or not.
            'dismissable'  => TRUE,
            // If false, a user cannot dismiss the nag message.
            'dismiss_msg'  => '',
            // If 'dismissable' is false, this message will be output at top of nag.
            'is_automatic' => FALSE,
            // Automatically activate plugins after installation or not.
            'message'      => '',
            // Message to output right before the plugins table.
            'strings'      => array(
                'page_title'                     => __('Install Recommended Plugins', 'teambooking'),
                'menu_title'                     => __('Install Plugins', 'teambooking'),
                'installing'                     => __('Installing Plugin: %s', 'teambooking'),
                // %s = plugin name.
                'notice_can_install_recommended' => _n_noop(
                    'TeamBooking recommends the following plugin: %1$s.',
                    'TeamBooking recommends the following plugins: %1$s.',
                    'teambooking'
                ),
                // %1$s = plugin name(s).
                'notice_ask_to_update'           => _n_noop(
                    'The following plugin needs to be updated to its latest version to ensure maximum compatibility with TeamBooking: %1$s.',
                    'The following plugins need to be updated to their latest version to ensure maximum compatibility with TeamBooking: %1$s.',
                    'teambooking'
                ),
                // %1$s = plugin name(s).
                'notice_ask_to_update_maybe'     => _n_noop(
                    'There is an update available for: %1$s.',
                    'There are updates available for the following plugins: %1$s.',
                    'teambooking'
                ),
                // %1$s = plugin name(s).
            ),
        );

        tgmpa($plugins, $config);
    }

    //------------------------------------------------------------

    /**
     * Register Shortcodes handler
     */
    public function register_shortcodes()
    {
        // Main shortcode
        add_shortcode('tb-calendar', array(
            $this,
            'shortcode',
        ));

        // User reservation list
        add_shortcode('tb-reservations', array(
            $this,
            'shortcode_reservations',
        ));
    }

    /**
     * Team booking Calendar Shortcode
     *
     * @param $atts
     * @return string
     * @throws Exception
     */
    public function shortcode($atts)
    {
        add_action('wp_footer', 'enqueueTeamBookingStylesFooter');

        wp_enqueue_script('semantic-script');
        wp_enqueue_script('tb-base64-decoder');

        $settings = tbGetSettings();

        // Set attributes
        extract(shortcode_atts(array(
            'booking'     => NULL,
            'coworker'    => NULL,
            'read_only'   => FALSE,
            'logged_only' => FALSE,
            'nofilter'    => FALSE,
            // Hide filtering button
        ), $atts, 'tb-calendar'));

        // Read-only mode is identified by lenght of istance id
        // This is the fastest way to keep things safe from exploits
        if (!$read_only) {
            $unique_id = tbRandomNumber(8);
        } else {
            $unique_id = tbRandomNumber(6);
        }

        if (!$logged_only || ($logged_only && is_user_logged_in())) {
            // Picking all active services
            $all_services = array_keys($settings->getServices());
            // Remove inactive services
            foreach ($all_services as $key => $service) {
                if (!$settings->getService($service)->isActive()) {
                    unset($all_services[$key]);
                }
            }
            if (!is_null($booking)) {
                $services = array_map('trim', explode(",", $booking));
                foreach ($services as $key => $booking) {
                    try {
                        // Remove inactive services
                        if (!$settings->getService($booking)->isActive()) {
                            unset($services[$key]);
                        }
                    } catch (Exception $exc) {
                        unset($services[$key]);
                        continue;
                    }
                }
                if (empty($services)) {
                    return __('WARNING: booking ID(s) not found. Please check the shortcode syntax.', 'teambooking');
                }
            } else {
                if (!is_null($settings->getServices())) {
                    // Service(s) not specified, picking all of them
                    $services = $all_services;
                } else {
                    // Service(s) not specified, but no service available
                    return __('WARNING: no booking(s) found. Please add one first.', 'teambooking');
                }
            }
            if (!is_null($coworker)) {
                $coworkers = array_map('trim', explode(",", $coworker));
            } else {
                $coworkers = array();
            }
            $calendar = new TeamBooking_Calendar();
            $parameters = new TeamBooking_RenderParameters();
            if (count($services) == 1 && $settings->getService(reset($services))->isClass('service')) {
                $parameters->setServiceIds($services);
            } else {
                $parameters->setServiceIds($all_services);
            }
            $parameters->setRequestedServiceIds($services);
            $parameters->setCoworkerIds($coworkers);
            $parameters->setInstance($unique_id);
            $parameters->setIsAjaxCallNo();
            if (!$nofilter) {
                $parameters->setNoFilterNo();
            } else {
                $parameters->setNoFilterYes();
            }
            ob_start();
            ?>

            <div class="ui calendar_main_container" data-postid="<?= get_the_ID() ?>">
                <?php
                $slots_obj = $calendar->getCalendar($parameters);
                if ($slots_obj instanceof TeamBooking_SlotsResults) {
                    $parameters->setSlotsObj($slots_obj);
                }
                ?>
            </div>
            <br>

            <?php
            return ob_get_clean();
        }
    }

    public function shortcode_reservations($atts)
    {
        if (!is_user_logged_in()) {
            return;
        }

        add_action('wp_footer', 'enqueueTeamBookingStylesFooter');

        wp_enqueue_script('semantic-script');
        wp_enqueue_script('tb-base64-decoder');

        $settings = tbGetSettings();

        // Set attributes
        extract(shortcode_atts(array(
            'read_only' => FALSE,
        ), $atts, 'tb-reservations'));

        $user_id = get_current_user_id();
        $timezone = tbGetTimezone();
        $now = time();
        $reservations = tbGetReservations();
        uasort($reservations, function ($a, $b) {
            /* @var $a TeamBooking_ReservationData */
            return ($a->getStart() > $b->getStart()) ? -1 : (($a->getStart() < $b->getStart()) ? 1 : 0);
        });

        echo "<table class='tb-reservations-list'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>" . esc_html__('Service', 'teambooking') . "</th>";
        echo "<th>" . esc_html__('When', 'teambooking') . "</th>";
        echo "<th>" . esc_html__('Status', 'teambooking') . "</th>";
        if (!$read_only) {
            echo "<th>" . esc_html__('Actions', 'teambooking') . "</th>";
        }
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        foreach ($reservations as $id => $reservation) {
            if ($reservation->getCustomerUserId() == $user_id) {

                $service = tbGetSettings()->getService($reservation->getServiceId());
                $date_time_object = new DateTime($reservation->getSlotStart());

                if ($date_time_object->getTimestamp() < $now) {
                    continue;
                }

                $date_time_object->setTimezone($timezone);
                $slot_starting_time_in_seconds = $date_time_object->getTimestamp() + $date_time_object->getOffset();
                $when_value = date_i18n_tb(get_option('date_format'), $slot_starting_time_in_seconds)
                    . " "
                    . date_i18n_tb(get_option('time_format'), $slot_starting_time_in_seconds);

                echo "<tr>";
                echo "<td>" . $reservation->getServiceName() . "</td>";
                echo "<td>" . $when_value . "</td>";
                echo "<td>" . $reservation->getStatus() . "</td>";
                if (!$read_only) {
                    echo "<td>"
                        . ((!$reservation->isCancelled()
                            && $service->getAllowCustomerCancellation() == TRUE
                            && !$service->isClass('service')
                            && ($slot_starting_time_in_seconds - $now) >= $service->getAllowCustomerCancellationUntil()) ?
                            '<a href="#" class="tb-cancel-reservation" data-id="'
                            . $id . '" data-hash="' . $reservation->getChecksum() . '">'
                            . esc_html__('cancel', 'teambooking')
                            . '</a>'
                            : '')
                        . "</td>";
                }
                echo "</tr>";
            }
        }
        echo "</tbody>";
        echo "</table>";

        ?>
        <div class="ui small tbk-modal tb-reservation-cancel-modal">
            <div class="tbk-header"><?= esc_html__('Are you sure?', 'teambooking') ?></div>
            <div class="tbk-content">
                <p>
                    <?= esc_html__("You're going to cancel this reservation, the action is irreversible.", 'teambooking') ?>
                </p>
            </div>
            <div class="actions">
                <div class="ui positive tbk-button"><?= esc_html__('Proceed', 'teambooking') ?></div>
                <div class="ui cancel tbk-button"><?= esc_html__('Cancel', 'teambooking') ?></div>
            </div>
        </div>
        <?php

    }

}
