<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_AdminFilterObject
{

    //------------------------------------------------------------

    private $page;
    private $max_entries;
    private $sort_by;
    private $pending_reservations;
    private $ids;
    private $search_term;
    private $status;
    private $payment_status;
    private $coworker;

    //------------------------------------------------------------

    public function encode()
    {
        return tb_base64_url_encode(serialize($this));
    }

    public function decode($object)
    {
        /* @var $instance TeamBooking_AdminFilterObject */
        $instance = unserialize(tb_base64_url_decode($object));
        $this->page = $instance->getPage();
        $this->max_entries = $instance->getMaxEntries();
        $this->sort_by = $instance->getSortBy();
        $this->pending_reservations = $instance->getPendingReservations();
        $this->ids = $instance->getIds();
        $this->search_term = $instance->getSearchTerm();
        $this->status = $instance->getStatus();
        $this->payment_status = $instance->getPaymentStatus();
        $this->coworker = $instance->getCoworker();
    }

    //------------------------------------------------------------

    public function getPage()
    {
        if (!$this->page) {
            return 1;
        } else {
            return $this->page;
        }
    }

    public function setPage($page)
    {
        $this->page = $page;
    }

    //------------------------------------------------------------

    public function getMaxEntries()
    {
        if (!$this->max_entries) {
            return 15;
        } else {
            return $this->max_entries;
        }
    }

    public function setMaxEntries($entries)
    {
        $this->max_entries = $entries;
    }

    //------------------------------------------------------------

    public function getSortBy()
    {
        if (!$this->sort_by) {
            return 'booked';
        } else {
            return $this->sort_by;
        }
    }

    public function setSortBy($rule)
    {
        $this->sort_by = $rule;
    }

    //------------------------------------------------------------

    public function getPendingReservations()
    {
        if (!$this->pending_reservations) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function setPendingReservations($boolean)
    {
        $this->pending_reservations = $boolean;
    }

    //------------------------------------------------------------

    public function getIds()
    {
        return $this->ids;
    }

    public function setIds($ids)
    {
        $this->ids = $ids;
    }

    //------------------------------------------------------------

    public function getSearchTerm()
    {
        return $this->search_term;
    }

    public function setSearchTerm($string)
    {
        $this->search_term = $string;
    }

    //------------------------------------------------------------

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    //------------------------------------------------------------

    public function getPaymentStatus()
    {
        return $this->payment_status;
    }

    public function setPaymentStatus($status)
    {
        $this->payment_status = $status;
    }

    //------------------------------------------------------------

    public function getCoworker()
    {
        return $this->coworker;
    }

    public function setCoworker($id)
    {
        $this->coworker = $id;
    }

}
