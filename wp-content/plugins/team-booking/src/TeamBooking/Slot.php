<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Slot
{

    //------------------------------------------------------------

    ///////////////////
    // service data  //
    ///////////////////
    private $service_id;
    private $service_name;
    private $service_info;
    ///////////////////////
    // Google event data //
    ///////////////////////
    private $start;
    private $end;
    private $event_id;
    private $calendar_id;
    private $container;
    private $multiple_services;
    private $attendees_number;
    private $allday;
    private $event_id_parent;
    ////////////////
    // slot data  //
    ////////////////
    private $coworker_id;
    private $soldout;
    private $location;

    //------------------------------------------------------------

    /**
     * Returns the Google Calendar ID in which
     * the event is.
     *
     * @return string
     */
    public function getCalendarId()
    {
        return $this->calendar_id;
    }

    /**
     * Sets the Google Calendar ID in which
     * the event is.
     *
     * @param string $id
     */
    public function setCalendarId($id)
    {
        $this->calendar_id = $id;
    }

    /**
     * Returns the services array in case of a multiple
     * service container event.
     *
     * @return array
     */
    public function getMultipleServices()
    {
        if (is_array($this->multiple_services)) {
            return $this->multiple_services;
        } else {
            return array();
        }
    }

    /**
     * Sets the services array in case of a multiple
     * service container event.
     *
     * @param array $services
     */
    public function setMultipleServices(array $services)
    {
        $this->multiple_services = $services;
    }

    /**
     * Returns the service name of the slot
     *
     * @return array
     */
    public function getServiceName()
    {
        return $this->service_name;
    }

    /**
     * Sets the service name of the slot
     *
     * @param string $name
     */
    public function setServiceName($name)
    {
        $this->service_name = $name;
    }

    /**
     * Returns the service description
     *
     * @return string
     */
    public function getServiceInfo()
    {
        return $this->service_info;
    }

    /**
     * Sets the service description
     *
     * @param string $info
     */
    public function setServiceInfo($info)
    {
        $this->service_info = $info;
    }

    /**
     * Returns the service id of the slot
     *
     * @return string
     */
    public function getServiceId()
    {
        return $this->service_id;
    }

    /**
     * Sets the service id of the slot
     *
     * @param string $id
     */
    public function setServiceId($id)
    {
        $this->service_id = $id;
    }

    /**
     * Returns the location of the slot
     *
     * @return string
     */
    public function getLocation()
    {
        return trim($this->location);
    }

    /**
     * Sets the location of the slot
     *
     * @param string $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * Sets the start time of the slot
     *
     * @param string $time in RFC3339 format
     */
    public function setStartTime($time)
    {
        $this->start = $time;
    }

    /**
     * Returns the start time of the slot
     *
     * @return string in RFC3339 format
     */
    public function getStartTime()
    {
        return $this->start;
    }

    /**
     * Sets the end time of the slot
     *
     * @param string $time in RFC3339 format
     */
    public function setEndTime($time)
    {
        $this->end = $time;
    }

    /**
     * Returns the end time of the slot
     *
     * @return string in RFC3339 format
     */
    public function getEndTime()
    {
        return $this->end;
    }

    /**
     * Returns the Google event id
     *
     * @return string
     */
    public function getEventId()
    {
        return $this->event_id;
    }

    /**
     * Sets the Google event id
     *
     * @param string $id
     */
    public function setEventId($id)
    {
        $this->event_id = $id;
    }

    /**
     * Returns the Google parent event id, if any
     *
     * @return string
     */
    public function getEventIdParent()
    {
        if (!$this->event_id_parent) {
            return FALSE;
        } else {
            return $this->event_id_parent;
        }
    }

    /**
     * Sets the Google parent event id, if any
     *
     * @param string $id
     */
    public function setEventIdParent($id)
    {
        $this->event_id_parent = $id;
    }

    /**
     * Returns the coworker id
     *
     * @return int
     */
    public function getCoworkerId()
    {
        return $this->coworker_id;
    }

    /**
     * Sets the coworker id
     *
     * @param int $id
     */
    public function setCoworkerId($id)
    {
        $this->coworker_id = $id;
    }

    /**
     * Returns the number of attendees for that slot
     *
     * @return int
     */
    public function getAttendeesNumber()
    {
        return $this->attendees_number;
    }

    /**
     * Sets the number of attendees for that slot
     *
     * @param int $number
     */
    public function setAttendeesNumber($number)
    {
        $this->attendees_number = $number;
    }

    /**
     * Sets the slot as All Day slot
     */
    public function setAllDayTrue()
    {
        $this->allday = TRUE;
    }

    /**
     * Sets the slot as Non All Day slot
     */
    public function setAllDayFalse()
    {
        $this->allday = FALSE;
    }

    /**
     * Checks if the slot is of All Day type
     *
     * @return boolean
     */
    public function isAllDay()
    {
        if ($this->allday) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Sets the slot as Container-derived
     */
    public function setContainerTrue()
    {
        $this->container = TRUE;
    }

    /**
     * Sets the slot as a Not Container-derived
     */
    public function setContainerFalse()
    {
        $this->container = FALSE;
    }

    /**
     * Checks if the slot is derived from a Container
     *
     * @return boolean
     */
    public function isContainer()
    {
        if (!$this->container) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Sets the slot as soldout/booked
     */
    public function setSoldoutTrue()
    {
        $this->soldout = TRUE;
    }

    /**
     * Sets the slot as free/available
     */
    public function setSoldoutFalse()
    {
        $this->soldout = FALSE;
    }

    /**
     * Checks if the slot is soldout/booked
     *
     * @return boolean
     */
    public function isSoldout()
    {
        if ($this->soldout) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
