<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Admin
{

    /** @var $settings TeamBookingSettings */
    static $settings;
    static $form_action;

    public function __construct()
    {
        // Admin panel callback registration
        add_action('admin_menu', array(
            $this,
            'addAdminMenu',
        ));
        // Admin panel form callback registration
        add_action('admin_post_option_submit', array(
            $this,
            'submitOptions',
        ));
        // Activate output buffer, needed until a better solution to wp_redirect "headers already sent" error will comes up
        add_action('init', array(
            $this,
            'activateOutputBuffer',
        ));
        add_action('admin_enqueue_scripts', array(
            $this,
            'addColorPicker',
        ));
        add_action('admin_enqueue_scripts', array(
            $this,
            'enqueueResources',
        ));
        // Add TinyMCE button
        add_action('admin_head', array(
            $this,
            'addTinyMCEButton',
        ));
        // Set properties
        static::$settings = tbGetSettings();
        static::$form_action = admin_url('admin-post.php');
        /*
         *  Clean reservation database
         *  TODO: slow down with a transient
         */
        add_action('init', 'tbCleanReservationsRoutine');
    }

    public function addAdminMenu()
    {
        if (current_user_can('manage_options')) {
            // User is an admin coworker
            $new_reservation_ids = $this->getNewReservationIds();
            $bubble_title = __('new reservations', 'teambooking');
            $bubble_count = count($new_reservation_ids);
            $menu_label = 'TeamBooking '
                . "<span class='update-plugins tb-bubble count-$bubble_count' title='$bubble_title'><span class='update-count'>"
                . number_format_i18n($bubble_count)
                . "</span></span>";

            add_menu_page("Team Booking", $menu_label, 'manage_options', 'team-booking', array(
                $this,
                'createAdminPage',
            ), TEAMBOOKING_URL . 'images/admin-icon.png');

        } elseif (current_user_can('tb_can_sync_calendar')) {
            // User is a non-admin coworker
            $new_reservation_ids = $this->getNewReservationIds(FALSE);
            $bubble_title = __('new reservations', 'teambooking');
            $bubble_count = count($new_reservation_ids);
            $menu_label = 'TeamBooking '
                . "<span class='update-plugins tb-bubble count-$bubble_count' title='$bubble_title'><span class='update-count'>"
                . number_format_i18n($bubble_count)
                . "</span></span>";

            add_menu_page("Team Booking", $menu_label, 'tb_can_sync_calendar', 'team-booking', array(
                $this,
                'createCoworkerPage',
            ), TEAMBOOKING_URL . 'images/admin-icon.png');

        } else {
            // Anyone else
            return;
        }
    }

    /**
     * @param bool|TRUE $is_admin
     * @return array
     */
    private function getNewReservationIds($is_admin = TRUE)
    {
        $transient = get_transient('teambooking_seen_reservations');
        if (!$transient) {
            $transient = array();
        }
        if (!isset($transient[get_current_user_id()])) {
            $transient[get_current_user_id()] = array();
        }

        if ($is_admin) {
            $reservation_keys = array_keys(tbGetReservations());
        } else {
            $reservation_keys = array_keys(tbGetReservationsByCoworker(get_current_user_id()));
        }

        return array_diff($reservation_keys, $transient[get_current_user_id()]);
    }

    /**
     * Fetch WP Roles
     *
     * @return unknown List of editable roles
     */
    private function fetchRoles()
    {
        return get_editable_roles();
    }

    /**
     * This method retrieve all the roles with tb_can_sync_calendar capability
     *
     * @return array List of roles allowed to link a calendar
     */
    public function getRolesWithSyncCap()
    {
        $roles = $this->fetchRoles();
        $list = array();
        foreach ($roles as $name => $role) {
            if (isset($role['capabilities']['tb_can_sync_calendar'])) {
                $list[$name] = $role['name'];
            }
        }

        return $list;
    }

    /**
     * Load WP Color Pickers
     */
    public function addColorPicker($hook)
    {
        // let's load only if we're on Team Booking dashboard
        if ($hook == "toplevel_page_team-booking") {
            ///////////////////////
            // Color Picker CSS  //
            ///////////////////////   
            wp_enqueue_style('wp-color-picker');
        }
    }

    /**
     * Load admin resources
     */
    public function enqueueResources($hook)
    {
        // let's load only if we're on Team Booking dashboard
        if ($hook == "toplevel_page_team-booking") {

            //////////////////////
            // jQuery resources //
            //////////////////////
            wp_enqueue_script('tb-admin-script', TEAMBOOKING_URL . 'js/admin.js', array(
                'jquery',
                'wp-color-picker',
                'jquery-ui-core',
                'jquery-ui-sortable',
            ));
            // in javascript, object properties are accessed as ajax_object.some_value
            wp_localize_script('tb-admin-script', 'TB_vars', array(
                'post_url' => admin_url('admin-post.php'),
                'wpNonce'  => wp_create_nonce('team_booking_options_verify'),
            ));

            //////////////////////
            //   admin CSS      //
            //////////////////////
            wp_enqueue_style('tb-admin-style', TEAMBOOKING_URL . 'css/admin.css');

            ////////////////////////
            // semantic UI stuff  //
            ////////////////////////
            $comp_path = 'libs/semantic/components/';
            wp_enqueue_script('semantic-modal-script', TEAMBOOKING_URL . $comp_path . 'modal.js', array('jquery'));
            wp_enqueue_script('semantic-transition-script', TEAMBOOKING_URL . $comp_path . 'transition.js', array('jquery'));
            wp_enqueue_style('semantic-modal-style', TEAMBOOKING_URL . $comp_path . 'modal.css');
            wp_enqueue_style('semantic-transition-style', TEAMBOOKING_URL . $comp_path . 'transition.css');
            wp_enqueue_script('semantic-dimmer-script', TEAMBOOKING_URL . $comp_path . 'dimmer.js', array('jquery'));
            wp_enqueue_style('semantic-dimmer-style', TEAMBOOKING_URL . $comp_path . 'dimmer.css');
            wp_enqueue_style('semantic-button-style', TEAMBOOKING_URL . $comp_path . 'button.css');
            wp_enqueue_style('semantic-icon-style', TEAMBOOKING_URL . $comp_path . 'icon.css');
            wp_enqueue_script('semantic-checkbox-script', TEAMBOOKING_URL . $comp_path . 'checkbox.js', array('jquery'));
            wp_enqueue_style('semantic-checkbox-style', TEAMBOOKING_URL . $comp_path . 'checkbox.css');
            wp_enqueue_style('semantic-label-style', TEAMBOOKING_URL . $comp_path . 'label.css');

            /////////////////////////
            // jQuery AreYouSure?  //
            /////////////////////////
            wp_enqueue_script('tb-areyousure-js', TEAMBOOKING_URL . 'js/assets/jquery.are-you-sure.js', array('jquery'));
        }
        //////////////////////////////
        //   TinyMCE button CSS     //
        //////////////////////////////
        wp_enqueue_style('tb-tinymcebutton-style', TEAMBOOKING_URL . 'css/tinymce.css');
    }

    /**
     *  Activates output buffering
     */
    public function activateOutputBuffer()
    {
        ob_start();
    }

    /**
     * This method just intercept $_GET data looking for notices
     */
    public function checkForNotices()
    {

        ////////////////////
        // Core notices   //
        ////////////////////
        if (isset($_GET['updated'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Settings saved!', 'teambooking'));
            echo $notice->getPositive();
        }
        if (isset($_GET['settings_imported'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Settings imported!', 'teambooking'));
            echo $notice->getPositive();
        }
        if (isset($_GET['version_mismatch'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Sorry, this settings file is from a newest plugin version and cannot be imported', 'teambooking'));
            echo $notice->getNegative();
        }
        if (isset($_GET['not_settings_file'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Sorry, this is not a Team Booking settings file!', 'teambooking'));
            echo $notice->getNegative();
        }
        /////////////////////////////
        // Manage Services notices //
        /////////////////////////////
        if (isset($_GET['event_added'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Service added succesfully!', 'teambooking'));
            echo $notice->getPositive();
        }
        if (isset($_GET['event_updated'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Service updated succesfully!', 'teambooking'));
            echo $notice->getPositive();
        }
        if (isset($_GET['event_deleted'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Service removed succesfully!', 'teambooking'));
            echo $notice->getPositive();
        }
        if (isset($_GET['selected_services_deleted'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Selected services removed succesfully!', 'teambooking'));
            echo $notice->getPositive();
        }
        ///////////////////////////////
        // Personal Settings notices //
        ///////////////////////////////
        if (isset($_GET['custom_settings_updated'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Custom settings updated succesfully!', 'teambooking'));
            echo $notice->getPositive();
        }
        if (isset($_GET['reset'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Custom settings deleted succesfully!', 'teambooking'));
            echo $notice->getPositive();
        }
        if (isset($_GET['partialreset'])) {
            $additional_content = sprintf(__('Go <a href="%s">here</a> and revoke auth to ', 'teambooking'), 'https://security.google.com/settings/security/permissions') . '<strong>' . static::$settings->getApplicationProjectName() . '</strong><br><br>';
            $notice = new TeamBooking_Components_Admin_Notice(__('Custom settings deleted, but for some reason you should revoke Google Auth manually!', 'teambooking'), $additional_content);
            echo $notice->getNegative();
        }
        if (isset($_GET['auth_success'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('You are now fully authorized!', 'teambooking'));
            echo $notice->getPositive();
        }
        if (isset($_GET['auth_failed'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Something went wrong, are you sure you have asked for authorization?', 'teambooking'));
            echo $notice->getNegative();
        }
        if (isset($_GET['auth_no_refresh'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Uh-oh, it seems that the authorization was already given in the past, but was not correctly revoked lately. Browse your authorized apps using the link in the useful links section, and try to manually revoke it, before trying again. It could also be possible that the Google Account you are logged in is already in use by another coworker.', 'teambooking'));
            echo $notice->getNegative();
        }
        if (isset($_GET['auth_already_used'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Uh-oh, it seems that the Google Account you are logged in is already in use by another coworker.', 'teambooking'));
            echo $notice->getNegative();
        }
        if (isset($_GET['duplicated_linked_title'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('This linked event title is already used!', 'teambooking'));
            echo $notice->getNegative();
        }
        if (isset($_GET['reset_failed'])) {
            $message = sprintf(__('Error revoking authorization: %s', 'teambooking'), urldecode($_GET['reset_failed']));
            $notice = new TeamBooking_Components_Admin_Notice($message);
            echo $notice->getNegative();
        }
        ///////////////////////
        // Overview  notices //
        ///////////////////////
        if (isset($_GET['log_deleted'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Reservation record succesfully deleted!', 'teambooking'));
            echo $notice->getPositive();
        }
        if (isset($_GET['deleted_all'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Reservations succesfully deleted!', 'teambooking'));
            echo $notice->getPositive();
        }
        if (isset($_GET['reservation_cancelled'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Reservation succesfully cancelled!', 'teambooking'));
            echo $notice->getPositive();
        }
        if (isset($_GET['reservation_approved'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Reservation succesfully confirmed!', 'teambooking'));
            echo $notice->getPositive();
        }
        if (isset($_GET['reservation_confirmed'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Reservation succesfully confirmed!', 'teambooking'));
            echo $notice->getPositive();
        }
        if (isset($_GET['deleted_selected'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Selected reservations succesfully deleted!', 'teambooking'));
            echo $notice->getPositive();
        }
        if (isset($_GET['not_revokable'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Cancellation not possible for Service class', 'teambooking'));
            echo $notice->getNegative();
        }
        if (isset($_GET['generic_error'])) {
            $notice = new TeamBooking_Components_Admin_Notice(urldecode($_GET['generic_error']));
            echo $notice->getNegative();
        }
        if (isset($_GET['error_logs_cleaned'])) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Error logs succesfully cleaned!', 'teambooking'));
            echo $notice->getPositive();
        }
        //////////////////////
        // General  notices //
        //////////////////////
        $client_id = static::$settings->getApplicationClientId();
        $client_secret = static::$settings->getApplicationClientSecret();
        if (empty($client_id) || empty($client_secret)) {
            $notice = new TeamBooking_Components_Admin_Notice(__('Team Booking is active, yet not configured!', 'teambooking'));
            echo $notice->getNegative();

            return;
        }
        try {
            $timezone = new DateTimeZone(get_option('timezone_string'));
        } catch (Exception $ex) {
            $timezone = tbGetTimezone();
            $notice = new TeamBooking_Components_Admin_Notice(sprintf(__('Please set your TimeZone in WordPress general settings! DSTs are not compatible with manual GMT offsets, so TeamBooking is assuming %s as your equivalent Timezone.', 'teambooking'), '<strong>' . $timezone->getName() . '</strong>'));
            echo $notice->getNegative();
        }
        // Sticky notice when PayPal Sandbox is active
        if (static::$settings->getPaymentGatewaySettingObject('paypal')->isActive() && static::$settings->getPaymentGatewaySettingObject('paypal')->isUseSandbox()) {
            $message = __('<strong>Advice</strong>: PayPal Sandbox Testing is active, remember to deactivate it after testing is done!', 'teambooking');
            $notice = new TeamBooking_Components_Admin_Notice($message);
            echo $notice->getNegative();
        }
    }

    /**
     * Builds the admin Teambooking page (Administrators)
     */
    public function createAdminPage()
    {
        ?>
        <div class="wrap">
            <h2 style="padding:0;"></h2><?php
            //Hack for Wordpress alerts that should stay always on top                                                                                                                                                                            
            // Checks for notices
            $this->checkForNotices();
            // Defining tabs
            $tabs = array(
                'overview'  => __('Overview', 'teambooking'),
                'events'    => __('Manage services', 'teambooking'),
                'coworkers' => __('Manage coworkers', 'teambooking'),
                'personal'  => __('Personal settings', 'teambooking'),
                'aspect'    => __('Frontend style', 'teambooking'),
                'general'   => __('Core settings', 'teambooking'),
                'payments'  => __('Payments gateway', 'teambooking'),
            );
            if (isset($_GET['tab'])) {
                // Select the active tab, if present...
                TeamBooking_Components_Admin_Misc::render()->getTabWrapper($tabs, $_GET['tab']);
                $function_to_call = 'buildAdminTab' . $_GET['tab'];
                $this->$function_to_call();
            } else {
                // ...or provide the default one
                TeamBooking_Components_Admin_Misc::render()->getTabWrapper($tabs, 'overview');
                $this->buildAdminTaboverview();
            }
            ?>
        </div>
        <?php
    }

    /**
     * Builds the admin Teambooking page (Coworkers)
     */
    public function createCoworkerPage()
    {
        ?>
        <div class="wrap">
            <h2 style="padding:0;"></h2><?php
            //Hack for Wordpress alerts that should stay always on top                                                                                                                                                                              
            // Checks for notices
            $this->checkForNotices();
            // Defining tabs
            $tabs = array(
                'overview' => __('Overview', 'teambooking'),
                'events'   => __('Manage services', 'teambooking'),
                'personal' => __('Personal settings', 'teambooking'),
            );
            if (isset($_GET['tab'])) {
                // Whitelisting tab name
                if (in_array($_GET['tab'], array_keys($tabs))) {
                    // Select the active tab, if present...
                    TeamBooking_Components_Admin_Misc::render()->getTabWrapper($tabs, $_GET['tab']);
                    $function_to_call = 'buildAdminTab' . $_GET['tab'];
                    $this->$function_to_call();
                } else {
                    // No authorized/valid tab name, so collapse to default
                    TeamBooking_Components_Admin_Misc::render()->getTabWrapper($tabs, 'overview');
                    $this->buildAdminTaboverview();
                }
            } else {
                // ...or provide the default one
                TeamBooking_Components_Admin_Misc::render()->getTabWrapper($tabs, 'overview');
                $this->buildAdminTaboverview();
            }
            ?>
        </div>
        <?php
    }

    /**
     * Builds the general settings tab
     */
    public function buildAdminTabgeneral()
    {
        // Template call
        $content = new TeamBooking_Components_Admin_Core(static::$settings);
        // Set variables
        $content->action = static::$form_action;
        $content->roles_all = $this->fetchRoles();
        $content->roles_allowed = $this->getRolesWithSyncCap();
        // Render page
        echo $content->getPostBody();
    }

    /**
     * This method handles every admin form submission.
     *
     * Flow:
     * 1) Do authority check (capability: tb_can_sync_calendar)
     * 2) Do nonce check (field name: team_booking_options_verify)
     * 3) Do actions, choice based on submit button name (TODO: use $_POST['_wp_http_referer'] instead?)
     * 4) Redirect back to tab, along with the right footprint for admin notices
     * 5) Exit
     */
    public function submitOptions()
    {
        //////////////////////
        // Authority check  //
        //////////////////////
        if (!current_user_can('tb_can_sync_calendar')) {
            wp_die(__('You are not allowed to be on this page.', 'teambooking'));
        }

        //////////////////////
        //   Nonce check    //
        //////////////////////
        check_admin_referer('team_booking_options_verify');

        ////////////////////////
        // Admin only actions //
        ////////////////////////
        if (isset($_POST['team_booking_options_submit'])) {
            $location = $this->saveCoreSettings();
        } elseif (isset($_POST['team_booking_insert_booking_type'])) {
            echo $this->addNewService();

            return;
        } elseif (isset($_POST['team_booking_delete_booking_confirm'])) {
            $location = $this->deleteService();
            echo $location;

            return;
        } elseif (isset($_POST['team_booking_delete_reservation_confirm'])) {
            if ($_POST['operation'] == 'delete') {
                $location = $this->deleteServiceReservation();
            } else {
                $location = $this->cancelServiceReservation();
            }
            echo $location;

            return;
        } elseif (isset($_POST['team_booking_approve_reservation_confirm'])) {
            $location = $this->approveServiceReservation();
            echo $location;

            return;
        } elseif (isset($_POST['team_booking_confirm_pending_reservation_confirm'])) {
            $location = $this->confirmPendingPaymentReservation();
            echo $location;

            return;
        } elseif (isset($_POST['team_booking_delete_all_reservations_confirm'])) {
            $location = $this->deleteAllReservations();
            echo $location;

            return;
        } elseif (isset($_POST['team_booking_remove_selected_reservations'])) {
            $location = $this->deleteSelectedReservations();
            echo $location;

            return;
        } elseif (isset($_POST['team_booking_remove_selected_services'])) {
            $location = $this->deleteSelectedServices();
            echo $location;

            return;
        } elseif (isset($_POST['team_booking_clean_error_logs'])) {
            $location = $this->cleanErrorLogs();
            echo $location;

            return;
        } elseif (isset($_POST['team_booking_remove_coworker_residual'])) {
            $location = $this->removeCoworkerResidualData();
            echo $location;

            return;
        } elseif (isset($_POST['team_booking_revoke_coworker_token_confirm'])) {
            echo $location = $this->revokeCoworker($_POST["coworker_id"]);

            return;
        } elseif (isset($_POST['team_booking_save_coworker_settings'])) {
            $form_data = $_POST['form_data'];
            parse_str($form_data, $form_data_parsed);
            static::$settings->updateCoworkerUrl($_POST["coworker_id"], trim($form_data_parsed['coworker_url']));
            static::$settings->save();
            echo $location = admin_url('admin.php?page=team-booking&tab=coworkers&updated=1');

            return;
        } elseif (isset($_POST['team_booking_payment_options_submit'])) {
            $location = $this->savePaymentsSettings();
        } elseif (isset($_POST['get_tb_settings_backup'])) {
            echo tbGenerateSettingsBackup();
            die();
        } elseif (isset($_POST['set_tb_settings_backup'])) {
            $location = $this->importSettingsFromFile();
        } elseif (isset($_POST['set_google_core_from_json'])) {
            echo $this->importGoogleProjectFromJSON();

            return;
        } elseif (isset($_POST['team_booking_frontend_style_submit'])) {
            $location = $this->saveFrontendStyle();
        } elseif (isset($_POST['team_booking_add_custom_field'])) {
            $this->addServiceCustomField();

            return;
        } elseif (isset($_POST['team_booking_remove_custom_field'])) {
            $this->removeServiceCustomField();

            return;
        } elseif (isset($_POST['team_booking_save_custom_field'])) {
            $this->saveServiceCustomField();

            return;
        } elseif (isset($_POST['team_booking_move_custom_field'])) {
            $this->moveServiceCustomField();

            return;
        } elseif (isset($_POST['team_booking_clone_service'])) {
            echo $this->cloneService();

            return;
        } elseif (isset($_POST['team_booking_clone_service_suggestion'])) {
            echo $this->cloneServiceSuggestion();

            return;
        } elseif (isset($_POST['team_booking_update_field_translations'])) {
            echo $this->updateFieldTranslations();

            return;
        } elseif (isset($_POST['tb_modify_reservation_save'])) {
            $reservation_database_id = $_POST['reservation_database_id'];
            $reservation = tbGetReservationById($reservation_database_id);

            if (!$_POST['reservation_payment_status']) {
                $reservation->setNotPaid();
            } else {
                $reservation->setPaid();
            }
            if ($reservation->getServiceClass() == 'service'
                && isset($_POST['reservation_unscheduled_status'])
            ) {
                if (!$_POST['reservation_unscheduled_status']) {
                    $reservation->setStatusConfirmed();
                } else {
                    $reservation->setStatusDone();
                }
            }
            tbUpdateReservationById($reservation_database_id, $reservation);
            $location = admin_url('admin.php?page=team-booking&tab=overview');
        }
        ////////////////////////////
        // Admin/Coworker actions //
        ////////////////////////////
        if (isset($_POST['team_booking_add_google_calendar'])) {
            $location = $this->addGoogleCalendar();
            echo $location;

            return;
        } elseif (isset($_POST['team_booking_update_booking_type'])) {
            $location = $this->saveServiceSettings();
        } elseif (isset($_POST['team_booking_remove_google_calendar'])) {
            $location = $this->removeGoogleCalendar();
            echo $location;

            return;
        } elseif (isset($_POST['team_booking_revoke_personal_token_confirm'])) {
            $location = $this->revokeCoworker();
            echo $location;

            return;
        } elseif (isset($_POST['team_booking_save_personal_event_settings'])) {
            $location = $this->saveCoworkerServiceSettings();
        } elseif (isset($_POST['get_csv_file_tb'])) {
            echo tbGenerateCSVFile($_POST['filter']);
            die();
        } elseif (isset($_POST['get_xlsx_file_tb'])) {
            echo tbGenerateXLSXFile($_POST['filter']);
            die();
        } elseif (isset($_POST['team_booking_toggle_service'])) {
            $this->toggleServiceActivation();

            return;
        } elseif (isset($_POST['reservation_search'])) {
            $filter = new TeamBooking_AdminFilterObject();
            if (!empty($_POST['filter'])) {
                $filter->decode($_POST['filter']);
            }
            $search_term = trim(tbFilterInput($_POST['reservation_search']));
            if (current_user_can('manage_options')) {
                // all reservations
                $reservations = tbGetReservations($filter->getPendingReservations());
            } else {
                // coworker's reservations
                $reservations = tbGetReservationsByCoworker(get_current_user_id(), $filter->getPendingReservations());
            }
            $return_ids = array();
            foreach ($reservations as $db_id => $reservation) {
                /* #var $reservation TeamBooking_ReservationData */
                $fields_array = array_map('strtolower', $reservation->getFieldsArray());
                if (in_array(strtolower($search_term), $fields_array)) {
                    $return_ids[] = $db_id;
                }
            }
            $filter->setIds($return_ids);
            $filter->setSearchTerm($search_term);
            $url = 'admin.php?page=team-booking&tab=overview&filter=' . $filter->encode();
            $location = admin_url($url);
        }

        ///////////////////////
        // Redirect and exit //
        ///////////////////////
        die(wp_redirect($location));
    }

    public function buildAdminTabcoworkers()
    {
        $content = new TeamBooking_Components_Admin_ManageCoworkers(static::$settings);
        echo $content->getPostBody();
    }

    public function buildAdminTabevents()
    {
        // Case: service details page
        if (isset($_GET['event'])) {
            // Checks if service_id exists, otherwise redirects to main list
            try {
                $service = self::$settings->getService($_GET['event']);
            } catch (Exception $e) {
                die(wp_redirect(admin_url('admin.php?page=team-booking&tab=events')));
            }
            // Template call
            $content = new TeamBooking_Components_Admin_Service(static::$settings, $service);
            // Set variables
            $content->action = self::$form_action;
            // Render page
            if (isset($_GET['email'])) {
                echo $content->getPostBodyEmail();
            } elseif (isset($_GET['gcal'])) {
                echo $content->getPostBodyGcalSettings();
            } elseif (isset($_GET['form'])) {
                if (!current_user_can('manage_options')) {
                    die(wp_redirect(admin_url('admin.php?page=team-booking&tab=events')));
                }
                echo $content->getPostBodyReservationForm();
            } else {
                if (!current_user_can('manage_options')) {
                    die(wp_redirect(admin_url('admin.php?page=team-booking&tab=events')));
                }
                echo $content->getPostBody();
            }
        } // Case: services list
        else {
            // Template call
            $content = new TeamBooking_Components_Admin_ManageServices(static::$settings);
            // Setting variables
            $content->action = static::$form_action;
            // Render content
            echo $content->getPostBody();
        }
    }

    public function buildAdminTabpersonal()
    {
        // Template call
        $content = new TeamBooking_Components_Admin_Personal(static::$settings);
        // Set template variables
        $content->action = static::$form_action;
        echo $content->getPostBody();
    }

    public function buildAdminTabaspect()
    {
        wp_enqueue_style('semantic-grid-style', TEAMBOOKING_URL . 'libs/semantic/components/grid.css');
        TeamBooking_Loader::load_fonts();
        wp_enqueue_style('teambooking_fonts');
        // Template call
        $content = new TeamBooking_Components_Admin_Style(static::$settings);
        // Set variables
        $content->action = static::$form_action;
        echo $content->getPostBody();
    }

    public function buildAdminTaboverview()
    {
        // Pagination & max logs filter
        $filter_object = new TeamBooking_AdminFilterObject();
        if (!empty($_GET['filter'])) {
            $filter_object->decode($_GET['filter']);
        } else {
            $filter_object->setPage(1);
            $filter_object->setMaxEntries(15);
        }

        // Retrieving reservations
        if ($filter_object->getPendingReservations()) {
            $reservations = tbGetReservations(TRUE);

        } else {
            $reservations = tbGetReservations();

            // Update the transient for new reservations notification
            $transient = get_transient('teambooking_seen_reservations');
            if (!$transient) {
                $transient = array();
            }
            if (!isset($transient[get_current_user_id()])) {
                $transient[get_current_user_id()] = array();
            }
            $reservation_keys = array_keys($reservations);
            $new_reservation_ids = array_diff($reservation_keys, $transient[get_current_user_id()]);
            $transient[get_current_user_id()] = $reservation_keys;
            set_transient('teambooking_seen_reservations', $transient);
        }

        if (empty($reservations)) {
            $reservations = array();
        }

        // Filtering parameters
        if (empty($_GET['service'])) {
            $filter_service = NULL;
            $list_title = __('Reservations', 'teambooking');
        } else {
            $filter_service = $_GET['service'];
            try {
                $list_title = sprintf(__('Reservations for %s', 'teambooking'), static::$settings->getService($filter_service)->getName());
            } catch (Exception $e) {
                $filter_service = NULL;
                $list_title = __('Reservations', 'teambooking');
            }
        }
        // Filtering logic
        if (!is_null($filter_service)) {
            foreach ($reservations as $id => $log) {
                /* @var $log TeamBooking_ReservationData */
                // This skips old logs before v.1.2 if present
                if (!$log instanceof TeamBooking_ReservationData) {
                    continue;
                }
                if ($log->getServiceId() != $filter_service) {
                    unset($reservations[$id]);
                }
                // If not admin, keep only logs relative to current coworker
                if (!current_user_can('manage_options')) {
                    $coworker_id = get_current_user_id();
                    if ($log->getCoworker() != $coworker_id) {
                        unset($reservations[$id]);
                    }
                }
            }
        }
        // Calling template
        $overview = new TeamBooking_Components_Admin_Overview(static::$settings);
        // Setting variables
        $overview->reservations = $reservations;
        if (!isset($new_reservation_ids)) {
            $new_reservation_ids = array();
        }
        $overview->new_reservation_ids = $new_reservation_ids;
        $overview->list_title = $list_title;
        $overview->filter_object = $filter_object;
        $overview->filter_service = $filter_service;
        // Render
        echo $overview->getPostBody();
    }

    public function buildAdminTabpayments()
    {
        // Template call
        $content = new TeamBooking_Components_Admin_Payments(static::$settings);
        // Set variables
        $content->action = static::$form_action;
        echo $content->getPostBody();
    }

    private function saveCoreSettings()
    {
        static::$settings->setApplicationClientId(trim(filter_input(INPUT_POST, 'client_id')));
        static::$settings->setApplicationClientSecret(trim(filter_input(INPUT_POST, 'client_secret')));
        static::$settings->setApplicationProjectName(filter_input(INPUT_POST, 'project_name'));
        static::$settings->setRegistrationUrl(filter_input(INPUT_POST, 'registration_url'));
        static::$settings->setDatabaseReservationTimeout(filter_input(INPUT_POST, 'database_reservation_timeout'));
        if (is_numeric(filter_input(INPUT_POST, 'max_google_api_results')) && filter_input(INPUT_POST, 'max_google_api_results') >= 600) {
            static::$settings->setMaxGoogleApiResults((int)filter_input(INPUT_POST, 'max_google_api_results', FILTER_SANITIZE_NUMBER_INT));
        } else {
            static::$settings->setMaxGoogleApiResults(600);
        }
        if (filter_input(INPUT_POST, 'autofill') == "yes") {
            static::$settings->setAutofillReservationFormOn();
        } elseif (filter_input(INPUT_POST, 'autofill') == "no") {
            static::$settings->setAutofillReservationFormOff();
        } else {
            static::$settings->setAutofillReservationFormOnAndHide();
        }
        if (filter_input(INPUT_POST, 'first_month_automatic') == "yes") {
            static::$settings->setFirstMonthWithFreeSlotIsShownOn();
        } else {
            static::$settings->setFirstMonthWithFreeSlotIsShownOff();
        }
        if (filter_input(INPUT_POST, 'drop_tables') == "yes") {
            static::$settings->setDropTablesOnUninstallYes();
        } else {
            static::$settings->setDropTablesOnUninstallNo();
        }
        if (filter_input(INPUT_POST, 'show_ical') == "yes") {
            static::$settings->setShowIcalYes();
        } else {
            static::$settings->setShowIcalNo();
        }
        $roles = $this->fetchRoles();
        // Roles
        global $wp_roles;
        // Clean capability assignment first...
        foreach ($roles as $name => $role) {
            $wp_roles->remove_cap($name, 'tb_can_sync_calendar');
        }
        //... and set new assignment after (if present)
        if (NULL !== filter_input(INPUT_POST, 'roles_allowed', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY)) {
            foreach (filter_input(INPUT_POST, 'roles_allowed', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY) as $name => $role) {
                $wp_roles->add_cap($name, 'tb_can_sync_calendar');
            }
        }
        if (filter_input(INPUT_POST, 'debug') == 'silent') {
            static::$settings->setSilentDebugOn();
        } else {
            static::$settings->setSilentDebugOff();
        }
        if (filter_input(INPUT_POST, 'skip_gmaps') == 'yes') {
            static::$settings->setSkipGmapLibsYes();
        } else {
            static::$settings->setSkipGmapLibsNo();
        }
        static::$settings->setMaxPendingTime(filter_input(INPUT_POST, 'max_pending_time'));
        static::$settings->save();
        // Prepare redirect URL
        if (!isset($location)) {
            $location = admin_url('admin.php?page=team-booking&tab=general&updated=1');
        }

        return $location;
    }

    private function addNewService()
    {
        $response = '';
        if (tbCheckServiceIdExistance($_POST['id']) ||
            tbCheckServiceNameExistance(trim($_POST['name']) ||
                empty($_POST['id']))
        ) {
            if (tbCheckServiceIdExistance($_POST['id']) || empty($_POST['id'])) {
                $response .= 'id_fail '; // end space mandatory
            }
            if (tbCheckServiceNameExistance($_POST['name'])) {
                $response .= 'name_fail';
            }
        } else {
            $service = new TeamBookingType();
            $service->setName($_POST['name']);
            $service->setClass($_POST['class']);
            $service->setId($_POST['id']);
            $service->save();
            $response = 'ok';
        }

        return trim($response);
    }

    private function importSettingsFromFile()
    {
        $settings_object = unserialize(file_get_contents($_FILES['settings_backup_file']['tmp_name']));
        if ($settings_object instanceof TeamBookingSettings) {
            $version_on_file = $settings_object->getVersion();
            if ($version_on_file <= TEAMBOOKING_VERSION) {
                // Replace settings
                update_option('team_booking', $settings_object);
                $location = admin_url('admin.php?page=team-booking&tab=general&settings_imported=1');
            } else {
                // Version mismatch
                $location = admin_url('admin.php?page=team-booking&tab=general&version_mismatch=1');
            }
        } else {
            // not the right file!
            $location = admin_url('admin.php?page=team-booking&tab=general&not_settings_file=1');
        }

        return $location;
    }

    private function importGoogleProjectFromJSON()
    {
        if (isset($_FILES['settings_json_file']['tmp_name'])) {
            $settings = json_decode(file_get_contents($_FILES['settings_json_file']['tmp_name']));
            if ($settings instanceof stdClass) {
                if ($settings->web->client_id && $settings->web->client_secret && $settings->web->redirect_uris && $settings->web->javascript_origins) {
                    // URIs check
                    $redirect_uris = $settings->web->redirect_uris;
                    $uris_found = FALSE;
                    $js_origins = $settings->web->javascript_origins;
                    foreach ($redirect_uris as $redirect_uri) {
                        if ($redirect_uri == admin_url() . 'admin-ajax.php?action=teambooking_oauth_callback') {
                            $uris_found = TRUE;
                            break;
                        }
                    }
                    if ($uris_found) {
                        $uris_found = FALSE;
                        foreach ($js_origins as $js_origin) {
                            if ($js_origin == strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, strpos($_SERVER["SERVER_PROTOCOL"], '/'))) . '://' . $_SERVER['HTTP_HOST']) {
                                $uris_found = TRUE;
                                break;
                            }
                        }
                    }
                    if (!$uris_found) {
                        return 'uri mismatch';
                    }
                    static::$settings->setApplicationClientId($settings->web->client_id);
                    static::$settings->setApplicationClientSecret($settings->web->client_secret);
                    static::$settings->save();
                    $response = admin_url('admin.php?page=team-booking&tab=general&settings_imported=1');
                } else {
                    // Invalid file
                    $response = 'invalid file';
                }
            } else {
                // Invalid file
                $response = 'invalid file';
            }

            return $response;
        } else {
            return 'no file';
        }
    }

    private function updateFieldTranslations()
    {
        $service = static::$settings->getService($_POST['service_id']);
        $service_fields = $service->getFormFields();
        $new_fields = new TeamBookingFormFields();
        $new_fields_builtin = $new_fields->getBuiltInFields();
        foreach ($new_fields_builtin as $hook => $field) {
            /* @var $field TeamBookingFormTextField */
            $service_fields->$hook->setLabel($field->getLabel());
        }
        $service->setFormFields($service_fields);
        $service->save();

        return;
    }

    private function saveServiceSettings()
    {
        $service = static::$settings->getService($_POST['service_id']);
        ////////////////////////////
        // General settings part  //
        ////////////////////////////
        if ($_POST['service_settings'] == 'general' && current_user_can('manage_options')) {
            $service->setName($_POST['event']['booking_type_name']);
            if (isset($_POST['event']['max_attendees'])) {
                if (round($_POST['event']['max_attendees']) > 200) {
                    $service->setAttendees(200);
                } else {
                    $service->setAttendees(round($_POST['event']['max_attendees']));
                }
            }
            if (isset($_POST['event']['max_tickets_per_reservation'])) {
                if ($_POST['event']['max_tickets_per_reservation'] > $service->getAttendees()) {
                    $service->setMaxReservationsPerUser($service->getAttendees());
                } else {
                    $service->setMaxReservationsPerUser(round($_POST['event']['max_tickets_per_reservation']));
                }
            }
            if (isset($_POST['event']['show_reservations_left'])) {
                if ($_POST['event']['show_reservations_left'] == TRUE) {
                    $service->setShowReservationsLeftOn();
                } else {
                    $service->setShowReservationsLeftOff();
                }
            }
            $service->setServiceColor($_POST['event']['service_color']);
            $service->setServiceInfo($_POST['event']['info']);
            if (isset($_POST['event']['direct_coworker'])) {
                $service->setCoworkerForDirectAssignment($_POST['event']['direct_coworker']);
            }
            $service->setPaymentMustBeDone($_POST['event']['payment_must_be_done']);
            if (isset($_POST['event']['assignment_rule'])) {
                $service->setAssignmentRule($_POST['event']['assignment_rule']);
            }
            if ($_POST['event']['allow_reservation'] == TRUE) {
                $service->setLoggedOnlyOn();
            } else {
                $service->setLoggedOnlyOff();
            }
            $show_times = $service->getShowTimes();
            $show_times->setValue($_POST['event']['show_times']);
            $service->setShowTimes($show_times);
            if ($_POST['event']['show_coworker'] == TRUE) {
                $service->setShowCoworkerOn();
            } else {
                $service->setShowCoworkerOff();
            }
            if ($_POST['event']['show_coworker_url'] == TRUE) {
                $service->setShowCoworkerUrlOn();
            } else {
                $service->setShowCoworkerUrlOff();
            }
            if ($_POST['event']['show_soldout'] == TRUE) {
                $service->setShowSoldoutOn();
            } else {
                $service->setShowSoldoutOff();
            }
            $service->setPrice(round($_POST['event']['service_price'], 2));
            if (isset($_POST['event']['duration_rule'])) {
                $service->setDurationRule($_POST['event']['duration_rule']);
            }
            if (isset($_POST['event']['default_duration_hours']) && isset($_POST['event']['default_duration_minutes'])) {
                $service->setDefaultDuration($_POST['event']['default_duration_hours'] * 3600 + $_POST['event']['default_duration_minutes'] * 60);
            }
            if ($_POST['event']['location_setting'] == 'none') {
                $service->setLocationSetting('none');
            } elseif ($_POST['event']['location_setting'] == 'inherited') {
                $service->setLocationSetting('inherited');
            } else {
                $service->setLocationSetting('fixed');
                if (isset($_POST['event']['location_address'])) {
                    $service->setLocationAddress($_POST['event']['location_address']);
                }
            }
            if (isset($_POST['event']['redirect'])) {
                $service->setRedirect(TRUE);
            } else {
                $service->setRedirect(FALSE);
            }
            if (isset($_POST['event']['redirect_url'])) {
                $service->setRedirectUrl(trim($_POST['event']['redirect_url']));
            }
            $service->setApproveRule($_POST['event']['approve_rule']);
            if ($_POST['event']['approve_until'] == 'yes') {
                $service->setFreeUntilApproval(TRUE);
            } else {
                $service->setFreeUntilApproval(FALSE);
            }
            if (isset($_POST['event']['approve_notification'])) {
                $service->setSendApprovalNotification(TRUE);
            } else {
                $service->setSendApprovalNotification(FALSE);
            }
            $service->setApprovalNotificationSubject(tbFilterInput($_POST['event']['approve_notification_subject']));
            $service->setApprovalNotificationText(tbFilterInput($_POST['event']['approve_notification_text']));
            if ($_POST['event']['allow_customer_cancellation'] == TRUE) {
                $service->setAllowCustomerCancellation(TRUE);
            } else {
                $service->setAllowCustomerCancellation(FALSE);
            }
            $service->setAllowCustomerCancellationUntil($_POST['event']['allow_customer_cancellation_until_value'] * $_POST['event']['allow_customer_cancellation_until_unit']);

            $location = admin_url('admin.php?page=team-booking&tab=events&event=' . $_POST['service_id'] . '&event_updated=1');
        }
        ////////////////////////////
        // Email settings part    //
        ////////////////////////////
        if ($_POST['service_settings'] == 'email') {
            if (current_user_can('manage_options')) {
                $service->setEmailForNotifications($_POST['event']['email_for_notifications']);
                isset($_POST['event']['email_to_email']) ? $service->setNotifyOn() : $service->setNotifyOff();
                isset($_POST['event']['email_to_customer']) ? $service->setConfirmOn() : $service->setConfirmOff();
                $frontend_email = $service->getFrontendEmail();
                $frontend_email->setSubject($_POST['event']['front_end_email']['subject']);
                $frontend_email->setBody($_POST['event']['front_end_email']['body']);
                $service->setFrontendEmail($frontend_email);
                $backend_email = $service->getBackendEmail();
                $backend_email->setSubject($_POST['event']['back_end_email']['subject']);
                $backend_email->setBody($_POST['event']['back_end_email']['body']);
                $service->setBackendEmail($backend_email);
                if ($_POST['event']['include_files_as_attachment'] == TRUE) {
                    $service->setIncludeFilesAsAttachmentOn();
                } else {
                    $service->setIncludeFilesAsAttachmentOff();
                }
                if ($_POST['event']['send_cancellation_email'] == TRUE) {
                    $service->setCancellationEmailOn();
                } else {
                    $service->setCancellationEmailOff();
                }
                $cancellation_email = $service->getCancellationEmail();
                $cancellation_email->setSubject($_POST['event']['cancellation_email']['subject']);
                $cancellation_email->setBody($_POST['event']['cancellation_email']['body']);
                $service->setCancellationEmail($cancellation_email);
            }
            $this->saveCoworkerServiceSettings(TRUE);
            $location = admin_url('admin.php?page=team-booking&tab=events&email=1&event=' . $_POST['service_id'] . '&event_updated=1');
        }
        ////////////////////////////
        // Reservation form part  //
        ////////////////////////////
        if ($_POST['service_settings'] == 'form' && current_user_can('manage_options')) {
            $form_fields = $service->getFormFields();
            // TODO: create a mapper for this
            isset($_POST['form_fields']['first_name']) ? $form_fields->first_name->setOn() : $form_fields->first_name->setOff();
            isset($_POST['form_fields']['first_name_required']) ? $form_fields->first_name->setRequiredOn() : $form_fields->first_name->setRequiredOff();
            if (isset($_POST['form_fields']['first_name_regex'])) {
                $form_fields->first_name->setValidationRegex(tbFilterInput($_POST['form_fields']['first_name_regex']));
            }
            if (isset($_POST['form_fields']['first_name_validation'])) {
                $form_fields->first_name->setValidationRule($_POST['form_fields']['first_name_validation']);
            }
            isset($_POST['form_fields']['second_name']) ? $form_fields->second_name->setOn() : $form_fields->second_name->setOff();
            isset($_POST['form_fields']['second_name_required']) ? $form_fields->second_name->setRequiredOn() : $form_fields->second_name->setRequiredOff();
            if (isset($_POST['form_fields']['second_name_regex'])) {
                $form_fields->second_name->setValidationRegex(tbFilterInput($_POST['form_fields']['second_name_regex']));
            }
            if (isset($_POST['form_fields']['second_name_validation'])) {
                $form_fields->second_name->setValidationRule($_POST['form_fields']['second_name_validation']);
            }
            isset($_POST['form_fields']['email']) ? $form_fields->email->setOn() : $form_fields->email->setOff();
            isset($_POST['form_fields']['email_required']) ? $form_fields->email->setRequiredOn() : $form_fields->email->setRequiredOff();
            if (isset($_POST['form_fields']['email_regex'])) {
                $form_fields->email->setValidationRegex(tbFilterInput($_POST['form_fields']['email_regex']));
            }
            if (isset($_POST['form_fields']['email_validation'])) {
                $form_fields->email->setValidationRule($_POST['form_fields']['email_validation']);
            }
            isset($_POST['form_fields']['address']) ? $form_fields->address->setOn() : $form_fields->address->setOff();
            isset($_POST['form_fields']['address_required']) ? $form_fields->address->setRequiredOn() : $form_fields->address->setRequiredOff();
            if (isset($_POST['form_fields']['address_regex'])) {
                $form_fields->address->setValidationRegex(tbFilterInput($_POST['form_fields']['address_regex']));
            }
            if (isset($_POST['form_fields']['address_validation'])) {
                $form_fields->address->setValidationRule($_POST['form_fields']['address_validation']);
            }
            isset($_POST['form_fields']['phone']) ? $form_fields->phone->setOn() : $form_fields->phone->setOff();
            isset($_POST['form_fields']['phone_required']) ? $form_fields->phone->setRequiredOn() : $form_fields->phone->setRequiredOff();
            if (isset($_POST['form_fields']['phone_regex'])) {
                $form_fields->phone->setValidationRegex(tbFilterInput($_POST['form_fields']['phone_regex']));
            }
            if (isset($_POST['form_fields']['phone_validation'])) {
                $form_fields->phone->setValidationRule($_POST['form_fields']['phone_validation']);
            }
            $service->setFormFields($form_fields);
            $location = admin_url('admin.php?page=team-booking&tab=events&form=1&event=' . $_POST['service_id'] . '&event_updated=1');
        }
        $service->save();

        return $location;
    }

    private function savePaymentsSettings()
    {
        if (!empty($_POST['currency_code'])) {
            static::$settings->setCurrencyCode($_POST['currency_code']);
        }
        foreach ($_POST['gateway_settings'] as $gateway_id => $settings) {
            $object = static::$settings->getPaymentGatewaySettingObject($gateway_id);
            if ($object instanceof TeamBooking_PaymentGateways_Settings) {
                $object->saveBackendSettings($settings, $_POST['currency_code']);
            }
        }
        static::$settings->save();
        $location = admin_url('admin.php?page=team-booking&tab=payments&updated=1');

        return $location;
    }

    private function deleteService()
    {
        static::$settings->dropService($_POST['booking_id']);
        $coworkers = tbGetCoworkersIdList();
        foreach ($coworkers as $coworker) {
            static::$settings->getCoworkerData($coworker)->dropCustomServiceSettings($_POST['booking_id']);
        }
        static::$settings->save();
        $location = admin_url('admin.php?page=team-booking&tab=events&event_deleted=1');

        return $location;
    }

    private function cleanErrorLogs()
    {
        $error_logs = array();
        static::$settings->setErrorLogs($error_logs);
        static::$settings->save();
        $location = admin_url('admin.php?page=team-booking&tab=overview&error_logs_cleaned=1');

        return $location;
    }

    private function deleteServiceReservation()
    {
        $filter = new TeamBooking_AdminFilterObject();
        $filter->decode($_POST['filter']);
        if (isset($_POST['is_pending'])) {
            tbDeleteReservationById($_POST['reservation_id'], TRUE);
            $filter->setPendingReservations(TRUE);
        } else {
            // TODO: cancel
            tbDeleteReservationById($_POST['reservation_id']);
        }
        $url = 'admin.php?page=team-booking&tab=overview&log_deleted=1&filter=' . $filter->encode();
        $location = admin_url($url);

        return $location;
    }

    private function approveServiceReservation()
    {
        /**
         * Retrieving the reservation record in database
         */
        $reservation_db_record = tbGetReservationById($_POST['reservation_id']);
        /**
         * Instantiating the reservation service class
         */
        $reservation = new TeamBooking_Reservation($reservation_db_record);
        /**
         * Do the reservation
         */
        $updated_record = $reservation->doReservation();
        if ($updated_record instanceof TeamBooking_ReservationData) {
            /**
             * Everything went fine, let's update the database record
             */
            $updated_record->setStatusConfirmed();
            tbUpdateReservationById($_POST['reservation_id'], $updated_record);
            $location = admin_url('admin.php?page=team-booking&tab=overview&reservation_approved=1');
        } elseif ($updated_record instanceof TeamBooking_Error) {
            /**
             * Something goes wrong
             */
            $message = $updated_record->getMessage();
            $location = 'ERROR: ' . $message;
        } else {
            /**
             * Not an Appointment, nor Event
             */
            $location = admin_url('admin.php?page=team-booking&tab=overview&not_approvable=1');
        }

        return $location;
    }

    private function confirmPendingPaymentReservation()
    {
        /**
         * Retrieving the reservation record in pending payment database
         */
        $reservation_db_record = tbGetReservationById($_POST['reservation_id'], TRUE);
        /**
         * Instantiating the reservation service class
         */
        $reservation = new TeamBooking_Reservation($reservation_db_record);
        /**
         * Do the reservation
         */
        $updated_record = $reservation->doReservation();
        if ($updated_record instanceof TeamBooking_ReservationData) {
            /**
             * Everything went fine, let's update the database record
             */
            $updated_record->setStatusConfirmed();
            if ($_POST['set_as_paid'] == 'true') {
                $updated_record->setPaid();
            } else {
                $updated_record->setNotPaid();
            }
            // remove from pending table
            tbDeleteReservationById($_POST['reservation_id'], TRUE);
            // insert in normal table
            tbInsertReservation($updated_record);
            $location = admin_url('admin.php?page=team-booking&tab=overview&reservation_confirmed=1');
        } elseif ($updated_record instanceof TeamBooking_Error) {
            /**
             * Something goes wrong
             */
            $message = $updated_record->getMessage();
            $location = 'ERROR: ' . $message;
        } else {
            /**
             * Not an Appointment, nor Event
             */
            $location = admin_url('admin.php?page=team-booking&tab=overview&not_approvable=1');
        }

        return $location;
    }

    private function cancelServiceReservation()
    {
        /**
         * Retrieving the reservation record in database
         */
        $reservation_db_record = tbGetReservationById($_POST['reservation_id']);
        /**
         * Instantiating the reservation service class
         */
        $reservation = new TeamBooking_Reservation($reservation_db_record);
        /**
         * Calling the cancel method
         */
        $updated_record = $reservation->cancelReservation($_POST['reservation_id']);
        if ($updated_record instanceof TeamBooking_ReservationData) {
            /**
             * Everything went fine, let's update the database record
             */
            tbUpdateReservationById($_POST['reservation_id'], $updated_record);
            $location = admin_url('admin.php?page=team-booking&tab=overview&filter=' . $_POST['filter'] . '&reservation_cancelled=1');
        } elseif ($updated_record instanceof TeamBooking_Error) {
            /**
             * Something goes wrong
             */
            if ($updated_record->getCode() == 7) {
                /*
                 * The reservation is already cancelled, let's update the database record
                 */
                $reservation_db_record->setStatusCancelled();
                tbUpdateReservationById($_POST['reservation_id'], $reservation_db_record);
            }
            $message = urlencode($updated_record->getMessage());
            $location = admin_url('admin.php?page=team-booking&tab=overview&filter=' . $_POST['filter'] . '&generic_error=' . $message);
        } else {
            /**
             * Not an Appointment, nor Event
             */
            $location = admin_url('admin.php?page=team-booking&tab=overview&filter=' . $_POST['filter'] . '&not_revokable=1');
        }

        return $location;
    }

    private function deleteSelectedServices()
    {
        $services = json_decode(str_replace("\\", "", $_POST['services']));
        $coworkers = tbGetCoworkersIdList();
        foreach ($services as $service_id) {
            static::$settings->dropService($service_id);
            foreach ($coworkers as $coworker) {
                static::$settings->getCoworkerData($coworker)->dropCustomServiceSettings($service_id);
            }
        }
        static::$settings->save();
        $location = admin_url('admin.php?page=team-booking&tab=events&selected_services_deleted=1');

        return $location;

    }

    private function deleteSelectedReservations()
    {
        $reservations = json_decode($_POST['reservations']);
        if (isset($_POST['is_pending'])) {
            $pending = TRUE;
            $filter = new TeamBooking_AdminFilterObject();
            $filter->setPendingReservations(TRUE);
            $url = 'admin.php?page=team-booking&tab=overview&deleted_selected=1&filter=' . $filter->encode();
        } else {
            $pending = FALSE;
            $url = 'admin.php?page=team-booking&tab=overview&deleted_selected=1';
        }
        foreach ($reservations as $reservation) {
            tbDeleteReservationById($reservation, $pending);
        }
        $location = admin_url($url);

        return $location;
    }

    private function deleteAllReservations()
    {
        if (isset($_POST['is_pending'])) {
            tbCleanReservations(TRUE, FALSE, FALSE, TRUE);
            $filter = new TeamBooking_AdminFilterObject();
            $filter->setPendingReservations(TRUE);
            $url = 'admin.php?page=team-booking&tab=overview&deleted_all=1&filter=' . $filter->encode();
        } else {
            tbCleanReservations(TRUE);
            $url = 'admin.php?page=team-booking&tab=overview&deleted_all=1';
        }
        $location = admin_url($url);

        return $location;
    }

    private function addGoogleCalendar()
    {
        $coworker = static::$settings->getCoworkerData(get_current_user_id());
        /**
         * Let's test if the Google Calendar ID belongs to
         * the authorized Google Account
         *
         * Even if the IDs are already picked in the right Google Account,
         * this second step provides a better security level.
         */
        $calendar = new TeamBooking_Calendar();
        $test = $calendar->testCalendarID(get_current_user_id(), trim($_POST['calendar_id']));
        if ($test === FALSE) {
            return 404;
        } else {
            $coworker->addCalendarId(trim($_POST['calendar_id']));
            static::$settings->save();
            $location = admin_url('admin.php?page=team-booking&tab=personal&custom_settings_updated=1');

            return $location;
        }
    }

    private function removeGoogleCalendar()
    {
        $coworker = static::$settings->getCoworkerData(get_current_user_id());
        $coworker->dropCalendarId(trim($_POST['calendar_id_key']));
        static::$settings->save();
        $location = admin_url('admin.php?page=team-booking&tab=personal&custom_settings_updated=1');

        return $location;
    }

    private function removeCoworkerResidualData()
    {
        static::$settings->dropCoworkerData($_POST['coworker_id']);
        static::$settings->save();
        $location = admin_url('admin.php?page=team-booking&tab=coworkers&updated=1');

        return $location;
    }

    private function revokeCoworker($coworker_id = FALSE)
    {
        if (!$coworker_id) {
            $coworker = static::$settings->getCoworkerData(get_current_user_id());
            $location_root = 'admin.php?page=team-booking&tab=personal';
        } else {
            $coworker = static::$settings->getCoworkerData($coworker_id);
            $location_root = 'admin.php?page=team-booking&tab=coworkers';
        }
        $location = admin_url($location_root . '&reset=1');
        // Prepare for programmatically revoke token
        if (isset(json_decode($coworker->getAccessToken())->refresh_token)) {
            $token = json_decode($coworker->getAccessToken())->refresh_token;
            // Revoke token
            $calendar_class = new TeamBooking_Calendar();
            $revoke_attempt = $calendar_class->revokeToken($token);
            if ($revoke_attempt !== TRUE) {
                $location = admin_url($location_root . '&reset_failed=' . urlencode($revoke_attempt));

                return $location;
            }
        } else {
            $location = admin_url($location_root . '&partialreset=1');
        }
        // Erase Access Token
        $coworker->setAccessToken('');
        // Remove Google Calendar ID(s)
        $coworker->dropAllCalendarIds();
        static::$settings->save();

        return $location;
    }

    private function saveCoworkerServiceSettings($email = FALSE)
    {
        $coworker_data = static::$settings->getCoworkerData(get_current_user_id());
        $settings = $coworker_data->getCustomEventSettings($_POST['service_id']);
        /////////////////
        // Email part  //
        /////////////////
        if ($email) {
            if ($_POST['data']['get_details_by_email'] == 'true') {
                $settings->setGetDetailsByEmailOn();
            } else {
                $settings->setGetDetailsByEmailOff();
            }
            if ($_POST['data']['include_uploaded_files_as_attachment'] == 'true') {
                $settings->setIncludeFilesAsAttachmentOn();
            } else {
                $settings->setIncludeFilesAsAttachmentOff();
            }
            $settings->setNotificationEmailBody($_POST['data']['email']['email_text']['body']);
            $settings->setNotificationEmailSubject($_POST['data']['email']['email_text']['subject']);
            $location = admin_url('admin.php?page=team-booking&tab=events&email=1&event=' . $_POST['service_id'] . '&event_updated=1');
        }
        ///////////////////////////
        // Google Calendar part  //
        ///////////////////////////
        if (!$email) {
            $services_id_list = static::$settings->getServiceIdList();
            $other_linked_event_titles = array();
            foreach ($services_id_list as $id) {
                if ($id != $_POST['service_id']) {
                    $other_linked_event_titles[] = strtolower($coworker_data->getCustomEventSettings($id)->getLinkedEventTitle());
                }
            }
            $linked_event_title_lowercase = strtolower($_POST['data']['linked_event_title']);
            // Checks for duplicate linked event title
            if (in_array($linked_event_title_lowercase, $other_linked_event_titles) && !static::$settings->getService($_POST['service_id'])->isClass('service')) {
                $location = admin_url('admin.php?page=team-booking&tab=events&gcal=1&event=' . $_POST['service_id'] . '&duplicated_linked_title=1');
            } else {
                if (isset($_POST['data']['booked_title'])) {
                    $settings->setAfterBookedTitle($_POST['data']['booked_title']);
                }
                $settings->setBookedEventColor($_POST['data']['booked_color']);
                if (isset($_POST['data']['duration_rule'])) {
                    $settings->setDurationRule($_POST['data']['duration_rule']);
                }
                if (isset($_POST['data']['default_duration_hours']) && isset($_POST['data']['default_duration_minutes'])) {
                    $settings->setFixedDuration($_POST['data']['default_duration_hours'] * 3600 + $_POST['data']['default_duration_minutes'] * 60);
                }
                if (isset($_POST['data']['buffer_hours']) && isset($_POST['data']['buffer_minutes'])) {
                    $settings->setBufferDuration($_POST['data']['buffer_hours'] * 3600 + $_POST['data']['buffer_minutes'] * 60);
                }
                $settings->setLinkedEventTitle($_POST['data']['linked_event_title']);
                $settings->setMinTime($_POST['data']['min_time']);
                if ($_POST['data']['min_time_reference'] == 'end') {
                    $settings->setMinTimeReferenceEnd();
                } else {
                    $settings->setMinTimeReferenceStart();
                }
                $settings->setReminder($_POST['data']['reminder']);
            }
            $location = admin_url('admin.php?page=team-booking&tab=events&gcal=1&event=' . $_POST['service_id'] . '&event_updated=1');
        }
        $coworker_data->setCustomEventSettings($settings, $_POST['service_id']);
        /**
         * We're using the update method, even if it's an
         * object, because if the settings are created just now,
         * then won't be saved (can't be used by reference)
         */
        static::$settings->updateCoworkerData($coworker_data, get_current_user_id());
        static::$settings->save();

        return $location;
    }

    private function saveFrontendStyle()
    {
        if (empty($_POST['tb-background-color'])) {
            $_POST['tb-background-color'] = "#FFFFFF";
        }
        if (empty($_POST['tb-weekline-color'])) {
            $_POST['tb-weekline-color'] = "#FFFFFF";
        }
        if (empty($_POST['tb-freeslot-color'])) {
            $_POST['tb-freeslot-color'] = "#FFFFFF";
        }
        if (empty($_POST['tb-soldoutslot-color'])) {
            $_POST['tb-freeslot-color'] = "#FFFFFF";
        }
        if ($_POST['tb-group-slots-by'] == 'bytime') {
            static::$settings->setGroupSlotsByTime();
        }
        if ($_POST['tb-group-slots-by'] == 'bycoworker') {
            static::$settings->setGroupSlotsByCoworker();
        }
        if ($_POST['tb-group-slots-by'] == 'byservice') {
            static::$settings->setGroupSlotsByService();
        }
        static::$settings->setColorBackground($_POST['tb-background-color']);
        static::$settings->setColorWeekLine($_POST['tb-weekline-color']);
        if (isset($_POST['tb-weekline-pattern'])) {
            static::$settings->setPatternWeekline($_POST['tb-weekline-pattern']);
        }
        if (isset($_POST['tb-calendar-pattern'])) {
            static::$settings->setPatternCalendar($_POST['tb-calendar-pattern']);
        }
        if (isset($_POST['tb-border-size'])) {
            static::$settings->setBorderSize($_POST['tb-border-size']);
        }
        if (isset($_POST['tb-border-radius'])) {
            static::$settings->setBorderRadius($_POST['tb-border-radius']);
        }
        if (isset($_POST['tb-border-color'])) {
            static::$settings->setBorderColor($_POST['tb-border-color']);
        }
        if (isset($_POST['tb-css-fix'])) {
            static::$settings->setFix62dot5On();
        } else {
            static::$settings->setFix62dot5Off();
        }
        static::$settings->setMapStyle($_POST['tb-map-style']);
        static::$settings->setColorFreeSlot($_POST['tb-freeslot-color']);
        static::$settings->setColorSoldoutSlot($_POST['tb-soldoutslot-color']);
        static::$settings->setPriceTagColor($_POST['tb-price-tag-color']);
        static::$settings->setNumberedDotsLogic($_POST['tb-numbered-dots-logic']);
        static::$settings->setNumberedDotsLowerBound($_POST['tb-numbered-dots-lower-bound']);
        static::$settings->save();
        $location = admin_url('admin.php?page=team-booking&tab=aspect&updated=1');

        return $location;
    }

    private function addServiceCustomField()
    {
        $service = static::$settings->getService($_POST['service_id']);
        $service_fields = $service->getFormFields();
        switch ($_POST['field']) {
            case 'text':
                $field = new TeamBookingFormTextField();
                $field->setLabel(__('A new custom text field', 'teambooking'));
                $hook = $_POST['service_id'] . '_custom_text';
                $i = 0;
                while ($service_fields->isHookDuplicate($hook)) {
                    $i++;
                    $hook .= "_" . $i;
                }
                $field->setHook($hook);
                break;
            case 'textarea':
                $field = new TeamBookingFormTextarea();
                $field->setLabel(__('A new custom textarea', 'teambooking'));
                $hook = $_POST['service_id'] . '_custom_textarea';
                $i = 0;
                while ($service_fields->isHookDuplicate($hook)) {
                    $i++;
                    $hook .= "_" . $i;
                }
                $field->setHook($hook);
                break;
            case 'select':
                $field = new TeamBookingFormSelect();
                $field->setLabel(__('A new custom select', 'teambooking'));
                $hook = $_POST['service_id'] . '_custom_select';
                $i = 0;
                while ($service_fields->isHookDuplicate($hook)) {
                    $i++;
                    $hook .= "_" . $i;
                }
                $field->setHook($hook);
                $field->setValue(__('Option One', 'teambooking'));
                $field->addOption(__('Option Two', 'teambooking'));
                $field->addOption(__('Option Three', 'teambooking'));
                break;
            case 'checkbox':
                $field = new TeamBookingFormCheckbox();
                $field->setLabel(__('A new custom checkbox', 'teambooking'));
                $hook = $_POST['service_id'] . '_custom_checkbox';
                $i = 0;
                while ($service_fields->isHookDuplicate($hook)) {
                    $i++;
                    $hook .= "_" . $i;
                }
                $field->setHook($hook);
                $field->setCheckedOff();
                $field->setValue($field->getLabel());
                break;
            case 'radio':
                $field = new TeamBookingFormRadio();
                $field->setLabel(__('A new custom radio group', 'teambooking'));
                $hook = $_POST['service_id'] . '_custom_radio';
                $i = 0;
                while ($service_fields->isHookDuplicate($hook)) {
                    $i++;
                    $hook .= "_" . $i;
                }
                $field->setHook($hook);
                $field->setValue(__('Option One', 'teambooking'));
                $field->addOption(__('Option Two', 'teambooking'));
                $field->addOption(__('Option Three', 'teambooking'));
                break;
            case 'file':
                $field = new TeamBookingFormFileUpload();
                $field->setLabel(__('A new file upload field', 'teambooking'));
                $hook = $_POST['service_id'] . '_custom_file';
                $i = 0;
                while ($service_fields->isHookDuplicate($hook)) {
                    $i++;
                    $hook .= "_" . $i;
                }
                $field->setHook($hook);
                break;
            default :
                return;
        }
        $field->setOn();
        $field->setRequiredOff();
        // Extreme check to avoid corruptions
        if (!is_null($field)) {
            $service_fields->addCustomField($field);
            $service->setFormFields($service_fields);
            static::$settings->updateService($service, $service->getId());
            static::$settings->save();
            echo '<li>';
            echo tbAdminFormCustomFieldsMapper($field, $service->getId());
            echo '</li>';
        }
    }

    private function removeServiceCustomField()
    {
        $service = static::$settings->getService($_POST['service_id']);
        $fields = $service->getFormFields();
        $fields->dropCustomField($_POST['hook']);
        $service->setFormFields($fields);
        static::$settings->updateService($service, $service->getId());
        static::$settings->save();
    }

    private function saveServiceCustomField()
    {
        $service = static::$settings->getService($_POST['service_id']);
        $fields = $service->getFormFields();
        $custom_fields = $fields->getCustomFields();
        foreach ($custom_fields as $custom_field) {
            if ($custom_field->getHook() == $_POST['hook']) {
                $field = $custom_field;
            }
        }
        // Parse data
        foreach ($_POST['inputs'] as $element) {
            $key = tbStripAllBrackets($element['name']);
            $parsed_data[substr($key, 22)] = $element['value'];
        }
        // Checking Hook availability, if new
        if ($_POST['hook'] != $parsed_data[$_POST['hook'] . "_hook"]) {
            if ($fields->isHookDuplicate($parsed_data[$_POST['hook'] . "_hook"])) {
                echo "duplicate_hook";

                return;
            }
        }
        // Sets data
        /* @var $field TeamBookingFormTextField */
        $field->setHook($parsed_data[$_POST['hook'] . "_hook"]);
        $field->setLabel($parsed_data[$_POST['hook'] . "_label"]);
        if (isset($parsed_data[$_POST['hook'] . "_show"])) {
            $field->setOn();
        } else {
            $field->setOff();
        }
        if (isset($parsed_data[$_POST['hook'] . "_extensions"])) {
            $field->setFileExtensions($parsed_data[$_POST['hook'] . "_extensions"]);
        }
        if (isset($parsed_data[$_POST['hook'] . "_required"])) {
            $field->setRequiredOn();
        } else {
            $field->setRequiredOff();
        }
        if (get_class($field) == 'TeamBookingFormCheckbox') {
            if (isset($parsed_data[$_POST['hook'] . "_selected"])) {
                $field->setCheckedOn();
            } else {
                $field->setCheckedOff();
            }
        }
        if (isset($parsed_data[$_POST['hook'] . "_default"])) {
            $field->setValue($parsed_data[$_POST['hook'] . "_default"]);
        }
        if (isset($parsed_data[$_POST['hook'] . "_options"])) {
            $options = explode(",", $parsed_data[$_POST['hook'] . "_options"]);
            $field->resetOptions();
            foreach ($options as $option) {
                $field->addOption(trim($option));
            }
        }
        if (isset($parsed_data[$_POST['hook'] . "_regex"])) {
            $field->setValidationRegex($parsed_data[$_POST['hook'] . "_regex"]);
        }
        if (isset($parsed_data[$_POST['hook'] . "_validation"])) {
            $field->setValidationRule($parsed_data[$_POST['hook'] . "_validation"]);
        }
        // Saving
        $fields->updateCustomField($_POST['hook'], $field);
        $service->setFormFields($fields);
        static::$settings->updateService($service, $service->getId());
        static::$settings->save();
    }

    private function moveServiceCustomField()
    {
        $service = static::$settings->getService($_POST['service_id']);
        $custom_fields = $service->getFormFields()->getCustomFields();
        foreach ($custom_fields as $key => $field) {
            if ($field->getHook() == $_POST['hook']) {
                $moved_field = $field;
                $service->getFormFields()->dropCustomField($_POST['hook']);
                break;
            }
        }
        $custom_fields = $service->getFormFields()->getCustomFields();
        array_splice($custom_fields, $_POST['where'], 0, array($moved_field));
        $service->getFormFields()->saveCustomFields($custom_fields);
        static::$settings->save();
    }

    private function toggleServiceActivation()
    {
        if (isset($_POST['personal'])) {
            $coworker_data = static::$settings->getCoworkerData(get_current_user_id());
            $custom_settings = $coworker_data->getCustomEventSettings($_POST['service_id']);
            if ($_POST['service_action'] == 'activate') {
                $custom_settings->setParticipateOn();
            } else {
                $custom_settings->setParticipateOff();
            }
            $coworker_data->setCustomEventSettings($custom_settings, $_POST['service_id']);
            static::$settings->save();
        } else {
            $service = static::$settings->getService($_POST['service_id']);
            if ($_POST['service_action'] == 'activate') {
                $service->setActiveOn();
            } else {
                $service->setActiveOff();
            }
            static::$settings->save();
        }
    }

    private function cloneService()
    {
        $new_service_id = tbFilterInput($_POST['new_service_id'], TRUE);
        if (tbCheckServiceIdExistance($new_service_id) || empty($new_service_id)) {
            return 'id already used';
        } else {
            $service_id = $_POST['service_id'];
            // Can't use clone() as it will produce shallow copies...
            $service = unserialize(serialize(static::$settings->getService($service_id)));
            $service->setId($new_service_id);
            $i = 1;
            $service_name = $service->getName();
            while (tbCheckServiceNameExistance($service_name)) {
                $i++;
                $service_name = $service->getName() . "-" . $i;
            }
            $service->setName($service_name);
            $service->save();

            return $service_id;
        }
    }

    private function cloneServiceSuggestion()
    {
        $service_id = $_POST['service_id'];
        $service = static::$settings->getService($service_id);
        $i = 1;
        while (tbCheckServiceIdExistance($service_id)) {
            $i++;
            $service_id = $service->getId() . "-" . $i;
        }

        return $service_id;
    }

    public function addTinyMCEButton()
    {
        global $typenow;
        // check user permissions
        if (!current_user_can('edit_posts') && !current_user_can('edit_pages')) {
            return;
        }
        // verify the post type
        if (!in_array($typenow, array(
            'post',
            'page',
        ))
        )
            return;
        // check if WYSIWYG is enabled
        if (get_user_option('rich_editing') == 'true') {
            add_filter('mce_external_plugins', array(
                $this,
                'addTinyMCEPlugin',
            ));
            add_filter('mce_buttons', array(
                $this,
                'registerTinyMCEButton',
            ));
        }
    }

    public function addTinyMCEPlugin($plugin_array)
    {
        $plugin_array['teambooking_tinymce_button'] = TEAMBOOKING_URL . 'js/tinymce_button.js';

        return $plugin_array;
    }

    public function registerTinyMCEButton($buttons)
    {
        array_push($buttons, 'teambooking_tinymce_button');

        return $buttons;
    }

}
