<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

/*
 * This class is more handy than the setting's form classes 
 * to just transport values for reservation pourposes
 * 
 */

class TeamBooking_ReservationFormField
{

    //------------------------------------------------------------

    private $value;
    private $name;

    //------------------------------------------------------------

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = $value;
    }

    //------------------------------------------------------------

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

}
