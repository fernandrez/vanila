<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_RenderParameters
{

    //------------------------------------------------------------

    private $month;
    private $year;
    private $day;
    private $service_ids;
    private $requested_service_ids;
    private $coworker_ids;
    private $is_widget;
    private $is_ajax_call;
    private $no_filter;
    private $instance;
    private $slots; // Array
    private $slots_obj; // Object

    //------------------------------------------------------------

    public function getMonth()
    {
        return $this->month;
    }

    public function setMonth($month)
    {
        // 'm'
        $this->month = $month;
    }

    //------------------------------------------------------------

    public function getYear()
    {
        return $this->year;
    }

    public function setYear($year)
    {
        // 'Y'
        $this->year = $year;
    }

    //------------------------------------------------------------

    public function getDay()
    {
        return $this->day;
    }

    public function setDay($day)
    {
        // 'd'
        $this->day = $day;
    }

    //------------------------------------------------------------

    public function getServiceIds()
    {
        return $this->service_ids;
    }

    public function setServiceIds(array $service_ids)
    {
        $this->service_ids = $service_ids;
    }

    //------------------------------------------------------------

    public function getRequestedServiceIds()
    {
        return $this->requested_service_ids;
    }

    public function setRequestedServiceIds(array $service_ids)
    {
        $this->requested_service_ids = $service_ids;
    }

    //------------------------------------------------------------

    public function getCoworkerIds()
    {
        return $this->coworker_ids;
    }

    public function setCoworkerIds(array $coworker_ids)
    {
        $this->coworker_ids = $coworker_ids;
    }

    //------------------------------------------------------------

    public function setIsWidgetYes()
    {
        $this->is_widget = TRUE;
    }

    public function setIsWidgetNo()
    {
        $this->is_widget = FALSE;
    }

    public function getIsWidget()
    {
        return $this->is_widget;
    }

    //------------------------------------------------------------

    public function setIsAjaxCallYes()
    {
        $this->is_ajax_call = TRUE;
    }

    public function setIsAjaxCallNo()
    {
        $this->is_ajax_call = FALSE;
    }

    public function getIsAjaxCall()
    {
        return $this->is_ajax_call;
    }

    //------------------------------------------------------------

    public function setNoFilterYes()
    {
        $this->no_filter = TRUE;
    }

    public function setNoFilterNo()
    {
        $this->no_filter = FALSE;
    }

    public function getNoFilter()
    {
        return $this->no_filter;
    }

    //------------------------------------------------------------

    public function getInstance()
    {
        return $this->instance;
    }

    public function setInstance($instance)
    {
        $this->instance = $instance;
    }

    //------------------------------------------------------------

    public function getSlots()
    {
        if (!empty($this->slots)) {
            return $this->slots;
        } else {
            return array();
        }
    }

    public function setSlots($slots)
    {
        $this->slots = $slots;
    }

    //------------------------------------------------------------

    public function clearSlotsObj()
    {
        $this->slots_obj = FALSE;
    }

    public function getSlotsObj()
    {
        if ($this->slots_obj instanceof TeamBooking_SlotsResults) {
            return $this->slots_obj;
        } else {
            return FALSE;
        }
    }

    public function setSlotsObj(TeamBooking_SlotsResults $slots_obj)
    {
        $this->slots_obj = $slots_obj;
    }

    //------------------------------------------------------------

    /**
     * @return string
     */
    public function encode()
    {
        return base64_encode(serialize($this));
    }

    /**
     * @param string $encoded
     * @return TeamBooking_RenderParameters
     */
    public function decode($encoded)
    {
        return unserialize(base64_decode($encoded));
    }

}
