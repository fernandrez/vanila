<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Reservation
{

    //------------------------------------------------------------

    private $has_coworker_token;

    /** @var TeamBooking_ReservationData */
    private $data;

    /** @var TeamBookingType */
    private $service;
    private $reservations;

    /** @var TeamBookingCoworker */
    private $coworker;

    /** @var TeamBookingCustomBTSettings */
    private $coworker_service_data;

    /** @var  Google_Service_Calendar */
    private static $google_service;

    /** @var TeamBookingSettings */
    private static $settings;

    /** @var Google_Service_Calendar_Event */
    private $event_to_patch;

    public function __construct(TeamBooking_ReservationData $data)
    {
        $this->has_coworker_token = FALSE;
        static::$settings = tbGetSettings();
        $this->data = $data;
        if (!$data->getCoworker()) {
            $this->data->setCoworker($this->chooseCoworker($data->getServiceId()));
        }
        $this->coworker = static::$settings->getCoworkerData($this->data->getCoworker());
        $this->coworker_service_data = $this->coworker->getCustomEventSettings($data->getServiceId());
        $this->service = static::$settings->getService($data->getServiceId());
        if ($this->service->isClass('event')) {
            $this->reservations = tbGetReservationsByService($this->service->getId());
        }
        // Create a new Google Client object
        $client = new Google_Client();
        $client->addScope('https://www.googleapis.com/auth/calendar');
        $client->setApplicationName(static::$settings->getApplicationProjectName());
        $client->setClientId(static::$settings->getApplicationClientId());
        $client->setClientSecret(static::$settings->getApplicationClientSecret());
        $client->setAccessType('offline');
        static::$google_service = new Google_Service_Calendar($client);
        $access_token = $this->coworker->getAccessToken();
        if (!empty($access_token)) {
            $client->setAccessToken($this->coworker->getAccessToken());
            $this->has_coworker_token = TRUE;
        }
    }

    //------------------------------------------------------------

    /**
     * Confirms the reservation
     *
     * @return \TeamBooking_Error if something goes wrong,
     * it returns updated reservation data otherwise
     */
    public function doReservation()
    {
        $optional_params = array();
        // NOTE: at this point, $data->getStart() is not defined yet
        if ($this->service->isClass('service')) {

            /*
             * Internal call for customized behaviour
             * after the reservation took place.
             */
            tbCustomAPICaller($this->data, 'before_reservation');

            // Send notification email to admin
            if ($this->service->getNotify()) {
                $this->sendNotificationEmail();
            }
            // Send notification email to coworker
            if ($this->coworker_service_data->getGetDetailsByEmail()) {
                $this->sendNotificationEmailToCoworker();
            }
            // Send confirmation email to customer
            if ($this->service->getConfirm() && $this->data->getCustomerEmail()) {
                $this->sendConfirmationEmail();
            }

            /*
             * Internal call for customized behaviour
             * after the reservation took place.
             */
            tbCustomAPICaller($this->data, 'after_reservation');

            return $this->data;
        } else {
            // Error checkpoint
            if (!$this->has_coworker_token) {
                $error = new TeamBooking_Error(1);

                return $error;
            }
            // Get the Google Calendar ID (backward-compatible)
            if (!$this->data->getGoogleCalendarId()) {
                foreach ($this->coworker->getCalendarId() as $calendar_id) {
                    $google_calendar_id = $calendar_id;
                    break;
                }
            } else {
                $google_calendar_id = $this->data->getGoogleCalendarId();
            }
            // Retrieve event to update
            /* @var $event_retrieved Google_Service_Calendar_Event */
            if ($this->data->getGoogleCalendarEvent() == NULL) {
                $event_retrieved = static::$google_service->events->get($google_calendar_id, $this->data->getGoogleCalendarEventParent());
            } else {
                $event_retrieved = static::$google_service->events->get($google_calendar_id, $this->data->getGoogleCalendarEvent());
            }
            // Error checkpoint
            if (!$this->isEventStillAvailable($event_retrieved)) {
                $error = new TeamBooking_Error(2);

                return $error;
            }
            $this->event_to_patch = new Google_Service_Calendar_Event();
			// Privacy precautions
            $this->event_to_patch->setGuestsCanSeeOtherGuests(FALSE);
            $this->event_to_patch->setGuestsCanInviteOthers(FALSE);
            $this->event_to_patch->setGuestsCanModify(FALSE);
            // Other settings
            $this->setNewTitle();
            $this->setSlotStartEndTimes($event_retrieved);
            $this->setUnhookedDescription($event_retrieved);
            $this->setSource();
            $this->setReminders();
            $this->setEventColor();
            $this->setLocation();
            if ($this->service->isClass('event')) {
                $add_attendee = $this->addAttendee($event_retrieved);

                // Error checkpoint
                if ($add_attendee instanceof TeamBooking_Error) {
                    return $add_attendee;
                }
            }

            /**
             * TODO: File attachment support (requires Google Drive)
             */
            #$file_references = $this->data->getFilesReferences();
            #if (!empty($file_references)) {
            #    $attachments = $event_retrieved->getAttachments();
            #    foreach ($file_references as $hook => $file_info_array) {
            #        if (filter_var($file_info_array['url'], FILTER_VALIDATE_URL)) {
            #            $attachments[] = array(
            #                'fileUrl' => $file_info_array['url'],
            #                'mimeType' => $file_info_array['type'],
            #                'title' => $hook
            #            );
            #        }
            #    }
            #    $this->event_to_patch->setAttachments($attachments);
            #    $optional_params['supportsAttachments'] = TRUE;
            #}

            /*
             * Internal call for customized behaviour
             * after the reservation took place.
             */
            tbCustomAPICaller($this->data, 'before_reservation');
            // Update event
            try {
                // we should create a new one?
                if ($this->data->getGoogleCalendarEvent() == NULL) {
                    $added_event = static::$google_service->events->insert($google_calendar_id, $this->event_to_patch, $optional_params);
                    $added_event_id = $added_event->getId();

                    if ($this->data->isWaitingApproval()) {
                        foreach ($this->reservations as $db_id => $reservation) {
                            /* @var $reservation TeamBooking_ReservationData */
                            if ($reservation->verifyChecksum($this->data)) {
                                continue;
                            }
                            if ($reservation->isWaitingApproval()
                                && $reservation->getGoogleCalendarEvent() == NULL
                                && $reservation->getGoogleCalendarEventParent() == $this->data->getGoogleCalendarEventParent()
                                && $reservation->getSlotStart() == $this->data->getSlotStart()
                                && $reservation->getSlotEnd() == $this->data->getSlotEnd()
                            ) {
                                $reservation->setGoogleCalendarEvent($added_event_id);
                                tbUpdateReservationById($db_id, $reservation);
                            }
                        }
                    }
                    $this->data->setGoogleCalendarEvent($added_event_id);
                } else {
                    static::$google_service->events->patch($google_calendar_id, $this->data->getGoogleCalendarEvent(), $this->event_to_patch, $optional_params);
                }
            } catch (Exception $ex) {
                // Error checkpoint
                if (strpos($ex->getMessage(), 'Invalid attendee email') !== FALSE) {
                    $error = new TeamBooking_Error(4);

                    return $error;
                } else {
                    $error = new TeamBooking_Error(5);
                    $error->setMessage("Google API error: " . $ex->getMessage());

                    return $error;
                }
            }

            // Send notification email to admin
            if ($this->service->getNotify()) {
                $this->sendNotificationEmail();
            }

            // Send notification email to coworker
            if ($this->coworker_service_data->getGetDetailsByEmail()) {
                $this->sendNotificationEmailToCoworker($event_retrieved->getHtmlLink());
            }

            // Send confirmation email to customer
            if ($this->service->getConfirm() && $this->data->getCustomerEmail()) {
                $this->sendConfirmationEmail();
            }

            /*
             * Internal call for customized behaviour
             * after the reservation took place.
             */
            tbCustomAPICaller($this->data, 'after_reservation');

            return $this->data;
        }
    }

    //------------------------------------------------------------

    /**
     * Revoke the reservation
     *
     * If everything went fine, it returns the updated
     * reservation data, which must be saved in the database
     * by the function caller (log_id is not available here).
     */
    public function cancelReservation($reservation_database_id)
    {
        if ($this->data->isWaitingApproval()) {
            /**
             * Updating the reservation data
             */
            $this->data->setStatusCancelled();
            if ($_POST['reason']) {
                $this->data->setCancellationReason(tbFilterInput($_POST['reason']));
            }

            $start = new DateTime($this->data->getSlotStart());
            $this->data->setStart($start->format('U'));
            $end = new DateTime($this->data->getSlotEnd());
            $this->data->setEnd($end->format('U'));

            /*
             * Send email to customer
             */
            if ($this->service->getSendCancellation() && $this->data->getCustomerEmail()) {
                $this->sendCancellationEmail($_POST['reason']);
            }

            return $this->data;
        }
        if ($this->service->isClass('appointment')) {
            $booked_title = static::$settings->getCoworkerData($this->data->getCoworker())->getCustomEventSettings($this->service->getId())->getAfterBookedTitle();
            $free_title = static::$settings->getCoworkerData($this->data->getCoworker())->getCustomEventSettings($this->service->getId())->getLinkedEventTitle();
            $gcal_event_id = $this->data->getGoogleCalendarEvent();
            // Check if the coworker is still active
            if (!$this->has_coworker_token) {
                $error = new TeamBooking_Error(1);

                return $error;
            }
            // Get the Google Calendar ID (backward-compatible)
            if (!$this->data->getGoogleCalendarId()) {
                foreach ($this->coworker->getCalendarId() as $calendar_id) {
                    $google_calendar_id = $calendar_id;
                    break;
                }
            } else {
                $google_calendar_id = $this->data->getGoogleCalendarId();
            }
            /* @var $event_retrieved Google_Service_Calendar_Event */
            $event_retrieved = static::$google_service->events->get($google_calendar_id, $gcal_event_id);
            // Check if the event is still available and marked as booked
            if (strtolower($event_retrieved->getSummary()) != strtolower($booked_title) || $event_retrieved->getStatus() == 'cancelled') {
                $error = new TeamBooking_Error(7);

                return $error;
            }
            try {
                if ($this->data->getGoogleCalendarEventParent()) {
                    /**
                     * If the Appointment was container-derived,
                     * just delete it
                     */
                    static::$google_service->events->delete($google_calendar_id, $gcal_event_id);
                } else {
                    /**
                     * If the Appointment was slot-derived, then
                     * we should reset the slot in it's available state
                     */
                    $event_retrieved->setSummary($free_title);
                    $event_retrieved->setColorId(NULL);
                    $event_retrieved->setDescription('');
                    static::$google_service->events->update($google_calendar_id, $gcal_event_id, $event_retrieved);
                }
            } catch (Exception $ex) {
                $error = new TeamBooking_Error(5);
                $error->setMessage("Google API error: " . $ex->getMessage());

                return $error;
            }
            /**
             * Updating the reservation data
             */
            $this->data->setStatusCancelled();
            if ($_POST['reason']) {
                $this->data->setCancellationReason(tbFilterInput($_POST['reason']));
            }
            /*
             * Send email to customer
             */
            if ($this->service->getSendCancellation() && $this->data->getCustomerEmail()) {
                $this->sendCancellationEmail($_POST['reason']);
            }

            return $this->data;
        } elseif ($this->service->isClass('event')) {
            $free_title = static::$settings->getCoworkerData($this->data->getCoworker())->getCustomEventSettings($this->service->getId())->getLinkedEventTitle();
            $gcal_event_id = $this->data->getGoogleCalendarEvent();
            // Check if the coworker is still active
            if (!$this->has_coworker_token) {
                $error = new TeamBooking_Error(1);

                return $error;
            }
            // Get the Google Calendar ID (backward-compatible)
            if (!$this->data->getGoogleCalendarId()) {
                foreach ($this->coworker->getCalendarId() as $calendar_id) {
                    $google_calendar_id = $calendar_id;
                    break;
                }
            } else {
                $google_calendar_id = $this->data->getGoogleCalendarId();
            }
            /* @var $event_retrieved Google_Service_Calendar_Event */
            $event_retrieved = static::$google_service->events->get($google_calendar_id, $gcal_event_id);
            // Check if the event is still available and referenced to the service
            if (strtolower($event_retrieved->getSummary()) != strtolower($free_title)
                || $event_retrieved->getStatus() == 'cancelled'
            ) {
                $error = new TeamBooking_Error(7);

                return $error;
            }
            try {
                $attendees = $event_retrieved->getAttendees();
                $attendee_to_modify = FALSE;
                $attendee_to_modify_id = FALSE;
                /**
                 * Let's find our customer
                 */
                foreach ($attendees as $id => $attendee) {
                    /* @var $attendee Google_Service_Calendar_EventAttendee */
                    if ($attendee->getEmail() == $this->data->getCustomerEmail()) {
                        // Found!
                        $attendee_to_modify = $attendee;
                        $attendee_to_modify_id = $id;
                        break;
                    }
                }
                /**
                 * Now, let's extract the total tickets bounded with this customer
                 */
                $old_ticket_number = 0;
                foreach ($this->reservations as $reservation) {
                    /* @var $reservation TeamBooking_ReservationData */
                    if ($reservation->getGoogleCalendarEvent() == $gcal_event_id &&
                        $reservation->isConfirmed() &&
                        $reservation->getCustomerEmail() == $this->data->getCustomerEmail()
                    ) {
                        $old_ticket_number += (int)$reservation->getTickets();
                    }
                }
                // Decreasing
                $new_ticket_number = $old_ticket_number - (int)$this->data->getTickets();
                /**
                 * If the new ticket number is zero, then just drop the attendee,
                 * otherwise update it
                 */
                if ($attendee_to_modify) {
                    if ($new_ticket_number <= 0) {
                        unset($attendees[$attendee_to_modify_id]);
                    } else {
                        $now = date_i18n(get_option('date_format'));
                        $attendee_to_modify->setComment(sprintf(__('Date of reservation: %s', 'teambooking'), $now));
                        $attendee_to_modify->setAdditionalGuests((int)$new_ticket_number - 1);
                        $attendees[$attendee_to_modify_id] = $attendee_to_modify;
                    }
                }
                $description = '';
                foreach ($this->reservations as $id => $reservation) {
                    /* @var $reservation TeamBooking_ReservationData */
                    if ($reservation->getGoogleCalendarEvent() == $event_retrieved->getId()
                        && $reservation->isConfirmed()
                        && $reservation_database_id != $id
                    ) {
                        $description .= $reservation->getCustomerDisplayName() . " " . $reservation->getCustomerEmail() . " +" . $reservation->getTickets();
                        $description .= "\n";
                    }
                }

                if ($this->data->getGoogleCalendarEventParent() && empty($description)) {

                    /**
                     * If the Event was container-derived and it has no attendees left,
                     * just delete it
                     */
                    static::$google_service->events->delete($google_calendar_id, $gcal_event_id);
                    foreach ($this->reservations as $db_id => $reservation) {
                        /* @var $reservation TeamBooking_ReservationData */
                        if ($reservation->isWaitingApproval()
                            && $reservation->getGoogleCalendarEventParent() == $this->data->getGoogleCalendarEventParent()
                            && $reservation->getSlotStart() == $this->data->getSlotStart()
                            && strtotime($reservation->getSlotEnd()) == strtotime($this->data->getSlotEnd())
                        ) {
                            $reservation->setGoogleCalendarEvent(NULL);
                            tbUpdateReservationById($db_id, $reservation);
                        }
                    }
                } else {

                    if ($this->data->getGoogleCalendarEventParent()) {
                        // We should pass this to another "stack"
                        foreach ($this->reservations as $db_id => $reservation) {
                            /* @var $reservation TeamBooking_ReservationData */
                            if (!$reservation->isWaitingApproval()
                                && $reservation->getGoogleCalendarEventParent() == NULL
                                && $reservation->getGoogleCalendarEvent() == $this->data->getGoogleCalendarEvent()
                                && $reservation->getSlotStart() == $this->data->getSlotStart()
                                && strtotime($reservation->getSlotEnd()) == strtotime($this->data->getSlotEnd())
                            ) {
                                $reservation->setGoogleCalendarEventParent($this->data->getGoogleCalendarEventParent());
                                tbUpdateReservationById($db_id, $reservation);
                                break;
                            }
                        }
                    }

                    /**
                     * If the Event was slot-derived or it has attendees left, then update it
                     */
                    $event_retrieved->setAttendees($attendees);
                    $event_retrieved->setDescription($description);
                    static::$google_service->events->update($google_calendar_id, $gcal_event_id, $event_retrieved);
                }
            } catch (Exception $ex) {
                $error = new TeamBooking_Error(5);
                $error->setMessage("Google API error: " . $ex->getMessage());

                return $error;
            }
            /**
             * Updating the reservation data
             */
            $this->data->setStatusCancelled();
            if ($_POST['reason']) {
                $this->data->setCancellationReason(tbFilterInput($_POST['reason']));
            }
            /*
             * Send email to customer
             */
            if ($this->service->getSendCancellation() && $this->data->getCustomerEmail()) {
                $this->sendCancellationEmail($_POST['reason']);
            }

            return $this->data;
        } else {
            return FALSE;
        }
    }

    //------------------------------------------------------------

    /**
     * Prepare and set the new google calendar event title
     */
    private function setNewTitle()
    {
        if (!($this->service->isClass('event'))) {
            $summary = $this->coworker_service_data->getAfterBookedTitle();
            $this->event_to_patch->setSummary($summary);
        } else {
            if ($this->data->isFromContainer()) {
                $summary = $this->service->getName();
                $this->event_to_patch->setSummary($summary);
            }
        }
    }

    //------------------------------------------------------------

    /**
     * Sets the event description, with all the hooks replaced
     *
     * @param Google_Service_Calendar_Event $event_retrieved
     */
    private function setUnhookedDescription(Google_Service_Calendar_Event $event_retrieved)
    {
        if ($this->service->isClass('event')) {
            $description = '';
            foreach ($this->reservations as $reservation) {
                /* @var $reservation TeamBooking_ReservationData */
                if ($reservation->getGoogleCalendarEvent() == $event_retrieved->getId() &&
                    $reservation->isConfirmed()
                ) {
                    $description .= $reservation->getCustomerDisplayName() . " " . $reservation->getCustomerEmail() . " +" . $reservation->getTickets();
                    $description .= "\n";
                }
            }
            $description .= $this->data->getCustomerDisplayName() . " " . $this->data->getCustomerEmail() . " +" . $this->data->getTickets();
        } else {
            $description = $this->coworker_service_data->getNotificationEmailBody();
            // Replace Hooks
            $description = tbFindAndReplaceHooks($description, $this->data->getHooksArray());
            $breaks = array(
                "<br />",
                "<br>",
                "<br/>",
            );
            $description = str_ireplace($breaks, "\n", $description);
        }
        $this->event_to_patch->setDescription(tbUnfilterInput(strip_tags($description)));
    }

    //------------------------------------------------------------

    /**
     * Sets the site as google calendar event source
     */
    private function setSource()
    {
        $source = new Google_Service_Calendar_EventSource;
        $source->setTitle(static::$settings->getApplicationProjectName());
        $source->setUrl(home_url());
        $this->event_to_patch->setSource($source);
    }

    //------------------------------------------------------------

    /**
     * Sets the reminders
     */
    private function setReminders()
    {
        $reminder_setting = $this->coworker_service_data->getReminder();
        $reminders = new Google_Service_Calendar_EventReminders;
        if ($reminder_setting != 'none') {
            $reminders->setUseDefault(FALSE);
            $reminder = array(
                'method'  => 'email',
                'minutes' => $reminder_setting,
            );
            $reminders->setOverrides(array($reminder));
        }
        $this->event_to_patch->setReminders($reminders);
    }

    //------------------------------------------------------------

    /**
     * Sets the google calendar event color
     */
    private function setEventColor()
    {
        $color_setting = $this->coworker_service_data->getBookedEventColor();
        if ($color_setting) {
            $this->event_to_patch->setColorId($color_setting);
        } else {
            $this->event_to_patch->setColorId(NULL);
        }
    }

    //------------------------------------------------------------

    /**
     * Sets the google calendar event location
     */
    private function setLocation()
    {
        if ($this->data->getCustomerAddress() && $this->service->getLocationSetting() == 'none' && !$this->service->isClass('event')) {
            $this->event_to_patch->setLocation($this->data->getCustomerAddress());
        }
    }

    //------------------------------------------------------------

    /**
     * Adds an attendee to the google calendar event
     *
     * @param Google_Service_Calendar_Event $event_retrieved
     * @return boolean|\TeamBooking_Error
     */
    private function addAttendee(Google_Service_Calendar_Event $event_retrieved)
    {
        $now = date_i18n(get_option('date_format'));
        // Check again the attendees limit
        $attendees = $event_retrieved->getAttendees();
        if ($this->service->getMaxReservationsPerUser() > 1) {
            $attendees_number = 0;
            $customer_total_tickets = 0;
            foreach ($this->reservations as $reservation) {
                /* @var $reservation TeamBooking_ReservationData */
                if ($reservation->getGoogleCalendarEvent() == $event_retrieved->getId() &&
                    $reservation->isConfirmed()
                ) {
                    $attendees_number += (int)$reservation->getTickets();
                    if ($reservation->getCustomerEmail() == $this->data->getCustomerEmail()) {
                        $customer_total_tickets += (int)$reservation->getTickets();
                    }
                }
            }
        } else {
            $attendees_number = count($attendees);
        }
        if ($attendees_number == $this->service->getAttendees()) {
            // the event is full
            $error = new TeamBooking_Error(6);

            return $error;
        }
        $new_attendee = new Google_Service_Calendar_EventAttendee();
        // set a random email, if not present
        if (!$this->data->getCustomerEmail()) {
            $domain = explode('@', $this->service->getEmailForNotifications());
            if (count($domain) == 2) {
                $domain = $domain[1];
            } else {
                $domain = 'example.com';
            }
            $new_attendee->setEmail(uniqid() . "@" . $domain);
            $new_ticket_number = (int)$this->data->getTickets();
        } else {
            // Check the email for already present reservations
            if ($this->hasCustomerAlreadyBooked($event_retrieved)) {
                // Check if more than one reservations per user are allowed, 
                // so we need to update the number or simply return an error
                if ($this->service->getMaxReservationsPerUser() > 1) {
                    // Increasing
                    $new_ticket_number = $customer_total_tickets + (int)$this->data->getTickets();
                    // Check if the new total tickets are more than what allowed
                    // per single user
                    if ($new_ticket_number > $this->service->getMaxReservationsPerUser()) {
                        // tickets are more than allowed
                        $error = new TeamBooking_Error(8);

                        return $error;
                    }
                    // If there is an attendee with that email, let's update it
                    foreach ($attendees as $attendee) {
                        /* @var $attendee Google_Service_Calendar_EventAttendee */
                        if ($attendee->getEmail() == $this->data->getCustomerEmail()) {
                            $attendee->setComment(sprintf(__('Date of reservation: %s', 'teambooking'), $now));
                            $attendee->setAdditionalGuests((int)$new_ticket_number - 1);
                            $attendee->setResponseStatus('accepted');
                            $this->event_to_patch->setAttendees($attendees);

                            return TRUE;
                        }
                    }
                } else {
                    // the user has already booked
                    $error = new TeamBooking_Error(3);

                    return $error;
                }
            } else {
                $new_ticket_number = (int)$this->data->getTickets();
            }
            $new_attendee->setEmail($this->data->getCustomerEmail());
        }
        $new_attendee->setDisplayName($this->data->getCustomerDisplayName());
        $new_attendee->setComment(sprintf(__('Date of reservation: %s', 'teambooking'), $now));
        $new_attendee->setAdditionalGuests((int)$new_ticket_number - 1);
        $new_attendee->setResponseStatus('accepted');
        $attendees[] = $new_attendee;
        $this->event_to_patch->setAttendees($attendees);

        return TRUE;
    }

    //------------------------------------------------------------

    /**
     * Sets the start/end boundings
     *
     * @param Google_Service_Calendar_Event $event_retrieved
     */
    private function setSlotStartEndTimes(Google_Service_Calendar_Event $event_retrieved)
    {
        if ($this->data->isFromContainer()) {
            $event_start_time = new Google_Service_Calendar_EventDateTime();
            $event_start_time->setDateTime($this->data->getSlotStart());
            $event_retrieved->setStart($event_start_time);
            $this->event_to_patch->setStart($event_start_time);
            $event_end_time = new Google_Service_Calendar_EventDateTime();
            $event_end_time->setDateTime($this->data->getSlotEnd());
            $event_retrieved->setEnd($event_end_time);
            $this->event_to_patch->setEnd($event_end_time);
        }
        if ($event_retrieved->getStart()->getDateTime()) {
            $this->data->setStart(strtotime($event_retrieved->getStart()->getDateTime()));
            $this->data->setEnd(strtotime($event_retrieved->getEnd()->getDateTime()));
        } else {
            // Is all-day
            $this->data->setStart(strtotime($event_retrieved->getStart()->getDate()));
            $this->data->setEnd(strtotime($event_retrieved->getEnd()->getDate()));
        }
    }

    //------------------------------------------------------------

    /**
     * Checks if the google calendar event is still available
     *
     * @param Google_Service_Calendar_Event $event_retrieved
     * @return boolean
     */
    private function isEventStillAvailable(Google_Service_Calendar_Event $event_retrieved)
    {
        if ($this->service->isClass('appointment') && $this->isSlotBooked($event_retrieved)) {
            return FALSE;
        }
        if ($event_retrieved->getStatus() == 'cancelled') {
            return FALSE;
        }

        return TRUE;
    }

    //------------------------------------------------------------

    /**
     * Uses a rule to check if an Appointment slot is booked or not
     *
     * @param Google_Service_Calendar_Event $slot
     * @return bool
     */
    private function isSlotBooked($slot)
    {
        // The new rule
        if (strtolower($slot->getSummary()) == strtolower(tbGetSettings()->getCoworkerData($this->coworker->getId())->getCustomEventSettings($this->service->getId())->getAfterBookedTitle())) {
            return TRUE;
        } else {
            return FALSE;
        }
        // The old rule
        #if (isGoogleEventFromThisSource($slot)) {
        #return TRUE;
        #} else {
        #return FALSE;
        #}
    }

    //------------------------------------------------------------

    /**
     * Checks if the customer has already booked
     *
     * @param Google_Service_Calendar_Event $event_retrieved
     * @return boolean
     */
    private function hasCustomerAlreadyBooked(Google_Service_Calendar_Event $event_retrieved)
    {
        foreach ($this->reservations as $reservation) {
            /* @var $reservation TeamBooking_ReservationData */
            if ($reservation->getCustomerEmail() == $this->data->getCustomerEmail() &&
                $reservation->getGoogleCalendarEvent() == $event_retrieved->getId() &&
                $reservation->isConfirmed()
            ) {
                return TRUE;
            }
        }

        return FALSE;
    }

    //------------------------------------------------------------

    /**
     * Sends the notification email to the coworker
     *
     * @param string $event_link
     * @return boolean
     */
    private function sendNotificationEmailToCoworker($event_link = "")
    {
        $body = tbFindAndReplaceHooks($this->coworker_service_data->getNotificationEmailBody(), $this->data->getHooksArray());
        if (!empty($event_link)) {
            $body .= __('<br>Event link: ', 'teambooking') . $event_link;
        }
        $email = new TeamBooking_Email();
        $email->setSubject(tbFindAndReplaceHooks($this->coworker_service_data->getNotificationEmailSubject(), $this->data->getHooksArray()));
        $email->setBody($body); // unhooked
        $sender = $this->prepareSender($this->data->getFieldsArray());
        $email->setFrom($this->service->getEmailForNotifications(), $sender['name']);
        $email->setReplyTo($sender['from']);
        $email->setTo($this->coworker->getEmail());
        $files_references = $this->data->getFilesReferences();
        if (!empty($files_references) && $this->coworker_service_data->getIncludeFilesAsAttachment()) {
            foreach ($files_references as $file_reference) {
                $email->setAttachment($file_reference['file']);
            }
        }
        $email->send();

        return TRUE;
    }

    //------------------------------------------------------------

    /**
     * Sends the confirmation email to customer
     *
     * @return boolean
     */
    private function sendConfirmationEmail()
    {
        $email = new TeamBooking_Email();
        $subject = tbFindAndReplaceHooks($this->service->getFrontendEmail()->getSubject(), $this->data->getHooksArray());
        $body = tbFindAndReplaceHooks($this->service->getFrontendEmail()->getBody(), $this->data->getHooksArray());
        // Set data
        $email->setSubject($subject);
        $email->setBody($body);
        $email->setFrom($this->service->getEmailForNotifications(), get_bloginfo('name'));
        $email->setTo($this->data->getCustomerEmail());
        $email->send();

        return TRUE;
    }

    //------------------------------------------------------------

    /**
     * Sends the notification email to admin
     *
     * @return boolean
     */
    private function sendNotificationEmail()
    {
        $email = new TeamBooking_Email();
        $subject = tbFindAndReplaceHooks($this->service->getBackendEmail()->getSubject(), $this->data->getHooksArray());
        $body = tbFindAndReplaceHooks($this->service->getBackendEmail()->getBody(), $this->data->getHooksArray());
        // Set data
        $email->setSubject($subject);
        $email->setBody($body);
        $sender = $this->prepareSender($this->data->getFieldsArray());
        $email->setFrom($this->service->getEmailForNotifications(), $sender['name']);
        $email->setReplyTo($sender['from']);
        $email->setTo($this->service->getEmailForNotifications());
        $files_references = $this->data->getFilesReferences();
        if (!empty($files_references) && $this->service->getIncludeFilesAsAttachment()) {
            foreach ($files_references as $file_reference) {
                $email->setAttachment($file_reference['file']);
            }
        }
        $email->send();

        return TRUE;
    }

    //------------------------------------------------------------

    /**
     * Send the cancellation email to customer
     *
     * @param string $reason
     * @return boolean
     */
    private function sendCancellationEmail($reason)
    {
        $email = new TeamBooking_Email();
        $email->setFrom($this->service->getEmailForNotifications(), get_option('blogname'));
        $subject = tbFindAndReplaceHooks($this->service->getCancellationEmail()->getSubject(), $this->data->getHooksArray());
        $body = tbFindAndReplaceHooks($this->service->getCancellationEmail()->getBody(), $this->data->getHooksArray());
        $body = $reason ? $reason . '<br>' . $body : $body;
        $email->setTo($this->data->getCustomerEmail());
        $email->setSubject($subject);
        $email->setBody($body);
        $email->send();

        return TRUE;
    }

    //------------------------------------------------------------

    /**
     * Prepares the confirmation email sender
     *
     * @param mixed $fields
     * @return string
     */
    private function prepareSender($fields)
    {
        $results = array(
            'from' => NULL,
            'name' => NULL,
        );
        if (isset($fields['email'])) {
            $results['from'] = $fields['email'];
        } else {
            $results['from'] = get_bloginfo('admin_email');
        }
        if (isset($fields['first_name'])) {
            $results['name'] .= $fields['first_name'];
        }
        if (isset($fields['second_name'])) {
            $results['name'] .= " " . $fields['second_name'];
        }
        if (is_null($results['name'])) {
            $results['name'] = get_bloginfo('name');
        }

        return $results;
    }

    //------------------------------------------------------------

    /**
     * Coworker assignation rules
     *
     * TODO: at the moment, the coworkers specified in the shortcode will be ignored
     * for unscheduled events: allow this
     *
     * @param string $service_id
     * @return int
     */
    public static function chooseCoworker($service_id)
    {
        $rule = tbGetSettings()->getService($service_id)->getAssignmentRule();
        if (empty($rule)) {
            $rule = 'equal';
        }
        $coworkers = tbGetCoworkersIdList(); // Authorized only?
        $reservations = tbGetReservations();
        if ($rule == 'equal') {
            // TODO: add timespan RULE!!!!!!!
            foreach ($coworkers as $coworker_id) {
                $coworkers_equalizer[$coworker_id] = 0;
            }
            // TODO: add also pending reservations????
            foreach ($reservations as $reservation) {
                /* @var $reservation TeamBooking_ReservationData */
                if ($reservation->getServiceId() == $service_id) {
                    $coworkers_equalizer[$reservation->getCoworker()]++;
                }
            }
            asort($coworkers_equalizer);
            // Pick the first
            foreach ($coworkers_equalizer as $coworker_id => $value) {
                $coworker_choosen = $coworker_id;
                break;
            }
        } elseif ($rule == 'random') {
            $random_key = array_rand($coworkers);
            $coworker_choosen = $coworkers[$random_key];
        } elseif ($rule == 'direct') {
            $coworker_choosen = tbGetSettings()->getService($service_id)->getCoworkerForDirectAssignment();
        }
        if (empty($coworker_choosen)) {
            $random_key = array_rand($coworkers);
            $coworker_choosen = $coworkers[$random_key];
        }

        return $coworker_choosen;
    }

}
