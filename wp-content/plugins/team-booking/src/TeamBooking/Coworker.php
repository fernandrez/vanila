<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

/**
 * This class defines a Coworker object.
 *
 * A coworker is bounded with a WordPress user,
 * and contains few properties, along with the
 * coworker's custom settings objects for each service.
 */
class TeamBookingCoworker
{

    //------------------------------------------------------------

    ////////////////
    // Properties //
    ////////////////
    private $coworker_id;
    private $calendar_id;
    private $access_token;
    //////////////////////////////
    // Custom services settings //
    //////////////////////////////
    private $custom_event_settings;

    public function __construct($coworker_id = NULL)
    {
        if (is_user_logged_in() && is_null($coworker_id)) {
            /*
             * the class is called without a specified ID,
             * let's load the logged one.
             */
            $this->coworker_id = wp_get_current_user()->data->ID;
        } else {
            $this->coworker_id = $coworker_id;
        }
    }

    //------------------------------------------------------------

    /**
     * Adds a Google Calendar ID
     *
     * @param string $calendar_id
     */
    public function addCalendarId($calendar_id)
    {
        if (is_array($this->calendar_id)) {
            $this->calendar_id[] = $calendar_id;
        } else {
            if (!empty($this->calendar_id)) {
                $array = array($this->calendar_id);
            } else {
                $array = array();
            }
            $array[] = $calendar_id;
            $this->calendar_id = $array;
        }
    }

    /**
     * Drops a Google Calendar ID (by key)
     *
     * @param int $key
     */
    public function dropCalendarId($key)
    {
        if (is_array($this->calendar_id)) {
            unset($this->calendar_id[$key]);
        }
    }

    /**
     * Returns the array of Coworker's Google Calendar IDs
     *
     * @return array Google Calendar IDs
     */
    public function getCalendarId()
    {
        if (is_array($this->calendar_id)) {
            return $this->calendar_id;
        } elseif (!is_null($this->calendar_id)) {
            // legacy
            return array($this->calendar_id);
        } else {
            return array();
        }
    }

    /**
     * Drops all the Google Calendars
     */
    public function dropAllCalendarIds()
    {
        $this->calendar_id = array();
    }

    //------------------------------------------------------------

    /**
     * Retrieve the oAuth access token
     *
     * @return string the access token
     */
    public function getAccessToken()
    {
        return $this->access_token;
    }

    /**
     * Sets the oAuth access token
     *
     * @param string $token
     */
    public function setAccessToken($token)
    {
        $this->access_token = $token;
    }

    //------------------------------------------------------------

    /**
     * Retrieve the custom settings object for a specified service
     *
     * @param string $service_id
     * @return \TeamBookingCustomBTSettings
     */
    public function getCustomEventSettings($service_id)
    {
        // Read & return settings object
        if (isset($this->custom_event_settings[$service_id])) {
            return $this->custom_event_settings[$service_id];
        } else {
            /*
             * Coworker hasn't customized anything yet for that service,
             * so let's return defaults.
             */
            try {
                $service = tbGetSettings()->getService($service_id);
            } catch (Exception $ex) {
                return new TeamBookingCustomBTSettings();
            }
            $return = new TeamBookingCustomBTSettings();
            // This string must not be wrapped in a GetText function
            $return->setAfterBookedTitle('New reservation for' . " " . $service->getName());
            $return->setBookedEventColor(0);
            $return->setGetDetailsByEmailOn();
            $return->setLinkedEventTitle($service->getName());
            $return->setMinTime('PT1H');
            $booking_name = $service->getName();
            $return->setNotificationEmailBody(sprintf(__('You have got a new reservation for %s<br>Date and time: [start_datetime]<br>Customer name: [first_name]<br>Customer email: [email]', 'teambooking'), $booking_name));
            $return->setNotificationEmailSubject(sprintf(__('New reservation for %s', 'teambooking'), $booking_name));
            $return->setReminder(0);
            $return->setParticipateOn();
            $return->setIncludeFilesAsAttachmentOff();

            return $return;
        }
    }

    /**
     * Save a custom event settings object for a specified service
     *
     * @param TeamBookingCustomBTSettings $settings
     * @param string $service_id
     */
    public function setCustomEventSettings(TeamBookingCustomBTSettings $settings, $service_id)
    {
        $this->custom_event_settings[$service_id] = $settings;
    }

    //------------------------------------------------------------

    /**
     * Returns the Coworker's ID (WordPress user ID)
     *
     * @return int Coworker / WordPress user ID
     */
    public function getId()
    {
        return $this->coworker_id;
    }

    //------------------------------------------------------------

    /**
     * Returns the Coworker's nicename (WordPress user nicename)
     *
     * @return string
     */
    public function getNiceName()
    {
        return get_userdata($this->coworker_id)->data->user_nicename;
    }

    //------------------------------------------------------------

    /**
     * Returns the Coworker's email (WordPress user email)
     *
     * @return string
     */
    public function getEmail()
    {
        return get_userdata($this->coworker_id)->data->user_email;
    }

    //------------------------------------------------------------

    /**
     * Returns the Coworker's display name (WordPress user display name)
     *
     * @return string
     */
    public function getDisplayName()
    {
        return get_userdata($this->coworker_id)->data->display_name;
    }

    //------------------------------------------------------------

    /**
     * Returns the Coworker's roles (WordPress user roles)
     *
     * @return array
     */
    public function getRoles()
    {
        return get_userdata($this->coworker_id)->roles;
    }

    //------------------------------------------------------------

    /**
     * Returns the Coworker's first name (WordPress user first name)
     *
     * @return string
     */
    public function getFirstName()
    {
        $tmp = get_user_meta($this->coworker_id); // PHP 5.3 compatibility
        return $tmp['first_name'][0];
    }

    //------------------------------------------------------------

    /**
     * Returns the Coworker's last name (WordPress user last name)
     *
     * @return string
     */
    public function getLastName()
    {
        $tmp = get_user_meta($this->coworker_id); // PHP 5.3 compatibility
        return $tmp['last_name'][0];
    }

    //------------------------------------------------------------

    /**
     * Drops the custom settings for a specified service
     *
     * @param string $service_id
     * @return boolean
     */
    public function dropCustomServiceSettings($service_id)
    {
        if (isset($this->custom_event_settings[$service_id])) {
            unset($this->custom_event_settings[$service_id]);

            return TRUE;
        } else {
            return FALSE;
        }
    }

}
