<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBooking_Email
{

    //------------------------------------------------------------

    ///////////////////
    //  PARAMETERS   //
    ///////////////////
    private $subject;
    private $body;
    private $to;
    private $headers;
    private $attachments;
    private $from_address;
    private $from_name;

    public function __construct()
    {
        ;
    }

    //------------------------------------------------------------

    public function setFrom($address, $name = NULL)
    {
        if (is_null($name)) {
            $this->from_address = $address;
        } else {
            $this->from_name = tbUnfilterInput($name);
            $this->from_address = $address;
        }
    }

    //------------------------------------------------------------

    public function setSubject($text)
    {
        $this->subject = tbUnfilterInput($text);
    }

    //------------------------------------------------------------

    public function setBody($text)
    {
        $this->body = tbUnfilterInput($text);
    }

    //------------------------------------------------------------

    public function setReplyTo($address)
    {
        if (!empty($address)) {
            $this->headers[] = 'Reply-To: ' . $address;
        }
    }

    //------------------------------------------------------------

    public function setCc($address, $name = NULL)
    {
        if (is_null($name)) {
            $this->headers[] = 'Cc: ' . $address;
        } else {
            $this->headers[] = 'Cc: ' . tbUnfilterInput($name) . ' <' . $address . '>';
        }
    }

    //------------------------------------------------------------

    public function setTo($address)
    {
        $this->to[] = $address;
    }

    //------------------------------------------------------------

    public function setAttachment($path)
    {
        $this->attachments[] = $path;
    }

    //------------------------------------------------------------

    public function setHtmlContentType()
    {
        return 'text/html';
    }

    //------------------------------------------------------------

    public function send()
    {
        $return = FALSE;
        // set HTML content type
        add_filter('wp_mail_content_type', array(
            $this,
            'setHtmlContentType',
        ));

        // From filters
        if (!empty($this->from_address)) {
            $obj = $this;
            add_filter('wp_mail_from', function () use ($obj) {
                return $obj->getFromAddress();
            });
        }
        if (!empty($this->from_name)) {
            $obj = $this;
            add_filter('wp_mail_from_name', function () use ($obj) {
                return $obj->getFromName();
            });
        }

        // Send email
        if (isset($this->attachments)) {
            if (wp_mail($this->to, $this->subject, $this->body, $this->headers, $this->attachments)) {
                $return = TRUE;
            }
        } else {
            if (wp_mail($this->to, $this->subject, $this->body, $this->headers)) {
                $return = TRUE;
            }
        }

        // Reset content-type to avoid conflicts -- http://core.trac.wordpress.org/ticket/23578
        remove_filter('wp_mail_content_type', 'setHtmlContentType');

        // Reset from filters
        if (!empty($this->from_address)) {
            remove_filter('wp_mail_from', function ($email) {
                return $this->from_address;
            });
        }
        if (!empty($this->from_name)) {
            remove_filter('wp_mail_from_name', function ($name) {
                return $this->from_name;
            });
        }

        return $return;
    }

    //------------------------------------------------------------

    public function getFromAddress()
    {
        return $this->from_address;
    }

    //------------------------------------------------------------

    public function getFromName()
    {
        return $this->from_name;
    }

}
