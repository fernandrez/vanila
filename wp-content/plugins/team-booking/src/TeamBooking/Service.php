<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBookingType
{

    //------------------------------------------------------------

    private $booking_name;
    private $booking_id;
    private $booking_class;
    private $service_color;
    private $email_for_notifications;
    private $notify;
    private $confirm;
    private $logged_only;
    private $show_times;
    private $show_soldout;
    private $show_coworker;
    private $show_coworker_url;
    private $attendees;
    private $max_reservations_per_user;
    private $show_reservations_left;
    private $service_info;
    private $price;
    private $active;
    private $coworker_assignment_rule;
    private $coworker_id_for_direct_assignment;
    private $payment_must_be_done;
    private $include_files_as_attachment; // To admin notification address
    private $duration_rule;
    private $default_duration;
    private $send_cancellation_email;
    private $location_setting;
    private $location_address;
    private $approve_rule;
    private $free_until_approval;
    private $send_approval_notification;
    private $approval_notification_subject;
    private $approval_notification_text;
    private $allow_customer_cancellation;
    private $allow_customer_cancellation_until; //seconds
    private $redirect;
    private $redirect_url;

    /** @var $cancellation_email TeamBookingServiceEmail */
    private $cancellation_email;

    /** @var $front_end_email TeamBookingServiceEmail */
    private $front_end_email;

    /** @var $back_end_email TeamBookingServiceEmail */
    private $back_end_email;

    /** @var $form_fields TeamBookingFormFields */
    private $form_fields;

    public function __construct()
    {
        $this->setConfirmOn();
        $this->setNotifyOn();
        $this->setCancellationEmailOn();
        $this->email_for_notifications = get_bloginfo('admin_email');
        $this->front_end_email = new TeamBookingServiceEmail();
        $this->back_end_email = new TeamBookingServiceEmail();
        $this->cancellation_email = new TeamBookingServiceEmail();
        // Default email content
        $this->front_end_email->setSubject(esc_html__('Your reservation details', 'teambooking'));
        $default_frontend_body = esc_html__('Thanks for your reservation!', 'teambooking') . '<br>'
            . esc_html__('Reservation date and time', 'teambooking') . ': [start_datetime]';
        $this->front_end_email->setBody($default_frontend_body);
        $this->back_end_email->setSubject(esc_html__('A new reservation', 'teambooking'));
        $default_backend_body = esc_html__('You have just got a new reservation!', 'teambooking') . '<br>'
            . esc_html__('Customer name', 'teambooking') . ': [first_name]<br>'
            . esc_html__('Customer email', 'teambooking') . ': [email]';
        $this->back_end_email->setBody($default_backend_body);
        $this->cancellation_email->setSubject(esc_html__('Reservation cancelled', 'teambooking'));
        $default_cancellation_body = esc_html__('Your reservation was cancelled.', 'teambooking');
        $this->cancellation_email->setBody($default_cancellation_body);
        $this->setLoggedOnlyOff();
        $this->setShowTimes(new TeamBookingShowTimes);
        $this->setShowCoworkerOff();
        $this->setShowCoworkerUrlOff();
        $this->setShowSoldoutOff();
        $this->setPrice(0);
        $this->setActiveOn();
        $this->setPaymentMustBeDone('immediately');
        $this->setIncludeFilesAsAttachmentOff();
        $this->setDurationRule('coworker');
        $this->setApproveRule('none');
        $this->setDefaultDuration(1 * HOUR_IN_SECONDS);
        // Set a random color
        $this->service_color = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
        $this->location_address = "";
        $this->free_until_approval = FALSE;
        $this->send_approval_notification = TRUE;
        $this->approval_notification_subject = esc_html__('A new reservation requires your approval', 'teambooking');
        $this->approval_notification_text = esc_html__('You have just got a new reservation that requires your approval', 'teambooking');
        $this->allow_customer_cancellation = TRUE;
        $this->allow_customer_cancellation_until = 1 * DAY_IN_SECONDS;
        $this->redirect = FALSE;
        $this->redirect_url = '';
    }

    //------------------------------------------------------------

    public function setConfirmOn()
    {
        $this->confirm = TRUE;
    }

    public function setNotifyOn()
    {
        $this->notify = TRUE;
    }

    public function setCancellationEmailOn()
    {
        $this->send_cancellation_email = TRUE;
    }

    public function setLoggedOnlyOff()
    {
        $this->logged_only = FALSE;
    }

    public function setShowCoworkerOff()
    {
        $this->show_coworker = FALSE;
    }

    public function setShowCoworkerUrlOff()
    {
        $this->show_coworker_url = FALSE;
    }

    public function setShowSoldoutOff()
    {
        $this->show_soldout = FALSE;
    }

    public function setActiveOn()
    {
        $this->active = TRUE;
    }

    public function setIncludeFilesAsAttachmentOff()
    {
        $this->include_files_as_attachment = FALSE;
    }

    public function setName($name)
    {
        $this->booking_name = tbFilterInput($name);
    }

    public function setId($id)
    {
        $this->booking_id = tbFilterInput($id, TRUE);
    }

    public function getLocationSetting()
    {
        if (!$this->location_setting) {
            if ($this->booking_class == 'service') {
                return 'none';
            } else {
                return 'inherited';
            }
        } else {
            return $this->location_setting;
        }
    }

    public function setLocationSetting($setting)
    {
        $this->location_setting = $setting;
    }

    public function getLocationAddress()
    {
        if (!$this->location_address) {
            return '';
        } else {
            return $this->location_address;
        }
    }

    public function setLocationAddress($address)
    {
        $this->location_address = $address;
    }

    public function getApproveRule()
    {
        if (!$this->approve_rule || $this->getPaymentMustBeDone() !== 'later') {
            return 'none';
        } else {
            return $this->approve_rule;
        }
    }

    public function setApproveRule($rule)
    {
        if ($rule === 'admin' || $rule === 'coworker') {
            $this->approve_rule = $rule;
        } else {
            $this->approve_rule = 'none';
        }
    }

    public function getPaymentMustBeDone()
    {
        if (!isset($this->payment_must_be_done)) {
            return 'immediately';
        } else {
            return $this->payment_must_be_done;
        }
    }

    public function setPaymentMustBeDone($how)
    {
        $this->payment_must_be_done = $how;
    }

    public function getFreeUntilApproval()
    {
        if (!$this->free_until_approval) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function setFreeUntilApproval($boolean)
    {
        if (!$boolean) {
            $this->free_until_approval = FALSE;
        } else {
            $this->free_until_approval = TRUE;
        }
    }

    public function setSendApprovalNotification($boolean)
    {
        if (!$boolean) {
            $this->send_approval_notification = FALSE;
        } else {
            $this->send_approval_notification = TRUE;
        }
    }

    public function getSendApprovalNotification()
    {
        if (!$this->send_approval_notification) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function setApprovalNotificationSubject($text)
    {
        $this->approval_notification_subject = $text;
    }

    public function getApprovalNotificationSubject()
    {
        if (!$this->approval_notification_subject) {
            return __('A new reservation requires your approval', 'teambooking');
        } else {
            return $this->approval_notification_subject;
        }

    }

    public function setApprovalNotificationText($text)
    {
        $this->approval_notification_text = $text;
    }

    public function getApprovalNotificationText()
    {
        if (!$this->approval_notification_text) {
            return __('You have just got a new reservation that requires your approval', 'teambooking');
        } else {
            return $this->approval_notification_text;
        }

    }

    public function setAllowCustomerCancellation($boolean)
    {
        if (!$boolean) {
            $this->allow_customer_cancellation = FALSE;
        } else {
            $this->allow_customer_cancellation = TRUE;
        }
    }

    public function getAllowCustomerCancellation()
    {
        if (!$this->allow_customer_cancellation) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function setAllowCustomerCancellationUntil($seconds)
    {
        $this->allow_customer_cancellation_until = (int)$seconds;
    }

    public function getAllowCustomerCancellationUntil()
    {
        if (!$this->allow_customer_cancellation_until) {
            return 1 * DAY_IN_SECONDS;
        } else {
            return $this->allow_customer_cancellation_until;
        }
    }

    public function setRedirect($boolean)
    {
        if (!$boolean) {
            $this->redirect = FALSE;
        } else {
            $this->redirect = TRUE;
        }
    }

    public function getRedirect()
    {
        if (!$this->redirect) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function setRedirectUrl($url)
    {
        $this->redirect_url = $url;
    }

    public function getRedirectUrl()
    {
        if (!$this->redirect_url) {
            return '';
        } else {
            return $this->redirect_url;
        }
    }

    public function getServiceColor()
    {
        if (!$this->service_color) {
            return '#e07b53'; // Orange
        } else {
            return $this->service_color;
        }
    }

    public function setServiceColor($color)
    {
        $this->service_color = $color;
    }

    public function getDurationRule()
    {
        if (!$this->duration_rule) {
            return 'coworker';
        } else {
            return $this->duration_rule;
        }
    }

    public function setDurationRule($rule)
    {
        $this->duration_rule = $rule;
    }

    public function getDefaultDuration()
    {
        if (!$this->default_duration) {
            return 3600;
        } else {
            return $this->default_duration;
        }
    }

    public function setDefaultDuration($seconds)
    {
        $this->default_duration = $seconds;
    }

    public function setClass($class)
    {
        $this->booking_class = $class;
        if ($this->booking_class == "event") {
            $this->form_fields = new TeamBookingFormFields();
            $this->attendees = 10;
            $this->max_reservations_per_user = 2;
            $this->show_reservations_left = TRUE;
        } elseif ($this->booking_class == "appointment" || $this->booking_class == "service") {
            $this->form_fields = new TeamBookingFormFields();
            $this->attendees = NULL;
            $this->show_reservations_left = FALSE;
        }
        if ($this->booking_class == "service") {
            $this->coworker_assignment_rule = "equal";
            #$this->coworker_assignment_rule = "direct";
            #$this->coworker_assignment_rule = "random";
        }
    }

    public function setIncludeFilesAsAttachmentOn()
    {
        $this->include_files_as_attachment = TRUE;
    }

    public function setNotifyOff()
    {
        $this->notify = FALSE;
    }

    public function setConfirmOff()
    {
        $this->confirm = FALSE;
    }

    public function setCancellationEmailOff()
    {
        $this->send_cancellation_email = FALSE;
    }

    public function setLoggedOnlyOn()
    {
        $this->logged_only = TRUE;
    }

    public function setShowReservationsLeftOn()
    {
        $this->show_reservations_left = TRUE;
    }

    public function setShowReservationsLeftOff()
    {
        $this->show_reservations_left = FALSE;
    }

    public function setShowSoldoutOn()
    {
        $this->show_soldout = TRUE;
    }

    public function setShowCoworkerOn()
    {
        $this->show_coworker = TRUE;
    }

    public function setShowCoworkerUrlOn()
    {
        $this->show_coworker_url = TRUE;
    }

    public function setAssignmentRule($rulename)
    {
        $this->coworker_assignment_rule = $rulename;
    }

    public function setCoworkerForDirectAssignment($coworker_id)
    {
        $this->coworker_id_for_direct_assignment = $coworker_id;
    }

    public function setActiveOff()
    {
        $this->active = FALSE;
    }

    public function getName()
    {
        return tbUnfilterInput($this->booking_name);
    }

    public function getId()
    {
        return $this->booking_id;
    }

    public function getClass()
    {
        return $this->booking_class;
    }

    public function isClass($class_name)
    {
        if (strtolower($this->booking_class) == strtolower($class_name)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function isActive()
    {
        if (!isset($this->active)) {
            return TRUE;
        } else {
            return $this->active;
        }
    }

    public function getEmailForNotifications()
    {
        return $this->email_for_notifications;
    }

    public function setEmailForNotifications($email)
    {
        $this->email_for_notifications = $email;
    }

    public function getIncludeFilesAsAttachment()
    {
        if (!$this->include_files_as_attachment) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function getNotify()
    {
        return $this->notify;
    }

    public function getConfirm()
    {
        return $this->confirm;
    }

    public function getSendCancellation()
    {
        if (isset($this->send_cancellation_email)) {
            return $this->send_cancellation_email;
        } else {
            return TRUE;
        }
    }

    public function getAttendees()
    {
        return $this->attendees;
    }

    public function setAttendees($number)
    {
        $this->attendees = $number;
    }

    public function getMaxReservationsPerUser()
    {
        // Backward compatibility before 1.2.5
        if (empty($this->max_reservations_per_user)) {
            $return = $this->attendees;
        } else {
            $return = $this->max_reservations_per_user;
        }

        return $return;
    }

    public function setMaxReservationsPerUser($number)
    {
        $this->max_reservations_per_user = $number;
    }

    public function getShowReservationsLeft()
    {
        return $this->show_reservations_left;
    }

    public function getShowSoldout()
    {
        return $this->show_soldout;
    }

    public function getServiceInfo()
    {
        return tbUnfilterInput($this->service_info);
    }

    public function setServiceInfo($info)
    {
        $this->service_info = tbFilterInput($info);
    }

    /**
     *
     * @return TeamBookingServiceEmail
     */
    public function getFrontendEmail()
    {
        return $this->front_end_email;
    }

    public function setFrontendEmail($email)
    {
        $this->front_end_email = $email;
    }

    /**
     *
     * @return TeamBookingServiceEmail
     */
    public function getBackendEmail()
    {
        return $this->back_end_email;
    }

    public function setBackendEmail($email)
    {
        $this->back_end_email = $email;
    }

    /**
     *
     * @return TeamBookingServiceEmail
     */
    public function getCancellationEmail()
    {
        if (isset($this->cancellation_email)) {
            return $this->cancellation_email;
        } else {
            $return = new TeamBookingServiceEmail();
            $return->setBody(__('Your reservation was cancelled.', 'teambooking'));
            $return->setSubject(__('Reservation cancelled', 'teambooking'));

            return $return;
        }
    }

    public function setCancellationEmail($email)
    {
        $this->cancellation_email = $email;
    }

    /**
     *
     * @return TeamBookingFormFields
     */
    public function getFormFields()
    {
        return $this->form_fields;
    }

    public function setFormFields($form_fields)
    {
        $this->form_fields = $form_fields;
    }

    public function getLoggedOnly()
    {
        return $this->logged_only;
    }

    /**
     *
     * @return TeamBookingShowTimes
     */
    public function getShowTimes()
    {
        return $this->show_times;
    }

    /**
     *
     * @param TeamBookingShowTimes $object
     */
    public function setShowTimes($object)
    {
        $this->show_times = $object;
    }

    public function getShowCoworker()
    {
        return $this->show_coworker;
    }

    public function getShowCoworkerUrl()
    {
        if (!$this->show_coworker_url) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getAssignmentRule()
    {
        return $this->coworker_assignment_rule;
    }

    public function getCoworkerForDirectAssignment()
    {
        return $this->coworker_id_for_direct_assignment;
    }

    public function save()
    {
        $settings = tbGetSettings();
        $settings->updateService($this, $this->booking_id);

        return $settings->save();
    }

}

/**
 * Just an object where are stored the frontend show times options
 */
class TeamBookingShowTimes
{
    //------------------------------------------------------------

    private $yes; // Start + end (or All day)
    private $no;  // Nothing
    private $start_time_only; // Start only (or All day)

    public function __construct()
    {
        $this->yes = TRUE;
    }

    //------------------------------------------------------------

    public function setValue($var_name)
    {
        switch ($var_name) {
            case "yes":
                $this->setYes();
                break;
            case "no":
                $this->setNo();
                break;
            case 'start_time_only':
                $this->setStartOnly();
                break;
            default:
                return FALSE;
        }

        return TRUE;
    }

    private function setYes()
    {
        $this->yes = TRUE;
        $this->no = FALSE;
        $this->start_time_only = FALSE;
    }

    private function setNo()
    {
        $this->yes = FALSE;
        $this->no = TRUE;
        $this->start_time_only = FALSE;
    }

    private function setStartOnly()
    {
        $this->yes = FALSE;
        $this->no = FALSE;
        $this->start_time_only = TRUE;
    }

    public function getValue()
    {
        foreach (get_object_vars($this) as $var => $value) {
            if ($value) {
                return $var;
            }
        }

        return NULL;
    }

    public function getOptionsList()
    {
        return get_object_vars($this);
    }

    public function getOptionDescription($var_name)
    {
        switch ($var_name) {
            case "yes":
                return __('Show start/end', 'teambooking');
            case "no":
                return __("Don't show", 'teambooking');
            case 'start_time_only':
                return __('Show start time only', 'teambooking');
            default:
                return NULL;
        }
    }

}

/**
 * Class TeamBookingServiceEmail
 */
class TeamBookingServiceEmail
{

    //------------------------------------------------------------

    private $subject;
    private $body;

    //------------------------------------------------------------

    public function getSubject()
    {
        return tbUnfilterInput($this->subject);
    }

    public function setSubject($text)
    {
        $this->subject = tbFilterInput($text);
    }

    public function getBody()
    {
        return tbUnfilterInput($this->body);
    }

    public function setBody($text)
    {
        $this->body = tbFilterInput($text);
    }

}

/**
 * Class TeamBookingFormFields
 */
class TeamBookingFormFields
{

    //------------------------------------------------------------

    /** @var TeamBookingFormTextField */
    public $first_name;

    /** @var TeamBookingFormTextField */
    public $second_name;

    /** @var TeamBookingFormTextField */
    public $email;

    /** @var TeamBookingFormTextField */
    public $address;

    /** @var TeamBookingFormTextField */
    public $phone;

    /** @var array */
    public $custom_fields = array();

    public function __construct()
    {
        $this->first_name = new TeamBookingFormTextField();
        $this->second_name = new TeamBookingFormTextField();
        $this->email = new TeamBookingFormTextField();
        $this->address = new TeamBookingFormTextField();
        $this->phone = new TeamBookingFormTextField();
        $this->first_name->setHook('first_name');
        $this->first_name->setLabel(__('First name', 'teambooking'));
        $this->second_name->setHook('second_name');
        $this->second_name->setLabel(__('Last name', 'teambooking'));
        $this->email->setHook('email');
        $this->email->setLabel(__('Email', 'teambooking'));
        $this->address->setHook('address');
        $this->address->setLabel(__('Address', 'teambooking'));
        $this->phone->setHook('phone');
        $this->phone->setLabel(__('Phone number', 'teambooking'));
        // Defaults
        $this->first_name->setOn();
        $this->first_name->setRequiredOn();
        $this->second_name->setOn();
        $this->second_name->setRequiredOn();
        $this->email->setOn();
        $this->email->setRequiredOn();
    }

    //------------------------------------------------------------

    public function getFirstName()
    {
        return $this->first_name;
    }

    public function getSecondName()
    {
        return $this->second_name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    //------------------------------------------------------------

    public function getCustomFields()
    {
        return array_filter($this->custom_fields);
    }

    public function addCustomField(TeamBookingFormTextField $field)
    {
        $this->custom_fields[] = $field;
    }

    public function updateCustomField($old_hook, TeamBookingFormTextField $new_field)
    {
        foreach ($this->custom_fields as $key => $field) {
            if (is_object($field)) {
                if ($field->getHook() == $old_hook) {
                    $this->custom_fields[$key] = $new_field;
                }
            }
        }
    }

    public function dropCustomField($hook)
    {
        foreach ($this->custom_fields as $key => $field) {
            if (is_object($field)) {
                if ($field->getHook() == $hook) {
                    unset($this->custom_fields[$key]);
                    $this->custom_fields = array_values($this->custom_fields);
                }
            }
        }
    }

    public function saveCustomFields(array $custom_fields)
    {
        $this->custom_fields = $custom_fields;
    }

    //------------------------------------------------------------

    public function getLabelFromHook($hook)
    {
        // Loop through built-in fields
        foreach ($this->getBuiltInFields() as $field) {
            if ($field->getHook() == $hook) {
                return $field->getLabel();
            }
        }
        // Loop through custom fields
        if (!empty($this->custom_fields)) {
            foreach ($this->custom_fields as $field) {
                if (is_object($field)) {
                    if ($field->getHook() == $hook) {
                        return $field->getLabel();
                    }
                }
            }
        }

        return FALSE;
    }

    //------------------------------------------------------------

    public function getBuiltInFields()
    {
        $return = array();
        $return['first_name'] = $this->first_name;
        $return['second_name'] = $this->second_name;
        $return['email'] = $this->email;
        $return['address'] = $this->address;
        $return['phone'] = $this->phone;

        return $return;
    }

    //------------------------------------------------------------

    public function isHookDuplicate($hook)
    {
        $hooks = $this->getHookList(TRUE);

        return in_array($hook, $hooks);
    }

    //------------------------------------------------------------

    /**
     * List of active hooks
     *
     * @param bool|FALSE $files_too
     * @return array
     */
    public function getHookList($files_too = FALSE)
    {
        $return = array();
        // Loop through built-in fields
        foreach ($this->getBuiltInFields() as $field) {
            if ($field->getIsActive()) {
                $return[] = $field->getHook();
            }
        }
        // Loop through custom fields
        if (!empty($this->custom_fields)) {
            foreach ($this->custom_fields as $field) {
                if (is_object($field)) {
                    if ($field->getIsActive()) {
                        if (get_class($field) != 'TeamBookingFormFileUpload' || $files_too == TRUE) {
                            $return[] = $field->getHook();
                        }
                    }
                }
            }
        }

        return $return;
    }

}
