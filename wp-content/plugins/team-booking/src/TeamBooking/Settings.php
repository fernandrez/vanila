<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

class TeamBookingSettings
{

    //------------------------------------------------------------

    ///////////////////////
    //   COWORKER DATA   //
    ///////////////////////
    private $coworkers_data;
    private $coworkers_url_array;
    ///////////////////////
    //   SERVICE DATA    //
    ///////////////////////
    private $bookings;
    ///////////////////////
    //   LOGS            //
    ///////////////////////
    private $logs;
    private $error_logs;
    ///////////////////////
    // UNINSTALL OPTIONS //
    ///////////////////////
    private $drop_tables_on_unistall;
    ///////////////////////
    //   ICAL SETTINGS   //
    ///////////////////////
    private $show_ical;
    /////////////////////////////
    //  GMAP LIBRARY LOADING   //
    /////////////////////////////
    private $skip_gmaps_library;
    /////////////////////////////
    // GOOGLE PROJECT SETTINGS //
    /////////////////////////////
    private $application_cliend_id;
    private $application_client_secret;
    private $application_project_name;
    /////////////////////////////
    // FRONTEND STYLE SETTINGS //
    /////////////////////////////
    private $color_background;
    private $color_weekline;
    private $pattern;
    private $color_free_slot;
    private $color_soldout_slot;
    private $group_slots_by;
    private $border;
    private $price_tag_color;
    private $numbered_dots_lower_bound;
    private $numbered_dots_logic;
    private $map_style;
    private $fix_62dot5;
    //////////////////////
    //  DEBUG SETTINGS  //
    //////////////////////
    private $silent_debug;
    ////////////////
    //  VERSION   //
    ////////////////
    private $version;
    ////////////////
    //  TWEAKS    //
    ////////////////
    private $first_month_with_free_slot_is_shown;
    private $autofill_reservation_form;
    private $max_google_api_results;
    private $database_reservation_timeout;
    private $max_pending_time;
    ////////////////////////
    //  REGISTRATION URL  //
    ////////////////////////
    private $registration_url;
    /////////////////////////
    //  PAYMENT GATEWAYS   //
    /////////////////////////
    private $payment_gateways;
    private $currency_code;

    public function __construct()
    {
        /////////////////////////////
        // FRONTEND STYLE SETTINGS //
        /////////////////////////////
        $this->color_background = "#FFFFFF";
        $this->color_weekline = "#FFBDBD";
        $this->color_free_slot = "#A1CF64";
        $this->color_soldout_slot = "#d95c5c";
        $this->price_tag_color = 'yellow';
        $this->border = array(
            'size'   => 5,
            //px
            'color'  => '#CCCCCC',
            //HEX
            'radius' => 0
            //px
        );
        $this->pattern = array(
            'weekline' => 0,
            // no pattern
            'calendar' => 0,
            // no pattern
        );
        $this->fix_62dot5 = FALSE;
        $this->numbered_dots_logic = 'slots'; // possible values: slots, tickets, hide
        $this->numbered_dots_lower_bound = 0;
        $this->map_style = 0;
        $this->setGroupSlotsByTime();
        ////////////////
        //  VERSION   //
        ////////////////
        $this->version = TEAMBOOKING_VERSION;
        ////////////////
        //  TWEAKS    //
        ////////////////
        $this->autofill_reservation_form = TRUE;
        $this->database_reservation_timeout = 60 * DAY_IN_SECONDS;
        $this->first_month_with_free_slot_is_shown = FALSE;
        $this->max_google_api_results = 600;
        $this->max_pending_time = 3600;
        /////////////////////////
        //  PAYMENT GATEWAYS   //
        /////////////////////////
        $this->payment_gateways = array();
        $this->addPaymentGatewaySettingObject(new TeamBooking_PaymentGateways_Stripe_Settings);
        $this->addPaymentGatewaySettingObject(new TeamBooking_PaymentGateways_PayPal_Settings);
        ///////////////////////
        //   COWORKER DATA   //
        ///////////////////////
        $this->coworkers_url_array = array();
        ///////////////////////
        // UNINSTALL OPTIONS //
        ///////////////////////
        $this->drop_tables_on_unistall = FALSE;
        ///////////////////////
        //   ICAL SETTINGS   //
        ///////////////////////
        $this->show_ical = TRUE;
        /////////////////////////////
        //  GMAP LIBRARY LOADING   //
        /////////////////////////////
        $this->skip_gmaps_library = FALSE;
    }

    //------------------------------------------------------------

    ///////////////////////
    //                   //
    //   COWORKER DATA   //
    //                   //
    ///////////////////////

    /**
     * Returns the Coworkers data array
     *
     * @return array
     */
    public function getCoworkersData()
    {
        if (!empty($this->coworkers_data)) {
            return $this->coworkers_data;
        } else {
            return array();
        }
    }

    /**
     * Set / update the data for a specific coworker
     *
     * @param TeamBookingCoworker $data
     * @param integer $coworker_id
     */
    public function updateCoworkerData(TeamBookingCoworker $data, $coworker_id)
    {
        $this->coworkers_data[$coworker_id] = $data;
    }

    /**
     * Drop the data of a specific coworker
     *
     * @param integer $coworker_id
     */
    public function dropCoworkerData($coworker_id)
    {
        unset($this->coworkers_data[$coworker_id]);
    }

    /**
     * Get the data for a specific coworker.
     *
     * If the coworker has no data saved yet,
     * then a new instance of data class will be returned.
     *
     * @param integer $id
     * @return \TeamBookingCoworker
     */
    public function getCoworkerData($id)
    {
        $coworkers_data = $this->getCoworkersData();
        if (isset($coworkers_data[$id])) {
            return $coworkers_data[$id];
        } else {
            return new TeamBookingCoworker($id);
        }
    }

    /**
     * Save/update a coworker URL
     *
     * @param integer $coworker_id
     * @param string $url
     */
    public function updateCoworkerUrl($coworker_id, $url)
    {
        $this->coworkers_url_array[$coworker_id] = $url;
    }

    /**
     * Drop a coworker URL
     *
     * @param integer $coworker_id
     */
    public function dropCoworkerUrl($coworker_id)
    {
        unset($this->coworkers_url_array[$coworker_id]);
    }

    /**
     * Get a coworker URL
     *
     * @param integer $coworker_id
     * @return string the coworker URL
     */
    public function getCoworkerUrl($coworker_id)
    {
        if (!isset($this->coworkers_url_array[$coworker_id])) {
            // No customized coworker URL, returning default
            return get_site_url() . '/?author=' . $coworker_id;
        } else {
            if (empty($this->coworkers_url_array[$coworker_id])) {
                // Empty customized coworker URL, returning default
                return get_site_url() . '/?author=' . $coworker_id;
            } else {
                return $this->coworkers_url_array[$coworker_id];
            }
        }
    }

    //------------------------------------------------------------

    ///////////////////////
    //                   //
    //   SERVICE DATA    //
    //                   //
    ///////////////////////

    /**
     * Get the array of service objects.
     *
     * Structure:
     * $array[service_id] = {TeamBookingType object}
     *
     * @return array Array of services
     */
    public function getServices()
    {
        if (!empty($this->bookings)) {
            return $this->bookings;
        } else {
            return array();
        }
    }

    /**
     * Update/save a service
     *
     * @param TeamBookingType $service
     * @param integer $id
     */
    public function updateService(TeamBookingType $service, $id)
    {
        $this->bookings[$id] = $service;
    }

    /**
     * Drop a service
     *
     * @param string $service_id
     */
    public function dropService($service_id)
    {
        unset($this->bookings[$service_id]);
    }

    /**
     * Get service object
     *
     * @param string $id
     * @return TeamBookingType
     * @throws Exception
     */
    public function getService($id)
    {
        $services = $this->getServices();
        if (isset($services[$id])) {
            return $services[$id];
        } else {
            throw new Exception('Service id not found');
        }
    }

    /**
     * Get the list of all services IDs
     *
     * @return array IDs list
     */
    public function getServiceIdList()
    {
        // TODO: using array_keys instead?
        $services = $this->getServices();
        $ids = array();
        foreach ($services as $service) {
            /* @var $service TeamBookingType */
            $ids[] = $service->getId();
        }

        return $ids;
    }

    //------------------------------------------------------------

    ///////////////////////
    //                   //
    //   LOGS            //
    //                   //
    ///////////////////////

    /**
     * Drop a reservation log
     *
     * @param string $log_key
     */
    public function dropLog($log_key)
    {
        unset($this->logs[$log_key]);
    }

    /**
     * Drop an error log
     *
     * @param string $log_key
     */
    public function dropErrorLog($log_key)
    {
        unset($this->error_logs[$log_key]);
    }

    /**
     * Set the error logs array
     *
     * @param array $error_logs
     */
    public function setErrorLogs(array $error_logs)
    {
        $this->error_logs = $error_logs;
    }

    /**
     * Get the error logs array
     *
     * @return array
     */
    public function getErrorLogs()
    {
        if (!$this->error_logs) {
            return array();
        } else {
            return $this->error_logs;
        }
    }

    /**
     * DEPRECATED (kept for backward compatibility)
     *
     * @return array
     */
    public function getLogs()
    {
        if (!$this->logs) {
            return array();
        } else {
            return $this->logs;
        }
    }

    //------------------------------------------------------------

    ///////////////////////
    //                   //
    // UNINSTALL OPTIONS //
    //                   //
    ///////////////////////

    public function setDropTablesOnUninstallYes()
    {
        $this->drop_tables_on_unistall = TRUE;
    }

    public function setDropTablesOnUninstallNo()
    {
        $this->drop_tables_on_unistall = FALSE;
    }

    public function getDropTablesOnUninstall()
    {
        if (!$this->drop_tables_on_unistall) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    //------------------------------------------------------------

    ///////////////////////
    //                   //
    //   ICAL SETTINGS   //
    //                   //
    ///////////////////////

    public function setShowIcalYes()
    {
        $this->show_ical = TRUE;
    }

    public function setShowIcalNo()
    {
        $this->show_ical = FALSE;
    }

    public function getShowIcal()
    {
        if (!$this->show_ical) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    //------------------------------------------------------------

    /////////////////////////////
    //                         //
    //  GMAP LIBRARY LOADING   //
    //                         //
    /////////////////////////////

    public function setSkipGmapLibsYes()
    {
        $this->skip_gmaps_library = TRUE;
    }

    public function setSkipGmapLibsNo()
    {
        $this->skip_gmaps_library = FALSE;
    }

    public function getSkipGmapLibs()
    {
        if (!$this->skip_gmaps_library) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    //------------------------------------------------------------

    /////////////////////////////
    //                         //
    // GOOGLE PROJECT SETTINGS //
    //                         //
    /////////////////////////////

    public function setApplicationClientId($id)
    {
        $this->application_cliend_id = $id;
    }

    public function getApplicationClientId()
    {
        return $this->application_cliend_id;
    }

    public function setApplicationClientSecret($secret)
    {
        $this->application_client_secret = $secret;
    }

    public function getApplicationClientSecret()
    {
        return $this->application_client_secret;
    }

    public function setApplicationProjectName($name)
    {
        $this->application_project_name = $name;
    }

    public function getApplicationProjectName()
    {
        return $this->application_project_name;
    }

    //------------------------------------------------------------

    /////////////////////////////
    //                         //
    // FRONTEND STYLE SETTINGS //
    //                         //
    /////////////////////////////

    public function setColorBackground($color)
    {
        $this->color_background = $color;
    }

    public function getColorBackground()
    {
        return $this->color_background;
    }

    public function setColorWeekLine($color)
    {
        $this->color_weekline = $color;
    }

    public function getColorWeekLine()
    {
        return $this->color_weekline;
    }

    public function setPatternCalendar($int)
    {
        $this->pattern['calendar'] = $int;
    }

    public function setPatternWeekline($int)
    {
        $this->pattern['weekline'] = $int;
    }

    public function getPattern()
    {
        if (!is_array($this->pattern)) {
            return array(
                'calendar' => 0,
                'weekline' => 0,
            );
        } else {
            return $this->pattern;
        }
    }

    public function setColorFreeSlot($color)
    {
        $this->color_free_slot = $color;
    }

    public function getColorFreeSlot()
    {
        return $this->color_free_slot;
    }

    public function setColorSoldoutSlot($color)
    {
        $this->color_soldout_slot = $color;
    }

    public function getColorSoldoutSlot()
    {
        if (!$this->color_soldout_slot) {
            return '#d95c5c';
        } else {
            return $this->color_soldout_slot;
        }
    }

    public function setNumberedDotsLogic($logic)
    {
        $this->numbered_dots_logic = $logic;
    }

    public function getNumberedDotsLogic()
    {
        if (!$this->numbered_dots_logic) {
            return 'slots';
        } else {
            return $this->numbered_dots_logic;
        }
    }

    public function setNumberedDotsLowerBound($number)
    {
        $this->numbered_dots_lower_bound = (int)abs($number);
    }

    public function getNumberedDotsLowerBound()
    {
        if (!$this->numbered_dots_lower_bound) {
            return 0;
        } else {
            return $this->numbered_dots_lower_bound;
        }
    }

    public function setMapStyle($style)
    {
        $this->map_style = (int)$style;
    }

    public function getMapStyle($id_only = FALSE)
    {
        if (!$this->map_style) {
            $style_id = 0;
        } else {
            $style_id = $this->map_style;
        }
        if (!$id_only) {
            include TEAMBOOKING_PATH . 'includes/tb_mapstyles.php';

            return $tb_mapstyles[$style_id];
        } else {
            return $style_id;
        }
    }

    public function setFix62dot5On()
    {
        $this->fix_62dot5 = TRUE;
    }

    public function setFix62dot5Off()
    {
        $this->fix_62dot5 = FALSE;
    }

    public function getFix62dot5()
    {
        if (!$this->fix_62dot5) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function setBorderSize($int)
    {
        if (!is_array($this->border)) {
            $this->border = array();
        }
        $this->border['size'] = (int)$int;
    }

    public function setBorderColor($hex)
    {
        if (!is_array($this->border)) {
            $this->border = array();
        }
        $this->border['color'] = $hex;
    }

    public function setBorderRadius($int)
    {
        if (!is_array($this->border)) {
            $this->border = array();
        }
        $this->border['radius'] = (int)$int;
    }

    public function getBorder()
    {
        if (!is_array($this->border)) {
            return array(
                'size'   => 5,
                'color'  => '#CCCCCC',
                'radius' => 0,
            );
        } else {
            return $this->border;
        }
    }

    public function setGroupSlotsByTime()
    {
        $this->group_slots_by = 'time';
    }

    public function isGroupSlotsByTime()
    {
        if ($this->group_slots_by == 'time') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function setGroupSlotsByCoworker()
    {
        $this->group_slots_by = 'coworker';
    }

    public function isGroupSlotsByCoworker()
    {
        if ($this->group_slots_by == 'coworker') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function setGroupSlotsByService()
    {
        $this->group_slots_by = 'service';
    }

    public function isGroupSlotsByService()
    {
        if ($this->group_slots_by == 'service') {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function setPriceTagColor($color)
    {
        $this->price_tag_color = $color;
    }

    public function getPriceTagColor()
    {
        if (!$this->price_tag_color) {
            return 'yellow';
        } else {
            return $this->price_tag_color;
        }
    }

    //------------------------------------------------------------

    //////////////////////
    //                  //
    //  DEBUG SETTINGS  //
    //                  //
    //////////////////////

    public function setSilentDebugOn()
    {
        $this->silent_debug = TRUE;
    }

    public function setSilentDebugOff()
    {
        $this->silent_debug = FALSE;
    }

    public function getSilentDebug()
    {
        return $this->silent_debug;
    }

    //------------------------------------------------------------

    ////////////////
    //            //
    //  VERSION   //
    //            //
    ////////////////

    public function setVersion($version)
    {
        $this->version = $version;
    }

    public function getVersion()
    {
        return $this->version;
    }

    //------------------------------------------------------------

    ////////////////
    //            //
    //  TWEAKS    //
    //            //
    ////////////////

    public function setAutofillReservationFormOn()
    {
        $this->autofill_reservation_form = TRUE;
    }

    public function setAutofillReservationFormOnAndHide()
    {
        $this->autofill_reservation_form = 'hide';
    }

    public function setAutofillReservationFormOff()
    {
        $this->autofill_reservation_form = FALSE;
    }

    public function getAutofillReservationForm()
    {
        return $this->autofill_reservation_form;
    }

    public function setFirstMonthWithFreeSlotIsShownOn()
    {
        $this->first_month_with_free_slot_is_shown = TRUE;
    }

    public function setFirstMonthWithFreeSlotIsShownOff()
    {
        $this->first_month_with_free_slot_is_shown = FALSE;
    }

    public function isFirstMonthWithFreeSlotShown()
    {
        if (!$this->first_month_with_free_slot_is_shown) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function setMaxGoogleApiResults($number)
    {
        $this->max_google_api_results = $number;
    }

    public function getMaxGoogleApiResults()
    {
        if (!isset($this->max_google_api_results)) {
            return 600;
        } else {
            return $this->max_google_api_results;
        }
    }

    public function setDatabaseReservationTimeout($seconds)
    {
        $this->database_reservation_timeout = $seconds;
    }

    public function getDatabaseReservationTimeout()
    {
        if (!isset($this->database_reservation_timeout)) {
            return 60 * DAY_IN_SECONDS;
        } else {
            return $this->database_reservation_timeout;
        }
    }

    public function setMaxPendingTime($seconds)
    {
        $this->max_pending_time = $seconds;
    }

    public function getMaxPendingTime()
    {
        if ($this->max_pending_time !== NULL) {
            return $this->max_pending_time;
        } else {
            return 3600;
        }
    }

    //------------------------------------------------------------

    ////////////////////////
    //                    //
    //  REGISTRATION URL  //
    //                    //
    ////////////////////////

    public function setRegistrationUrl($url)
    {
        $this->registration_url = $url;
    }

    public function getRegistrationUrl()
    {
        if (empty($this->registration_url)) {
            $url = get_bloginfo('wpurl') . '/wp-login.php?action=register';

            return $url;
        } else {
            return $this->registration_url;
        }
    }

    //------------------------------------------------------------

    ///////////////////////
    //                   //
    // PAYMENT GATEWAYS  //
    //                   //
    ///////////////////////

    public function addPaymentGatewaySettingObject(TeamBooking_PaymentGateways_Settings $object)
    {
        $this->payment_gateways[$object->getGatewayId()] = $object;
    }

    public function getPaymentGatewaySettingObjects()
    {
        if (isset($this->payment_gateways)) {
            return $this->payment_gateways;
        } else {
            return array();
        }
    }

    public function dropPaymentGatewaySettingObject($id)
    {
        unset($this->payment_gateways[$id]);
    }

    /**
     *
     * @param string $gateway_id
     * @return boolean|TeamBooking_PaymentGateways_Settings|TeamBooking_PaymentGateways_PayPal_Settings|TeamBooking_PaymentGateways_Stripe_Settings
     */
    public function getPaymentGatewaySettingObject($gateway_id)
    {
        if (isset($this->payment_gateways[$gateway_id])) {
            return $this->payment_gateways[$gateway_id];
        } else {
            return FALSE;
        }
    }

    public function getPaymentGatewaysActive()
    {
        $results_array = array();
        foreach ($this->getPaymentGatewaySettingObjects() as $gateway) {
            /* @var $gateway TeamBooking_PaymentGateways_Settings */
            if ($gateway->isActive()) {
                $results_array[] = $gateway;
            }
        }

        return $results_array;
    }

    public function thereIsAtLeastOneActivePaymentGateway()
    {
        $active_gateways = $this->getPaymentGatewaysActive();
        if (!empty($active_gateways)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function setCurrencyCode($code)
    {
        $this->currency_code = $code;
    }

    public function getCurrencyCode()
    {
        return $this->currency_code;
    }

    //------------------------------------------------------------

    ///////////////////////
    //                   //
    //  SAVE SETTINGS    //
    //                   //
    ///////////////////////

    public function save()
    {
        if (update_option('team_booking', $this)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //------------------------------------------------------------

    ///////////////////////
    //                   //
    //  CLEAN SETTINGS   //
    //                   //
    ///////////////////////

    public function clean()
    {
        unset($this->paypal_email);
        unset($this->paypal_sandbox_test);
        unset($this->paypal_save_ipn_logs);
        unset($this->after_payment_url);
        unset($this->max_pending_time);
        unset($this->revoke_tokens_on_uninstall);
    }

}
