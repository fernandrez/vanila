jQuery(document).ready(function ($) {
// Modals
    $('#tb-personal-token-revoke-modal')
        .uiModal({
            blurring: true,
            transition: 'fade up',
        })
        .uiModal('attachEvents', '#team_booking_revoke_personal_token', 'show')
        .uiModal('setting', 'onApprove', function () {
            $.post(
                TB_vars.post_url,
                {
                    // wp action
                    action: 'option_submit',
                    // send the nonce along with the request
                    _wpnonce: TB_vars.wpNonce,
                    team_booking_revoke_personal_token_confirm: true
                },
                function (response) {
                    window.location.href = response;
                }
            );
        })
    ;
    $('#tb-personal-add-calendar-modal')
        .uiModal({
            blurring: true,
            transition: 'fade up',
        })
        .uiModal('attachEvents', '#tb-add-calendar', 'show')
        .uiModal('setting', 'onApprove', function ($clicked) {
            $clicked.addClass('loading');
            $('#tbk-gcal-not-found').hide();
            var calendar_id = $('#tb-personal-add-calendar-id').val();
            $.post(
                TB_vars.post_url,
                {
                    // wp action
                    action: 'option_submit',
                    // send the nonce along with the request
                    _wpnonce: TB_vars.wpNonce,
                    team_booking_add_google_calendar: true,
                    calendar_id: calendar_id
                },
                function (response) {
                    if (response === '404') {
                        $('#tbk-gcal-not-found').show();
                        $clicked.removeClass('loading');
                    } else {
                        window.location.href = response;
                    }
                }
            );
            return false;
        })
    ;
    $('.tb-remove-calendar-id').click(function () {
        var calendar_id_key = $(this).data('key');
        $('#tb-personal-remove-calendar-modal')
            .uiModal({
                blurring: true,
                transition: 'fade up',
            })
            .uiModal('setting', 'onApprove', function () {
                $.post(
                    TB_vars.post_url,
                    {
                        // wp action
                        action: 'option_submit',
                        // send the nonce along with the request
                        _wpnonce: TB_vars.wpNonce,
                        team_booking_remove_google_calendar: true,
                        calendar_id_key: calendar_id_key
                    },
                    function (response) {
                        window.location.href = response;
                    }
                );
            })
            .uiModal('show')
        ;
    });
    $('.tbk-revoke-coworker-token').click(function (e) {
        e.preventDefault();
        var coworker_id = $(this).data('coworker');
        var coworker_name = $(this).data('name');
        $('#tb-coworker-token-revoke-modal')
            .uiModal({
                blurring: true,
                transition: 'fade up',
            })
            .uiModal('setting', 'onShow', function () {
                $('#revoke-coworker-name').html(coworker_name);
            })
            .uiModal('setting', 'onApprove', function () {
                $.post(
                    TB_vars.post_url,
                    {
                        // wp action
                        action: 'option_submit',
                        // send the nonce along with the request
                        _wpnonce: TB_vars.wpNonce,
                        team_booking_revoke_coworker_token_confirm: true,
                        coworker_id: coworker_id
                    },
                    function (response) {
                        window.location.href = response;
                    }
                );
            })
            .uiModal('show')
        ;
    });
    $('[id^=team_booking_coworker_settings_]').on('click', function (e) {
        e.preventDefault();
        var coworker_id = $(this).data('coworker');
        var modal_id = '#tb-coworker-settings-modal-' + coworker_id;
        $(modal_id)
            .uiModal({
                blurring: true,
                transition: 'fade up',
            })
            .uiModal('setting', 'onApprove', function () {
                var form_data = $('#team-booking-coworker-settings-form-' + coworker_id).serialize();
                $.post(
                    TB_vars.post_url,
                    {
                        // wp action
                        action: 'option_submit',
                        // send the nonce along with the request
                        _wpnonce: TB_vars.wpNonce,
                        team_booking_save_coworker_settings: true,
                        coworker_id: coworker_id,
                        form_data: form_data
                    },
                    function (response) {
                        window.location.href = response;
                    }
                );
            })
            .uiModal('show')
        ;
    });
    $('.tb-remove-residual-data').click(function () {
        var coworker_id = $(this).data('coworker');
        $.post(
            TB_vars.post_url,
            {
                // wp action
                action: 'option_submit',
                // send the nonce along with the request
                _wpnonce: TB_vars.wpNonce,
                team_booking_remove_coworker_residual: true,
                coworker_id: coworker_id,
            },
            function (response) {
                window.location.href = response;
            }
        );
    });
    $('.clean-error-logs').click(function (e) {
        e.preventDefault();
        $.post(
            TB_vars.post_url,
            {
                // wp action
                action: 'option_submit',
                // send the nonce along with the request
                _wpnonce: TB_vars.wpNonce,
                team_booking_clean_error_logs: true,
            },
            function (response) {
                window.location.href = response;
            }
        );
    });
    $('.team-booking-new-service').click(function (e) {
        e.preventDefault(); //
        $('#tb-booking-new-service-modal')
            .uiModal({
                blurring: true,
                transition: 'fade up',
            })
            .uiModal('setting', 'onApprove', function ($clicked) {
                if ($clicked.hasClass('loading')) {
                    return false;
                }
                $('#tbk-new-id-already-existant').hide();
                $('#tbk-new-name-already-existant').hide();
                $clicked.addClass('loading');
                var new_service_id = $('#tb-booking-new-service-id').val();
                var new_service_name = $('#tb-booking-new-service-name').val();
                var new_service_class = $('input[name=class]:checked', '#tb-booking-new-service-modal').val();
                $.post(
                    TB_vars.post_url,
                    {
                        // wp action
                        action: 'option_submit',
                        // send the nonce along with the request
                        _wpnonce: TB_vars.wpNonce,
                        team_booking_insert_booking_type: true,
                        id: new_service_id,
                        name: new_service_name,
                        class: new_service_class
                    },
                    function (response) {
                        if (response === 'ok') {
                            location.reload();
                        } else {
                            if (response === 'id_fail') {
                                $('#tbk-new-id-already-existant').show();
                            }
                            if (response === 'name_fail') {
                                $('#tbk-new-name-already-existant').show();
                            }
                            if (response === 'id_fail name_fail') {
                                $('#tbk-new-id-already-existant').show();
                                $('#tbk-new-name-already-existant').show();
                            }
                            $clicked.removeClass('loading');
                        }

                    }
                );
                return false;
            })
            .uiModal('show')
        ;
    });
    $('.team-booking-clone-service').click(function (e) {
        e.preventDefault();
        var service_id = $(this).data('serviceid');
        $('#tb-booking-clone-service-modal')
            .uiModal({
                blurring: true,
                transition: 'fade up',
            })
            .uiModal('setting', 'onShow', function () {
                $('#tbk-clone-id-already-existant').hide();
                $.post(
                    TB_vars.post_url,
                    {
                        // wp action
                        action: 'option_submit',
                        // send the nonce along with the request
                        _wpnonce: TB_vars.wpNonce,
                        team_booking_clone_service_suggestion: true,
                        service_id: service_id
                    },
                    function (response) {
                        $('#tb-booking-clone-service-new-id').val(response);
                    }
                );
            })
            .uiModal('setting', 'onApprove', function ($clicked) {
                $('#tbk-clone-id-already-existant').hide();
                $clicked.addClass('loading');
                var new_service_id = $('#tb-booking-clone-service-new-id').val();
                $.post(
                    TB_vars.post_url,
                    {
                        // wp action
                        action: 'option_submit',
                        // send the nonce along with the request
                        _wpnonce: TB_vars.wpNonce,
                        team_booking_clone_service: true,
                        service_id: service_id,
                        new_service_id: new_service_id
                    },
                    function (response) {
                        if (response === 'id already used') {
                            $('#tbk-clone-id-already-existant').show();
                            $clicked.removeClass('loading');
                        } else {
                            location.reload();
                        }
                    }
                );
                return false;
            })
            .uiModal('show')
        ;
    });
    $('.team-booking-delete-service').click(function (e) {
        e.preventDefault();
        var service = $(this).data('servicename');
        var service_id = $(this).data('serviceid');
        $('#tb-booking-delete-modal')
            .uiModal({
                blurring: true,
                transition: 'fade up',
            })
            .uiModal('setting', 'onShow', function () {
                $(this).find('.service-name').text(service)
            })
            .uiModal('setting', 'onApprove', function () {
                $.post(
                    TB_vars.post_url,
                    {
                        // wp action
                        action: 'option_submit',
                        // send the nonce along with the request
                        _wpnonce: TB_vars.wpNonce,
                        team_booking_delete_booking_confirm: true,
                        booking_id: service_id
                    },
                    function (response) {
                        window.location.href = response;
                    }
                );
            })
            .uiModal('show')
        ;
    });
    $('.team-booking-approve-reservation').click(function (e) {
        e.preventDefault();
        var reservation_id = $(this).data('reservationid');
        var modal_id = '#tb-reservation-approve-modal-' + reservation_id;
        $(modal_id + ' p.error').hide();
        $(modal_id)
            .uiModal({
                blurring: true,
                transition: 'fade up',
            })
            .uiModal('setting', 'onApprove', function ($clicked) {
                $clicked.addClass('loading');
                $.post(
                    TB_vars.post_url,
                    {
                        // wp action
                        action: 'option_submit',
                        // send the nonce along with the request
                        _wpnonce: TB_vars.wpNonce,
                        team_booking_approve_reservation_confirm: true,
                        reservation_id: reservation_id
                    },
                    function (response) {
                        if (response.substr(0, 5) === 'ERROR') {
                            $clicked.removeClass('loading');
                            $(modal_id + ' p.error').show();
                            $(modal_id + ' p.error > span').html(response);
                        } else {
                            window.location.href = response;
                        }
                    }
                );
                return false;
            })
            .uiModal('show')
        ;
    });
    $('.team-booking-confirm-pending-reservation').click(function (e) {
        e.preventDefault();
        var reservation_id = $(this).data('reservationid');
        var modal_id = '#tb-reservation-confirm-pending-modal-' + reservation_id;
        $(modal_id + ' p.error').hide();
        $(modal_id)
            .uiModal({
                blurring: true,
                transition: 'fade up',
            })
            .uiModal('setting', 'onApprove', function ($clicked) {
                $clicked.addClass('loading');
                $.post(
                    TB_vars.post_url,
                    {
                        // wp action
                        action: 'option_submit',
                        // send the nonce along with the request
                        _wpnonce: TB_vars.wpNonce,
                        team_booking_confirm_pending_reservation_confirm: true,
                        set_as_paid: $clicked.hasClass('setpaid'),
                        reservation_id: reservation_id
                    },
                    function (response) {
                        if (response.substr(0, 5) === 'ERROR') {
                            $clicked.removeClass('loading');
                            $(modal_id + ' p.error').show();
                            $(modal_id + ' p.error > span').html(response);
                        } else {
                            window.location.href = response;
                        }
                    }
                );
                return false;
            })
            .uiModal('show')
        ;
    });
    $('.team-booking-delete-reservation, .team-booking-cancel-reservation').click(function (e) {
        e.preventDefault();
        var reservation_id = $(this).data('reservationid');
        var is_pending = $(this).data('pending');
        var is_previously_confirmed = $(this).data('previouslyconfirmed');
        var filter = $('#tbk-overview-search input[name=filter]').val();
        var reason;
        var operation;
        if ($(this).hasClass('team-booking-delete-reservation')) {
            $('#tb-reservation-delete-modal #multiple-delete').hide();
            $('#tb-reservation-delete-modal #single-delete').show();
            $('#tb-reservation-delete-modal')
                .uiModal({
                    blurring: true,
                    transition: 'fade up',
                })
                .uiModal('setting', 'onApprove', function () {
                    operation = 'delete';
                    $.post(
                        TB_vars.post_url,
                        {
                            // wp action
                            action: 'option_submit',
                            // send the nonce along with the request
                            _wpnonce: TB_vars.wpNonce,
                            team_booking_delete_reservation_confirm: true,
                            operation: operation,
                            reason: reason,
                            is_pending: is_pending,
                            reservation_id: reservation_id,
                            filter: filter
                        },
                        function (response) {
                            window.location.href = response;
                        }
                    );
                })
                .uiModal('show')
            ;
        } else {
            $('#tb-reservation-cancel-modal .previously-confirmed').show();
            if (is_previously_confirmed === 'no') {
                $('#tb-reservation-cancel-modal .previously-confirmed').hide();
            }
            $('#tb-reservation-cancel-modal')
                .uiModal({
                    blurring: true,
                    transition: 'fade up',
                })
                .uiModal('setting', 'onApprove', function ($clicked) {
                    operation = 'cancel';
                    reason = $(this).find('textarea#reason').val();
                    $clicked.addClass('loading');
                    $.post(
                        TB_vars.post_url,
                        {
                            // wp action
                            action: 'option_submit',
                            // send the nonce along with the request
                            _wpnonce: TB_vars.wpNonce,
                            team_booking_delete_reservation_confirm: true,
                            operation: operation,
                            reason: reason,
                            reservation_id: reservation_id,
                            filter: filter
                        },
                        function (response) {
                            window.location.href = response;
                        }
                    );
                    return false;
                }
            )
                .
                uiModal('show')
            ;
        }
    });
    $('#tb-delete-all-reservations-modal')
        .uiModal({
            blurring: true,
            transition: 'fade up',
        })
        .uiModal('attach events', '#delete_all_tb_reservations', 'show')
        .uiModal('setting', 'onApprove', function () {
            var is_pending = $('#delete_all_tb_reservations').data('pending');
            $.post(
                TB_vars.post_url,
                {
                    // wp action
                    action: 'option_submit',
                    // send the nonce along with the request
                    _wpnonce: TB_vars.wpNonce,
                    is_pending: is_pending,
                    team_booking_delete_all_reservations_confirm: true
                },
                function (response) {
                    window.location.href = response;
                }
            );
        })
    ;
// Get the CSV log file
    $('.tb-get-csv').click(function (e) {
        e.preventDefault();
        form = $('#tb-get-csv-form');
        form.submit();
    });
// Get the XLSX log file
    $('.tb-get-xlsx').click(function (e) {
        e.preventDefault();
        form = $('#tb-get-xlsx-form');
        form.submit();
    });
// Add a custom field
    $('.tb-add-custom-field').click(function (e) {
        e.preventDefault();
        var field = $(this).data('field');
        var serviceid = $(this).data('serviceid');
        $.post(
            TB_vars.post_url,
            {
                // wp action
                action: 'option_submit',
                // send the nonce along with the request
                _wpnonce: TB_vars.wpNonce,
                team_booking_add_custom_field: true,
                field: field,
                service_id: serviceid
            },
            function (response) {
                if ($('.tb-no-custom-fields').length) {
                    $('.tb-no-custom-fields').remove();
                }
                $('.tb-custom-fields-list').append(response);
                $('body, html').animate({scrollTop: $(".tb-custom-fields-list li:last").offset().top}, 1000);
            }
        );
    });
// Remove a custom field
    $('.tb-custom-fields-list').on("click", ".tb-remove-custom-field", function (e) {
        e.preventDefault();
        var hook = $(this).data('hook');
        var serviceid = $(this).data('serviceid');
        $(this).parents().eq(5).addClass('toberemoved');
        $.post(
            TB_vars.post_url,
            {
                // wp action
                action: 'option_submit',
                // send the nonce along with the request
                _wpnonce: TB_vars.wpNonce,
                team_booking_remove_custom_field: true,
                hook: hook,
                service_id: serviceid
            },
            function (response) {
                $('.toberemoved').next('br').remove();
                $('.toberemoved').remove();
            }
        );
    });
// Save a custom field
    $('.tb-custom-fields-list').on("click", ".tb-save-custom-field", function (e) {
        e.preventDefault();
        if ($(this).hasClass('tobesaved')) {
            return;
        }
        var hook = $(this).data('hook');
        var serviceid = $(this).data('serviceid');
        var inputs = $(this).parents().eq(5).find('input, select').serializeArray();
        var spinner = $(this).parent().find('.spinner');
        spinner.addClass('is-active');
        $(this).addClass('tobesaved').attr("disabled", true);
        $.post(
            TB_vars.post_url,
            {
                // wp action
                action: 'option_submit',
                // send the nonce along with the request
                _wpnonce: TB_vars.wpNonce,
                team_booking_save_custom_field: true,
                hook: hook,
                service_id: serviceid,
                inputs: inputs
            },
            function (response) {
                if (response === 'duplicate_hook') {
                    $(".tobesaved").attr("disabled", false);
                    alert('Hook already existant! Please choose another one.'); //TODO: better handling
                } else {
                    spinner.removeClass('is-active');
                    $(".tobesaved").attr("disabled", false).removeClass('tobesaved');
                }
            }
        );
    });
// Expand custom field
    $('.tb-custom-fields-list').on("click", ".tb-expand-handle", function (e) {
        $(this).closest('li').find('.tb-hide').show();
        $(this).closest('li').find('.tb-collapse-handle').show();
        $(this).hide();
    });
// Collapse custom field
    $('.tb-custom-fields-list').on("click", ".tb-collapse-handle", function (e) {
        $(this).closest('li').find('.tb-hide').hide();
        $(this).closest('li').find('.tb-expand-handle').show();
        $(this).hide();
    });
// Draggable custom fields
    $(".sortable").sortable({
        axis: "y",
        handle: ".tb-drag-handle",
        placeholder: "tb-placeholder",
        opacity: 0.5,
        scroll: false,
        forceHelperSize: true,
        update: function (event, ui) {
            var hook = ui.item.find('.tb-save-custom-field').data('hook');
            var serviceid = ui.item.find('.tb-save-custom-field').data('serviceid');
            var where = ui.item.index();
            $.post(
                TB_vars.post_url,
                {
                    // wp action
                    action: 'option_submit',
                    // send the nonce along with the request
                    _wpnonce: TB_vars.wpNonce,
                    team_booking_move_custom_field: true,
                    hook: hook,
                    service_id: serviceid,
                    where: where
                },
                function (response) {
                }
            );
        }
    });
    $('#team_booking_update_translations').on('click', function () {
        var service_id = $(this).data('serviceid');
        $.post(
            TB_vars.post_url,
            {
                // wp action
                action: 'option_submit',
                // send the nonce along with the request
                _wpnonce: TB_vars.wpNonce,
                team_booking_update_field_translations: true,
                service_id: service_id,
            },
            function (response) {
                location.reload();
            }
        );
    })

    $('#remove-selected-reservations').on('click', function (e) {
        // preventing default anchor action
        e.preventDefault();
        // see if reservations are pending
        var is_pending = $(this).data('pending');
        // collecting reservation ids
        reservations = [];
        $('.select-reservation:checked').each(function () {
            reservations.push($(this).data('reservationid'));
        });
        // configuring and calling the confirmation modal
        $('#tb-reservation-delete-modal')
            .uiModal({
                blurring: true,
                transition: 'fade up',
            })
            .uiModal('setting', 'onApprove', function () {
                $.post(
                    TB_vars.post_url,
                    {
                        // wp action
                        action: 'option_submit',
                        // send the nonce along with the request
                        _wpnonce: TB_vars.wpNonce,
                        team_booking_remove_selected_reservations: true,
                        is_pending: is_pending,
                        reservations: JSON.stringify(reservations),
                    },
                    function (response) {
                        location.reload();
                    }
                );
            })
            .uiModal('show')
        ;
    });

    $('#remove-selected-services').on('click', function (e) {
        // preventing default anchor action
        e.preventDefault();
        // collecting services ids
        services = [];
        $('.select-service:checked').each(function () {
            services.push($(this).data('serviceid'));
        });
        // configuring and calling the confirmation modal
        $('#tb-service-delete-selected-modal')
            .uiModal({
                blurring: true,
                transition: 'fade up',
            })
            .uiModal('setting', 'onApprove', function () {
                $.post(
                    TB_vars.post_url,
                    {
                        // wp action
                        action: 'option_submit',
                        // send the nonce along with the request
                        _wpnonce: TB_vars.wpNonce,
                        team_booking_remove_selected_services: true,
                        services: JSON.stringify(services),
                    },
                    function (response) {
                        if (response.slice(0, 4) == 'http') {
                            location.reload();
                        } else {
                            console.log(response);
                        }
                    }
                );
            })
            .uiModal('show')
        ;
    });

    tbToggleService = function (activate, service_id, personal) {
        $.post(
            TB_vars.post_url,
            {
                // wp action
                action: 'option_submit',
                // send the nonce along with the request
                _wpnonce: TB_vars.wpNonce,
                service_action: activate,
                team_booking_toggle_service: true,
                service_id: service_id,
                personal: personal
            },
            function (response) {
            }
        );
    };
    tbInsertAtCaret = function (areaId, text) {
        var txtarea = document.getElementById(areaId);
        var scrollPos = txtarea.scrollTop;
        var strPos = 0;
        var br = ((txtarea.selectionStart || txtarea.selectionStart === '0') ?
            "ff" : (document.selection ? "ie" : false));
        if (br === "ie") {
            txtarea.focus();
            var range = document.selection.createRange();
            range.moveStart('character', -txtarea.value.length);
            strPos = range.text.length;
        }
        else if (br === "ff")
            strPos = txtarea.selectionStart;
        var front = (txtarea.value).substring(0, strPos);
        var back = (txtarea.value).substring(strPos, txtarea.value.length);
        txtarea.value = front + text + back;
        strPos = strPos + text.length;
        if (br === "ie") {
            txtarea.focus();
            var range = document.selection.createRange();
            range.moveStart('character', -txtarea.value.length);
            range.moveStart('character', strPos);
            range.moveEnd('character', 0);
            range.select();
        }
        else if (br === "ff") {
            txtarea.selectionStart = strPos;
            txtarea.selectionEnd = strPos;
            txtarea.focus();
        }
        txtarea.scrollTop = scrollPos;
    };
    $('.tb-hook-placeholder').click(function () {
        var text = $(this).text();
        var textarea_id = $(this).closest('li').find('textarea').attr("id");
        tbInsertAtCaret(textarea_id, text);
    });
    $('#team-booking-export-settings').click(function (e) {
        e.preventDefault();
        form = $('#team-booking-export-settings_form');
        form.submit();
    });
    $('#team-booking-import-settings_modal')
        .uiModal({
            blurring: true,
            transition: 'fade up',
        })
        .uiModal('attach events', '#team-booking-import-settings', 'show')
    ;
    $('#team-booking-import-core-json_modal')
        .uiModal({
            blurring: true,
            transition: 'fade up',
        })
        .uiModal('attach events', '#team-booking-import-core-json', 'show')
        .uiModal('setting', 'onApprove', function ($clicked) {
            $clicked.addClass('loading');
            form = $('#team-booking-import-core-json_form');
            form.find('.json-errors').hide();
            // File handling is done with FormData objects
            // It requires the jQuery.ajax method
            var data = new FormData();
            data.append('_wpnonce', TB_vars.wpNonce);
            data.append('action', 'option_submit');
            data.append('set_google_core_from_json', '1');
            form.find('input[type="file"]').each(function () {
                var fileInputName = $(this).attr('name'); //settings_json_file
                data.append(fileInputName, this.files[0]);
            });
            $.ajax({
                url: TB_vars.post_url,
                type: 'POST',
                // Collect form data
                data: data,
                processData: false,
                contentType: false,
                success: function (response) {
                    if (response === 'no file') {
                        form.find('.no_file').show();
                    } else if (response === 'uri mismatch') {
                        form.find('.uri_mismatch').show();
                    } else if (response === 'invalid file') {
                        form.find('.invalid_file').show();
                    } else {
                        window.location.href = response;
                    }
                    $clicked.removeClass('loading');
                    return;
                }
            });
            return false;
        })
    ;

    $('#team-booking-import-settings_form_submit').click(function (e) {
        e.preventDefault();
        form = $('#team-booking-import-settings_form');
        form.submit();
    });

    // AreYouSUre?
    $('form').areYouSure({'message': 'The changes you made will be lost if you navigate away from this page.'});

})
; // <-- end of document.ready

(function ($) {
    // Add Color Picker to all inputs that have 'tb-color-field' class
    $(function () {
        $('.tb-color-field').wpColorPicker({
            change: function (event, ui) {
                var name = $(this).attr('name');
                // event = standard jQuery event, produced by whichever control was changed.
                // ui = standard jQuery UI object, with a color member containing a Color.js object
                var c = ui.color.toString().substring(1);      // strip #
                var rgb = parseInt(c, 16);   // convert rrggbb to decimal
                var r = (rgb >> 16) & 0xff;  // extract red
                var g = (rgb >> 8) & 0xff;  // extract green
                var b = (rgb >> 0) & 0xff;  // extract blue
                var luma = 0.2126 * r + 0.7152 * g + 0.0722 * b; // per ITU-R BT.709
                if (name === 'tb-background-color') {
                    $(".tb-frontend-calendar").css('background', ui.color.toString());
                    if (luma > 145) {
                        $(".tb-day:not(#free-slot, #soldout-slot), .tbk-row-preview").css('color', '#414141');
                    } else {
                        $(".tb-day:not(#free-slot, #soldout-slot), .tbk-row-preview").css('color', '#FFFFFF');
                    }
                } else if (name === 'tb-weekline-color') {
                    $("#week-line").css('background-color', ui.color.toString());
                    if (luma > 145) {
                        $("#week-line").css('color', '#414141');
                    } else {
                        $("#week-line").css('color', '#FFFFFF');
                    }
                }
                else if (name === 'tb-freeslot-color') {
                    $("#free-slot").css('background', ui.color.toString());
                    if (luma > 145) {
                        $("#free-slot").css('color', '#414141');
                    } else {
                        $("#free-slot").css('color', '#FFFFFF');
                    }
                }
                else if (name === 'tb-soldoutslot-color') {
                    $("#soldout-slot").css('background', ui.color.toString());
                    if (luma > 145) {
                        $("#soldout-slot").css('color', '#414141');
                    } else {
                        $("#soldout-slot").css('color', '#FFFFFF');
                    }
                }
            }
        });
    });
})(jQuery);