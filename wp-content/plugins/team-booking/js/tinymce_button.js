(function () {
    tinymce.PluginManager.add('teambooking_tinymce_button', function (editor, url) {
        editor.addButton('teambooking_tinymce_button', {
            title: 'TeamBooking shortcodes',
            icon: 'icon teambooking-tinymce-icon',
            type: 'menubutton',
            menu: [
                {
                    text: 'Add calendar',
                    value: 'add calendar',
                    onclick: function () {
                        var data = {'action': 'teambooking_get_tinymce_vars'};
                        var body = [
                            {
                                type: 'checkbox',
                                name: 'read_only',
                                text: 'Read only'
                            },
                            {
                                type: 'checkbox',
                                name: 'no_filter',
                                text: 'Hide filter buttons'
                            },
                            {
                                type: 'checkbox',
                                name: 'logged_only',
                                text: 'Show to logged users only'
                            }
                        ];

                        var q = jQuery.ajax({
                            type: 'POST',
                            url: ajaxurl,
                            data: data,
                            async: false,
                            dataType: 'json'
                        });
                        body.push(
                            {
                                type: 'label',
                                style: 'font-weight:700;height:40px;padding-top:10px;',
                                text: 'Specific service(s), leave blank for all'
                            }
                        );
                        for (var key in q.responseJSON.services) {
                            body.push(
                                {
                                    type: 'checkbox',
                                    name: '[srv]' + key,
                                    text: q.responseJSON.services[key]
                                }
                            );
                        }
                        body.push(
                            {
                                type: 'label',
                                style: 'font-weight:700;height:40px;padding-top:10px;',
                                text: 'Specific coworker(s), leave blank for all'
                            }
                        );
                        for (var key in q.responseJSON.coworkers) {
                            body.push(
                                {
                                    type: 'checkbox',
                                    name: '[cwk]' + key,
                                    text: q.responseJSON.coworkers[key]
                                }
                            );
                        }
                        editor.windowManager.open({
                            title: 'TeamBooking - calendar options',
                            body: body,
                            onsubmit: function (e) {
                                // Set var for sending back to editor
                                attributes = '';
                                // Check first checkbox
                                if (e.data.read_only === true) {
                                    attributes += ' read_only="yes"';
                                }
                                if (e.data.no_filter === true) {
                                    attributes += ' nofilter="yes"';
                                }
                                if (e.data.logged_only === true) {
                                    attributes += ' logged_only="yes"';
                                }
                                coworkers = '';
                                services = '';
                                for (var key in e.data) {
                                    if (e.data[key] === true) {
                                        if (key.substr(0, 5) === '[cwk]') {
                                            coworkers += key.substr(5) + ', ';
                                        }
                                        if (key.substr(0, 5) === '[srv]') {
                                            services += key.substr(5) + ', ';
                                        }
                                    }
                                }
                                coworkers = coworkers.slice(0, -2);
                                services = services.slice(0, -2);
                                if (services !== '') {
                                    attributes += ' booking="' + services + '"';
                                }
                                if (coworkers !== '') {
                                    attributes += ' coworker="' + coworkers + '"';
                                }
                                editor.insertContent('[tb-calendar' + attributes + ']');
                            }
                        });
                    }
                },
                {
                    text: 'Add reservations list',
                    value: 'add reservations list',
                    onclick: function () {
                        var data = {'action': 'teambooking_get_tinymce_vars'};
                        var body = [
                            {
                                type: 'checkbox',
                                name: 'read_only',
                                text: 'Read only'
                            }
                        ];
                        editor.windowManager.open({
                            title: 'TeamBooking - list options',
                            body: body,
                            onsubmit: function (e) {
                                // Set var for sending back to editor
                                attributes = '';
                                // Check first checkbox
                                if (e.data.read_only === true) {
                                    attributes += ' read_only="yes"';
                                }
                                editor.insertContent('[tb-reservations' + attributes + ']');
                            }
                        });
                    }
                }
            ]
        });
    });
})();

