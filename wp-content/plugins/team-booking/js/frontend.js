jQuery(document).ready(function ($) {

        /*
         * Changing the month on the frontend calendar.
         */
        $('.calendar_main_container, .calendar_widget_container').on("click", ".tb-change-month", function () {
            // Let's grab the entire calendar area
            var calendar_area = $(this).closest('.tb-frontend-calendar');
            // We're put the calendar area in a loading state
            calendar_area.toggleClass('loading');
            // Let's grab the frontend calendar parameters
            var params = calendar_area.attr('data-params');
            // We need also the explicit instance value
            var instance = calendar_area.attr('data-instance');
            // Let's post the request for the new month via Ajax
            $.post(
                TB_Ajax.ajax_url,
                {
                    // wp ajax action
                    action: 'tbajax_action_change_month',
                    // vars
                    month: $(this).attr('data-month'),
                    year: $(this).attr('data-year'),
                    params: params
                },
                function (response) {
                    // Cleaning up the old modals
                    $(document).find('.ui.dimmer > .ui.tbk-modal[id*=tb-modal-list-' + instance + ']').remove();
                    // Let's replace the entire calendar area with the response
                    calendar_area.replaceWith(response);
                }
            );
            return false;
        });

        /*
         * Open the selected day slots list
         */
        $('.calendar_main_container, .calendar_widget_container').on("click", ".ui.tb-day.slots", function () {
            // Let's grab the calendar area...
            var calendar_area = $(this).closest('.calendar_main_container, .calendar_widget_container').find('.tb-frontend-calendar');
            // Let's grab the calendar area parameters
            var params = calendar_area.attr('data-params');
            // We need also the explicit instance value
            var instance = calendar_area.attr('data-instance');
            // We're put the calendar area in a loading state
            calendar_area.toggleClass('loading');
            $.post(
                TB_Ajax.ajax_url,
                {
                    // wp ajax action
                    action: 'tbajax_action_show_day_schedule',
                    // vars
                    day: $(this).attr('data-day'),
                    slots: $(this).attr('data-slots'),
                    params: params
                },
                function (response) {
                    $('#tb-modal-list-' + instance).html(response);
                    $('#tb-modal-list-' + instance)
                        .uiModal({
                            detachable: true,
                            observeChanges: true,
                            blurring: false,
                            transition: 'fade up',
                            closable: true,
                            onApprove: function () {
                                return false;
                            }
                        })
                        .uiModal('show')
                    ;
                    ;
                    // Cleaning up the old modals and gmap scripts
                    if (typeof google !== 'undefined') {
                        $(document).find('.ui.dimmer > .ui.tbk-modal[id*=tb-modal-' + instance + ']').find(".segment.tb-map").gmap3('destroy');
                    }
                    $(document).find('.ui.dimmer > .ui.tbk-modal[id*=tb-modal-' + instance + ']').remove();
                    $(document).find('.ui.dimmer + .pac-container').remove();
                    calendar_area.toggleClass('loading');
                }
            );
            return false;
        });

        /*
         * Open the reservation form modal
         */
        $(document).on("click", ".card.tb-book .extra.tbk-content > .tbk-button", function () {
            var card = $(this).closest('.card.tb-book');
            // Put the book button in a loading status
            $(this).toggleClass('loading');
            // Let's collect useful variables
            var times = card.attr('data-times');
            var coworker_string = card.attr('data-coworkerstring');
            var slot = card.attr('data-slot');
            var instance = card.attr('data-instance');
            var mapstyle = card.data('mapstyle');
            var address = card.data('address');

            // Let's load the modal content
            $.post(
                TB_Ajax.ajax_url,
                {
                    // wp ajax action
                    action: 'tbajax_action_get_reservation_modal',
                    // vars
                    slot: slot,
                    start_end: times,
                    coworker_string: coworker_string,
                    instance: instance
                },
                function (response) {
                    // Put the response inside the modal
                    $('#tb-modal-' + instance).html(response);

                    // Let's show the modal
                    $('#tb-modal-' + instance)
                        .uiModal({
                            detachable: true,
                            observeChanges: true,
                            blurring: false,
                            transition: 'fade up',
                            closable: false,
                            onApprove: function () {
                                return false;
                            },
                            onVisible: function () {
                                // If there is an address, let's load the Google Maps
                                if (address.length !== 0) {
                                    $('#tb-modal-' + instance).find("p.tb-map").html(address.toLowerCase().replace(/^[\u00C0-\u1FFF\u2C00-\uD7FF\w]|\s[\u00C0-\u1FFF\u2C00-\uD7FF\w]/g, function (letter) {
                                        return letter.toUpperCase();
                                    }));
                                    $('#tb-modal-' + instance).find(".tb-map").show();
                                    if (typeof google !== 'undefined') {
                                        $('#tb-modal-' + instance).find(".segment.tb-map").gmap3({
                                            marker: {
                                                address: address
                                            },
                                            map: {
                                                options: {
                                                    zoom: 14,
                                                    scrollwheel: false,
                                                    mapTypeId: 'style',
                                                    mapTypeControl: false
                                                }
                                            },
                                            styledmaptype: {
                                                id: "style",
                                                options: {
                                                    name: "Map"
                                                },
                                                styles: mapstyle
                                            }
                                        });
                                    } else {
                                        $('#tb-modal-' + instance).find(".segment.tb-map").html('Google Maps library not loaded.');
                                    }
                                }

                            }
                        })
                        .uiModal('show')
                    ;
                    // Remove the loading class
                    $(this).toggleClass('loading');
                });
        });

        /*
         * Open the register/login modal
         */
        $(document).on("click", ".card.tb-book-advice", function () {
            // Put the book button in a loading status
            var book_button = $(this).find('.extra.tbk-content > .ui.tbk-button');
            book_button.toggleClass('loading');
            // Let's collect useful variables
            var instance = $(this).attr('data-instance');
            var event = $(this).attr('data-event');
            // Let's load the modal content
            $.post(
                TB_Ajax.ajax_url,
                {
                    // wp ajax action
                    action: 'tbajax_action_get_register_modal',
                    // vars
                    event: event,
                    instance: instance
                },
                function (response) {
                    // Put the response inside the modal
                    $('#tb-modal-' + instance).html(response);
                    $('#tb-modal-' + instance).uiModal('setting', {
                        blurring: false,
                        detachable: true,
                        closable: false,
                        transition: 'fade up',
                        onApprove: function () {
                            return false;
                        }
                    })
                        .uiModal('show')
                    ;
                    // Remove the loading class
                    book_button.toggleClass('loading');
                });

        });

        /*
         * Fast month dropdown init
         */
        $('.tbk-fast-month-dropdown')
            .uiDropdown()
        ;

        /*
         * Fast month selector button
         */
        $('.calendar_main_container, .calendar_widget_container').on("click", ".item.tbk-month-selector", function () {
            // Grab some variables
            var month = $(this).attr('data-month'); // from 01 to 12
            var calendar = $(this).closest('.calendar_main_container, .calendar_widget_container').find('.tb-frontend-calendar');
            var params = calendar.attr('data-params');
            // Let's put the calendar container in a loading state
            calendar.toggleClass('loading');
            // Let's post the slots for the filtered calendar via Ajax
            $.post(
                TB_Ajax.ajax_url,
                {
                    // wp ajax action
                    action: 'tbajax_action_fast_month_selector',
                    // data
                    month: month,
                    params: params
                },
                function (response) {
                    // Let's replace the calendar
                    calendar.replaceWith(response);
                });
        });

        /*
         * Fast year dropdown init
         */
        $('.tbk-fast-year-dropdown')
            .uiDropdown()
        ;

        /*
         * Fast year selector button
         */
        $('.calendar_main_container, .calendar_widget_container').on("click", ".item.tbk-year-selector", function () {
            // Grab some variables
            var year = $(this).attr('data-year');
            var calendar = $(this).closest('.calendar_main_container, .calendar_widget_container').find('.tb-frontend-calendar');
            var params = calendar.attr('data-params');
            // Let's put the calendar container in a loading state
            calendar.toggleClass('loading');
            // Let's post the slots for the filtered calendar via Ajax
            $.post(
                TB_Ajax.ajax_url,
                {
                    // wp ajax action
                    action: 'tbajax_action_fast_year_selector',
                    // data
                    year: year,
                    params: params
                },
                function (response) {
                    // Let's replace the calendar
                    calendar.replaceWith(response);
                });
        });

        /*
         * Fast service selector dropdown init
         */
        $('.tbk-service-filter-dropdown')
            .uiDropdown()
        ;

        /*
         * Fast service selector button
         */
        $('.calendar_main_container, .calendar_widget_container').on("click", ".item.tbk-service-selector", function () {
            // Grab some variables
            var service = false;
            var services = false;
            if ($(this).hasClass('tb-reset-filter')) {
                services = $(this).attr('data-services');
            } else {
                service = $(this).attr('data-service');
            }
            var calendar = $(this).closest('.calendar_main_container, .calendar_widget_container').find('.tb-frontend-calendar');
            var params = calendar.attr('data-params');
            // Let's put the whole calendar container in a loading state
            calendar.toggleClass('loading');
            // Let's post the slots for the filtered calendar via Ajax
            $.post(
                TB_Ajax.ajax_url,
                {
                    // wp ajax action
                    action: 'tbajax_action_filter_calendar',
                    // data
                    service: service,
                    services: services,
                    params: params
                },
                function (response) {
                    // Let's replace all with the results (response is a JSON object)
                    calendar.replaceWith(response.calendar);
                }, "json");
        });

        /*
         * Get the ICAL file
         */
        $('.tb-get-ical').click(function (e) {
            e.preventDefault();
            // This should be unique, so ID is fine
            form = $('#tb-get-ical-form');
            form.submit();
        });

        /*
         * Modal services form handle
         */
        tbScheduledServiceSubmit = function (event, instance, start, withFiles) {
            // Setting a unique tail to identify form and modal
            var uniqueTail = event + '-' + instance + '-' + start;
            // Let's get the form id
            var form_id = '#tb-reservation-form-' + uniqueTail;
            // Let's get the post id (TODO: Better specificity)
            var post_id = $(".tb-frontend-calendar[data-instance='" + instance + "']").closest("div[class*='calendar_']").attr('data-postid');
            $(form_id + " input[name='post_id']").val(post_id);

            /*
             * Validation logic
             */
            $(form_id).find("input[type!='hidden']").each(function () {
                $(form_id).on('input', "input[type!='hidden']", function () {
                    $(this).closest('.field').removeClass('error');
                });
                $(form_id).on('change', "input[type='file']", function () {
                    $(this).closest('.field').removeClass('error');
                });

                if ($(this).prop('required') && !$(this).val()) {
                    $(this).closest('.field').addClass('error');
                } else {
                    $(this).closest('.field').removeClass('error');
                }
                if (typeof $(this).data('validation') !== "undefined") {
                    var regex = new RegExp(tb_base64_decode($(this).data('validation')));
                    if (regex.test($(this).val()) === false) {
                        $(this).siblings('.prompt.label').show();
                        $(this).closest('.field').addClass('error');
                    } else {
                        $(this).siblings('.prompt.label').hide();
                        if ($(this).val()) {
                            $(this).closest('.field').removeClass('error');
                        }
                    }
                }
            });
            // Select validation
            $(form_id).find(".uiDropdown.selection").each(function () {
                if ($(this).find("input").prop('required') && !$(this).find("input").val()) {
                    $(this).closest('.field').addClass('error');
                    $(form_id).on('change', ".uiDropdown.selection", function () {
                        $(this).closest('.field').removeClass('error');
                    });
                } else {
                    $(this).closest('.field').removeClass('error');
                }
            });
            // Checkboxes form validation
            $(form_id).find("input[type='checkbox']").each(function () {
                if ($(this).prop('required') && !$(this).prop("checked")) {
                    $(this).closest('.field').addClass('error');
                    $(form_id).on('change', "input[type='checkbox']", function () {
                        $(this).closest('.field').removeClass('error');
                    });
                } else {
                    $(this).closest('.field').removeClass('error');
                }
            });
            // Check for invalid data
            var found = false;
            $(form_id).find("input[type!='hidden']").each(function () {
                if ($(this).closest('.field').hasClass('error')) {
                    found = true;
                }
            });
            $(form_id).find(".uiDropdown.selection").each(function () {
                if ($(this).closest('.field').hasClass('error')) {
                    found = true;
                }
            });
            $(form_id).find("input[type='checkbox']").each(function () {
                if ($(this).closest('.field').hasClass('error')) {
                    found = true;
                }
            });

            /*
             * Submit, if validation is passed
             */
            if (!found) {
                $('.tb-modal-submit-' + uniqueTail).addClass('loading');
                if (withFiles) {
                    // File handling is done with FormData objects
                    // It requires the jQuery.ajax method
                    var data = new FormData();
                    // Seems redundant, but we're doing serialize()
                    // so the form listener can be just one
                    data.append('data', $(form_id).serialize());
                    data.append('action', 'tbajax_action_submit_form');
                    $(form_id).find('input[type="file"]').each(function () {
                        var fileInputName = $(this).attr('name');
                        data.append(fileInputName, this.files[0]);
                    });
                    $.ajax({
                        url: TB_Ajax.ajax_url,
                        type: 'POST',
                        // Collect form data
                        data: data,
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            console.log(response); // for debug
                            if (response.slice(0, 4) == 'http') {
                                // redirecting
                                window.location.href = response;
                                return;
                            } else {
                                // Remove the modal submit buttons
                                $('.tb-modal-submit-' + uniqueTail).remove();
                                // Remove the 'or' separators
                                $('.ui.tbk-modal').find('.or').remove();
                                // Load the response box inside the modal content

                                $('.ui.tbk-modal .tbk-content').html(response);
                                // Binding the reload location to the close button
                                $('#tb-modal-close-' + uniqueTail).click(function () {
                                    location.reload(true);
                                });
                            }
                        }
                    });
                } else {
                    $.post(
                        TB_Ajax.ajax_url,
                        {
                            // wp ajax action
                            action: 'tbajax_action_submit_form',
                            // Collect form data
                            data: $(form_id).serialize()
                        },
                        function (response) {
                            console.log(response); // for debug
                            if (response.slice(0, 4) == 'http') {
                                // redirecting
                                window.location.href = response;
                                return;
                            } else {
                                // Remove the modal submit buttons
                                $('.tb-modal-submit-' + uniqueTail).remove();
                                // Remove the 'or' separators
                                $('.ui.tbk-modal').find('.or').remove();
                                // Load the response box inside the modal content
                                $('.ui.tbk-modal .tbk-content').html(response);
                                // Binding the reload location to the close button
                                $('#tb-modal-close-' + uniqueTail).click(function () {
                                    location.reload(true);
                                });
                            }
                        }
                    );
                }
            }
            ;
        };

        /*
         * Unscheduled services form handle
         */
        tbUnscheduledServiceSubmit = function (service_id, instance, withFiles) {
            // Setting a unique tail to identify form and modal
            var uniqueTail = service_id + '-' + instance;
            // Setting some defaults
            withFiles = withFiles || false;
            // Let's get the form id
            var form_id = '#tb-reservation-form-' + uniqueTail;
            // Let's get the post id
            var post_id = $(".tb-frontend-calendar[data-instance='" + instance + "']").closest("div[class*='calendar_']").attr('data-postid');
            $(form_id + " input[name='post_id']").val(post_id);

            /*
             * Validation logic
             */
            $(form_id).find("input[type!='hidden']").each(function () {
                $(form_id).on('input', "input[type!='hidden']", function () {
                    $(this).closest('.field').removeClass('error');
                });
                $(form_id).on('change', "input[type='file']", function () {
                    $(this).closest('.field').removeClass('error');
                });

                if ($(this).prop('required') && !$(this).val()) {
                    $(this).closest('.field').addClass('error');
                } else {
                    $(this).closest('.field').removeClass('error');
                }

                if (typeof $(this).data('validation') !== "undefined") {
                    var regex = new RegExp(tb_base64_decode($(this).data('validation')));
                    if (regex.test($(this).val()) === false) {
                        $(this).siblings('.prompt.label').show();
                        $(this).closest('.field').addClass('error');
                    } else {
                        $(this).siblings('.prompt.label').hide();
                        if ($(this).val()) {
                            $(this).closest('.field').removeClass('error');
                        }
                    }
                }
            });
            // Select validation
            $(form_id).find(".uiDropdown.selection").each(function () {
                if ($(this).find("input").prop('required') && !$(this).find("input").val()) {
                    $(this).closest('.field').addClass('error');
                    $(form_id).on('change', ".uiDropdown.selection", function () {
                        $(this).closest('.field').removeClass('error');
                    });
                } else {
                    $(this).closest('.field').removeClass('error');
                }
            });
            // Checkboxes form validation
            $(form_id).find("input[type='checkbox']").each(function () {
                if ($(this).prop('required') && !$(this).prop("checked")) {
                    $(this).closest('.field').addClass('error');
                    $(form_id).on('change', "input[type='checkbox']", function () {
                        $(this).closest('.field').removeClass('error');
                    });
                } else {
                    $(this).closest('.field').removeClass('error');
                }
            });
            // Check for invalid data
            var found = false;
            $(form_id).find("input[type!='hidden']").each(function () {
                if ($(this).closest('.field').hasClass('error')) {
                    found = true;
                }
            });
            $(form_id).find(".uiDropdown.selection").each(function () {
                if ($(this).closest('.field').hasClass('error')) {
                    found = true;
                }
            });
            $(form_id).find("input[type='checkbox']").each(function () {
                if ($(this).closest('.field').hasClass('error')) {
                    found = true;
                }
            });

            /*
             * Submit, if validation is passed
             */
            if (!found) {
                $('#tb-unscheduled-service-submit-' + uniqueTail).addClass('loading');
                if (withFiles) {
                    // File handling is done with FormData objects
                    // It requires the jQuery.ajax method
                    var data = new FormData();
                    // Seems redundant, but doing serialize(),
                    // the form listener can be the same
                    data.append('data', $(form_id).serialize());
                    data.append('action', 'tbajax_action_submit_form');
                    $(form_id).find('input[type="file"]').each(function () {
                        var fileInputName = $(this).attr('name');
                        data.append(fileInputName, this.files[0]);
                    });
                    $.ajax({
                        url: TB_Ajax.ajax_url,
                        type: 'POST',
                        // Collect form data
                        data: data,
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            console.log(response); // for debug
                            if (response.slice(0, 4) == 'http') {
                                // redirecting
                                window.location.href = response;
                                return;
                            } else {
                                $(form_id).parent().find('.or').remove();
                                var content = $($.parseHTML(response, document, true));
                                $(form_id).replaceWith(content);
                                $('.ui.success.message').show();
                                $('#tb-unscheduled-service-submit-' + uniqueTail).remove();
                            }
                        }
                    });
                } else {
                    var data = $(form_id).serialize();
                    $.post(
                        TB_Ajax.ajax_url,
                        {
                            // wp ajax action
                            action: 'tbajax_action_submit_form',
                            // Collect form data
                            data: data
                        },
                        function (response) {
                            console.log(response); // for debug
                            if (response.slice(0, 4) == 'http') {
                                // redirecting
                                window.location.href = response;
                                return;
                            } else {
                                $(form_id).parent().find('.or').remove();
                                var content = $($.parseHTML(response, document, true));
                                $(form_id).replaceWith(content);
                                $('.ui.success.message').show();
                                $('#tb-unscheduled-service-submit-' + uniqueTail).remove();
                            }
                        }
                    );
                }
            }
            ;
        }

        // Submit payments
        tbSubmitPayment = function () {
            $('.tbk-pay-button').click(function () {
                if ($(this).hasClass('loading')) {
                    return;
                }
                $(this).addClass('loading');
                var clicked_button = $(this);
                var gateway_id = $(this).data('gateway');
                var offsite = $(this).data('offsite');
                var is_service_form = $(this).closest('.segment').data('isform');
                var reservation_data = $(this).closest('.segment').data('reservation');
                var reservation_database_id = $(this).closest('.segment').data('id');
                $.post(
                    TB_Ajax.ajax_url,
                    {
                        // wp ajax action
                        action: 'tb_submit_payment',
                        // Collect form data
                        reservation_data: reservation_data,
                        gateway_id: gateway_id,
                        reservation_database_id: reservation_database_id
                    },
                    function (response) {
                        if (offsite == true || response.slice(0, 4) == 'http') {
                            // redirecting
                            window.location.href = response;
                            return;
                        } else {
                            // parse the dinamically loaded scripts
                            var content = $($.parseHTML(response, document, true));
                            // Load the response box
                            if (typeof is_service_form !== "undefined") {
                                clicked_button.closest('.segment').html(content);
                            } else {
                                clicked_button.closest('.ui.tbk-modal .tbk-content').html(content);
                            }
                        }
                    }
                );
            });
        }

        // Cancel reservation action (frontend reservation list)
        $("table.tb-reservations-list a.tb-cancel-reservation").on('click', function (e) {
            e.preventDefault();
            var reservation_id = $(this).data('id');
            var reservation_hash = $(this).data('hash');

            $('.tb-reservation-cancel-modal')
                .uiModal({
                    blurring: true,
                    transition: 'fade up',
                })
                .uiModal('setting', 'onApprove', function ($clicked) {
                    $clicked.addClass('loading');
                    $.post(
                        TB_Ajax.ajax_url,
                        {
                            // wp action
                            action: 'tbajax_action_cancel_reservation',
                            reservation_id: reservation_id,
                            reservation_hash: reservation_hash
                        },
                        function (response) {
                            if (response == 'ok') {
                                location.reload(true);
                            } else {
                                $clicked.removeClass('loading');
                                alert(response);
                            }
                        }
                    );
                    return false;
                })
                .uiModal('show')
            ;

        });
    }
);