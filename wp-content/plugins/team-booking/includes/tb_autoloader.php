<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

spl_autoload_register('teambooking_autoloader');

function teambooking_autoloader($class_name) {
    if (false !== strpos($class_name, 'TeamBooking')) {
        // legacy
        switch ($class_name) {
            case 'TeamBookingSettings':
                $class_name = 'TeamBooking_Settings';
                break;
            case 'TeamBookingCoworker':
                $class_name = 'TeamBooking_Coworker';
                break;
            case 'TeamBookingType':
                $class_name = 'TeamBooking_Service';
                break;
            case 'TeamBookingFormTextField':
                $class_name = 'TeamBooking_Components_Form_TextField';
                break;
            case 'TeamBookingFormTextarea':
                $class_name = 'TeamBooking_Components_Form_TextArea';
                break;
            case 'TeamBookingFormSelect':
                $class_name = 'TeamBooking_Components_Form_Select';
                break;
            case 'TeamBookingFormRadio':
                $class_name = 'TeamBooking_Components_Form_Radio';
                break;
            case 'TeamBookingFormFileUpload':
                $class_name = 'TeamBooking_Components_Form_FileUpload';
                break;
            case 'TeamBookingFormCheckbox':
                $class_name = 'TeamBooking_Components_Form_Checkbox';
                break;
            case 'TeamBookingCustomBTSettings':
                $class_name = 'TeamBooking_CoworkerServiceSettings';
                break;
        }
        $classes_dir = realpath(TEAMBOOKING_PATH) . DIRECTORY_SEPARATOR . 'src' . DIRECTORY_SEPARATOR;
        $class_file = str_replace('_', DIRECTORY_SEPARATOR, $class_name) . '.php';
        require_once $classes_dir . $class_file;
    }
}
