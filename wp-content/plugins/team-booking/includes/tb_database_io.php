<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

/**
 * Insert a reservation record
 *
 * @global type $wpdb
 * @param TeamBooking_ReservationData $data
 * @param boolean $pending
 * @return int
 */
function tbInsertReservation(TeamBooking_ReservationData $data, $pending = FALSE)
{
    global $wpdb;
    if (!$pending) {
        $table_name = $wpdb->prefix . "teambooking_reservations";
    } else {
        $table_name = $wpdb->prefix . "teambooking_reservations_pending";
    }
    /* @var $log TeamBooking_ReservationData */
    $created = current_time('mysql');
    $data_object = serialize($data);
    $wpdb->insert($table_name, array(
        'created'     => $created,
        'service_id'  => $data->getServiceId(),
        'coworker_id' => $data->getCoworker(),
        'data_object' => $data_object,
    ));
    $inserted_id = $wpdb->insert_id;

    return $inserted_id;
}

/**
 * Get a reservation record by id
 *
 * @param int $id
 * @return TeamBooking_ReservationData
 */
function tbGetReservationById($id, $pending = FALSE)
{
    global $wpdb;
    if (!$pending) {
        $table_name = $wpdb->prefix . "teambooking_reservations";
    } else {
        $table_name = $wpdb->prefix . "teambooking_reservations_pending";
    }
    $result = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table_name WHERE id = %d", $id));

    return unserialize($result->data_object);
}

function tbGetReservationsByIds(array $ids, $pending = FALSE)
{
    global $wpdb;
    if (!$pending) {
        $table_name = $wpdb->prefix . "teambooking_reservations";
    } else {
        $table_name = $wpdb->prefix . "teambooking_reservations_pending";
    }
    $how_many = count($ids);
    $placeholders = array_fill(0, $how_many, '%d');
    $format = implode(', ', $placeholders);
    $results = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE id IN ($format)", $ids));
    $return = array();
    foreach ($results as $result) {
        $return[$result->id] = unserialize($result->data_object);
    }

    return $return;
}

function tbDeleteReservationById($id, $pending = FALSE)
{
    global $wpdb;
    if (!$pending) {
        $table_name = $wpdb->prefix . "teambooking_reservations";
    } else {
        $table_name = $wpdb->prefix . "teambooking_reservations_pending";
    }
    $result = $wpdb->delete($table_name, array('id' => $id));

    return $result;
}

function tbUpdateReservationById($id, TeamBooking_ReservationData $data)
{
    global $wpdb;
    $data_object = serialize($data);
    $table_name = $wpdb->prefix . "teambooking_reservations";
    $result = $wpdb->update($table_name, array('data_object' => $data_object), array('id' => $id));

    return $result;
}

function tbGetReservationsByCoworker($id, $pending = FALSE)
{
    global $wpdb;
    if (!$pending) {
        $table_name = $wpdb->prefix . "teambooking_reservations";
    } else {
        $table_name = $wpdb->prefix . "teambooking_reservations_pending";
    }
    $results = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE coworker_id = %d", $id));
    $return = array();
    foreach ($results as $result) {
        $return[$result->id] = unserialize($result->data_object);
    }

    return $return;
}

function tbGetReservationsByServices(array $services, $pending = FALSE)
{
    global $wpdb;
    if (!$pending) {
        $table_name = $wpdb->prefix . "teambooking_reservations";
    } else {
        $table_name = $wpdb->prefix . "teambooking_reservations_pending";
    }
    $how_many = count($services);
    $placeholders = array_fill(0, $how_many, '%d');
    $format = implode(', ', $placeholders);
    $results = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE service_id IN ($format)", $services));
    $return = array();
    foreach ($results as $result) {
        $return[$result->id] = unserialize($result->data_object);
    }

    return $return;
}

function tbGetReservationsByService($id)
{
    global $wpdb;
    $table_name = $wpdb->prefix . "teambooking_reservations";
    $results = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name WHERE service_id = %s", $id));
    $return = array();
    foreach ($results as $result) {
        $return[$result->id] = unserialize($result->data_object);
    }

    return $return;
}

/**
 * @param bool|FALSE $pending
 * @return TeamBooking_ReservationData[]
 */
function tbGetReservations($pending = FALSE)
{
    global $wpdb;
    if (!$pending) {
        $table_name = $wpdb->prefix . "teambooking_reservations";
    } else {
        $table_name = $wpdb->prefix . "teambooking_reservations_pending";
    }
    $results = $wpdb->get_results("SELECT * FROM $table_name");
    $return = array();
    foreach ($results as $result) {
        $return[$result->id] = unserialize($result->data_object);
    }

    return $return;
}

function tbGetReservationsByTime($min_time, $max_time, $pending = FALSE)
{
    global $wpdb;
    $timezone = tbGetTimezone();
    $results = tbGetReservations($pending);
    $return = array();
    foreach ($results as $id => $reservation_data) {
        $start_time = new DateTime($reservation_data->getSlotStart());
        $start_time->setTimezone($timezone);
        $end_time = new DateTime($reservation_data->getSlotEnd());
        $end_time->setTimezone($timezone);
        if (($start_time->format('U') <= strtotime($max_time) || $max_time == NULL) &&
            $start_time->format('U') >= strtotime($min_time)
        ) {
            $return[$id] = $reservation_data;
        }
    }

    return $return;
}
