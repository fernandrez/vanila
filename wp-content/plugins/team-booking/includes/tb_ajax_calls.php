<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

////////////////////////////////
//                            //
//  FRONTEND AJAX CALLBACKS   //
//                            //
////////////////////////////////

/**
 * Ajax callback: change month on frontend calendar
 */
function tbajax_action_change_month_callback()
{
    $calendar = new TeamBooking_Calendar();
    $parameters = new TeamBooking_RenderParameters();
    $parameters = $parameters->decode($_POST['params']);
    $parameters->clearSlotsObj();
    $parameters->setMonth(date("m", strtotime($_POST['month'] . '/01')));
    $parameters->setYear($_POST['year']);
    $parameters->setIsAjaxCallYes();
    // generate and output the response
    $response = $calendar->getCalendar($parameters);
    exit;
}

/**
 * Ajax callback: show day schedule on frontend calendar
 */
function tbajax_action_show_day_schedule_callback()
{
    $calendar = new TeamBooking_Calendar();
    $parameters = new TeamBooking_RenderParameters();
    $parameters = $parameters->decode($_POST['params']);
    if ($parameters instanceof TeamBooking_RenderParameters) {
        $parameters->setDay($_POST['day']);
        $parameters->setSlots($parameters->decode($_POST['slots']));
        $parameters->setIsAjaxCallYes();
        // generate and output the response
        $response = $calendar->getSchedule($parameters);
    } else {
        $error = new TeamBooking_Error(9);
        echo $error->getMessage();
        echo "<br><br>";
        var_dump($parameters);
        var_dump($_POST['slots']);
    }
    exit;
}

/**
 * Ajax callback: get reservation modal content
 */
function tbajax_action_get_reservation_modal_callback()
{
    $settings = tbGetSettings();
    $slot = unserialize(base64_decode($_POST['slot']));
    $start_end = base64_decode($_POST['start_end']);
    $instance = filter_input(INPUT_POST, 'instance', FILTER_SANITIZE_NUMBER_INT);
    // Calling the template
    $modal = new TeamBooking_Components_Frontend_Modal($slot, $settings);
    // Setting instance
    $modal->instance = $instance;
    $modal->start_end_times = $start_end;
    if (isset($_POST['coworker_string'])) {
        $coworker_string = base64_decode($_POST['coworker_string']);
        $modal->coworker_string = $coworker_string;
    }
    // Render
    echo $modal->getContent();
    exit;
}

/**
 * Ajax callback: get register/login modal content
 */
function tbajax_action_get_register_modal_callback()
{
    $settings = tbGetSettings();
    // Not used ATM
    #$event = $_POST['event'];
    $instance = filter_input(INPUT_POST, 'instance', FILTER_SANITIZE_NUMBER_INT);
    // Calling the template
    $slot = new TeamBooking_Slot();
    $modal = new TeamBooking_Components_Frontend_Modal($slot, $settings);
    // Setting instance
    $modal->instance = $instance;
    // Render
    echo $modal->getContentRegisterAdvice();
    exit;
}

function tbajax_action_filter_calendar_callback()
{
    $calendar = new TeamBooking_Calendar();
    $parameters = new TeamBooking_RenderParameters();
    $parameters = $parameters->decode($_POST['params']);
    if ($_POST['services'] !== 'false') {
        $parameters->setRequestedServiceIds(unserialize(base64_decode($_POST['services'])));
    } else {
        $parameters->setRequestedServiceIds(array($_POST['service']));
    }
    $parameters->setIsAjaxCallYes();
    // generate the responses
    ob_start();
    $calendar->getCalendar($parameters);
    $response_one = ob_get_clean();
    // responses output
    echo json_encode(array('calendar' => $response_one));
    exit;
}

function tbajax_action_fast_month_selector_callback()
{
    $calendar = new TeamBooking_Calendar();
    $parameters = new TeamBooking_RenderParameters();
    $parameters = $parameters->decode($_POST['params']);
    $parameters->clearSlotsObj();
    $parameters->setMonth($_POST['month']);
    $parameters->setIsAjaxCallYes();
    // generate the response
    ob_start();
    $calendar->getCalendar($parameters);
    $response = ob_get_clean();
    // responses output
    echo $response;
    exit;
}

function tbajax_action_fast_year_selector_callback()
{
    $calendar = new TeamBooking_Calendar();
    $parameters = new TeamBooking_RenderParameters();
    $parameters = $parameters->decode($_POST['params']);
    $parameters->clearSlotsObj();
    $parameters->setYear($_POST['year']);
    $parameters->setIsAjaxCallYes();
    // generate the response
    ob_start();
    $calendar->getCalendar($parameters);
    $response = ob_get_clean();
    // responses output
    echo $response;
    exit;
}

function tbajax_action_cancel_reservation_callback()
{
    $hash = $_POST['reservation_hash'];
    $id = $_POST['reservation_id'];
    /**
     * Retrieving the reservation record in database
     */
    $reservation_db_record = tbGetReservationById($id);
    /**
     * Hash check
     */
    if ($reservation_db_record->getChecksum() != $hash) {
        exit;
    }
    /**
     * Instantiating the reservation service class
     */
    $reservation = new TeamBooking_Reservation($reservation_db_record);
    /**
     * Calling the cancel method
     */
    $updated_record = $reservation->cancelReservation($id);
    if ($updated_record instanceof TeamBooking_ReservationData) {
        /**
         * Everything went fine, let's update the database record
         */
        tbUpdateReservationById($id, $updated_record);
        echo 'ok';
    } elseif ($updated_record instanceof TeamBooking_Error) {
        /**
         * Something goes wrong
         */
        if ($updated_record->getCode() == 7) {
            /*
             * The reservation is already cancelled, let's update the database record
             */
            $reservation_db_record->setStatusCancelled();
            tbUpdateReservationById($id, $reservation_db_record);
        }
        echo $updated_record->getMessage();
    }
    exit;
}

///////////////////////////////
//                           //
//  BACKEND AJAX CALLBACKS   //
//                           //
///////////////////////////////

/**
 * oAuth2 callback, for authentication
 */
function teambooking_oauth_callback()
{
    // Check if there is auth code and user capability
    if (isset($_GET['code']) && current_user_can('tb_can_sync_calendar')) {
        // casting the REQUEST array to an object and saving it
        $request = (object)$_REQUEST;
        // initialize session
        if (!session_id()) {
            session_start();
        }
        // nonce check (defined in TeamBooking_Calendar class)
        if (isset($_SESSION['tbk-auth-state'])) {
            if ($_SESSION['tbk-auth-state'] != $request->state) {
                $location = admin_url('admin.php?page=team-booking&tab=personal&auth_failed=1');
                die(wp_redirect($location));
            }
        }
        // get settings
        $settings = tbGetSettings();
        // get the coworker data
        $coworker = $settings->getCoworkerData(get_current_user_id());
        // instatiate a calendar class
        $calendar = new TeamBooking_Calendar();
        // retrieve the coworker's access token, if present
        $access_token = $coworker->getAccessToken();
        if (!empty($access_token)) {
            // an access token is already present... revoke it?
        } else {
            /**
             * Access token not present, let's exchange the auth code
             * for access token, refresh token, id token set.
             *
             * Before saving them, we'll check if there is a refresh token.
             * If the refresh token is not present, the Google Account
             * thinks that this application is already trusted, and a previous
             * refresh token was already granted without being revoked yet.
             *
             * We'll check also if the Google Account email is actually used
             * by another coworker.
             *
             */
            $tokens = $calendar->authenticate($request->code);
            // is there a refresh token?
            if (is_null($calendar->setAccessToken($tokens))) {
                // There is no refresh token
                $location = admin_url('admin.php?page=team-booking&tab=personal&auth_no_refresh=1');
                die(wp_redirect($location));
            } else {
                /**
                 * There is a refresh token, is the Google Account already used?
                 *
                 * NOTE:
                 * this configuration (refresh token provided && Google Account already authorized)
                 * should NOT be possible, according to oAuth flow.
                 *
                 * TODO: test if this whole block is useless
                 */
                $already_used = FALSE;
                $this_email = $calendar->getTokenEmailAccount($tokens, get_current_user_id());
                if ($this_email instanceof Google_Auth_Exception) {
                    $this_email = $this_email->getMessage();
                }
                $coworker_id_list = tbGetAuthCoworkersList();
                foreach ($coworker_id_list as $coworker_id => $coworker_data) {
                    $that_email = $calendar->getTokenEmailAccount($coworker_data['tokens'], $coworker_id);
                    if ($that_email instanceof Google_Auth_Exception) {
                        $that_email = $that_email->getMessage();
                    }
                    if ($this_email == $that_email) {
                        $already_used = TRUE;
                    }
                }
                if ($already_used) {
                    $location = admin_url('admin.php?page=team-booking&tab=personal&auth_already_used=1');
                    die(wp_redirect($location));
                }

                /**
                 * All is correct, let's save the tokens
                 */
                $coworker->setAccessToken($tokens);
                $settings->updateCoworkerData($coworker, $coworker->getId());
                $settings->save();
                // set redirect
                $location = admin_url('admin.php?page=team-booking&tab=personal&auth_success=1');
                die(wp_redirect($location));
            }
        }
        exit;
    } elseif (isset($_GET['error'])) {
        // Handle the access_denied error
        var_dump($_GET['error']);
        exit;
    }
    exit;
}

/**
 * Instantiates the listeners for IPN payments of any gateway
 */
function teambooking_ipn_listener()
{
    foreach (tbGetSettings()->getPaymentGatewaySettingObjects() as $gateway_id => $gateway) {
        /* @var $gateway TeamBooking_PaymentGateways_Settings */
        if (isset($_REQUEST[$gateway_id])) {
            $ipn_data = $_POST;
            $gateway->listenerIPN($ipn_data);
            exit;
        }
    }
    exit;
}

/**
 * This function passes variables to the tinymce plugin button
 */
function teambooking_get_tinymce_vars()
{
    $services = tbGetSettings()->getServiceIdList();
    $coworkers = tbGetSettings()->getCoworkersData();
    $array = array();
    foreach ($services as $service_id) {
        $array['services'][$service_id] = tbGetSettings()->getService($service_id)->getName();
    }
    foreach ($coworkers as $coworker_id => $coworker) {
        /* @var $coworker TeamBookingCoworker */
        $array['coworkers'][$coworker_id] = $coworker->getFirstName() . ' ' . $coworker->getLastName();
    }
    echo json_encode($array);
    exit;
}
