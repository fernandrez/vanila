<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

/**
 * Parses the modal form data to a reservation object
 *
 * @param array $form_data
 * @return TeamBooking_ReservationData
 */
function tbModalFormMapper($form_data)
{
    // Parse data
    $parsed_data = array();
    parse_str($form_data, $parsed_data);
    // verify nonce
    $nonce = $parsed_data['nonce'];
    if (!wp_verify_nonce($nonce, 'teambooking_submit_reservation')) {
        return FALSE;
    }
    $expected_fields = tbGetSettings()->getService($parsed_data['service'])->getFormFields()->getHookList();
    $returned_fields = array();
    foreach ($expected_fields as $name) {
        $form_field = new TeamBooking_ReservationFormField();
        $form_field->setName($name);
        if (isset($parsed_data['form_fields'][$name])) {
            $form_field->setValue(tbFilterInput($parsed_data['form_fields'][$name]));
        } else {
            // We are facing unchecked checkboxes here...
            $form_field->setValue(__('Not selected', 'teambooking'));
        }
        $returned_fields[] = $form_field;
    }
    $return = new TeamBooking_ReservationData();
    $return->setServiceLocation($parsed_data['service_location']);
    $return->setTickets($parsed_data['tickets']);
    $return->setPostId($parsed_data['post_id']);
    $return->setPostTitle(get_the_title($parsed_data['post_id']));
    if (isset($parsed_data['owner'])) {
        $return->setCoworker($parsed_data['owner']);
    }
    $return->setPrice(tbGetSettings()->getService($parsed_data['service'])->getPrice());
    $return->setServiceClass(tbGetSettings()->getService($parsed_data['service'])->getClass());
    $return->setCurrencyCode(tbGetSettings()->getCurrencyCode());
    if (isset($parsed_data['event_id'])) {
        $return->setGoogleCalendarEvent($parsed_data['event_id']);
    }
    if (isset($parsed_data['calendar_id'])) {
        $return->setGoogleCalendarId(tb_base64_url_decode($parsed_data['calendar_id']));
    }
    $return->setIpnUniqueId(tbRandomNumber(10) . current_time('timestamp', TRUE));
    $return->setServiceName(tbGetSettings()->getService($parsed_data['service'])->getName());
    $return->setServiceId($parsed_data['service']);
    $return->setFormFields($returned_fields);
    if (isset($parsed_data['event_id_parent'])) {
        if ($parsed_data['event_id_parent']) {
            $return->setGoogleCalendarEventParent($parsed_data['event_id_parent']);
        }
    }
    if (isset($parsed_data['slot_start'])) {
        $return->setSlotStart($parsed_data['slot_start']);
    }
    if (isset($parsed_data['slot_end'])) {
        $return->setSlotEnd($parsed_data['slot_end']);
    }
    if (isset($parsed_data['customer_wp_id'])) {
        $return->setCustomerUserId($parsed_data['customer_wp_id']);
    }
    $now = tbGetNowInSecondsUTC();
    $return->setCreationInstant($now);

    return $return;
}

/**
 * It takes a form field and returns the HTML for admin builder
 *
 * @param TeamBookingFormTextField $field
 * @param $service_id
 * @return string
 */
function tbAdminFormCustomFieldsMapper(TeamBookingFormTextField $field, $service_id)
{
    $fieldname = "event[form_fields][custom]";
    ob_start();
    ?>
    <fieldset>
        <label for="<?= $fieldname ?>[<?= $field->getHook() ?>]">
            <input data-ays-ignore="true" name="<?= $fieldname ?>[<?= $field->getHook() ?>_show]" type="checkbox"
                   value="<?= $field->getHook() ?>" <?php checked(TRUE, $field->getIsActive()) ?>/><span> <?= __('Show', 'teambooking') ?></span><br>
            <?php if (get_class($field) != 'TeamBookingFormRadio') { ?>
                <input data-ays-ignore="true" name="<?= $fieldname ?>[<?= $field->getHook() ?>_required]"
                       type="checkbox"
                       value="<?= $field->getHook() ?>" <?php checked(TRUE, $field->getIsRequired()) ?>/>
                <span> <?= __('Required', 'teambooking') ?></span><br>
            <?php } ?>
        </label>
    </fieldset>
    <?php
    $fieldset = ob_get_clean();
    ob_start();
    ?>
    <div class="spinner" style="float: none"></div>
    <?php
    if (get_class($field) == 'TeamBookingFormTextField') {
        ?>
        <a id="tb-field-regex-open-<?= $field->getHook() ?>" class="button">
            <span class="dashicons dashicons-editor-spellcheck"
                  style="line-height: inherit;<?= ($field->getValidationRegex()) ? 'color:#B66262;' : '' ?>"></span>
        </a>
        <?= tbGetValidationModal($field, $fieldname) ?>
    <?php } ?>
    <a class="button tb-remove-custom-field" data-hook="<?= $field->getHook() ?>" data-serviceid="<?= $service_id ?>"
       href="#"><span class="dashicons dashicons-trash" style="line-height: inherit;color: #B93B3B;"></span></a>
    <a class="button tb-save-custom-field" data-hook="<?= $field->getHook() ?>" data-serviceid="<?= $service_id ?>"
       href="#"><?= __('Save', 'teambooking') ?></a>
    <?php
    $buttons = ob_get_clean();
    ob_start();
    ?>
    <span class="dashicons dashicons-menu tb-drag-handle"></span>
    <span class="dashicons dashicons-arrow-down tb-expand-handle"></span>
    <span style="display: none;" class="dashicons dashicons-arrow-up tb-collapse-handle"></span>
    <?php
    $move_buttons = ob_get_clean();
    if (get_class($field) == 'TeamBookingFormTextField') {
        ob_start();
        ?>
        <table class="widefat" style="width:730px;">
            <tbody>
            <tr>
                <td rowspan="3" class="tb-no-select" style="width: 20px;">
                    <?= $move_buttons ?>
                </td>
                <td rowspan="3" style="width:150px;">
                    <span style="font-size: initial;"><strong><?= __('Text field', 'teambooking') ?></strong></span>
                    <!-- hidden content -->
                    <div class="tb-hide">
                        <br>
                        <?= $fieldset ?>
                    </div>
                </td>
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_label]" value="<?= $field->getLabel() ?>">
                    <span class="description"><?= __('Label', 'teambooking') ?></span>

                    <div style="float:right;">
                        <?= $buttons ?>
                    </div>
                </td>
            </tr>
            <!-- hidden content -->
            <tr class="tb-hide">
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_default]" value="<?= $field->getValue() ?>"
                           placeholder="<?= __('none', 'teambooking') ?>">
                    <span class="description"><?= __('Default value', 'teambooking') ?></span>
                </td>
            </tr>
            <tr class="tb-hide">
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_hook]" value="<?= $field->getHook() ?>"
                           placeholder="<?= __('without square brackets', 'teambooking') ?>">
                    <span class="description"><?= __('Hook', 'teambooking') ?></span>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
        $return = ob_get_clean();
    } elseif (get_class($field) == 'TeamBookingFormTextarea') {
        /* @var $field TeamBookingFormTextarea */
        ob_start();
        ?>
        <table class="widefat" style="width:730px;">
            <tbody>
            <tr>
                <td rowspan="4" class="tb-no-select" style="width: 20px;">
                    <?= $move_buttons ?>
                </td>
                <td rowspan="4" style="width:150px;">
                    <span style="font-size: initial;"><strong><?= __('Textarea', 'teambooking') ?></strong></span>
                    <!-- hidden content -->
                    <div class="tb-hide">
                        <br>
                        <?= $fieldset ?>
                    </div>
                </td>
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_label]" value="<?= $field->getLabel() ?>">
                    <span class="description"><?= __('Label', 'teambooking') ?></span>

                    <div style="float:right;">
                        <?= $buttons ?>
                    </div>
                </td>
            </tr>
            <!-- hidden content -->
            <tr class="tb-hide">
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_default]" value="<?= $field->getValue() ?>"
                           placeholder="<?= __('none', 'teambooking') ?>">
                    <span class="description"><?= __('Default text', 'teambooking') ?></span>
                </td>
            </tr>
            <tr class="tb-hide">
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_hook]" value="<?= $field->getHook() ?>"
                           placeholder="<?= __('without square brackets', 'teambooking') ?>">
                    <span class="description"><?= __('Hook', 'teambooking') ?></span>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
        $return = ob_get_clean();
    } elseif (get_class($field) == 'TeamBookingFormCheckbox') {
        /* @var $field TeamBookingFormCheckbox */
        ob_start();
        ?>
        <table class="widefat" style="width:730px;">
            <tbody>
            <tr>
                <td rowspan="4" class="tb-no-select" style="width: 20px;">
                    <?= $move_buttons ?>
                </td>
                <td rowspan="4" style="width:150px;">
                    <span style="font-size: initial;"><strong><?= __('Checkbox', 'teambooking') ?></strong></span>
                    <!-- hidden content -->
                    <div class="tb-hide">
                        <br>
                        <?= $fieldset ?>
                    </div>
                </td>
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_label]" value="<?= $field->getLabel() ?>">
                    <span class="description"><?= __('Label', 'teambooking') ?></span>

                    <div style="float:right;">
                        <?= $buttons ?>
                    </div>
                </td>
            </tr>
            <!-- hidden content -->
            <tr class="tb-hide">
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_hook]" value="<?= $field->getHook() ?>"
                           placeholder="<?= __('without square brackets', 'teambooking') ?>">
                    <span class="description"><?= __('Hook', 'teambooking') ?></span>
                </td>
            </tr>
            <tr class="tb-hide">
                <td>
                    <input data-ays-ignore="true" type="checkbox" value="1"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_selected]" <?php checked(TRUE, $field->isChecked()) ?>>
                    <span class="description"><?= __('Selected by default', 'teambooking') ?></span>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
        $return = ob_get_clean();
    } elseif (get_class($field) == 'TeamBookingFormSelect') {
        /* @var $field TeamBookingFormSelect */
        ob_start();
        ?>
        <table class="widefat" style="width:730px;">
            <tbody>
            <tr>
                <td rowspan="4" class="tb-no-select" style="width: 20px;">
                    <?= $move_buttons ?>
                </td>
                <td rowspan="4" style="width:150px;">
                    <span style="font-size: initial;"><strong><?= __('Select', 'teambooking') ?></strong></span>
                    <!-- hidden content -->
                    <div class="tb-hide">
                        <br>
                        <?= $fieldset ?>
                    </div>
                </td>
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_label]" value="<?= $field->getLabel() ?>">
                    <span class="description"><?= __('Label', 'teambooking') ?></span>

                    <div style="float:right;">
                        <?= $buttons ?>
                    </div>
                </td>
            </tr>
            <!-- hidden content -->
            <tr class="tb-hide">
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_default]" value="<?= $field->getValue() ?>"
                           placeholder="<?= __('none', 'teambooking') ?>">
                    <span class="description"><?= __('First listed option', 'teambooking') ?></span>
                </td>
            </tr>
            <tr class="tb-hide">
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_options]"
                           value="<?= implode(", ", $field->getOptions()) ?>">
                    <span class="description"><?= __('Other options (comma-separated)', 'teambooking') ?></span>
                </td>
            </tr>
            <tr class="tb-hide">
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_hook]" value="<?= $field->getHook() ?>"
                           placeholder="<?= __('without square brackets', 'teambooking') ?>">
                    <span class="description"><?= __('Hook', 'teambooking') ?></span>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
        $return = ob_get_clean();
    } elseif (get_class($field) == 'TeamBookingFormRadio') {
        /* @var $field TeamBookingFormRadio */
        ob_start();
        ?>
        <table class="widefat" style="width:730px;">
            <tbody>
            <tr>
                <td rowspan="4" class="tb-no-select" style="width: 20px;">
                    <?= $move_buttons ?>
                </td>
                <td rowspan="4" style="width:150px;">
                    <span style="font-size: initial;"><strong>Radio group</strong></span>
                    <!-- hidden content -->
                    <div class="tb-hide">
                        <br>
                        <?= $fieldset ?>
                    </div>
                </td>
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_label]" value="<?= $field->getLabel() ?>">
                    <span class="description"><?= __('Label', 'teambooking') ?></span>

                    <div style="float:right;">
                        <?= $buttons ?>
                    </div>
                </td>
            </tr>
            <!-- hidden content -->
            <tr class="tb-hide">
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_default]" value="<?= $field->getValue() ?>">
                    <span class="description"><?= __('Default selected option', 'teambooking') ?></span>
                </td>
            </tr>
            <tr class="tb-hide">
                <td>
                    <input type="text" class="all-options" name="<?= $fieldname ?>[<?= $field->getHook() ?>_options]"
                           value="<?= implode(", ", $field->getOptions()) ?>">
                    <span class="description"><?= __('Other options (comma-separated)', 'teambooking') ?></span>
                </td>
            </tr>
            <tr class="tb-hide">
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_hook]" value="<?= $field->getHook() ?>"
                           placeholder="<?= __('without square brackets', 'teambooking') ?>">
                    <span class="description"><?= __('Hook', 'teambooking') ?></span>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
        $return = ob_get_clean();
    } elseif (get_class($field) == 'TeamBookingFormFileUpload') {
        /* @var $field TeamBookingFormFileUpload */
        ob_start();
        ?>
        <table class="widefat" style="width:730px;">
            <tbody>
            <tr>
                <td rowspan="3" class="tb-no-select" style="width: 20px;">
                    <?= $move_buttons ?>
                </td>
                <td rowspan="3" style="width:150px;">
                    <span style="font-size: initial;"><strong><?= __('File Field', 'teambooking') ?></strong></span>
                    <!-- hidden content -->
                    <div class="tb-hide">
                        <br>
                        <?= $fieldset ?>
                    </div>
                </td>
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_label]" value="<?= $field->getLabel() ?>">
                    <span class="description"><?= __('Label', 'teambooking') ?></span>

                    <div style="float:right;">
                        <?= $buttons ?>
                    </div>
                </td>
            </tr>
            <!-- hidden content -->
            <tr class="tb-hide">
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="<?= $fieldname ?>[<?= $field->getHook() ?>_hook]" value="<?= $field->getHook() ?>"
                           placeholder="<?= __('without square brackets', 'teambooking') ?>">
                    <span class="description"><?= __('Hook', 'teambooking') ?></span>
                </td>
            </tr>
            <tr class="tb-hide">
                <td>
                    <input data-ays-ignore="true" type="text" class="all-options"
                           name="event[form_fields][custom][<?= $field->getHook() ?>_extensions]"
                           value="<?= $field->getFileExtensions(TRUE) ?>">
                    <span
                        class="description"><?= __('Allowed extensions (no dots, comma separated)', 'teambooking') ?></span>
                </td>
            </tr>
            </tbody>
        </table>
        <?php
        $return = ob_get_clean();
    }

    return $return;
}

/**
 * Used for abstractions
 */
function tbReservationDataToSlot(TeamBooking_ReservationData $data)
{
    $slot = new TeamBooking_Slot();
    $slot->setServiceId($data->getServiceId());
    $slot->setServiceName($data->getServiceName());
    $slot->setServiceInfo($data->getServiceDescription());
    $slot->setStartTime($data->getSlotStart());
    $slot->setEndTime($data->getSlotEnd());
    $slot->setEventId($data->getGoogleCalendarEvent());
    $slot->setCalendarId($data->getGoogleCalendarId());
    $slot->setEventIdParent($data->getGoogleCalendarEventParent());
    $slot->setAttendeesNumber($data->getTickets());
    $slot->setAllDayFalse();
    $slot->setCoworkerId($data->getCoworker());
    if (tbGetSettings()->getService($data->getServiceId())->getAttendees() <= $slot->getAttendeesNumber() ||
        $data->getServiceClass() == 'appointment'
    ) {
        $slot->setSoldoutTrue();
    }

    return $slot;
}
