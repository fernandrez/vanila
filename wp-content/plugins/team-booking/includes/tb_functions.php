<?php
// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

/////////////////////////////////
//                             //
//  INCLUDING AJAX CALLBACKS   //
//                             //
/////////////////////////////////
include_once dirname(TEAMBOOKING_FILE_PATH) . '/includes/tb_ajax_calls.php';

/////////////////////////////////
//                             //
//  INCLUDING FILE GENERATORS  //
//                             //
/////////////////////////////////
include_once dirname(TEAMBOOKING_FILE_PATH) . '/includes/tb_file_generators.php';

/////////////////////////
//                     //
//  INCLUDING TOOLKIT  //
//                     //
/////////////////////////
include_once dirname(TEAMBOOKING_FILE_PATH) . '/includes/tb_toolkit.php';

//////////////////////////////
//                          //
//  INCLUDING DATABASE I/O  //
//                          //
//////////////////////////////
include_once dirname(TEAMBOOKING_FILE_PATH) . '/includes/tb_database_io.php';

/**
 * Returns the plugin settings object
 *
 * @return TeamBookingSettings Plugin settings object
 */
function tbGetSettings()
{
    global $team_booking_settings;
    if ($team_booking_settings instanceof TeamBookingSettings) {
        return $team_booking_settings;
    } else {
        return get_option('team_booking');
    }
}

/**
 * Get the Coworker ID list
 *
 * @return array Coworkers ID list
 */
function tbGetCoworkersIdList()
{
    $all_users = get_users();
    $coworkers = array();
    foreach ($all_users as $user) {
        if ($user->has_cap('tb_can_sync_calendar')) {
            $coworkers[] = $user->ID;
        }
    }
    if (!is_null($coworkers)) {
        return array_values($coworkers);
    } else {
        return array();
    }
}

/**
 * Get the authorized Coworker ID list
 *
 * @return array Coworkers ID list
 */
function tbGetAuthCoworkersIdList()
{
    $coworkers_data = tbGetSettings()->getCoworkersData();
    $list = array();
    if (!is_null($coworkers_data)) {
        foreach ($coworkers_data as $coworker_id => $data) {
            $token = $data->getAccessToken();
            if (!empty($token)) {
                $list[] = $coworker_id;
            }
        }
    }

    return $list;
}

/**
 * Get the authorized Coworkers list
 *
 * @return array
 */
function tbGetAuthCoworkersList()
{
    $results = array();
    $coworkers_data = tbGetSettings()->getCoworkersData();
    foreach ($coworkers_data as $id => $data) {
        /* @var $data TeamBookingCoworker */
        if ($data->getAccessToken()) {
            $results[$id]['name'] = $data->getDisplayName();
            $results[$id]['email'] = $data->getEmail();
            $results[$id]['roles'] = $data->getRoles();
            $results[$id]['tokens'] = $data->getAccessToken();
        }
    }

    return $results;
}

function tbGetAllCoworkersList()
{
    $results = array();
    // Get the ids of users with TB capability right now
    $ids = tbGetCoworkersIdList();
    // Get the eventual list of coworkers already present
    $present_data = tbGetSettings()->getCoworkersData();
    foreach ($ids as $id) {
        if (isset($present_data[$id])) {
            $coworker = $present_data[$id];
            unset($present_data[$id]);
        } else {
            $coworker = new TeamBookingCoworker($id);
        }
        $results[$id]['name'] = $coworker->getDisplayName();
        $results[$id]['email'] = $coworker->getEmail();
        $results[$id]['calendar'] = $coworker->getCalendarId();
        $results[$id]['roles'] = $coworker->getRoles();
        if (isset(json_decode($coworker->getAccessToken())->refresh_token)) {
            $results[$id]['token'] = 'refresh';
        } elseif (isset(json_decode($coworker->getAccessToken())->access_token)) {
            $results[$id]['token'] = 'access';
        } else {
            $results[$id]['token'] = '';
        }
    }
    // There are coworkers without TB capability left?
    if (!empty($present_data)) {
        foreach ($present_data as $id => $data) {
            if (get_userdata($id) == FALSE) {
                // User not exists anymore
                $settings = tbGetSettings();
                $settings->dropCoworkerData($id);
                $settings->save();
                continue;
            }
            $results[$id]['name'] = $data->getDisplayName();
            $results[$id]['email'] = $data->getEmail();
            $results[$id]['calendar'] = $data->getCalendarId();
            $results[$id]['roles'] = $data->getRoles();
            if (isset(json_decode($data->getAccessToken())->refresh_token)) {
                $results[$id]['token'] = 'refresh';
            } elseif (isset(json_decode($data->getAccessToken())->access_token)) {
                $results[$id]['token'] = 'access';
            } else {
                $results[$id]['token'] = '';
            }
            $results[$id]['allowed_no_more'] = TRUE;
        }
    }

    return $results;
}

/**
 * Renders the field validation modal in service settings tab
 */
function tbGetValidationModal($field, $fieldname)
{
    ob_start();
    ?>
    <div class="ui small modal" id="tb-field-regex-modal-<?= $field->getHook() ?>">
        <i class="close tb-icon"></i>

        <div class="content" style="display:block;width: initial;">
            <div style="font-style: italic;font-weight: 300;">
                <?= __('Validation rule', 'teambooking') ?>
            </div>
            <select style="margin-bottom:10px;width:99%;" name="<?= $fieldname ?>[<?= $field->getHook() ?>_validation]">
                <option
                    value="none" <?php selected('none', $field->getValidationRule(), TRUE); ?>><?= __('No validation', 'teambooking') ?></option>
                <option
                    value="email" <?php selected('email', $field->getValidationRule(), TRUE); ?>><?= __('Email', 'teambooking') ?></option>
                <option
                    value="alphanumeric" <?php selected('alphanumeric', $field->getValidationRule(), TRUE); ?>><?= __('Alphanumeric', 'teambooking') ?></option>
                <option
                    value="phone" <?php selected('phone', $field->getValidationRule(), TRUE); ?>><?= __('Phone number (US only)', 'teambooking') ?></option>
                <option
                    value="custom" <?php selected('custom', $field->getValidationRule(), TRUE); ?>><?= __('Custom', 'teambooking') ?></option>
            </select>

            <div style="font-style: italic;font-weight: 300;">
                <?= __('Custom validation regex (works if "custom" is selected)', 'teambooking') ?>
            </div>
            <input type="text" class="large-text" style="margin-bottom:10px;"
                   name="<?= $fieldname ?>[<?= $field->getHook() ?>_regex]"
                   value="<?= $field->getValidationRegex(TRUE) ?>">

            <div class="ui button tb-close" style="height:auto">
                <?= __('Close', 'teambooking') ?>
            </div>
        </div>
    </div>
    <script>
        jQuery('#tb-field-regex-open-<?= $field->getHook() ?>').on('click', function (e) {
            e.preventDefault();
            jQuery('#tb-field-regex-modal-<?= $field->getHook() ?>')
                .uiModal({detachable: false})
                .uiModal('attach events', '.tb-close', 'hide')
                .uiModal('show')
            ;
        });
    </script>
    <?php
    return ob_get_clean();
}

/**
 * Checks if there is a booking with that ID already
 *
 * @param string $id
 * @return boolean TRUE if exists
 */
function tbCheckServiceIdExistance($id)
{
    $services = tbGetSettings()->getServices();

    return (array_key_exists($id, $services));
}

/**
 * Checks if there is a booking with that name already
 *
 * @param string $name
 * @return boolean
 */
function tbCheckServiceNameExistance($name)
{
    $services = tbGetSettings()->getServices();
    $response = FALSE;
    foreach ($services as $service) {
        /* @var $service TeamBookingType */
        if (strtolower($service->getName()) == strtolower($name)) {
            $response = TRUE;
            break;
        }
    }

    return $response;
}

/**
 *
 * Error log
 *
 * @param TeamBooking_ErrorLog $log
 */
function tbErrorLog(TeamBooking_ErrorLog $log)
{
    $settings = tbGetSettings();
    $error_logs = $settings->getErrorLogs();
    $now = current_time('timestamp');
    $log->setTimestamp($now);
    if (count($error_logs) >= 10) {
        array_shift($error_logs);
    }
    $error_logs[] = $log;
    $settings->setErrorLogs($error_logs);
    $settings->save();
}

/**
 * Returns appropriate text color
 *
 * @param string $color (hexadecimal color value)
 * @param bool|FALSE $prefer_white
 * @return string
 */
function tbGetRightTextColor($color, $prefer_white = FALSE)
{
    if (!$prefer_white) {
        $brightness_limit = 145;
    } else {
        $brightness_limit = 185;
    }
    $rgb = tbHex2RGB($color);
    if (!$rgb) {
        return "inherit";
    }
    $brightness = sqrt(
        $rgb["red"] * $rgb["red"] * .299 +
        $rgb["green"] * $rgb["green"] * .587 +
        $rgb["blue"] * $rgb["blue"] * .114);
    if ($brightness < $brightness_limit) {
        return "#FFFFFF";
    } else {
        return "#414141";
    }
}

/**
 * Returns appropriate hover color
 *
 * @param string $color (hexadecimal color value)
 * @return boolean
 */
function tbGetRightHoverColor($color)
{
    $rgb = tbHex2RGB($color);
    $brightness = sqrt(
        $rgb["red"] * $rgb["red"] * .299 +
        $rgb["green"] * $rgb["green"] * .587 +
        $rgb["blue"] * $rgb["blue"] * .114);
    if ($brightness < 145) {
        return "rgba(255, 255, 255, 0.15)";
    } else {
        return "rgba(0, 0, 0, 0.15)";
    }
}

function tbCurrencyCodeToSymbol($cc, $amount = 0)
{
    $currencies = tbGetCurrencies();
    if (!isset($currencies[$cc])) {
        $symbol = "$";
        $position = 'before';
    } else {
        $symbol = $currencies[$cc]['symbol'];
        $position = $currencies[$cc]['format'];
    }

    if (is_null($amount)) {
        return $symbol;
    } else {
        if ($position == 'after') {
            return number_format((float)$amount, 2) . $symbol;
        } else {
            return $symbol . number_format((float)$amount, 2);
        }
    }
}

function enqueueTeamBookingStylesFooter()
{
    if (!tbGetSettings()->getSkipGmapLibs()) {
        wp_enqueue_script('google-places-script');
    }
    if (tbGetSettings()->getPaymentGatewaySettingObject('stripe')->isActive()
        && tbGetSettings()->getPaymentGatewaySettingObject('stripe')->isLoadLibrary()
    ) {
        wp_enqueue_script('stripejs');
    }
    wp_enqueue_script('tb-geocomplete-script');
    wp_enqueue_script('tb-gmap3-script');
    wp_enqueue_style('semantic-style');
    wp_enqueue_style('teambooking-style-frontend');
    wp_enqueue_script('tb-frontend-script');
    wp_enqueue_style('teambooking_fonts');
}

function tbCleanReservations($all = FALSE, $just_check = FALSE, $timeout_override = FALSE, $pending = FALSE)
{
    global $wpdb;
    if (!$pending) {
        $table_name = $wpdb->prefix . "teambooking_reservations";
    } else {
        $table_name = $wpdb->prefix . "teambooking_reservations_pending";
    }
    if ($timeout_override) {
        $timeout = $timeout_override;
    } else {
        $timeout = tbGetSettings()->getDatabaseReservationTimeout();
    }
    if ($all) {
        $wpdb->query("TRUNCATE TABLE $table_name");
    } else {
        $now = tbGetNowInSecondsUTC(); // UTC, seconds
        $reservations = tbGetReservations($pending);
        $count = 0;
        foreach ($reservations as $id => $reservation) {
            /* @var $reservation TeamBooking_ReservationData */
            if ($reservation instanceof TeamBooking_ReservationData) {
                $age = $now - $reservation->getCreationInstant(); // UTC, seconds
                if ($age > $timeout && $timeout) {
                    if (!$just_check) {
                        $wpdb->delete($table_name, array('id' => $id));
                    } else {
                        $count++;
                    }
                }
            }
        }
        if ($count) {
            return $count;
        } else {
            return FALSE;
        }
    }
}

function tbCleanReservationsRoutine()
{
    global $pagenow;
    if (($pagenow == 'admin.php') && ($_GET['page'] == 'team-booking')) {
        tbCleanReservations(FALSE, FALSE, tbGetSettings()->getMaxPendingTime(), TRUE);
        tbCleanReservations();

        return TRUE;
    }
}

function tbIsReservationTimedOut(TeamBooking_ReservationData $reservation, TeamBookingSettings $settings)
{
    $timeout = $settings->getMaxPendingTime(); // seconds
    $now = tbGetNowInSecondsUTC(); // UTC, seconds
    $reservation_time = $reservation->getCreationInstant(); // UTC, seconds
    if (($now - $reservation_time) > $timeout) {
        return TRUE;
    } else {
        return FALSE;
    }
}

/**
 * Handler for uploaded files
 *
 * Returns associative array:
 * $movefile[file] The local path to the uploaded file.
 * $movefile[url] The public URL for the uploaded file.
 * $movefile[type] The MIME type.
 *
 * @param $file
 * @return bool
 */
function tbHandleFileUpload($file)
{
    if (!function_exists('wp_handle_upload')) {
        require_once(ABSPATH . 'wp-admin/includes/file.php');
    }
    $upload_overrides = array('test_form' => FALSE);
    $movefile = wp_handle_upload($file, $upload_overrides);
    if ($movefile) {
        return $movefile;
    } else {
        return FALSE;
    }
}

function tbCustomAPICaller(TeamBooking_ReservationData $data, $location)
{
    /**
     * Locations:
     *
     * after_reservation
     * before_reservation
     */
    foreach (get_declared_classes() as $class_name) {
        if (in_array('TeamBooking_API', class_implements($class_name))) {
            $class = new $class_name;
            $class->setData($data);
            $class->$location;
        }
    }

    return;
}

function date_i18n_tb($format, $value)
{
    $prev = date_default_timezone_get();
    date_default_timezone_set('UTC');
    $return = date_i18n($format, $value);
    date_default_timezone_set($prev);

    return $return;
}
