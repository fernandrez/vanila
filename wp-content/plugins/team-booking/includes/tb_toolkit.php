<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

/**
 * Find and replace hooks in a string.
 *
 * Hooks must be in the form: [hook]
 *
 * @param string $string String with hooks
 * @param array $variables Hooks values
 * @return string String with hooks replaced by values
 */
function tbFindAndReplaceHooks($string, array $variables)
{
    $regex = "/(\[.*?\])/";
    // Closures requires PHP >= 5.3
    $return = preg_replace_callback($regex, function ($matches) use ($variables) {
        if (isset($variables[strtolower(trim($matches[1], "[]"))])) {
            return $variables[strtolower(trim($matches[1], "[]"))];
        } else {
            return $matches[1];
        }
    }, $string);

    return $return;
}

/**
 * Generates a random number
 *
 * Used in session ID for Google Calendar events caching
 *
 * @param int $length
 * @return int
 */
function tbRandomNumber($length)
{
    $result = '';
    for ($i = 0; $i < $length; $i++) {
        $result .= mt_rand(0, 9);
    }

    return $result;
}

/**
 * String filter function
 *
 * Checks for magic quotes (by PHP server side, or WP own method)
 * If check is true, then stripslashes.
 *
 * It also does htmlentities() for convenience
 *
 * If $remove_special_chars is TRUE, then get rid of
 * spaces and special chars and lowercase all (mandatory for IDs)
 *
 * @param string $text
 * @return string filtered text
 */
function tbFilterInput($text, $remove_special_chars = FALSE)
{
    if (get_magic_quotes_gpc() || function_exists('wp_magic_quotes')) {
        $text = stripslashes($text);
    }
    $return = htmlentities($text, ENT_QUOTES, 'UTF-8');
    if ($remove_special_chars) {
        $return = str_replace(' ', '-', $return); // Replaces all spaces with hyphens.
        $return = preg_replace('/[^A-Za-z0-9\-\_]/', '', $return); // Removes special chars except underscores
        $return = preg_replace('/-+/', '-', $return); // Replaces multiple hyphens with single one.
        $return = strtolower($return); // Lowercase all
    }

    return $return;
}

function tbStripTags($text)
{
    $default_attr = array(
        'id'    => array(),
        'class' => array(),
        'title' => array(),
        'style' => array(),
    );
    $allowedtags = array(
        'a'      => array_merge(array(
            'href' => TRUE,
        ), $default_attr),
        'img'    => array_merge(array(
            'src'    => TRUE,
            'width'  => TRUE,
            'height' => TRUE,
        ), $default_attr),
        'style'  => array(),
        'b'      => array($default_attr),
        'code'   => array($default_attr),
        'em'     => array($default_attr),
        'i'      => array($default_attr),
        'strong' => array(),
        'ul'     => array($default_attr),
        'ol'     => array($default_attr),
        'li'     => array($default_attr),
        'div'    => array($default_attr),
        'span'   => array($default_attr),
        'table'  => array($default_attr),
        'td'     => array($default_attr),
        'th'     => array($default_attr),
        'tr'     => array($default_attr),
        'pre'    => array($default_attr),
    );

    return wp_kses($text, $allowedtags);
}

/**
 * String unfilter function
 *
 * Centralize any unfiltering need on fields
 * Must be used together with filterInputTB
 *
 * @param string $text
 * @return string un-filtered text
 */
function tbUnfilterInput($text)
{
    return html_entity_decode($text, ENT_QUOTES, 'UTF-8');
}

/**
 * http://www.php.net/manual/en/function.hexdec.php#99478
 *
 * Convert a hexa decimal color code to its RGB equivalent
 *
 * @param string $hexStr (hexadecimal color value)
 * @param boolean $returnAsString (if set true, returns the value separated by the separator character.
 * Otherwise returns associative array)
 * @param string $seperator (to separate RGB values. Applicable only if second parameter is true.)
 * @return array or string (depending on second parameter. Returns False if invalid hex color value)
 */
function tbHex2RGB($hexStr, $returnAsString = FALSE, $seperator = ',')
{
    $hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
    $rgbArray = array();
    if (strlen($hexStr) == 6) { //If a proper hex code, convert using bitwise operation. No overhead... faster
        $colorVal = hexdec($hexStr);
        $rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
        $rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
        $rgbArray['blue'] = 0xFF & $colorVal;
    } elseif (strlen($hexStr) == 3) { //if shorthand notation, need some string manipulations
        $rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
        $rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
        $rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
    } else {
        return FALSE; //Invalid hex color code
    }

    return $returnAsString ? implode($seperator, $rgbArray) : $rgbArray; // returns the rgb string or the associative array
}

/**
 * Strips all brackets in a string
 *
 * @param string $string
 * @return string
 */
function tbStripAllBrackets($string)
{
    $string = str_replace("(", "", str_replace(")", "", $string)); // round
    $string = str_replace("[", "", str_replace("]", "", $string)); // square
    $string = str_replace("{", "", str_replace("}", "", $string)); // curly
    return $string;
}

/**
 * Returns the correct offset of ANY GIVEN TIME,
 * taking DST into account.
 *
 * DST is available only when a proper WordPress timezone
 * is present.
 *
 * @param int $the_time
 * @return int
 */
function tbGetRightGmtOffset($the_time)
{
    $timezone_string = get_option('timezone_string');
    if (empty($timezone_string)) {
        $gmt_offset = get_option('gmt_offset');
    } else {
        $tz = new DateTimeZone($timezone_string);
        $transition = $tz->getTransitions($the_time, $the_time);
        $gmt_offset = $transition[0]['offset'] / 3600;
    }

    return $gmt_offset;
}

/**
 * A list of almost all currencies
 *
 * @return array
 */
function tbGetCurrencies()
{
    $currencies = array(
        'AED' => array(
            'label'  => 'United Arab Emirates Dirham',
            'format' => 'after',
            'locale' => 'ar_AE',
            'symbol' => '&#x62f;&#x2e;&#x625;',
        ),
        'AFN' => array(
            'label'  => 'Afghan Afghani',
            'format' => 'after',
            'locale' => 'fa_AF',
            'symbol' => '&#1547;',
        ),
        'ALL' => array(
            'label'  => 'Albanian Lek',
            'format' => 'before',
            'locale' => 'sq_AL',
            'symbol' => 'Lek',
        ),
        'AMD' => array(
            'label'  => 'Armenian Dram',
            'format' => 'before',
            'locale' => 'hy_AM',
            'symbol' => '&#1423;',
        ),
        'ANG' => array(
            'label'  => 'Netherlands Antillean Gulden',
            'format' => 'before',
            'locale' => 'nl_SX',
            'symbol' => '&#402;',
        ),
        'AOA' => array(
            'label'  => 'Angolan Kwanza',
            'format' => 'before',
            'locale' => 'pt_AO',
            'symbol' => 'Kz;',
        ),
        'ARS' => array(
            'label'  => 'Argentine Peso',
            'format' => 'before',
            'locale' => 'es_AR',
            'symbol' => '$',
        ),
        'AUD' => array(
            'label'  => 'Australian Dollar',
            'format' => 'before',
            'locale' => 'en_AU',
            'symbol' => '$',
        ),
        'AWG' => array(
            'label'  => 'Aruban Florin',
            'format' => 'before',
            'locale' => 'nl_AW',
            'symbol' => '&#402;',
        ),
        'AZN' => array(
            'label'  => 'Azerbaijani Manat',
            'format' => 'before',
            'locale' => 'az_Latn_AZ',
            'symbol' => '&#8380;',
        ),
        'BAM' => array(
            'label'  => 'Bosnia & Herzegovina Convertible Mark',
            'format' => 'before',
            'locale' => 'hr_BA',
            'symbol' => 'KM',
        ),
        'BBD' => array(
            'label'  => 'Barbadian Dollar',
            'format' => 'before',
            'locale' => 'en_BB',
            'symbol' => '$',
        ),
        'BDT' => array(
            'label'  => 'Bangladeshi Taka',
            'format' => 'before',
            'locale' => 'bn_BD',
            'symbol' => '&#2547;'
            // or Tk
        ),
        'BGN' => array(
            'label'  => 'Bulgarian Lev',
            'format' => 'before',
            'locale' => 'bg_BG',
            'symbol' => 'лв',
        ),
        'BIF' => array(
            'label'  => 'Burundian Franc',
            'format' => 'before',
            'locale' => 'rn_BI',
            'symbol' => 'FBu',
        ),
        'BMD' => array(
            'label'  => 'Bermudian Dollar',
            'format' => 'before',
            'locale' => 'en_BM',
            'symbol' => '$',
        ),
        'BND' => array(
            'label'  => 'Brunei Dollar',
            'format' => 'before',
            'locale' => 'ms_Latn_BN',
            'symbol' => '$',
        ),
        'BOB' => array(
            'label'  => 'Bolivian Boliviano',
            'format' => 'before',
            'locale' => 'es_BO',
            'symbol' => 'Bs.',
        ),
        'BRL' => array(
            'label'  => 'Brazilian Real',
            'format' => 'before',
            'locale' => 'pt_BR',
            'symbol' => 'R$',
        ),
        'BRL' => array(
            'label'  => 'Brazilian Real',
            'format' => 'before',
            'locale' => 'pt_BR',
            'symbol' => 'R$',
        ),
        'BSD' => array(
            'label'  => 'Bahamian Dollar',
            'format' => 'before',
            'locale' => 'en_BS',
            'symbol' => 'B$',
        ),
        'BWP' => array(
            'label'  => 'Botswana Pula',
            'format' => 'before',
            'locale' => 'en_BW',
            'symbol' => 'P',
        ),
        'BZD' => array(
            'label'  => 'Belize Dollar',
            'format' => 'before',
            'locale' => 'en_BZ',
            'symbol' => 'BZ$',
        ),
        'CAD' => array(
            'label'  => 'Canadian Dollar',
            'format' => 'before',
            'locale' => 'en_CA',
            'symbol' => '$',
        ),
        'CDF' => array(
            'label'  => 'Congolese Franc',
            'format' => 'before',
            'locale' => 'fr_CD',
            'symbol' => 'FC',
        ),
        'CHF' => array(
            'label'  => 'Swiss Franc',
            'format' => 'before',
            'locale' => 'fr_CH',
            'symbol' => 'Fr',
        ),
        'CLP' => array(
            'label'  => 'Chilean Peso',
            'format' => 'before',
            'locale' => 'es_CL',
            'symbol' => '$',
        ),
        'CNY' => array(
            'label'  => 'Chinese Renminbi Yuan',
            'format' => 'before',
            'locale' => 'zh_Hans_CN',
            'symbol' => '&#165;',
        ),
        'COP' => array(
            'label'  => 'Colombian Peso',
            'format' => 'before',
            'locale' => 'es_CO',
            'symbol' => '$',
        ),
        'CRC' => array(
            'label'  => 'Costa Rican Colón',
            'format' => 'before',
            'locale' => 'es_CR',
            'symbol' => '&#8353;',
        ),
        'CVE' => array(
            'label'  => 'Cape Verdean Escudo',
            'format' => 'before',
            'locale' => 'pt_CV',
            'symbol' => 'Esc',
        ),
        'CZK' => array(
            'label'  => 'Czech Koruna',
            'format' => 'after',
            'locale' => 'cs_CZ',
            'symbol' => 'Kč',
        ),
        'DJF' => array(
            'label'  => 'Djiboutian Franc',
            'format' => 'before',
            'locale' => 'fr_DJ',
            'symbol' => 'Fdj',
        ),
        'DKK' => array(
            'label'  => 'Danish Krone',
            'format' => 'before',
            'locale' => 'da_DK',
            'symbol' => 'kr',
        ),
        'DOP' => array(
            'label'  => 'Dominican Peso',
            'format' => 'before',
            'locale' => 'es_DO',
            'symbol' => '$',
        ),
        'DZD' => array(
            'label'  => 'Algerian Dinar',
            'format' => 'before',
            'locale' => 'fr_DZ',
            'symbol' => '&#1583;.&#1580;',
        ),
        'EGP' => array(
            'label'  => 'Egyptian Pound',
            'format' => 'before',
            'locale' => 'ar_EG',
            'symbol' => 'E&pound;',
        ),
        'ETB' => array(
            'label'  => 'Ethiopian Birr',
            'format' => 'before',
            'locale' => 'so_ET',
            'symbol' => 'Br',
        ),
        'EUR' => array(
            'label'  => 'Euro',
            'format' => 'before',
            'locale' => '',
            'symbol' => '&euro;',
        ),
        'FJD' => array(
            'label'  => 'Fijian Dollar',
            'format' => 'before',
            'locale' => 'en_FJ',
            'symbol' => '$',
        ),
        'FKP' => array(
            'label'  => 'Falkland Islands Pound',
            'format' => 'before',
            'locale' => 'en_FK',
            'symbol' => '&pound;',
        ),
        'GBP' => array(
            'label'  => 'British Pound',
            'format' => 'before',
            'locale' => 'en_UK',
            'symbol' => '&pound;',
        ),
        'GEL' => array(
            'label'  => 'Georgian Lari',
            'format' => 'before',
            'locale' => 'ka_GE',
            'symbol' => '&#4314;',
        ),
        'GIP' => array(
            'label'  => 'Gibraltar Pound',
            'format' => 'before',
            'locale' => 'en_GI',
            'symbol' => '&pound;',
        ),
        'GMD' => array(
            'label'  => 'Gambian Dalasi',
            'format' => 'before',
            'locale' => 'en_GM',
            'symbol' => 'D',
        ),
        'GNF' => array(
            'label'  => 'Guinean Franc',
            'format' => 'before',
            'locale' => 'fr_GN',
            'symbol' => 'FG',
        ),
        'GTQ' => array(
            'label'  => 'Guatemalan Quetzal',
            'format' => 'before',
            'locale' => 'es_GT',
            'symbol' => 'Q',
        ),
        'GYD' => array(
            'label'  => 'Guyanese Dollar',
            'format' => 'before',
            'locale' => 'en_GY',
            'symbol' => '$',
        ),
        'HKD' => array(
            'label'  => 'Hong Kong Dollar',
            'format' => 'before',
            'locale' => 'en_HK',
            'symbol' => 'HK$',
        ),
        'HNL' => array(
            'label'  => 'Honduran Lempira',
            'format' => 'before',
            'locale' => 'es_HN',
            'symbol' => 'L',
        ),
        'HRK' => array(
            'label'  => 'Croatian Kuna',
            'format' => 'before',
            'locale' => 'hr_HR',
            'symbol' => 'kn',
        ),
        'HTG' => array(
            'label'  => 'Haitian Gourde',
            'format' => 'before',
            'locale' => 'fr_HT',
            'symbol' => 'G',
        ),
        'HUF' => array(
            'label'  => 'Hungarian Forint',
            'format' => 'before',
            'locale' => 'hu_HU',
            'symbol' => 'Ft',
        ),
        'IDR' => array(
            'label'  => 'Indonesian Rupiah',
            'format' => 'before',
            'locale' => 'id_ID',
            'symbol' => 'Rp',
        ),
        'ILS' => array(
            'label'  => 'Israeli New Sheqel',
            'format' => 'before',
            'locale' => 'he_IL',
            'symbol' => '&#8362;',
        ),
        'INR' => array(
            'label'  => 'Indian Rupee',
            'format' => 'before',
            'locale' => 'en_IN',
            'symbol' => '&#8377;',
        ),
        'ISK' => array(
            'label'  => 'Icelandic Króna',
            'format' => 'before',
            'locale' => 'is_IS',
            'symbol' => 'kr',
        ),
        'JMD' => array(
            'label'  => 'Jamaican Dollar',
            'format' => 'before',
            'locale' => 'en_JM',
            'symbol' => '$',
        ),
        'JPY' => array(
            'label'  => 'Japanese Yen',
            'format' => 'before',
            'locale' => 'ja_JP',
            'symbol' => '&yen;',
        ),
        'KES' => array(
            'label'  => 'Kenyan Shilling',
            'format' => 'before',
            'locale' => 'en_KE',
            'symbol' => 'KSh',
        ),
        'KGS' => array(
            'label'  => 'Kyrgyzstani Som',
            'format' => 'before',
            'locale' => 'ru_KG',
            'symbol' => '&#1083;&#1074;',
        ),
        'KHR' => array(
            'label'  => 'Cambodian Riel',
            'format' => 'before',
            'locale' => 'km_KH',
            'symbol' => '&#6107;',
        ),
        'KMF' => array(
            'label'  => 'Comorian Franc',
            'format' => 'before',
            'locale' => 'fr_KM',
            'symbol' => 'CF',
        ),
        'KRW' => array(
            'label'  => 'South Korean Won',
            'format' => 'before',
            'locale' => 'ko_KR',
            'symbol' => '&#8361;',
        ),
        'KYD' => array(
            'label'  => 'Cayman Islands Dollar',
            'format' => 'before',
            'locale' => 'en_KY',
            'symbol' => '$',
        ),
        'KZT' => array(
            'label'  => 'Kazakhstani Tenge',
            'format' => 'before',
            'locale' => 'ru_KZ',
            'symbol' => '&#8376;',
        ),
        'LAK' => array(
            'label'  => 'Lao Kipa',
            'format' => 'before',
            'locale' => 'lo_LA',
            'symbol' => '&#8365;',
        ),
        'LBP' => array(
            'label'  => 'Lebanese Pound',
            'format' => 'after',
            'locale' => 'ar_LB',
            'symbol' => '&#1604;.&#1604;',
        ),
        'LKR' => array(
            'label'  => 'Sri Lankan Rupee',
            'format' => 'before',
            'locale' => 'si_LK',
            'symbol' => '&#588;s',
        ),
        'LRD' => array(
            'label'  => 'Liberian Dollar',
            'format' => 'before',
            'locale' => 'en_LR',
            'symbol' => '$',
        ),
        'LSL' => array(
            'label'  => 'Lesotho Loti',
            'format' => 'before',
            'locale' => '',
            'symbol' => 'L',
        ),
        'MAD' => array(
            'label'  => 'Moroccan Dirham',
            'format' => 'before',
            'locale' => 'ar_MA',
            'symbol' => '&#1583;.&#1605;.',
        ),
        'MDL' => array(
            'label'  => 'Moldovan Leu',
            'format' => 'before',
            'locale' => 'ro_MD',
            'symbol' => 'L',
        ),
        'MGA' => array(
            'label'  => 'Malagasy Ariary',
            'format' => 'before',
            'locale' => 'en_MG',
            'symbol' => 'Ar',
        ),
        'MKD' => array(
            'label'  => 'Macedonian Denar',
            'format' => 'before',
            'locale' => 'mk_MK',
            'symbol' => '&#1076;&#1077;&#1085;',
        ),
        'MNT' => array(
            'label'  => 'Mongolian Tögrög',
            'format' => 'before',
            'locale' => 'mn_Cyrl_MN',
            'symbol' => '&#8366;',
        ),
        'MOP' => array(
            'label'  => 'Macanese Pataca',
            'format' => 'before',
            'locale' => 'pt_MO',
            'symbol' => 'MOP$',
        ),
        'MRO' => array(
            'label'  => 'Mauritanian Ouguiya',
            'format' => 'before',
            'locale' => 'ar_MR',
            'symbol' => 'UM',
        ),
        'MUR' => array(
            'label'  => 'Mauritian Rupee',
            'format' => 'before',
            'locale' => 'en_MU',
            'symbol' => '&#588;s',
        ),
        'MVR' => array(
            'label'  => 'Maldivian Rufiyaa',
            'format' => 'before',
            'locale' => '',
            'symbol' => 'Rf',
        ),
        'MWK' => array(
            'label'  => 'Malawian Kwacha',
            'format' => 'before',
            'locale' => 'en_MW',
            'symbol' => 'MK',
        ),
        'MXN' => array(
            'label'  => 'Mexican Peso',
            'format' => 'before',
            'locale' => 'es_MX',
            'symbol' => '$',
        ),
        'MYR' => array(
            'label'  => 'Malaysian Ringgit',
            'format' => 'before',
            'locale' => 'ta_MY',
            'symbol' => 'RM',
        ),
        'MZN' => array(
            'label'  => 'Mozambican Metical',
            'format' => 'before',
            'locale' => 'mgh_MZ',
            'symbol' => 'MT',
        ),
        'NAD' => array(
            'label'  => 'Namibian Dollar',
            'format' => 'before',
            'locale' => 'naq_NA',
            'symbol' => '$',
        ),
        'NGN' => array(
            'label'  => 'Nigerian Naira',
            'format' => 'before',
            'locale' => 'en_NG',
            'symbol' => '&#8358;',
        ),
        'NIO' => array(
            'label'  => 'Nicaraguan Córdoba',
            'format' => 'before',
            'locale' => 'es_NI',
            'symbol' => 'C$',
        ),
        'NOK' => array(
            'label'  => 'Norwegian Krone',
            'format' => 'before',
            'locale' => 'se_NO',
            'symbol' => 'kr',
        ),
        'NPR' => array(
            'label'  => 'Nepalese Rupee',
            'format' => 'before',
            'locale' => 'ne_NP',
            'symbol' => 'N&#588;s',
        ),
        'NZD' => array(
            'label'  => 'New Zealand Dollar',
            'format' => 'before',
            'locale' => 'en_NZ',
            'symbol' => '$',
        ),
        'PAB' => array(
            'label'  => 'Panamanian Balboa',
            'format' => 'before',
            'locale' => 'es_PA',
            'symbol' => 'B/.',
        ),
        'PEN' => array(
            'label'  => 'Peruvian Nuevo Sol',
            'format' => 'before',
            'locale' => 'es_PE',
            'symbol' => 'S/.',
        ),
        'PGK' => array(
            'label'  => 'Papua New Guinean Kina',
            'format' => 'before',
            'locale' => 'en_PG',
            'symbol' => 'K',
        ),
        'PHP' => array(
            'label'  => 'Philippine Peso',
            'format' => 'before',
            'locale' => 'en_PH',
            'symbol' => '&#8369;',
        ),
        'PKR' => array(
            'label'  => 'Pakistani Rupee',
            'format' => 'before',
            'locale' => 'en_PK',
            'symbol' => '&#588;s',
        ),
        'PLN' => array(
            'label'  => 'Polish Złoty',
            'format' => 'before',
            'locale' => 'pl_PL',
            'symbol' => 'z&#322;',
        ),
        'PYG' => array(
            'label'  => 'Paraguayan Guaraní',
            'format' => 'before',
            'locale' => 'es_PY',
            'symbol' => '&#8370;',
        ),
        'QAR' => array(
            'label'  => 'Qatari Riyal',
            'format' => 'after',
            'locale' => 'ar_QA',
            'symbol' => '&#1585;.&#1602;',
        ),
        'RON' => array(
            'label'  => 'Romanian Leu',
            'format' => 'before',
            'locale' => 'ro_RO',
            'symbol' => 'L',
        ),
        'RSD' => array(
            'label'  => 'Serbian Dinar',
            'format' => 'before',
            'locale' => 'sr_Latn_RS',
            'symbol' => '&#1044;&#1080;&#1085;.',
        ),
        'RUB' => array(
            'label'  => 'Russian Ruble',
            'format' => 'before',
            'locale' => 'ru_RU',
            'symbol' => '&#1088;&#1091;&#1073;',
        ),
        'RWF' => array(
            'label'  => 'Rwandan Franc',
            'format' => 'before',
            'locale' => 'en_RW',
            'symbol' => 'RF',
        ),
        'SAR' => array(
            'label'  => 'Saudi Riyal',
            'format' => 'after',
            'locale' => 'ar_SA',
            'symbol' => '&#1585;.&#1587;',
        ),
        'SBD' => array(
            'label'  => 'Solomon Islands Dollar',
            'format' => 'before',
            'locale' => 'en_SB',
            'symbol' => '$',
        ),
        'SCR' => array(
            'label'  => 'Seychellois Rupee',
            'format' => 'before',
            'locale' => 'fr_SC',
            'symbol' => '&#588;s',
        ),
        'SEK' => array(
            'label'  => 'Swedish Krona',
            'format' => 'before',
            'locale' => 'sv_SE',
            'symbol' => 'kr',
        ),
        'SGD' => array(
            'label'  => 'Singapore Dollar',
            'format' => 'before',
            'locale' => 'en_SG',
            'symbol' => '$',
        ),
        'SHP' => array(
            'label'  => 'Saint Helenian Pound',
            'format' => 'before',
            'locale' => 'en_SH',
            'symbol' => '&pound;',
        ),
        'SLL' => array(
            'label'  => 'Sierra Leonean Leone',
            'format' => 'before',
            'locale' => 'en_SL',
            'symbol' => 'Le',
        ),
        'SOS' => array(
            'label'  => 'Somali Shilling',
            'format' => 'before',
            'locale' => 'so_SO',
            'symbol' => 'So. Sh.',
        ),
        'SRD' => array(
            'label'  => 'Surinamese Dollar',
            'format' => 'before',
            'locale' => 'nl_SR',
            'symbol' => '$',
        ),
        'STD' => array(
            'label'  => 'São Tomé and Príncipe Dobra',
            'format' => 'before',
            'locale' => 'pt_ST',
            'symbol' => 'Db',
        ),
        'SZL' => array(
            'label'  => 'Swazi Lilangeni',
            'format' => 'before',
            'locale' => 'en_SZ',
            'symbol' => 'L',
        ),
        'THB' => array(
            'label'  => 'Thai Baht',
            'format' => 'before',
            'locale' => 'th_TH',
            'symbol' => '&#3647;',
        ),
        'TJS' => array(
            'label'  => 'Tajikistani Somoni',
            'format' => 'before',
            'locale' => 'tg_Cyrl_TJ',
            'symbol' => 'SM',
        ),
        'TOP' => array(
            'label'  => 'Tongan Paʻanga',
            'format' => 'before',
            'locale' => 'en_TO',
            'symbol' => '$',
        ),
        'TRY' => array(
            'label'  => 'Turkish Lira',
            'format' => 'before',
            'locale' => 'tr_TR',
            'symbol' => '&#8378;',
        ),
        'TTD' => array(
            'label'  => 'Trinidad and Tobago Dollar',
            'format' => 'before',
            'locale' => 'en_TT',
            'symbol' => '$',
        ),
        'TWD' => array(
            'label'  => 'New Taiwan Dollar',
            'format' => 'before',
            'locale' => 'zh_Hant_TW',
            'symbol' => 'NT$',
        ),
        'TZS' => array(
            'label'  => 'Tanzanian Shilling',
            'format' => 'before',
            'locale' => 'en_TZ',
            'symbol' => 'TSh',
        ),
        'UAH' => array(
            'label'  => 'Ukrainian Hryvnia',
            'format' => 'before',
            'locale' => 'uk_UA',
            'symbol' => '&#8372;',
        ),
        'UGX' => array(
            'label'  => 'Ugandan Shilling',
            'format' => 'before',
            'locale' => 'en_UG',
            'symbol' => 'USh',
        ),
        'USD' => array(
            'label'  => 'United States Dollar',
            'format' => 'before',
            'locale' => 'en_US',
            'symbol' => '$',
        ),
        'UYU' => array(
            'label'  => 'Uruguayan Peso',
            'format' => 'before',
            'locale' => 'es_UY',
            'symbol' => '$U',
        ),
        'UZS' => array(
            'label'  => 'Uzbekistani Som',
            'format' => 'before',
            'locale' => 'uz_Latn_UZ',
            'symbol' => '&#1083;&#1074;',
        ),
        'VEF' => array(
            'label'  => 'Venezuelan Bolívar',
            'format' => 'before',
            'locale' => 'es_VE',
            'symbol' => 'Bs',
        ),
        'VND' => array(
            'label'  => 'Vietnamese Đồng',
            'format' => 'before',
            'locale' => 'vi_VN',
            'symbol' => '&#8363;',
        ),
        'VUV' => array(
            'label'  => 'Vanuatu Vatu',
            'format' => 'after',
            'locale' => 'en_VU',
            'symbol' => 'VT',
        ),
        'WST' => array(
            'label'  => 'Samoan Tala',
            'format' => 'before',
            'locale' => 'en_WS',
            'symbol' => 'WS$',
        ),
        'XAF' => array(
            'label'  => 'Central African Cfa Franc',
            'format' => 'before',
            'locale' => 'fr_CF',
            'symbol' => 'CFA',
        ),
        'XCD' => array(
            'label'  => 'East Caribbean Dollar',
            'format' => 'before',
            'locale' => 'en_AI',
            'symbol' => 'EC$',
        ),
        'XOF' => array(
            'label'  => 'West African Cfa Franc',
            'format' => 'before',
            'locale' => 'fr_BF',
            'symbol' => 'CFA',
        ),
        'XPF' => array(
            'label'  => 'Cfp Franc',
            'format' => 'before',
            'locale' => 'fr_PF',
            'symbol' => 'F',
        ),
        'YER' => array(
            'label'  => 'Yemeni Rial',
            'format' => 'after',
            'locale' => 'ar_YE',
            'symbol' => '&#65020;',
        ),
        'ZAR' => array(
            'label'  => 'South African Rand',
            'format' => 'before',
            'locale' => 'en_LS',
            'symbol' => 'R',
        ),
        'ZMW' => array(
            'label'  => 'Zambian Kwacha',
            'format' => 'before',
            'locale' => 'en_ZM',
            'symbol' => 'ZMW',
        ),
    );

    return $currencies;
}

/**
 * Returns the current time UTC in seconds
 *
 * @return int
 */
function tbGetNowInSecondsUTC()
{
    $default = date_default_timezone_get();
    date_default_timezone_set('UTC');
    $return = time();
    date_default_timezone_set($default);

    return $return;
}

/**
 * If a manual GMT offset is selected on WordPress
 * general settings, in order to work with DST
 * TeamBooking must pick the first Timezone
 * with equal offset.
 *
 * Correct results are not guaranteed, so
 * it's best for the user to set the right
 * Timezone in WordPress general settings.
 *
 * @return string
 */
function tbApproximateTimezone()
{
    $array = DateTimeZone::listIdentifiers();
    $now = new DateTime();
    $manual_offset = get_option('gmt_offset');
    $offset = ceil($manual_offset) * 3600 + ($manual_offset - ceil($manual_offset)) * 60 * 60;
    foreach ($array as $tz) {
        $tz_new = new DateTimeZone($tz);
        if ($tz_new->getOffset($now) == $offset) {
            $return = $tz_new->getName();
            break;
        }
    }

    return $return;
}

/**
 * Returns the site's local timezone
 *
 * @return \DateTimeZone
 */
function tbGetTimezone()
{
    try {
        $timezone = new DateTimeZone(get_option('timezone_string'));
    } catch (Exception $ex) {
        $timezone = new DateTimeZone(tbApproximateTimezone());
    }

    return $timezone;
}

/**
 * Extracts a domain from a given URL
 *
 * @param string $url
 * @return boolean/string
 */
function tbGetDomainFromUrl($url)
{
    $pieces = parse_url($url);
    $domain = isset($pieces['host']) ? $pieces['host'] : '';
    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
        return $regs['domain'];
    }

    return FALSE;
}

/**
 * URL safe base64 encoding
 *
 * @param string $input
 * @return string
 */
function tb_base64_url_encode($input)
{
    return strtr(base64_encode($input), '+/=', '-_,');
}

/**
 * URL safe base64 decoding
 *
 * @param string $input
 * @return string
 */
function tb_base64_url_decode($input)
{
    return base64_decode(strtr($input, '-_,', '+/='));
}

function tb_looking_for_JSON_in_string($string)
{
    $regex = "/(\{.*?\})/s";
    preg_match($regex, $string, $matches);

    return $matches;
}

function tb_debug()
{
    $trace = debug_backtrace();
    $rootPath = dirname(dirname(__FILE__));
    $file = str_replace($rootPath, '', $trace[0]['file']);
    $line = $trace[0]['line'];
    $var = $trace[0]['args'][0];
    $lineInfo = sprintf('<div><strong>%s</strong> (line <strong>%s</strong>)</div>', $file, $line);
    $debugInfo = sprintf('<pre>%s</pre>', print_r($var, TRUE));
    print_r($lineInfo . $debugInfo);
}

function tb_get_pattern($int, $hex)
{
    if ($int == 0) {
        // no pattern
        return;
    }

    $rgb = tbHex2RGB($hex);
    $brightness = sqrt(
        $rgb["red"] * $rgb["red"] * .299 +
        $rgb["green"] * $rgb["green"] * .587 +
        $rgb["blue"] * $rgb["blue"] * .114
    );
    $pattern_bright = '';
    $pattern_dark = '';
    switch ($int) {
        case 1:
            // diagonal stripes
            $pattern_dark = 'iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAMUlEQVQYV2NkwA+MGfHIGwPlzuJSAJYEacamAC6JTQGKJLoCDElkBVglYQpwSoIUAABJpQc89jWkNQAAAABJRU5ErkJggg==';
            $pattern_bright = 'iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAANUlEQVQYV2NkwAP+//9vzIhLHizJyHgWqwKYJEgzhgJkSQwF6JIoCrBJwhXgkgQrwCcJUgAAs0cfOXd7VnAAAAAASUVORK5CYII=';
            break;
        case 2:
            // vertical stripes
            $pattern_dark = 'iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAAEElEQVQIW2NkwA6MGQdSAgBOigE5ywFb9QAAAABJRU5ErkJggg==';
            $pattern_bright = 'iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAAE0lEQVQIW2NkwAL+//9vzDiQEgDlKxMnFgWj7QAAAABJRU5ErkJggg==';
            break;
        case 3:
            // horizontal stripes
            $pattern_dark = 'iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAAEklEQVQIW2NkwAEY6SRhjM0eAAbSADpE2BypAAAAAElFTkSuQmCC';
            $pattern_bright = 'iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAAFklEQVQIW2NkwAEY6SHx//9/Y2z2AABLjQM3c7wntwAAAABJRU5ErkJggg==';
            break;
        case 4:
            // small dots
            $pattern_dark = 'iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAAGElEQVQIW2NkYGAwZmBgOMsABYwwBvECAD26AQUATQ6nAAAAAElFTkSuQmCC';
            $pattern_bright = 'iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAAGUlEQVQIW2P8//+/MSMj41kGKGCEMYgXAAADvQQFZLynHQAAAABJRU5ErkJggg==';
            break;
        case 5:
            // horizontal triangle wave
            $pattern_dark = 'iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAALUlEQVQIW2NkwAEYsYgbMzAwnEWXAAsyMDAYgyTgHKgg2BCYDpgk3GRsdoAlAW7SBTrmWe1iAAAAAElFTkSuQmCC';
            $pattern_bright = 'iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAALklEQVQIW2NkwAEY0cX///9vzMjIeBZFAiYIplE4jIxnYSaAdcAkkY3FsAMmCQC89xc3gbr6PgAAAABJRU5ErkJggg==';
            break;
        case 6:
            // vertical triangular wave
            $pattern_dark = 'iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAAK0lEQVQIW2NkQABjBgaGszAuI5SBIggSg0mA2Fh1wEyASyLrQJFEl4DrAACoegYHteWU+AAAAABJRU5ErkJggg==';
            $pattern_bright = 'iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAAKklEQVQIW2NkgIL///8bMzIynoXxGUEMdEGQGFgCmyRcAl0SRQJZEqcOAPd9GAdSaieBAAAAAElFTkSuQmCC';
            break;
    }

    if ($brightness < 145) {
        return 'data:image/png;base64,' . $pattern_bright;
    } else {
        return 'data:image/png;base64,' . $pattern_dark;
    }
}