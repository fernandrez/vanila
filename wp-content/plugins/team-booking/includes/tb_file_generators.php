<?php

// Blocks direct access to this file
defined('ABSPATH') or die("No script kiddies please!");

/**
 * Generate ICS file
 *
 * $summary     - text title of the event
 * $datestart   - starting date (in seconds since unix epoch)
 * $dateend     - ending date (in seconds since unix epoch)
 * address      - event's address
 * $uri         - URL of the event (add http://)
 * $description - text description of the event
 * $filename    - file name
 */
function tbGenerateICSFile($filename, $datestart, $dateend, $address, $description, $uri, $summary)
{
    // 1. Set the correct headers for this file
    header('Content-type: text/calendar; charset=utf-8');
    header('Content-Disposition: attachment; filename=' . $filename);
    // 2. Echo out the ics file's contents
    echo "BEGIN:VCALENDAR\r\n";
    echo "VERSION:2.0\r\n";
    echo "PRODID:-//hacksw/handcal//NONSGML v1.0//EN\r\n";
    echo "CALSCALE:GREGORIAN\r\n";
    echo "BEGIN:VEVENT\r\n";
    echo "DTEND:" . date('Ymd\THis\Z', $dateend) . "\r\n";
    echo "UID:" . uniqid() . "\r\n";
    echo "DTSTAMP:" . date('Ymd\THis\Z', current_time('timestamp')) . "\r\n";
    echo "LOCATION:" . preg_replace('/([\,;])/', '\\\$1', $address) . "\r\n";
    echo "DESCRIPTION:" . preg_replace('/([\,;])/', '\\\$1', $description) . "\r\n";
    echo "URL;VALUE=URI:" . preg_replace('/([\,;])/', '\\\$1', $uri) . "\r\n";
    echo "SUMMARY:" . preg_replace('/([\,;])/', '\\\$1', $summary) . "\r\n";
    echo "DTSTART:" . date('Ymd\THis\Z', $datestart) . "\r\n";
    echo "END:VEVENT\r\n";
    echo "END:VCALENDAR\r\n";
}

/**
 * Generate an XLSX file with given reservation records
 */
function tbGenerateXLSXFile($filter)
{
    $reservations = tbGetReservations();
    ob_start();
    $filename = "reservations.xlsx";
    header('Content-disposition: attachment; filename="' . XLSXWriter::sanitize_filename($filename) . '"');
    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');

    $writer = new XLSXWriter();
    $writer->setAuthor(get_bloginfo('name'));
    $headers = array();
    $rows = array();

    foreach ($reservations as $id => $reservation) {
        /* @var $reservation TeamBooking_ReservationData */
        $time = $reservation->getCreationInstant(); // UTC, seconds
        // This skips old logs before v.1.2 if present
        if (!$reservation instanceof TeamBooking_ReservationData) {
            continue;
        }

        if (!empty($filter) && $reservation->getServiceId() != $filter) {
            continue;
        }

        // If not admin, keep only logs relative to current coworker
        $coworker_id = get_current_user_id();
        if (!current_user_can('manage_options') && $reservation->getCoworker() != $coworker_id) {
            continue;
        }

        $headers[$reservation->getServiceId()] = array(
            __('Service', 'teambooking')             => 'string',
            __('When', 'teambooking')                => 'string',
            __('Date of reservation', 'teambooking') => 'string',
            __('Reservation Status', 'teambooking')  => 'string',
            __('Payment Status', 'teambooking')      => 'string',
            __('Price', 'teambooking')               => 'money',
            __('Currency', 'teambooking')            => 'string',
        );
        if (current_user_can('manage_options')) {
            $headers[$reservation->getServiceId()][__('Coworker', 'teambooking')] = 'string'; //4th column
        }
        foreach ($reservation->getFieldsArray() as $key => $value) {
            $headers[$reservation->getServiceId()][ucwords(str_replace("_", " ", $key))] = 'string';
        }

        // Prepare the payment status
        if ($reservation->isPaid()) {
            $payment_status = __('paid', 'teambooking');
        } elseif ($reservation->getPaymentMustBeManuallyConfirmedOnPayPal()) {
            $payment_status = __('must be manually confirmed on PayPal', 'teambooking');
        } else {
            $payment_status = __('not paid', 'teambooking');
        }

        if ($reservation->getStatus() == 'confirmed' && $reservation->getServiceClass() == 'service') {
            $reservation_status = __('todo', 'teambooking');
        } else {
            $reservation_status = $reservation->getStatus();
        }

        $row = array(
            $reservation->getServiceName(),
            date_i18n_tb(get_option('date_format') . " " . get_option('time_format'), $reservation->getStart() + get_option('gmt_offset') * 3600),
            date_i18n_tb(get_option('date_format') . " " . get_option('time_format'), $time + get_option('gmt_offset') * 3600),
            $reservation_status,
            $payment_status,
            $reservation->getPrice(),
            $reservation->getCurrencyCode(),
        );
        if (current_user_can('manage_options')) {
            $row[] = tbGetSettings()->getCoworkerData($reservation->getCoworker())->getEmail(); //4th column
        }
        foreach ($reservation->getFieldsArray() as $key => $value) {
            $row[] = $value;
        }
        $rows[$reservation->getServiceId()][] = $row;
    }
    foreach ($headers as $service => $header) {
        $title = $rows[$service][0][0];
        $writer->writeSheet($rows[$service], $title, $header);
    }
    $writer->writeToStdOut();

    return ob_get_clean();
}

/**
 * Generate a CSV file with given reservation records
 */
function tbGenerateCSVFile($filter)
{
    $reservations = tbGetReservations();
    ob_start();
    header('Content-Type: text/csv; charset=utf-8');
    header('Content-Disposition: attachment; filename=reservations.csv');
    $output = fopen('php://output', 'w');

    // Preparing the header array
    $header_array = array(
        __('Service', 'teambooking'),
        __('When', 'teambooking'),
        __('Date of reservation', 'teambooking'),
        __('Reservation Status', 'teambooking'),
        __('Payment Status', 'teambooking'),
        __('Price', 'teambooking'),
    );
    if (current_user_can('manage_options')) {
        $header_array[] = __('Coworker', 'teambooking'); //4th column
    }
    $header_array[] = __('Details', 'teambooking');
    // Output header
    fputcsv($output, $header_array);
    foreach ($reservations as $id => $reservation) {
        /* @var $reservation TeamBooking_ReservationData */
        $time = $reservation->getCreationInstant(); // UTC, seconds
        // This skips old logs before v.1.2 if present
        if (!$reservation instanceof TeamBooking_ReservationData) {
            continue;
        }
        if (!empty($filter) && $reservation->getServiceId() != $filter) {
            continue;
        }

        // If not admin, keep only logs relative to current coworker
        $coworker_id = get_current_user_id();
        if (!current_user_can('manage_options') && $reservation->getCoworker() != $coworker_id) {
            continue;
        }

        // Prepare the payment status
        if ($reservation->isPaid()) {
            $payment_status = __('paid', 'teambooking');
        } elseif ($reservation->getPaymentMustBeManuallyConfirmedOnPayPal()) {
            $payment_status = __('must be manually confirmed on PayPal', 'teambooking');
        } else {
            $payment_status = __('not paid', 'teambooking');
        }

        // Prepare price
        if ($reservation->getPrice() > 0) {
            $price_string = $reservation->getCurrencyCode() . " " . $reservation->getPrice();
        } else {
            $price_string = 0;
        }

        if ($reservation->getStatus() == 'confirmed' && $reservation->getServiceClass() == 'service') {
            $reservation_status = __('todo', 'teambooking');
        } else {
            $reservation_status = $reservation->getStatus();
        }

        $row = array(
            $reservation->getServiceName(),
            date_i18n_tb(get_option('date_format') . " " . get_option('time_format'), $reservation->getStart() + get_option('gmt_offset') * 3600),
            date_i18n_tb(get_option('date_format') . " " . get_option('time_format'), $time + get_option('gmt_offset') * 3600),
            $reservation_status,
            $payment_status,
            $price_string,
        );
        if (current_user_can('manage_options')) {
            $row[] = tbGetSettings()->getCoworkerData($reservation->getCoworker())->getEmail(); //4th column
        }
        $details = "";
        foreach ($reservation->getFieldsArray() as $key => $value) {
            $details .= ucwords(str_replace("_", " ", $key)) . ": " . $value . "-";
        }
        $row[] = $details;
        fputcsv($output, $row);
    }

    return ob_get_clean();
}

/**
 * Generate the settings backup file
 */
function tbGenerateSettingsBackup()
{
    $settings = serialize(tbGetSettings());
    ob_start();
    header('Content-Type: text/plain; charset=utf-8');
    header('Content-Disposition: attachment; filename=team-booking-settings-backup.tbk');
    $output = fopen('php://output', 'w');
    fputs($output, $settings);

    return ob_get_clean();
}
