<?php

$tb_lightgrey = array(
    0 =>
    array(
        'featureType' => 'landscape',
        'stylers' =>
        array(
            0 =>
            array(
                'saturation' => -100,
            ),
            1 =>
            array(
                'lightness' => 65,
            ),
            2 =>
            array(
                'visibility' => 'on',
            ),
        ),
    ),
    1 =>
    array(
        'featureType' => 'poi',
        'stylers' =>
        array(
            0 =>
            array(
                'saturation' => -100,
            ),
            1 =>
            array(
                'lightness' => 51,
            ),
            2 =>
            array(
                'visibility' => 'simplified',
            ),
        ),
    ),
    2 =>
    array(
        'featureType' => 'road.highway',
        'stylers' =>
        array(
            0 =>
            array(
                'saturation' => -100,
            ),
            1 =>
            array(
                'visibility' => 'simplified',
            ),
        ),
    ),
    3 =>
    array(
        'featureType' => 'road.arterial',
        'stylers' =>
        array(
            0 =>
            array(
                'saturation' => -100,
            ),
            1 =>
            array(
                'lightness' => 30,
            ),
            2 =>
            array(
                'visibility' => 'on',
            ),
        ),
    ),
    4 =>
    array(
        'featureType' => 'road.local',
        'stylers' =>
        array(
            0 =>
            array(
                'saturation' => -100,
            ),
            1 =>
            array(
                'lightness' => 40,
            ),
            2 =>
            array(
                'visibility' => 'on',
            ),
        ),
    ),
    5 =>
    array(
        'featureType' => 'transit',
        'stylers' =>
        array(
            0 =>
            array(
                'saturation' => -100,
            ),
            1 =>
            array(
                'visibility' => 'simplified',
            ),
        ),
    ),
    6 =>
    array(
        'featureType' => 'administrative.province',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'off',
            ),
        ),
    ),
    7 =>
    array(
        'featureType' => 'water',
        'elementType' => 'labels',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'on',
            ),
            1 =>
            array(
                'lightness' => -25,
            ),
            2 =>
            array(
                'saturation' => -100,
            ),
        ),
    ),
    8 =>
    array(
        'featureType' => 'water',
        'elementType' => 'geometry',
        'stylers' =>
        array(
            0 =>
            array(
                'hue' => '#ffff00',
            ),
            1 =>
            array(
                'lightness' => -25,
            ),
            2 =>
            array(
                'saturation' => -97,
            ),
        ),
    ),
);

$tb_unsaturated_browns = array(
    0 =>
    array(
        'elementType' => 'geometry',
        'stylers' =>
        array(
            0 =>
            array(
                'hue' => '#ff4400',
            ),
            1 =>
            array(
                'saturation' => -68,
            ),
            2 =>
            array(
                'lightness' => -4,
            ),
            3 =>
            array(
                'gamma' => 0.71999999999999997,
            ),
        ),
    ),
    1 =>
    array(
        'featureType' => 'road',
        'elementType' => 'labels.icon',
    ),
    2 =>
    array(
        'featureType' => 'landscape.man_made',
        'elementType' => 'geometry',
        'stylers' =>
        array(
            0 =>
            array(
                'hue' => '#0077ff',
            ),
            1 =>
            array(
                'gamma' => 3.1000000000000001,
            ),
        ),
    ),
    3 =>
    array(
        'featureType' => 'water',
        'stylers' =>
        array(
            0 =>
            array(
                'hue' => '#00ccff',
            ),
            1 =>
            array(
                'gamma' => 0.44,
            ),
            2 =>
            array(
                'saturation' => -33,
            ),
        ),
    ),
    4 =>
    array(
        'featureType' => 'poi.park',
        'stylers' =>
        array(
            0 =>
            array(
                'hue' => '#44ff00',
            ),
            1 =>
            array(
                'saturation' => -23,
            ),
        ),
    ),
    5 =>
    array(
        'featureType' => 'water',
        'elementType' => 'labels.text.fill',
        'stylers' =>
        array(
            0 =>
            array(
                'hue' => '#007fff',
            ),
            1 =>
            array(
                'gamma' => 0.77000000000000002,
            ),
            2 =>
            array(
                'saturation' => 65,
            ),
            3 =>
            array(
                'lightness' => 99,
            ),
        ),
    ),
    6 =>
    array(
        'featureType' => 'water',
        'elementType' => 'labels.text.stroke',
        'stylers' =>
        array(
            0 =>
            array(
                'gamma' => 0.11,
            ),
            1 =>
            array(
                'weight' => 5.5999999999999996,
            ),
            2 =>
            array(
                'saturation' => 99,
            ),
            3 =>
            array(
                'hue' => '#0091ff',
            ),
            4 =>
            array(
                'lightness' => -86,
            ),
        ),
    ),
    7 =>
    array(
        'featureType' => 'transit.line',
        'elementType' => 'geometry',
        'stylers' =>
        array(
            0 =>
            array(
                'lightness' => -48,
            ),
            1 =>
            array(
                'hue' => '#ff5e00',
            ),
            2 =>
            array(
                'gamma' => 1.2,
            ),
            3 =>
            array(
                'saturation' => -23,
            ),
        ),
    ),
    8 =>
    array(
        'featureType' => 'transit',
        'elementType' => 'labels.text.stroke',
        'stylers' =>
        array(
            0 =>
            array(
                'saturation' => -64,
            ),
            1 =>
            array(
                'hue' => '#ff9100',
            ),
            2 =>
            array(
                'lightness' => 16,
            ),
            3 =>
            array(
                'gamma' => 0.46999999999999997,
            ),
            4 =>
            array(
                'weight' => 2.7000000000000002,
            ),
        ),
    ),
);

$tb_pale_dawn = array(
    0 =>
    array(
        'featureType' => 'administrative',
        'elementType' => 'all',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'on',
            ),
            1 =>
            array(
                'lightness' => 33,
            ),
        ),
    ),
    1 =>
    array(
        'featureType' => 'landscape',
        'elementType' => 'all',
        'stylers' =>
        array(
            0 =>
            array(
                'color' => '#f2e5d4',
            ),
        ),
    ),
    2 =>
    array(
        'featureType' => 'poi.park',
        'elementType' => 'geometry',
        'stylers' =>
        array(
            0 =>
            array(
                'color' => '#c5dac6',
            ),
        ),
    ),
    3 =>
    array(
        'featureType' => 'poi.park',
        'elementType' => 'labels',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'on',
            ),
            1 =>
            array(
                'lightness' => 20,
            ),
        ),
    ),
    4 =>
    array(
        'featureType' => 'road',
        'elementType' => 'all',
        'stylers' =>
        array(
            0 =>
            array(
                'lightness' => 20,
            ),
        ),
    ),
    5 =>
    array(
        'featureType' => 'road.highway',
        'elementType' => 'geometry',
        'stylers' =>
        array(
            0 =>
            array(
                'color' => '#c5c6c6',
            ),
        ),
    ),
    6 =>
    array(
        'featureType' => 'road.arterial',
        'elementType' => 'geometry',
        'stylers' =>
        array(
            0 =>
            array(
                'color' => '#e4d7c6',
            ),
        ),
    ),
    7 =>
    array(
        'featureType' => 'road.local',
        'elementType' => 'geometry',
        'stylers' =>
        array(
            0 =>
            array(
                'color' => '#fbfaf7',
            ),
        ),
    ),
    8 =>
    array(
        'featureType' => 'water',
        'elementType' => 'all',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'on',
            ),
            1 =>
            array(
                'color' => '#acbcc9',
            ),
        ),
    ),
);

$tb_orange = array(
    0 =>
    array(
        'featureType' => 'all',
        'elementType' => 'all',
        'stylers' =>
        array(
            0 =>
            array(
                'hue' => '#ff6800',
            ),
            1 =>
            array(
                'saturation' => '20',
            ),
            2 =>
            array(
                'lightness' => '-8',
            ),
            3 =>
            array(
                'gamma' => '1.00',
            ),
            4 =>
            array(
                'weight' => '1.12',
            ),
        ),
    ),
);

$tb_coy_beauty = array(
    0 =>
    array(
        'featureType' => 'all',
        'elementType' => 'geometry.stroke',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'simplified',
            ),
        ),
    ),
    1 =>
    array(
        'featureType' => 'administrative',
        'elementType' => 'all',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'off',
            ),
        ),
    ),
    2 =>
    array(
        'featureType' => 'administrative',
        'elementType' => 'labels',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'simplified',
            ),
            1 =>
            array(
                'color' => '#a31645',
            ),
        ),
    ),
    3 =>
    array(
        'featureType' => 'landscape',
        'elementType' => 'all',
        'stylers' =>
        array(
            0 =>
            array(
                'weight' => '3.79',
            ),
            1 =>
            array(
                'visibility' => 'on',
            ),
            2 =>
            array(
                'color' => '#ffecf0',
            ),
        ),
    ),
    4 =>
    array(
        'featureType' => 'landscape',
        'elementType' => 'geometry',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'on',
            ),
        ),
    ),
    5 =>
    array(
        'featureType' => 'landscape',
        'elementType' => 'geometry.stroke',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'on',
            ),
        ),
    ),
    6 =>
    array(
        'featureType' => 'poi',
        'elementType' => 'all',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'simplified',
            ),
            1 =>
            array(
                'color' => '#a31645',
            ),
        ),
    ),
    7 =>
    array(
        'featureType' => 'poi',
        'elementType' => 'geometry',
        'stylers' =>
        array(
            0 =>
            array(
                'saturation' => '0',
            ),
            1 =>
            array(
                'lightness' => '0',
            ),
            2 =>
            array(
                'visibility' => 'off',
            ),
        ),
    ),
    8 =>
    array(
        'featureType' => 'poi',
        'elementType' => 'geometry.stroke',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'off',
            ),
        ),
    ),
    9 =>
    array(
        'featureType' => 'poi.business',
        'elementType' => 'all',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'simplified',
            ),
            1 =>
            array(
                'color' => '#d89ca8',
            ),
        ),
    ),
    10 =>
    array(
        'featureType' => 'poi.business',
        'elementType' => 'geometry',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'on',
            ),
        ),
    ),
    11 =>
    array(
        'featureType' => 'poi.business',
        'elementType' => 'geometry.fill',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'on',
            ),
            1 =>
            array(
                'saturation' => '0',
            ),
        ),
    ),
    12 =>
    array(
        'featureType' => 'poi.business',
        'elementType' => 'labels',
        'stylers' =>
        array(
            0 =>
            array(
                'color' => '#a31645',
            ),
        ),
    ),
    13 =>
    array(
        'featureType' => 'poi.business',
        'elementType' => 'labels.icon',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'simplified',
            ),
            1 =>
            array(
                'lightness' => '84',
            ),
        ),
    ),
    14 =>
    array(
        'featureType' => 'road',
        'elementType' => 'all',
        'stylers' =>
        array(
            0 =>
            array(
                'saturation' => -100,
            ),
            1 =>
            array(
                'lightness' => 45,
            ),
        ),
    ),
    15 =>
    array(
        'featureType' => 'road.highway',
        'elementType' => 'all',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'simplified',
            ),
        ),
    ),
    16 =>
    array(
        'featureType' => 'road.arterial',
        'elementType' => 'labels.icon',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'off',
            ),
        ),
    ),
    17 =>
    array(
        'featureType' => 'transit',
        'elementType' => 'all',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'off',
            ),
        ),
    ),
    18 =>
    array(
        'featureType' => 'water',
        'elementType' => 'all',
        'stylers' =>
        array(
            0 =>
            array(
                'color' => '#d89ca8',
            ),
            1 =>
            array(
                'visibility' => 'on',
            ),
        ),
    ),
    19 =>
    array(
        'featureType' => 'water',
        'elementType' => 'geometry.fill',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'on',
            ),
            1 =>
            array(
                'color' => '#fedce3',
            ),
        ),
    ),
    20 =>
    array(
        'featureType' => 'water',
        'elementType' => 'labels',
        'stylers' =>
        array(
            0 =>
            array(
                'visibility' => 'off',
            ),
        ),
    ),
);

$tb_mapstyles = array(
    0 => $tb_lightgrey,
    1 => $tb_unsaturated_browns,
    2 => $tb_pale_dawn,
    3 => $tb_orange,
    4 => $tb_coy_beauty,
);
