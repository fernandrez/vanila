=== NextGEN Facebook - Complete Meta Tags for FB, Google, Pinterest, Twitter, LinkedIn & More ===
Plugin Name: NextGEN Facebook (NGFB)
Plugin Slug: nextgen-facebook
Text Domain: nextgen-facebook
Domain Path: /languages
Contributors: jsmoriss
Donate Link: https://surniaulula.com/extend/plugins/nextgen-facebook/
Tags: amp, bbPress, buddypress, buttons, e-commerce, easy digital downloads, edd, Facebook, g+, gallery, google, google plus, hashtags, imagebrowser, like, linkedin, marketpress, meta-tags, multilingual, multisite, nextgen gallery, nggalbum, nggallery, open graph, pinterest, player card, polylang, rich pins, schema, seo, Share, shortcode, slideshare, social, stumbleupon, Summary Card, tumblr, twitter, twitter cards, vimeo, widget, wistia, woocommerce, wp_cache, youtube, yotpo, yourls, whatsapp
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl.txt
Requires At Least: 3.1
Tested Up To: 4.5.2
Stable Tag: 8.32.1-1

Display your content in the best possible way on Facebook, Google+, Twitter, Pinterest, etc. - no matter how your webpage is shared!

== Description ==

<p><img src="https://surniaulula.github.io/nextgen-facebook/assets/icon-256x256.png" style="width:33%;min-width:128px;max-width:256px;float:left;margin:10px 60px 40px 0;" /><strong>Make sure social websites present your content in the best possible way, no matter <em>how</em> your webpage is shared</strong> &mdash; from sharing buttons on the webpage, browser add-ons / extensions, or URLs pasted directly on timelines and private messages (PM) &mdash; the NextGEN Facebook (NGFB) plugin has you covered.</p>

**A solid social meta tag plugin is the foundation of any good social strategy** &mdash; NextGEN Facebook (NGFB) gives you total control over all the information [social website crawlers](https://developers.facebook.com/docs/sharing/webmasters/crawler) need, improving Google Search ranking, social engagement, and click-through-rates on Facebook, Google+, Twitter, Pinterest, LinkedIn, StumbleUpon, Tumblr and and many more &mdash; along with offering many of the most popular social sharing buttons!

**Tired of average and sub-standard social meta tags from other SEO plugins?** &mdash; NGFB Pro automatically retrieves and uses information from a wide range or sources, including All in One SEO Pack, bbPress, BuddyPress, Co-Authors Plus, Easy Digital Downloads, HeadSpace2 SEO, NextGEN Gallery, MarketPress - WordPress eCommerce, Polylang, rtMedia for WordPress (BuddyPress and bbPress), The SEO Framework, WooCommerce, WP e-Commerce, WordPress REST API, Yoast SEO (aka WordPress SEO), Yotpo Social Reviews for WooCommerce, along with service APIs like Bitly, Google URL Shortener, Gravatar, Ow.ly, Slideshare, TinyURL, Vimeo, Wistia, Your Own URL Shortener (YOURLS), and YouTube.

= Quick List of Features =

**NGFB Free / Basic Features**

* Adds Open Graph / Rich Pin meta tags (Facebook, Google+, Pinterest, LinkedIn, etc.).
* Twitter Card meta tags:
	* [Summary Card with Large Image](https://dev.twitter.com/cards/types/summary-large-image)
	* [Summary Card](https://dev.twitter.com/cards/types/summary)
	* [Player Card](https://dev.twitter.com/cards/types/player) is available in the Pro version (requires video discovery modules, provided with the Pro version).
* Support for Automattic's [Accelerated Mobile Pages (AMP)](https://wordpress.org/plugins/amp/) plugin.
* Support for featured, attached, gallery shortcode, and/or image HTML tags in content.
* Customizable image dimensions for Facebook / Open Graph, Pinterest, Schema and each Twitter Card type.
* Fallback to image alt values if the content and except do not include any text.
* Optional fallback to a default image and video for index and search webpages.
* Validates image dimensions and aspect ratios for reliable sharing results.
* Auto-regeneration of inaccurate / missing WordPress image sizes.
* Fully renders content (including shortcodes) for accurate description texts.
* Configurable title separator character (hyphen by default).
* Configurable title and description lengths (Open Graph, Twitter Card, SEO).
* Includes author, co-author / contributor and publisher markup for Facebook, Pinterest and Google.
* Includes WordPress tags as hashtags (including a configurable maximum).
* Includes a Google / SEO description meta tag (if an SEO plugin is not detected).
* Includes <a href="https://developers.google.com/structured-data/customize/social-profiles">author (Person) and publisher (Organization) social profiles</a>, <a href="https://developers.google.com/structured-data/customize/logos">publisher (Organization) logo</a>, and <a href="https://developers.google.com/structured-data/site-name">WebSite Site Name</a> in schema.org JSON-LD format for Google Search and their <em>Knowledge Graph</em>.
* Configurable website / business social accounts for JSON-LD markup:
	* Facebook Business Page URL
	* Google+ Business Page URL
	* Pinterest Company Page URL
	* Twitter Business @username
	* Instagram Business URL
	* LinkedIn Company Page URL
	* MySpace Business (Brand) URL
* User profile contact fields for Open Graph, Twitter Card, and JSON-LD markup:
	* Facebook URL
	* Google+ URL
	* Instagram URL
	* LinkedIn URL
	* MySpace URL
	* Pinterest URL
	* Skype Username
	* Tumblr URL
	* Twitter @username
	* YouTube Channel URL
* Validation tools, social preview, and meta tag value tabs on admin editing pages.
* Customizable **multilingual** / multi-language Site Title and Site Description texts.
* Contextual help for *every* plugin option and [comprehensive online documentation](http://surniaulula.com/codex/plugins/nextgen-facebook/).
* Uses object and transient caches to provide incredibly fast execution speeds.
* Include / exclude each social sharing button based on the viewing device (desktop and/or mobile).
* Social sharing buttons for the content, excerpt, in a widget, as a shortcode, floating sidebar, and/or PHP function.
	* Buffer
	* Email
	* Facebook
	* Google+
	* LinkedIn
	* ManageWP
	* Pinterest
	* Reddit
	* StumbleUpon
	* Tumblr
	* Twitter
	* WhatsApp (for Mobile Devices)

**NGFB Free / Basic Example Meta Tags**

Example meta tags from the *Tiled Gallery* post provided by the [WP Test](http://wptest.io/) data.

<pre>
&lt;link rel="author" href="https://plus.google.com/u/1/+JSMorisset"/&gt;
&lt;link rel="publisher" href="https://plus.google.com/+SurniaUlula"/&gt;
&lt;meta property="fb:app_id" content="525239184171769"/&gt;
&lt;meta property="og:url" content="http://test.surniaulula.com/2013/03/15/tiled-gallery/"/&gt;
&lt;meta property="og:type" content="article"/&gt;
&lt;!-- article:author:1 --&gt;&lt;meta property="article:author" content="https://www.facebook.com/jsmoriss"/&gt;
&lt;meta property="article:section" content="Social Networking"/&gt;
&lt;meta property="article:published_time" content="2013-03-15T17:23:27+00:00"/&gt;
&lt;meta property="article:modified_time" content="2016-05-13T11:02:32+00:00"/&gt;
&lt;meta property="og:locale" content="en_US"/&gt;
&lt;meta property="og:site_name" content="Test Site"/&gt;
&lt;meta property="og:title" content="Tiled Gallery"/&gt;
&lt;meta property="og:description" content="This is a test for Jetpack&amp;#039;s Tiled Gallery. You can install Jetpack or Slim Jetpack to test it out. This is some text after the Tiled Gallery just to make sure that everything spaces nicely."/&gt;
&lt;meta property="article:publisher" content="https://www.facebook.com/SurniaUlulaCom/"/&gt;
&lt;!-- og:image:1 --&gt;&lt;meta property="og:image" content="http://test.surniaulula.com/wp-content/uploads/2013/03/captain-america-600x315.jpg"/&gt;
&lt;!-- og:image:1 --&gt;&lt;meta property="og:image:width" content="600"/&gt;
&lt;!-- og:image:1 --&gt;&lt;meta property="og:image:height" content="315"/&gt;
&lt;meta name="twitter:domain" content="test.surniaulula.com"/&gt;
&lt;meta name="twitter:site" content="@surniaululacom"/&gt;
&lt;meta name="twitter:title" content="Tiled Gallery"/&gt;
&lt;meta name="twitter:description" content="This is a test for Jetpack&amp;#039;s Tiled Gallery. You can install Jetpack or Slim Jetpack to test it out. This is some text after the Tiled Gallery just to make sure that everything spaces nicely."/&gt;
&lt;meta name="twitter:card" content="summary_large_image"/&gt;
&lt;meta name="twitter:image" content="http://test.surniaulula.com/wp-content/uploads/2013/03/captain-america-800x1212.jpg"/&gt;
&lt;meta itemprop="url" content="http://test.surniaulula.com/2013/03/15/tiled-gallery/"/&gt;
&lt;meta itemprop="name" content="Tiled Gallery"/&gt;
&lt;meta itemprop="description" content="This is a test for Jetpack&amp;#039;s Tiled Gallery. You can install Jetpack or Slim Jetpack to test it out. This is some text after the Tiled Gallery just to make sure that everything spaces nicely."/&gt;
&lt;meta itemprop="datepublished" content="2013-03-15T17:23:27+00:00"/&gt;
&lt;meta itemprop="datemodified" content="2016-05-13T11:02:32+00:00"/&gt;
&lt;meta name="description" content="This is a test for Jetpack&amp;#039;s Tiled Gallery. You can install Jetpack or Slim Jetpack to test it out. This is some text after the Tiled Gallery just to..."/&gt;
&lt;noscript itemprop="image" itemscope itemtype="http://schema.org/ImageObject"&gt;
	&lt;meta itemprop="url" content="http://test.surniaulula.com/wp-content/uploads/2013/03/captain-america-800x1212.jpg"/&gt;
	&lt;meta itemprop="width" content="800"/&gt;
	&lt;meta itemprop="height" content="1212"/&gt;
&lt;/noscript&gt;
&lt;noscript itemprop="author" itemscope itemtype="http://schema.org/Person"&gt;
	&lt;meta itemprop="url" content="http://surniaulula.com/"/&gt;
	&lt;meta itemprop="name" content="JS Morisset"/&gt;
	&lt;meta itemprop="description" content="PHP developer and author of several Pro / Freemium plugins for WordPress."/&gt;
&lt;/noscript&gt;
</pre>

<blockquote>
<p>Download the Free version from <a href="http://surniaulula.github.io/nextgen-facebook/">GitHub</a> or <a href="https://wordpress.org/plugins/nextgen-facebook/">WordPress.org</a>.</p>
</blockquote>

<div style="clear:both;"></div>
<div style="width:40%;min-width:256px;max-width:1200px;height:auto;float:right;margin:10px 0 40px 60px;">
<p><img src="https://surniaulula.github.io/nextgen-facebook/images/social/twitter-player-card-single.png" width="100%" height="100%"/></p>
</div>

= Quick List of Features (Continued) =

**NGFB Pro / Power-User Features**

* A "Social Settings" metabox for Post, Page, custom post type, user profile, and term / taxonomy (category and tag) editing pages &mdash; the "Social Settings" metbox allows you to customize the article topic, the shared title, the Open Graph / Facebook / Rich Pin, Google Search, and Twitter Card descriptions, along with the shared image and/or video.
* Support for embedded videos in content text ("iframe" and "object" HTML markup).
* Additional Open Graph / Rich Pin meta tags for videos and e-commerce products.
* Twitter [Player Card](https://dev.twitter.com/cards/types/player) markup for embedded videos from Slideshare, Vimeo, Wistia, and/or Youtube.
* Configurable user profile contact field names and labels for customized theme / SEO plugin integration.
* Include or exclude individual Google / SEO, Open Graph, Twitter Card, and Schema meta tags from webpage headers.
* File caching for social sharing button images and JavaScript, maximizing performance on VPS and dedicated hardware hosting platforms.
* A stylesheets editor for each social sharing button locations (content, excerpt, shortcode, widget, etc.).
* Dynamic button language switching based on the current WordPress locale.
* Optional URL shortening with Bitly, Google, Ow.ly, TinyURL, or YOURLS.
* Ability to include / exclude sharing buttons by post type.
* Selection of preset button options by location (content, excerpt, shortcode, widget, etc.).
* A stylesheets editor for each social sharing button locations (content, excerpt, shortcode, widget, etc.).
* Integrates with **3rd party plugins and services** for additional image, video, product, and content information (see [About Pro Modules](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/) and [Integration Notes](http://surniaulula.com/codex/plugins/nextgen-facebook/installation/integration/) for details). The following modules are included with the Pro version, and are automatically loaded if/when the supported plugins and/or services are detected.
	* **Supported 3rd Party Plugins**
		* [All in One SEO Pack](https://wordpress.org/plugins/all-in-one-seo-pack/)
		* [bbPress](https://wordpress.org/plugins/bbpress/)
		* [BuddyPress](https://wordpress.org/plugins/buddypress/) (including Group Forum Topics)
		* [Co-Authors Plus](https://wordpress.org/plugins/co-authors-plus/) (including Guest Authors)
		* [Easy Digital Downloads](https://wordpress.org/plugins/easy-digital-downloads/)
		* [HeadSpace2 SEO](https://wordpress.org/plugins/headspace2/)
		* [NextGEN Gallery](https://wordpress.org/plugins/nextgen-gallery/)
		* [MarketPress - WordPress eCommerce](https://wordpress.org/plugins/wordpress-ecommerce/)
		* [Polylang](https://wordpress.org/plugins/polylang/)
		* [rtMedia for WordPress, BuddyPress and bbPress](https://wordpress.org/plugins/buddypress-media/)
		* [The SEO Framework](https://wordpress.org/plugins/autodescription/)
		* [WooCommerce](https://wordpress.org/plugins/woocommerce/) (version 1 and 2)
		* [WP eCommerce](https://wordpress.org/plugins/wp-e-commerce/)
		* [WordPress REST API](https://wordpress.org/plugins/rest-api/) (version 2)
		* [Yoast SEO](https://wordpress.org/plugins/wordpress-seo/) (aka WordPress SEO)
		* [Yotpo Social Reviews for WooCommerce](https://wordpress.org/plugins/yotpo-social-reviews-for-woocommerce/)
	* **Supported Service APIs**
		* [Bitly](https://bitly.com/)
		* [Google URL Shortener](https://goo.gl/)
		* [Gravatar](https://en.gravatar.com/) (Author Image)
		* [Ow.ly](http://ow.ly/)
		* [Slideshare](http://www.slideshare.net/) Presentations
		* [TinyURL](http://tinyurl.com/)
		* [Vimeo](https://vimeo.com/) Videos
		* [Wistia](http://wistia.com/) Videos
		* [Your Own URL Shortener](http://yourls.org/) (YOURLS)
		* [YouTube](https://www.youtube.com/) Videos and Playlists

<blockquote>
<p><a href="http://wpsso.com/extend/plugins/wpsso/">Purchase the Pro version</a> (includes a <em>No Risk 30 Day Refund Policy</em>).</p>
</blockquote>

<div style="clear:both;"></div>
<div style="width:40%;min-width:256px;max-width:1200px;height:auto;float:right;margin:10px 0 40px 60px;">
<p><img src="https://surniaulula.github.io/nextgen-facebook/images/settings/ngfb-social-sharing-buttons.png" width="100%" height="100%"/></p>
</div>

= Social Sharing Buttons =

NGFB comes with several sharing buttons, that you can optionally include on Post / Page editing pages, above / below your content or excerpt, [bbPress](https://wordpress.org/plugins/bbpress/) single pages, [BuddyPress](https://wordpress.org/plugins/buddypress/) activity entries, as a sidebar, widget, shortcode, or even call a function from your theme template(s). Each of the following sharing buttons can be enabled, configured, and styled individually:

* **Email**
* **Buffer**
* **Facebook** (Like, Send, and Share)
* **Google+**
* **LinkedIn**
* **ManageWP**
* **Pinterest**
* **Reddit**
* **StumbleUpon**
* **Tumblr** (Links, Quotes, Images, Videos)
* **Twitter**
* **WhatsApp** (for Mobile Devices)

The Facebook, Google+ and Twitter sharing buttons support *multiple languages*. A default language can be chosen in the NGFB settings, and the [Pro version](http://surniaulula.com/extend/plugins/nextgen-facebook/) switches the sharing button language with the webpage language / WordPress locale. NGFB can also include hashtags from WordPress and NextGEN Gallery tag names in the Open Graph (Facebook) and Pinterest Rich Pin descriptions, Tweet text, and other social captions.

<div style="clear:both;"></div>
<div style="width:40%;min-width:256px;max-width:1200px;height:auto;float:right;margin:10px 0 40px 60px;">
<p><img src="https://surniaulula.github.io/nextgen-facebook/images/social/google-social-profiles-search-results.png" width="100%" height="100%"/></p>
</div>

= Social Profiles for Google Search =

NGFB provides <a href="https://developers.google.com/structured-data/customize/social-profiles">author (Person) and publisher (Organization) social profiles</a>, <a href="https://developers.google.com/structured-data/customize/logos">publisher (Organization) logo</a>, and <a href="https://developers.google.com/structured-data/site-name">WebSite Site Name</a> in schema.org JSON-LD format for Google Search and their <em>Knowledge Graph</em>. The author (Person) markup includes the author's website URL, their profile social sharing image, and all listed contact URLs from their user profile. The publisher (Organization) markup includes the website URL, a corporate logo, a default image, and the publisher Facebook, Google+, LinkedIn, Pinterest, and Twitter business pages.

<div style="clear:both;"></div>

= User Profile Social Contacts =

NGFB Pro allows you to customize the field names, label, and add / remove the following contacts on user profile pages, including [Co-Authors Plus](https://wordpress.org/plugins/co-authors-plus/) guest author profiles:

* AIM
* Facebook URL
* Google Talk
* Google+ URL
* Instagram URL
* LinkedIn URL
* MySpace URL
* Pinterest URL
* Skype Username
* Tumblr URL
* Twitter @username
* Yahoo IM
* YouTube Channel URL

= Complete Social Meta Tags =

NGFB adds [Open Graph](http://ogp.me/) (Facebook), [Pinterest Rich Pins](http://developers.pinterest.com/rich_pins/), [Twitter Cards](https://dev.twitter.com/docs/cards), and [Search Engine Optimization](http://en.wikipedia.org/wiki/Search_engine_optimization) meta tags to the head section of webpages. These meta tags are used by Google Search and all social websites to describe and display your content correctly (title, description, hashtags, images, videos, product, author profile / authorship, publisher, etc.). NGFB is a complete social sharing solution that uses the *existing* content of your webpages to build HTML meta tags &mdash; There's no need to manually enter / configure any additional values or settings (although many settings and options *are* available in the Pro version). <a href="http://surniaulula.com/extend/plugins/nextgen-facebook/screenshots/">See examples from Google Search, Google+, Facebook, Twitter, Pinterest, StumbleUpon, Tumblr, etc.</a> &mdash; along with screenshots of the NGFB settings pages.

NGFB provides the [Summary](https://dev.twitter.com/cards/types/summary), [Summary with Large Image](https://dev.twitter.com/cards/types/summary-large-image), and [Player](https://dev.twitter.com/cards/types/player) Twitter Cards &mdash; *including configurable image sizes for each card type*. The [Photo](https://dev.twitter.com/cards/types/photo), [Gallery](https://dev.twitter.com/cards/types/gallery), and [Product](https://dev.twitter.com/cards/types/product) Twitter Cards were deprecated by Twitter on July 3rd, 2015.

= Available in Multiple Languages =

* English (US)
* French (France)
* More to come...

= 3rd Party Integration (Pro version) =

<div style="clear:both;"></div>
<div style="width:40%;min-width:256px;max-width:1200px;height:auto;float:right;margin:10px 0 40px 60px;">
<p><img src="https://surniaulula.github.io/nextgen-facebook/images/social/twitter-large-image-summary-single.png" width="100%" height="100%"/></p>
</div>

**Images and Videos**

NGFB detects and uses all images - associated or included in your content - including the WordPress image gallery and [NextGEN Gallery](https://wordpress.org/plugins/nextgen-gallery/) shortcodes. WordPress Media Library images (and NextGEN Gallery in the Pro version) are resized according to their intended consumer (Facebook, Twitter, Pinterest, etc). The Pro version detects embedded videos from Slideshare, Vimeo, Wistia, and Youtube (including preview image, video title, and video description). NGFB Pro also includes support for [NextGEN Gallery](https://wordpress.org/plugins/nextgen-gallery/) albums, galleries and images (shortcodes, image tags, album / gallery preview images, etc.).

**Enhanced SEO**

NGFB Pro integrates with [All in One SEO Pack](https://wordpress.org/plugins/all-in-one-seo-pack/), [HeadSpace2 SEO](https://wordpress.org/plugins/headspace2/), [The SEO Framework](https://wordpress.org/plugins/autodescription/), and [Yoast SEO](https://wordpress.org/plugins/wordpress-seo/) (aka WordPress SEO) &mdash; making sure your custom SEO settings are reflected in the Open Graph, Rich Pin, Schema, and Twitter Card meta tags.

<div style="clear:both;"></div>
<div style="width:40%;min-width:256px;max-width:1200px;height:auto;float:right;margin:10px 0 40px 60px;">
<p><img src="https://surniaulula.github.io/nextgen-facebook/images/social/twitter-product-ngfb-single.png" width="100%" height="100%"/></p>
</div>

**eCommerce Products**

NGFB Pro also supports [Easy Digital Downloads](https://wordpress.org/plugins/easy-digital-downloads/), [MarketPress - WordPress eCommerce](https://wordpress.org/plugins/wordpress-ecommerce/), [WooCommerce](https://wordpress.org/plugins/woocommerce/), and [WP e-Commerce](https://wordpress.org/plugins/wp-e-commerce/) product pages, creating appropriate meta tags for [Facebook Products](https://developers.facebook.com/docs/payments/product/, and [Pinterest Rich Pins](http://developers.pinterest.com/rich_pins/), including variations and additional / custom images.

**Forums and Social**

NGFB Pro supports [bbPress](https://wordpress.org/plugins/bbpress/), [BuddyPress](https://wordpress.org/plugins/buddypress/) (see the [BuddyPress Integration Notes](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/buddypress-integration/)), and [rtMedia for WordPress, BuddyPress and bbPress](https://wordpress.org/plugins/buddypress-media/), making sure your meta tags reflect the page content, including appropriate titles, descriptions, images, author information, etc. Social sharing buttons can also be added to [bbPress](https://wordpress.org/plugins/bbpress/) single template pages and [BuddyPress](https://wordpress.org/plugins/buddypress/) activities.

= Proven Performance =

NGFB is *fast and coded for performance*, making full use of all available caching techniques (persistent / non-persistent object and disk caching). NGFB loads only the library files and object classes it needs, keeping it small, fast, and yet still able to support a wide range of 3rd party integration features.

= Professional Support =

NGFB support and development is on-going. You can review the [FAQ](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/) and [Notes](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/) pages for additional setup information. If you have any suggestions or comments, post them to the [WordPress support forum](https://wordpress.org/support/plugin/nextgen-facebook) or the [Pro version support website](http://nextgen-facebook.support.surniaulula.com/).

Follow Surnia Ulula on [Google+](https://plus.google.com/+SurniaUlula/?rel=author), [Facebook](https://www.facebook.com/SurniaUlulaCom), [Twitter](https://twitter.com/surniaululacom), and [YouTube](http://www.youtube.com/user/SurniaUlulaCom).

== Installation ==

= Install and Uninstall =

* [Install the Plugin](http://surniaulula.com/codex/plugins/nextgen-facebook/installation-to/install-the-plugin/)
* [Integration Notes](http://surniaulula.com/codex/plugins/nextgen-facebook/installation/integration/)
	* [BuddyPress Integration](http://surniaulula.com/codex/plugins/nextgen-facebook/installation/integration/buddypress-integration/)
* [Uninstall the Plugin](http://surniaulula.com/codex/plugins/nextgen-facebook/installation/uninstall-the-plugin/)
* [Performance Tuning](http://surniaulula.com/codex/plugins/nextgen-facebook/installation/performance-tuning/)
* [Debugging and Problem Solving](http://surniaulula.com/codex/plugins/nextgen-facebook/installation/debugging-and-problem-solving/)
* [Developer Special - Buy one, Get one Free](http://surniaulula.com/codex/plugins/nextgen-facebook/installation/developer-special-buy-one-get-one-free/)

= Plugin Setup =

* [A Setup Guide for NGFB](http://surniaulula.com/codex/plugins/nextgen-facebook/installation/a-setup-guide/)

== Frequently Asked Questions ==

= Frequently Asked Questions =

* [Can I use the Pro version on multiple websites?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/can-i-use-the-pro-version-on-multiple-websites/)
* [Does LinkedIn read the Open Graph meta tags?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/does-linkedin-read-the-open-graph-meta-tags/)
* [Doesn't an SEO plugin cover that?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/doesnt-an-seo-plugin-cover-that/)
* [How can I exclude / ignore certain parts of the content text?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-exclude-certain-parts-of-the-content-text/)
* [How can I fix a ERR_TOO_MANY_REDIRECTS error?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-fix-a-err_too_many_redirects-error/)
* [How can I fix an "HTTP error" when uploading images?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-fix-an-http-error-when-uploading-images/)
* [How can I have smaller dimensions for the default image?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-have-smaller-dimensions-for-the-default-image/)
* [How can I see what the Facebook crawler sees?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-see-what-the-facebook-crawler-sees/)
* [How can I share a single NextGEN Gallery image?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-can-i-share-a-single-nextgen-gallery-image/)
* [How do I attach an image without showing it on the webpage?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-do-i-attach-an-image-without-showing-it-on-the-webpage/)
* [How do I fix my theme's front page pagination?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-do-i-fix-my-themes-front-page-pagination/)
* [How do I install the Pro version?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-do-i-install-the-pro-version/)
* [How do I remove duplicate meta tags?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-do-i-remove-duplicate-meta-tags/)
* [How does NGFB find / detect / select images?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/how-does-ngfb-find-detect-select-images/)
* [Social Sharing Buttons](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/)
	* [Can I share a single image on a webpage?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/can-i-share-a-single-image-on-a-webpage/)
	* [Can I use other social sharing buttons?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/can-i-use-other-social-sharing-buttons/)
	* [How do I turn on Social Sharing Buttons for a page?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/how-do-i-turn-on-social-sharing-buttons-for-a-page/)
	* [Why are the buttons showing the wrong language?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/why-are-the-buttons-showing-the-wrong-language/)
	* [Why does the Facebook "Like" button flyout get clipped?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/why-does-the-facebook-like-button-flyout-get-clipped/)
	* [Why doesn't the Pinterest button show?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/why-doesnt-the-pinterest-button-show/)
	* [Why doesn't the Twitter count increase?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/social-sharing-buttons/why-doesnt-the-twitter-count-increase/)
* [W3C says "there is no attribute 'property'"](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/w3c-says-there-is-no-attribute-property/)
* [What about Google Search and Google Plus?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/what-about-google-search-and-google-plus/)
* [What features of NextGEN Gallery are supported?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/what-features-of-nextgen-gallery-are-supported/)
* [What is the difference between NGFB and WPSSO?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/what-is-the-difference-between-ngfb-and-wpsso/)
* [What is the difference between the Free and Pro versions?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/what-is-the-difference-between-the-free-and-pro-versions/)
* [Why aren't Pins from my website posting Rich?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-arent-pins-from-my-website-posting-rich/)
* [Why do my Facebook shares have small images?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-do-my-facebook-shares-have-small-images/)
* [Why does Facebook play videos instead of linking them?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-does-facebook-play-videos-instead-of-linking-them/)
* [Why does Facebook show the wrong image / text?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-does-facebook-show-the-wrong-image-text/)
* [Why does Google Structured Data Testing Tool show errors?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-does-google-structured-data-testing-tool-show-errors/)
* [Why does the plugin ignore some &lt;img/&gt; HTML tags?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-does-the-plugin-ignore-some-img-html-tags/)
* [Why don't my Twitter Cards show on Twitter?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-dont-my-twitter-cards-show-on-twitter/)
* [Why is the Open Graph title the same for every webpage?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-is-the-open-graph-title-the-same-for-every-webpage/)
* [Why is the page blank or its components misaligned?](http://surniaulula.com/codex/plugins/nextgen-facebook/faq/why-is-the-page-blank-or-its-components-misaligned/)

== Other Notes ==

= Additional Documentation =

* [About Pro Modules](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/)
	* [Author Gravatar](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/author-gravatar/)
	* [Easy Digital Downloads](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/easy-digital-downloads/)
	* [HeadSpace2 SEO](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/headspace2-seo/)
	* [Slideshare, Vimeo, Wistia, Youtube APIs](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/videos-apis/)
	* [WooCommerce](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/woocommerce/)
	* [WordPress REST API version 2](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/modules/wordpress-rest-api-v2/)
* [Contact Information and Feeds](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/contact-information/)
* [Developer Resources](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/)
	* [Constants](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/constants/)
		* [Disable the Open Graph Meta Tags](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/constants/disable-the-open-graph-meta-tags/)
	* [Filters](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/)
		* [Examples](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/)
			* [Detect YouTube URL Links as Videos](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/detect-youtube-url-links-as-videos/)
			* [Modify Shortcode Attributes (URL)](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/modify-shortcode-attributes-url/)
			* [Modify the "article:tag" Keywords / Names](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/modify-the-articletag-keywords-names/)
			* [Modify the Default Topics List](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/modify-the-default-topics-list/)
			* [Modify the Home Page Title for Facebook / Open Graph](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/examples/modify-the-home-page-title-for-facebook-open-graph/)
		* [Filters by Category](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/)
			* [Head Filters](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/head/)
			* [Media Filters](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/media/)
			* [Open Graph Filters](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/open-graph/)
			* [Twitter Card Filters](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/twitter-card/)
			* [Webpage Filters](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-category/webpage/)
		* [Filters by Name](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/by-name/)
		* [Other Filters](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/filters/other/)
	* [Sharing Buttons Function](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/developer/sharing-buttons-function/)
* [Inline Variables](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/inline-variables/)
* [Multisite / Network Support](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/multisite-network-support/)
* [NGFB Shortcode for Sharing Buttons](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/ngfb-shortcode/)
* [Styling Social Buttons](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/styling-social-buttons/)
* [Working with Image Attachments](http://surniaulula.com/codex/plugins/nextgen-facebook/notes/working-with-image-attachments/)

== Screenshots ==

01. NGFB General Settings Page &mdash; Includes options for Open Graph (All Publishers) site information, titles, descriptions, images, videos, and authors. There is also a Publisher Specific section for Facebook, Google (G+ and Search), LinkedIn, Pinterest Rich Pins, and Twitter Cards.
02. NGFB Advanced Settings Page &mdash; Allows you to fine-tune some advanced plugin settings, the use of various content filters, custom post meta fields, caching techniques, user profile contacts, and enable / disable a long list of meta tags.
03. NGFB Social Settings on Posts, Pages, Taxonomy / Terms, and User editing pages &mdash; The Social Settings metabox allows you to modify the default title, description, image, video, preview an example share, preview the meta tags, and validate the webpage markup with online tools.
04. NGFB "Social Image" and "Social Description" preview columns (available on posts, pages, category, tags, and custom taxonomy editing pages).
05. Example Pinterest Product Pin (Zoomed).
06. Example Facebook Link Share.
07. Example Facebook Video Share.
08. Example Twitter 'Summary' Card.
09. Example Twitter 'Large Image Summary' Card.
10. Example Twitter 'Photo' Card (The [Photo](https://dev.twitter.com/cards/types/photo) Card was deprecated by Twitter on July 3rd, 2015).
11. Example Twitter 'Gallery' Card (The [Gallery](https://dev.twitter.com/cards/types/gallery) Card was deprecated by Twitter on July 3rd, 2015).
12. Example Twitter 'Product' Card (The [Product](https://dev.twitter.com/cards/types/product) Card was deprecated by Twitter on July 3rd, 2015).
13. Example Twitter 'Player' Card.
14. Example Social Profiles in Google Search &mdash; NGFB includes author (Person) and publisher (Organization) social profiles in schema.org JSON-LD format for Google Search and their Knowledge Graph.
15. Example Showing All Social Sharing Buttons Enabled.

== Changelog ==

<blockquote id="changelog_top_info">
<p>New versions of the plugin are released approximately every week (more or less). New features are added, tested, and released incrementally, instead of grouping them together in a major version release. When minor bugs fixes and/or code improvements are applied, new versions are also released. This release schedule keeps the code stable and reliable, at the cost of more frequent updates.</p>

<p>See <a href="https://en.wikipedia.org/wiki/Release_early,_release_often">release early, release often (RERO) software development philosophy</a> on Wikipedia for more information on the benefits of smaller / more frequent releases.</p>
</blockquote>

= Free / Basic Version Repository =

* [GitHub](https://github.com/SurniaUlula/nextgen-facebook)
* [WordPress.org](https://wordpress.org/plugins/nextgen-facebook/developers/)

= Changelog / Release Notes =

**Version 8.32.1-1 (2016/05/27)**

Official announcement: N/A

* *New Features*
	* None
* *Improvements*
	* Added a warning if The SEO Framework's Knowledge Graph is enabled.
	* Added support for The SEO Framework's custom canonical URL (Pro version).
	* Added a hook for 'the_seo_framework_current_object_id' coming in The SEO Framework v2.6.2 (Pro version).
	* Added top-level schema.org/CreativeWork to the available Schema types.
* *Bugfixes*
	* Refactored the `get_tweet_max_len()` method to provide more accurate and predictable results.
* *Developer Notes*
	* Replaced the `$use_post` variable by `$mod` in the NgfbHead `get_single_mt()` method.
	* Replaced the `$use_post` variable by `$mod` in the NgfbUtil `replace_inline_vars()` method.
	* Refactored the `is_post_page()`, `is_term_page()`, and `is_user_page()` method to use `$screen->base` instead of `$screen->id`.
	* Refactored the NgfbUtil `get_sharing_url()` method and changed several filter arguments:
		* apply_filters( 'ngfb_post_url', $url, $mod, $add_page );
		* apply_filters( 'ngfb_term_url', $url, $mod, $add_page );
		* apply_filters( 'ngfb_author_url', $url, $mod, $add_page );
		* apply_filters( 'ngfb_sharing_url', $url, $mod, $add_page );

**Version 8.32.0-1 (2016/05/23)**

Official announcement: N/A

* *New Features*
	* Added support for [The SEO Framework](https://wordpress.org/plugins/autodescription/) plugin (Pro version).
* *Improvements*
	* Added new 'Show "NGFB Img" Column for' and 'Show "NGFB Desc" Column for' options.
	* Removed the 'Show Social Img / Desc Columns for' option.
* *Bugfixes*
	* None
* *Developer Notes*
	* Renamed the 'ngfb_section' filter to 'ngfb_article_section'.
	* Added a new 'ngfb_social_accounts' filter.

**Version 8.31.2-1 (2016/05/20)**

Official announcement: N/A

* *New Features*
	* None
* *Improvements*
	* Changed the default "Enforce Image Dimensions Check" value to unchecked.
	* Moved the Social Preview tab in the Social Settings metabox to the left-most position.
	* Added a Verify Image Dimensions module, enabled when the "Enforce Image Dimensions Check" option is checked (Pro version).
* *Bugfixes*
	* None
* *Developer Notes*
	* Added an internal / non-standard 'article:author:name' meta tag (used for the Social Preview feature).

**Version 8.31.1-1 (2016/05/18)**

Official announcement: N/A

* *New Features*
	* None
* *Improvements*
	* Added new Open Graph meta tags for WooCommerce (Pro version):
		* product:id
		* product:sku
		* product:url
		* product:title
		* product:image:id
		* product:dimensions
		* product:width (cm)
		* product:height (cm)
		* product:length (cm)
		* product:weight (kg)
		* product:color
		* product:size
		* product:category 
		* product:tag
	* Added new Open Graph meta tags for Easy Digital Downloads (Pro version):
		* product:category 
		* product:tag
	* Added the current locale value to the sharing buttons CSS class (example: .ngfb-buttons.en_US).
	* Added an "unhide these rows" link for internal / non-standard meta tags under the Head Tags tab in the Social Settings metabox.
* *Bugfixes*
	* None
* *Developer Notes*
	* Added a new 'ngfb_og_add_product_mt_offer' filter (returns false by default) to include "product:offer" meta tags in the Open Graph array for WooCommerce products (Pro version).
		* product:offer:id
		* product:offer:sku
		* product:offer:url
		* product:offer:title
		* product:offer:image:id
		* product:offer:dimensions
		* product:offer:width (cm)
		* product:offer:height (cm)
		* product:offer:length (cm)
		* product:offer:weight (kg)
		* product:offer:color
		* product:offer:size
		* product:offer:category 
		* product:offer:tag
		* product:offer:description

**Version 8.31.0-1 (2016/05/14)**

Official announcement: N/A

* *New Features*
	* Added support for the [Co-Authors Plus](https://wordpress.org/plugins/co-authors-plus/) plugin (including Guest Authors).
* *Improvements*
	* Categorized the display of Free / Pro features in the sidebar metabox.
* *Bugfixes*
	* None
* *Developer Notes*
	* Renamed the 'taxonomy' module and all related filters to 'term'.

== Upgrade Notice ==

= 8.32.1-1 =

(2016/05/27) Improvements for The SEO Framework plugin. Refactored the NgfbUtil get_sharing_url() method and changed several filter arguments.

= 8.32.0-1 =

(2016/05/23) Added new 'Show "NGFB Img" Column for' and 'Show "NGFB Desc" Column for' options. Added support for The SEO Framework plugin (Pro version).

= 8.31.2-1 =

(2016/05/20) Moved the Social Preview tab to the left-most position. Added a new Verify Image Dimensions module (Pro version).

