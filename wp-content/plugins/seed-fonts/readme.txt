=== Seed Fonts ===
Contributors: SeedThemes
Donate link: http://seedthemes.com/
Tags: webfont,web fonts, @font-face embed, typography
Requires at least: 4.0.1
Tested up to: 4.5.2
Stable tag: 1.0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Use web fonts (@font-face) by choosing from 5 ready-generated Thai-English fonts or upload your own web fonts to theme folder.


== Description ==

Seed Fonts is WordPress plugin that helps you use web fonts (@font-face embed) easier. You can choose from 5 ready-generated Thai-English web fonts, or use your own web fonts (such as generated from <a href="https://www.fontsquirrel.com/tools/webfont-generator" target="_blank">Font Squirrel’s Webfont Generator</a>) by uploading to theme folder (/wp-content/YOUR-THEME-NAME/vendor/fonts/YOUR-FONT-NAME).


== Installation ==


1. Upload the plugin files to the `/wp-content/plugins/seed-fonts` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress
1. Go to Appearance -> Fonts to set you web fonts. Thais use รูปแบบบล็อก -> ฟอนต์


== Frequently Asked Questions ==

= How to generate and upload my own web fonts? =

Please see <a href="https://www.seedthemes.com/plugin/seed-fonts/#upload-your-fonts" target="_blank">https://www.seedthemes.com/plugin/seed-fonts/#upload-your-fonts</a>.

== Screenshots ==

1. Example Thai fonts.


== Changelog ==

= 1.0.1 =
* Fix PHP short form tags

= 1.0.0 =
* Use Settings API.
* Fix double trailing slash after plugin_dir_url().
* Add body class (.seed-fonts-FONT-NAME).
* Optimize code.

= 0.9.5 =
* First public version.


== Upgrade Notice ==

= 1.0.0 =
Use Settings API & Optimize code.

= 0.9.5 =
Just start.