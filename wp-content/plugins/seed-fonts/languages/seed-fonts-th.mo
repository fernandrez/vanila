��          �       \      \  _   ]  �   �     t     |     �     �     �     �  
   �  
   �  	   �     �     �  �        �     �     �     �  ,   �  n    �   �  }  @     �	     �	     �	  $   �	  4   #
  (   X
  
   �
  
   �
  	   �
  \   �
  0   �
  �  /     �  	   �  8   �       ,   4   400 = Normal, 700 = Bold. For more detail, please see <a href="%1$s" target="_blank">W3.org</a> Enable web fonts on Appearance -> Fonts. You can add more by <a href="https://www.seedthemes.com/plugin/seed-fonts/" target="_blank">uploading your web fonts to the theme folder</a>. Enable? Font Fonts Fonts Settings Force Using This Font? Generated CSS Seed Fonts SeedThemes Selectors Separate selectors with commas Settings updated successfully. This plugin comes with 5 Thai web fonts. You can add your own collection by <a href="%1$s" target="_blank">uploading your web fonts to the theme folder</a> Weight Yes Yes (!important added) https://www.seedthemes.com https://www.seedthemes.com/plugin/seed-fonts Project-Id-Version: Seed Fonts
Report-Msgid-Bugs-To: 
POT-Creation-Date: Sat May 07 2016 19:06:50 GMT+0700 (ICT)
PO-Revision-Date: Sat May 14 2016 05:56:54 GMT+0700 (ICT)
Last-Translator: root <info@mennstudio.com>
Language-Team: 
Language: Thai
Plural-Forms: nplurals=1; plural=0
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-SourceCharset: UTF-8
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: .
X-Poedit-KeywordsList: _:1;gettext:1;dgettext:2;ngettext:1,2;dngettext:2,3;__:1;_e:1;_c:1;_n:1,2;_n_noop:1,2;_nc:1,2;__ngettext:1,2;__ngettext_noop:1,2;_x:1,2c;_ex:1,2c;_nx:1,2,4c;_nx_noop:1,2,3c;_n_js:1,2;_nx_js:1,2,3c;esc_attr__:1;esc_html__:1;esc_attr_e:1;esc_html_e:1;esc_attr_x:1,2c;esc_html_x:1,2c;comments_number_link:2,3;t:1;st:1;trans:1;transChoice:1,2
X-Loco-Target-Locale: th_TH
X-Generator: Loco - https://localise.biz/ 400 = ตัวธรรมดา, 700 = ตัวหนา. รายละเอียดเพิ่มเติม ดูที่ <a href="%1$s" target="_blank">W3.org</a>
 เปิดการใช้งานฟอนต์สำหรับเว็บที่ รูปแบบบล็อก -> ฟอนต์ หากต้องการเพิ่มฟอนต์สามารถ <a href="https://www.seedthemes.com/plugin/seed-fonts/#upload-your-fonts" target="_blank">อัพโหลดไปไว้ในธีม</a> ได้ เปิดใช้งาน? ฟอนต์ ฟอนต์ ตั้งค่าฟอนต์ บังคับใช้ฟอนต์นี้? CSS ที่สร้างขึ้น Seed Fonts SeedThemes Selectors คั่นระหว่าง Selector ด้วยเครื่องหมาย "," ตั้งค่าเรียบร้อย ปลั๊กอินนี้มาพร้อมกับฟอนต์ไทย 5 ฟอนต์ หากต้องการใช้ฟอนต์อื่น ให้สร้างฟอนต์สำหรับเว็บแล้ว <a href="https://www.seedthemes.com/plugin/seed-fonts/#upload-your-fonts" target="_blank">อัพโหลดไปไว้ในธีม</a> น้ำหนัก ใช่ ใช่ (เพิ่มคำสั่ง !important) https://www.seedthemes.com https://www.seedthemes.com/plugin/seed-fonts 